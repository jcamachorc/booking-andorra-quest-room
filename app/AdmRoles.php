<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmRoles extends Model {

    public static function existsByNombre($nombre){
        try {
            $user = AdmRoles::where('nombre', $nombre)->count();
            return ($user != 0);
        } catch (ErrorException $e) {
            return false;
        }
    }

}
