<?php
class CarnetJove{
    const ERROR_01 = 'Los datos introducidos referentes al carnte jove son incorrectos';
    const ERROR_02 = 'Error interno del sistema al validar datos del carnet jove';
    const SUCCESS = 'Validación correcta';

    public function checkCarnet($dni, $fechaNacimiento, $nombre, $numCarnet){
        $dni = str_replace(' ','', $dni);
        $dni = str_replace('-','', $dni);
        $bornDate = str_replace('-','', $fechaNacimiento);
        try {
            $client = new \SoapClient('http://www.carnetjove.cat/carnetjove/service/webService?wsdl');
            $params = array(
                "login" => 'entitat_colaboradora',
                "password" => "h14np0PG5s",
                "numeroDocument" => $dni,
                "nom" => $nombre,
                "cognom1" => '_',
                "cognom2" => '_',
                "dataNaixement" => $bornDate,
                "numCarnet" => $numCarnet,
            );
            $response = $client->usuariExistentAmbCarnet($params);
            if(!$response->return)
                return self::ERROR_01;
        }catch (SoapFault $soapFault) {
            return self::ERROR_02;
        }
        return self::SUCCESS;
    }
}
?>