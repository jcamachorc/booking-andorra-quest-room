<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\AdmMailings;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');

        //Aqui insertamos la configuracion de nuestro server usando la base de datos
        $mail =  AdmMailings::find(1)->toArray();
        $config = array(
            'driver' => 'smtp',
            'host' => $mail['host'],
            'port' => $mail['port'],
            'from' => array('address' => $mail['username'], 'name' => $mail['name']),
            'encryption' => $mail['encrypt'],
            'username' => $mail['username'],
            'password' => $mail['password'],
            'pretend' => false,
            'sendmail' => '/usr/sbin/sendmail -bs',
        );
        Config::set('mail',$config);

        extract(Config::get('mail'));

        // create new mailer with new settings
        $transport = \Swift_SmtpTransport::newInstance($host, $port);
        // set encryption
        if (isset($encryption)) $transport->setEncryption($encryption);
        // set username and password
        if (isset($username))
        {
            $transport->setUsername($username);
            $transport->setPassword($password);
        }
        // set new swift mailer
        Mail::setSwiftMailer(new \Swift_Mailer($transport));
        // set from name and address
        if (is_array($from) && isset($from['address']))
        {
            Mail::alwaysFrom($from['address'], $from['name']);
        }
	}

}
