<?php namespace App\Http\Controllers\BonoRegalo;

use App\Http\Controllers\Controller;
use App\SysMailing;
use App\TsBonos;
use App\TsCalendario;
use App\TsCjtPrecios;
use App\TsConfigs;
use App\TsDescuentos;
use App\TsMailing;
use App\TsPrecios;
use App\TsRelFacturacion;
use App\TsReservas;
use App\TsVacaciones;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use yajra\Datatables\Datatables;

class ViewController extends Controller {

    private $precioBono = -1;

	public function __construct(){
        $dttt = TsConfigs::find('precio_bono')->toArray();
        $this->precioBono = $dttt['value']; //12%
    }

	public function index()
	{
		return view('bonoregalo.listar', array());
	}

    public function checkout(){
        $input = Input::all();
        
        $extrabono = 0;
        //Check tipus bono regal
        
        if($input['txtBono'] == '0') {
            $dttt = TsConfigs::find('bono_b1')->toArray();
            $extrabono = $dttt['value']; 
        }elseif($input['txtBono'] == '1'){
            $dttt = TsConfigs::find('bono_b2')->toArray();
            $extrabono = $dttt['value']; 
        }elseif($input['txtBono'] == '2'){
            $dttt = TsConfigs::find('bono_b3')->toArray();
            $extrabono = $dttt['value'];
        }elseif($input['txtBono'] == '3'){
            $dttt = TsConfigs::find('bono_b4')->toArray();
            $extrabono = $dttt['value'];
        }elseif($input['txtBono'] == '4'){
            $dttt = TsConfigs::find('bono_b5')->toArray();
            $extrabono = $dttt['value'];    
        }else {
            $dttt = TsConfigs::find('bono_b1')->toArray();
            $extrabono = $dttt['value']; 
        }
        $input['tipobono'] = $extrabono;

        if(empty($input['txtNombre']) ||
          
            empty($input['txtEmail']) ||
            empty($input['txtTelefono']) ||
            empty($input['txtMetodoPago'])){
            //Return hay campos basicos que deben ser rellenados
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados']);
        }
        //Factura
        if(empty($input['txtNombreF']) && $input['txtFactura'] == 1){
            return Redirect::back()->withInput(Input::all())->withErrors(['Si deseas una factura, debes rellenar todos los campos'])->with('error-input', 'txtNombreF');
        }
        if(empty($input['txtDNI']) && $input['txtFactura'] == 1){
            return Redirect::back()->withInput(Input::all())->withErrors(['Si deseas una factura, debes rellenar todos los campos'])->with('error-input', 'txtDNI');
        }
        if(empty($input['txtDireccion']) && $input['txtFactura'] == 1){
            return Redirect::back()->withInput(Input::all())->withErrors(['Si deseas una factura, debes rellenar todos los campos'])->with('error-input', 'txtDireccion');
        }

   

        $descuento = 0;
      
        $input['descuento'] = $descuento;
        $input['precioFinal'] = round($this->precioBono-($this->precioBono*($descuento/100)-$extrabono),2);
      //  $input['precioFinal'] = round($this->precioBono + $extrabono,2);

        return view('bonoregalo.checkout', array('input' => $input, 'descuento' => $descuento));
    }

    public function payTPV(){
        $input = Input::all();

        $reserva = new TsBonos();
        $reserva->codigo = self::getCodigo();
        $reserva->precio = $input['precioFinal'];
        //$this->precioBono;
        $reserva->metodoPago = $input['txtMetodoPago'];
        $reserva->email = $input['txtEmail'];
        //$reserva->idioma = session()->get('applocale');
        $reserva->DNI = $input['txtDNI'];
        $reserva->nombre = $input['txtNombre'];
        $reserva->nombreDestinatario = $input['txtNombreDest'];
        $reserva->emailDestinatario = $input['txtEmailDest'];
        $reserva->telefono = $input['txtTelefono'];
        $reserva->descuento = 0;
        $reserva->codigoDescuento = '     ';
        $reserva->sendMailDest = ($input['txtSendDest'] == '1');
        $reserva->nombreF = $input['txtNombreF'];
        $reserva->factura = ($input['txtFactura'] == 1);
        $reserva->direccion = $input['txtDireccion'];
        $reserva->fechaCaducidad = Carbon::now()->addMonths(6)->toDateTimeString();
        $reserva->save();

        $tpv = new \TPV();
        $url = url('bonoregalo/tpv.php');
        $formPago = $tpv->generateFormData("0000B".$reserva->id, //Si se toca, tocar el TPVmerchantURL() -> order substr
                                            $input['precioFinal'],
                                            url('bonoregalo/codigo/'.$reserva->codigo),
                                            url(getConfig('payment_ko')),
                                            $url);
	$formPago['id'] = $reserva->id;
        return response()->json($formPago);

    }

    private function getCodigo(){
        $codigo = uniqid();
        while(!self::checkUnico($codigo)){
            $codigo = uniqid();
        }
        return $codigo;
    }

    private function checkUnico($codigo){
        return TsBonos::where('codigo', $codigo)->count() == 0;
    }

    public function codigo($codigo){
        return view('bonoregalo.codigo', array('codigo' => $codigo));
    }

    public function renderCode($codigo){
        $img = public_path('images/codigo.png');
        header('Content-Type: image/png');
        $im = imagecreatefrompng($img);
        $rojo = imagecolorallocate($im, 255, 0, 0);
        $fuente = public_path('fonts/lcd.ttf');

        // Añadir el texto
        imagettftext($im, 30, 0, 820, 710, $rojo, $fuente, $codigo);

        // Usar imagepng() resultará en un texto más claro comparado con imagejpeg()
        imagepng($im);
        imagedestroy($im);
    }

    public function TPVmerchantURL(){
        $input = Input::all();

        $obj = new \RedsysAPI();
        $decod = $obj->decodeMerchantParameters($input['Ds_MerchantParameters']);
        //Fix pq los del banco son idiotas
        if(strpos($decod, "}") === false){
            if(json_decode($decod) == null){
                $decod .= ':""}';
            }
        }
        $dt = get_object_vars(json_decode($decod));
        $order = substr($dt['Ds_Order'], 5);

    //    if($dt['Ds_Response'] >= 0 && $dt['Ds_Response'] <= 99) {
            //Ok -> confirma and send mail
            $reserva = TsBonos::find($order);
            $reserva->fechaPago = Carbon::now()->toDateTimeString();
            $reserva->codigoPago = $dt['Ds_Response'];
            $reserva->save();

            if(TsRelFacturacion::where('idBono', $order)->count() == 0) {
                $fact = new TsRelFacturacion();
                $fact->idBono = $order;
                $fact->idReserva = null;
                if((bool) $reserva->factura) {
                    $fact->fechaPago = Carbon::now()->toDateTimeString();
                    $fact->factura = 1;
                }else{
                    $fact->fechaPago = Carbon::now()->toDateString();
                    $fact->factura = 0;
                }
                $fact->save();
            }
            self::sendMailConfirmado($order);
      //  }else{
            //Error -> guarda error
   //         $reserva = TsReservas::find($order);
    //        $reserva->estado = -1;
    //        $reserva->codigoPago = $dt['Ds_Response'];
     //       $reserva->save();
       // }
    }

    public function pay(){
        $input = Input::all();

        //Redirect segun pasarela de pago
        if($input['txtMetodoPago'] == 'paypal'){
            $reserva = new TsBonos();
            $reserva->codigo = self::getCodigo();
            $reserva->precio = $this->precioBono;
            $reserva->metodoPago = $input['txtBono'];
            $reserva->metodoPago = $input['txtMetodoPago'];
            $reserva->email = $input['txtEmail'];
            //$reserva->idioma = session()->get('applocale');
            $reserva->DNI = $input['txtDNI'];
            $reserva->nombre = $input['txtNombre'];
            $reserva->nombreDestinatario = $input['txtNombreDest'];
            $reserva->emailDestinatario = $input['txtEmailDest'];
            $reserva->telefono = $input['txtTelefono'];
            $reserva->descuento = 0;
            $reserva->codigoDescuento = '    ';
            $reserva->sendMailDest = ($input['txtSendDest'] == '1');
            $reserva->fechaCaducidad = Carbon::now()->addMonths(6)->toDateTimeString();
            $reserva->nombreF = $input['txtNombreF'];
            $reserva->factura = ($input['txtFactura'] == 1);
            $reserva->direccion = $input['txtDireccion'];
            $reserva->save();

            //Pago paypal
            /*$sanboxed = \App\TsConfigs::find('payments_mode_sandboxed')->toArray();
            if($sanboxed['value'] == "1") {
                $paypal = new \Paypal("https://api.sandbox.paypal.com/v1/oauth2/token", "https://api.sandbox.paypal.com/v1/payments/payment");
            }else {*/
                $paypal = new \Paypal("https://api.paypal.com/v1/oauth2/token", "https://api.paypal.com/v1/payments/payment");
            //}
            $paypal->connect();
            $url = $paypal->seconStage(url('calendario/paypal'), getConfig('payment_ko'), $input['precioFinal'], 'Pago del cupón regalo para la experiencia Enigma .- '.$input['precioFinal'].'€');

            session(['paypalTkn' => $paypal->getToken()]);
            session(['paypalId' => $reserva->id]);
            session(['paypalCodigo' => $reserva->id]);

            return Redirect::away($url);
        }
        return Redirect::away(getConfig('payment_ko'));    //Error wordpress
    }

    private function sendMailConfirmado($idReserva){
      $reserva = TsBonos::find($idReserva)->toArray();
      
      $contenido = TsMailing::where('nombre','bonoregalo_comprador')->first()->toArray();
      switch(strtolower($reserva['idioma'])){
        case 'english':
          $titulo = $contenido['titulo_ingles'];
          $contenido = $contenido['ingles'];
          break;
        case 'catala':
          $titulo = $contenido['titulo_catalan'];
          $contenido = $contenido['catalan'];
          break;
        case 'frances':
          $titulo = $contenido['titulo_frances'];
          $contenido = $contenido['frances'];
          break;
        default:
          $titulo = $contenido['titulo_contenido'];
          $contenido = $contenido['contenido'];
          break;
      }
        
      $data = array('{nombreDestinatario}','{bono}');
      $replace = array($reserva['nombreDestinatario'], $reserva['codigo']);

      $contenidoTitle = $titulo;
      $contenidoBody = str_replace($data,$replace,$contenido);

      $mail = new \Mailer();
      $mail->addAddress($reserva['nombre'], $reserva['email']);
      $mail->setContent($contenidoTitle, $contenidoBody);
      $mail->sendMail();

      if($reserva['sendMailDest']){
        $contenido = TsMailing::where('nombre','bonoregalo_receptor')->first()->toArray();
        switch(strtolower($reserva['idioma'])){
          case 'english':
            $titulo = $contenido['titulo_ingles'];
            $contenido = $contenido['ingles'];
            break;
          case 'catala':
            $titulo = $contenido['titulo_catalan'];
            $contenido = $contenido['catalan'];
            break;
          case 'frances':
            $titulo = $contenido['titulo_frances'];
            $contenido = $contenido['frances'];
            break;
          default:
            $titulo = $contenido['titulo_contenido'];
            $contenido = $contenido['contenido'];
            break;
        }
          
        $data = array('{nombreComprador}','{bono}');
        $replace = array($reserva['nombre'], $reserva['codigo']);

        $contenidoTitle = $titulo;
        $contenidoBody = str_replace($data,$replace,$contenido);

        $mailDest = new \Mailer();
        $mailDest -> addAddress($reserva['nombreDestinatario'], $reserva['emailDestinatario']);
        $mailDest -> setContent($contenidoTitle, $contenidoBody);
        $mailDest -> sendMail();
      }
    }

    public function PayPalOkURL(){
        $input = Session::all();
        Session::forget('paypalTkn');

        /*$sanboxed = \App\TsConfigs::find('payments_mode_sandboxed')->toArray();
        if($sanboxed['value'] == 'true') {
            $paypal = new \Paypal("https://api.sandbox.paypal.com/v1/payments/payment/".$_GET['paymentId']."/execute/",NULL);
        }else{*/
            $paypal = new \Paypal("https://api.paypal.com/v1/payments/payment/".$_GET['paymentId']."/execute/",NULL);
        //}
        $status = $paypal->pay($_GET['PayerID'], $input['paypalTkn']);

        if($status){
            //Ok -> confirma and send mail
            $reserva = TsBonos::find($input['paypalId']);
            $reserva->fechaPago = Carbon::now()->toDateTimeString();
            $reserva->codigoPago = 'Ok';
            $reserva->save();

            if(TsRelFacturacion::where('idBono', $input['paypalId'])->count() == 0) {
                $fact = new TsRelFacturacion();
                $fact->idBono = $input['paypalId'];
                $fact->idReserva = null;
                if((bool) $reserva->factura) {
                    $fact->fechaPago = Carbon::now()->toDateTimeString();
                    $fact->factura = 1;
                }else{
                    $fact->fechaPago = Carbon::now()->toDateString();

                }
                $fact->factura = $reserva->factura;
                $fact->save();
            }

            self::sendMailConfirmado($input['paypalId']);

            return Redirect::to('bonoregalo/codigo/'.$input['paypalId']);    //Error wordpress
        }else{
            //Error -> guarda error
            $reserva = TsBonos::find($input['paypalId']);
            $reserva->codigoPago = 'Error en el pago paypal';
            $reserva->save();

            return Redirect::away(getConfig('payment_ko'));    //Error wordpress
        }
    }
}
