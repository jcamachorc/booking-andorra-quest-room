<?php namespace App\Http\Controllers\Booking;

use App\Http\Controllers\Controller;
use App\TsBonoDias;
use App\TsBonos;
use App\TsCjtPrecios;
use App\TsDescuentos;
use App\TsMailing;
use App\TsPrecios;
use App\TsRelFacturacion;
use App\TsReservas;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use yajra\Datatables\Datatables;

class BonosViewController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function listar()
	{
        $especial = array();
        $especial[0] = 'Todos';
        $data = TsBonos::where('metodoPago','BNN')->groupBy('nombreDestinatario')->get()->toArray();
        foreach($data as $item){
            if(!empty($item['nombreDestinatario'])) {
                $especial[$item['nombreDestinatario']] = $item['nombreDestinatario'];
            }
        }

        return view('booking.bonos', array('especial' => $especial));
	}

    public function delMasivo($name, $mes, $ano){
        $mailings = TsBonos::where('metodoPago','BNN')->where('nombreDestinatario',$name)->where('idReserva', null);
        if(isset($mes) && !empty($mes)){
            $mailings = $mailings->where('metodoPago','BNN')->where(DB::raw('MONTH(created_at)'), '=', $mes);
        }
        if(isset($ano) && !empty($ano)){
            $mailings = $mailings->where('metodoPago','BNN')->where(DB::raw('YEAR(created_at)'), '=', $ano);
        }
        $mailings->delete();

        return Redirect::back()->with('message', 'Borrado masivo correcto');
    }

    public function addBonoMasivo(){
        return view('booking.addBonoMasivo', array('caducidad' => Carbon::now()->addMonths(6)->toDateString()));
    }

    public function addBonoMasivoPost(){
        $input = Input::all();

        if(empty($input['txtNombre'])){
            return Response::json(array('message' => 'Error, debes introducir un nombre (puede ser igual a uno ya introducido)', 'name' => 'txtNombre'), 400);
        }

        if(!is_numeric($input['txtCantidad']) || $input['txtCantidad'] <= 0){
            return Response::json(array('message' => 'Error, el valor debe ser numerico y superior a 0', 'name' => 'txtCantidad'), 400);
        }

        if(!is_numeric($input['txtPrecio']) || $input['txtPrecio'] <= 0){
            return Response::json(array('message' => 'Error, el valor debe ser numerico y superior a 0', 'name' => 'txtPrecio'), 400);
        }

        $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
        $errorDias = true;
        foreach ($dias as $k => $dia) {
            if (isset($input['txt' . $dia])) {
                $errorDias = false;
            }
        }
        if($errorDias){
            return Response::json(array('message' => 'Error, debes seleccionar almenos un dia', 'name' => ''), 400);
        }

        $descuentosListado = '';
        for($i = 0; $i < $input['txtCantidad']; ++$i){
            $codigo = "BNN".self::getCodigo();
            $descuento = new TsBonos();
            $descuento->nombreDestinatario = $input['txtNombre'];
            $descuento->emailDestinatario = $input['txtCorreo'];
            $descuento->idioma = 'contenido';
            $descuento->DNI = "";
            $descuento->telefono = "";
            $descuento->precio = $input['txtPrecio'];
            $descuento->codigo = $codigo;
            $descuento->fechaPago = Carbon::now();
            $descuento->metodoPago = 'BNN';
            $descuento->fechaCaducidad = Carbon::now()->addMonths(6);
            $descuento->descuento = 0;
            $descuento->nombre = "";
            $descuento->email = "";
            if(!empty($input['txtCorreo'])) {
                $descuento->factura = 1;
            }
            $descuento->save();
            $descuentosListado .= $codigo."<br/>";

            foreach ($dias as $k => $dia) {
                if(isset($input['txt'.$dia])) {
                    $bonoDia = new TsBonoDias();
                    $bonoDia->idBono = $descuento->id;
                    $bonoDia->dia = $k;
                    $bonoDia->save();
                }
            }
        }

        $descuentosListado = 'Cantidad de bonos generados con '.$input['txtPrecio'].'&euro;: '.$i.'<br/>'.$descuentosListado;
        $mailAdmin = new \Mailer();
        $adminMail = \App\TsConfigs::find('correo_admin')->toArray();
        $mailAdmin->addAddress('Admin Confirmacion Bonos', $adminMail['value']);
        $mailAdmin->setContent('[Admin] Confirmacion de reserva', $descuentosListado);
        $mailAdmin->sendMail();
    }



    public function addBonoNuevo(){
        return view('booking.addBonoNuevo', array('codigo' => self::getCodigo()));
    }

    public function addBonoNuevoPost(){
        $input = Input::all();

        if($input['txtPrecio'] <= 0){
            return Response::json(array('message' => 'Error, el precio es el que se le va a descontar del precio final (y también es el que se factura)', 'name' => 'txtPrecio'), 400);
        }

        $bono = new TsBonos();
        $bono->nombreDestinatario = $input['txtNombre'];
        $bono->emailDestinatario = $input['txtCorreo'];
        //$bono->idioma = $input['txtIdioma'];
        $bono->DNI = $input['txtDNI'];
        $bono->telefono = $input['txtTelefono'];
        $bono->precio = $input['txtPrecio'];
        $bono->codigo = $input['txtCodigo'];
        $bono->fechaPago = Carbon::now();
        $bono->fechaCaducidad = Carbon::now()->addMonths(6);
        $bono->descuento = 0;//$input['txtPrecio'];
        $bono->nombre = empty($input['txtNombreF'])?'interno':$input['txtNombreF'];
        $bono->email = 'interno@interno.com';
        $bono->save();

        if($input['txtPrecio'] > 0) {
            $tsRel = new TsRelFacturacion();
            $tsRel->idBono = $bono->id;
            $tsRel->fechaPago = Carbon::now()->toDateString();
            $tsRel->save();
        }
    }

    /*public function sendCode($idReserva){
        $reserva = TsBonos::find($idReserva)->toArray();

        $contenido = TsMailing::where('nombre','bonoregalo_comprador')->first()->toArray();
        $idioma = 'es';
        if(isset($reserva['idioma'])){
            $idomoa = strtolower($reserva['idioma']);
        }
        
        switch($idioma){
            case 'english':
            $titulo = $contenido['titulo_ingles'];
            $contenido = $contenido['ingles'];
            break;
            case 'catala':
            $titulo = $contenido['titulo_catalan'];
            $contenido = $contenido['catalan'];
            break;
            case 'frances':
            $titulo = $contenido['titulo_frances'];
            $contenido = $contenido['frances'];
            break;
            default:
            $titulo = $contenido['titulo_contenido'];
            $contenido = $contenido['contenido'];
            break;
        }
        

        $mail = new \Mailer();
        $mail->addAddress($reserva['nombre'], $reserva['email']);
        $body = '<a href="http://www.andorraquestroom.com/es/bono-regalo/"><img src="'.url('bonoregalo/renderCodigo/'.$reserva['codigo']).'" style="width: 100%" /></a>';
        $mail->setContent('Compra de bono regalo', $body);
        $mail->sendMail();

        if($reserva['sendMailDest']){
            $body = '<a href="http://www.andorraquestroom.com/es/bono-regalo/"><img src="'.url('bonoregalo/renderCodigo/'.$reserva['codigo']).'" style="width: 100%" /></a>';
            $mailDest = new \Mailer();
            $mailDest->addAddress($reserva['nombreDestinatario'], $reserva['emailDestinatario']);
            $mailDest->setContent('Has sido invitado a Andorra Quest', $body);
            $mailDest->sendMail();
        }

        return Redirect::back()->with('message', 'Correo enviado correctamente');
    }*/
    
    public function sendCode($idReserva){
      $reserva = TsBonos::find($idReserva)->toArray();
      
      $contenido = TsMailing::where('nombre','bonoregalo_comprador')->first()->toArray();
      $idioma = 'es';
        if(isset($reserva['idioma'])){
            $idioma = strtolower($reserva['idioma']);
        }

      switch($idioma){
        case 'english':
          $titulo = $contenido['titulo_ingles'];
          $contenido = $contenido['ingles'];
          break;
        case 'catala':
          $titulo = $contenido['titulo_catalan'];
          $contenido = $contenido['catalan'];
          break;
        case 'frances':
          $titulo = $contenido['titulo_frances'];
          $contenido = $contenido['frances'];
          break;
        default:
          $titulo = $contenido['titulo_contenido'];
          $contenido = $contenido['contenido'];
          break;
      }
        
      $data = array('{nombreDestinatario}','{bono}');
      $replace = array($reserva['nombreDestinatario'], $reserva['codigo']);

      $contenidoTitle = $titulo;
      $contenidoBody = str_replace($data,$replace,$contenido);

      $mail = new \Mailer();
      $mail->addAddress($reserva['nombre'], $reserva['email']);
      $mail->setContent($contenidoTitle, $contenidoBody);
      $mail->sendMail();

      if($reserva['sendMailDest']){
        $contenido = TsMailing::where('nombre','bonoregalo_receptor')->first()->toArray();
        switch($idioma){
          case 'english':
            $titulo = $contenido['titulo_ingles'];
            $contenido = $contenido['ingles'];
            break;
          case 'catala':
            $titulo = $contenido['titulo_catalan'];
            $contenido = $contenido['catalan'];
            break;
          case 'frances':
            $titulo = $contenido['titulo_frances'];
            $contenido = $contenido['frances'];
            break;
          default:
            $titulo = $contenido['titulo_contenido'];
            $contenido = $contenido['contenido'];
            break;
        }
          
        $data = array('{nombreComprador}','{bono}');
        $replace = array($reserva['nombre'], $reserva['codigo']);

        $contenidoTitle = $titulo;
        $contenidoBody = str_replace($data,$replace,$contenido);

        $mailDest = new \Mailer();
        $mailDest -> addAddress($reserva['nombreDestinatario'], $reserva['emailDestinatario']);
        $mailDest -> setContent($contenidoTitle, $contenidoBody);
        $mailDest -> sendMail();
      }
      
      return Redirect::back()->with('message', 'Correo enviado correctamente');
    }

    public function renderCode($codigo){
        $img = public_path('images/codigo.png');
        header('Content-Type: image/png');
        $im = imagecreatefrompng($img);
        $rojo = imagecolorallocate($im, 255, 0, 0);
        $fuente = public_path('fonts/lcd.ttf');

        // Añadir el texto
        imagettftext($im, 30, 0, 820, 525, $rojo, $fuente, $codigo);

        // Usar imagepng() resultará en un texto más claro comparado con imagejpeg()
        imagepng($im);
        imagedestroy($im);
    }

    //Descuento 
    public function addBono(){
        
     
        return view('booking.addBono', array('codigo' => self::getCodigoDesc(), 'caducidad' => Carbon::now()->addMonths(6)->toDateString()));
    }

    public function addBonoPost(){
        
        
        $input = Input::all();
       //$fecha = $input['txtCaducidad']." 00:00:00";
       
        
        
        $descuento = new TsDescuentos();
        $descuento->fechaCaducidad = $input['txtCaducidad'] ;//Carbon::now()->addMonths(6);
        $descuento->descuento = $input['txtDescuento'];
        //$descuento->tipobono = $input['txtBono'];
        $descuento->ilimitados = isset($input['txtIlimitado']);
        $descuento->id = $input['txtCodigo'];
        
        $descuento->save();
        
      
        
      //if($input['txDescuento'] == 0){
         //  return Response::json(array('message' => 'No se puede generar un descuento con valor ', 'name' => 'txtPrecio'), 400);
      //  }else {
        //    return Response::json(array('message' => 'prueba)', 'name' => 'txDescuento'), 400);
       // }
        //return Redirect::back()->with('message', 'hola hola');
    }

    public function generaDescuentos($cantidad){
        if(!is_numeric($cantidad)){
            echo "Error generando los descuentos";
            exit;
        }
        $descuentosListado = '';
        for($i = 0; $i < $cantidad; ++$i){
            $codigo = "GRP".self::getCodigo();
            $descuento = new TsDescuentos();
            $descuento->fechaCaducidad = Carbon::now()->addMonths(6);
            $descuento->descuento = 100;
            $descuento->ilimitados = false;
            $descuento->id = $codigo;
            $descuento->save();

            $descuentosListado .= $codigo."<br/>";
        }

        $descuentosListado = 'Cantidad de descuentos generados: '.$i.'<br/>'.$descuentosListado;

        $mpdf = new \mPDF();
        $mpdf->WriteHTML($descuentosListado);
        //$mpdf->Output();
        $mpdf->Output('descuentosGrupon.pdf', 'D');
    }

    private function getCodigoDesc(){
        $codigo = uniqid();
        while(!self::checkUnicoDesc($codigo)){
            $codigo = uniqid();
        }
        return $codigo;
    }

    private function checkUnicoDesc($codigo){
        return TsDescuentos::where('id', $codigo)->count() == 0;
    }

    private function getCodigo(){
        $codigo = uniqid();
        while(!self::checkUnico($codigo)){
            $codigo = uniqid();
        }
        return $codigo;
    }

    private function checkUnico($codigo){
        return TsBonos::where('codigo', $codigo)->count() == 0;
    }

    public function editar($id){
        $data = TsBonos::find($id)->toArray();
        return view('booking.editarBono', array('input' => $data, 'id' => $id));
    }

    public function saveEditar($id){
        $input = Input::all();
        $bono = TsBonos::find($id);
        $bono->metodoPago = $input['txtMetodoPago'];
        $bono->email = $input['txtEmail'];
        $bono->DNI = $input['txtDNI'];
        $bono->nombre = $input['txtNombre'];
        $bono->nombreDestinatario = $input['txtNombreDest'];
        $bono->emailDestinatario = $input['txtEmailDest'];
        $bono->telefono = $input['txtTelefono'];
        $bono->codigoDescuento = $input['txtCarnetJove'];
        $bono->save();

        return Redirect::back()->withInput(Input::all())->with('message', 'Cambios realizados correctamente');
    }

    public function editarDescuento($id){
        $data = TsDescuentos::find($id);
        return view('booking.editarDescuento', array('input' => $data, 'id' => $id));
    }

    public function saveDescuentoEditar(){
        $input = Input::all();
        if($input['txtId'] != $input['txtCodigo'] && TsDescuentos::find($input['txtCodigo']) != null && TsDescuentos::find($input['txtCodigo'])->count() > 0){
            return Redirect::back()->withInput(Input::all())->with('error', 'El código ya existe');
        }
        $desc = TsDescuentos::find($input['txtId']);
        $desc->id = $input['txtCodigo'];
        //$desc->descuento = 100;
        $desc->descuento = $input['txtDescuento'];
        $desc->ilimitados = isset($input['txtIlimitado']);
        $desc->fechaCaducidad= $input['txtCaducidad'];
        $desc->save();

        return Redirect::to('/booking/bonos/editarDescuento/'.$input['txtCodigo'])->withInput(Input::all())->with('message', 'Cambios realizados correctamente');
    }

    public function listarDescuentos(){
        $mailings = TsDescuentos::select(['id', 'fechaCaducidad', 'descuento', 'idReserva']);

        return Datatables::of($mailings)
            ->editColumn('idReserva', function($reserva){
                if($reserva->idReserva == null){
                    return 'Sin usar';
                }else{
                    return '<a href="'.url('booking/bookings/editar/'.$reserva->idReserva).'">Reserva nº '.$reserva->idReserva.'</a>';
                }
            })
            ->addColumn('accion', function ($mailing) {
                $dt = '<a class="btn btn-xs btn-primary editar" href="'.url('/booking/bonos/editarDescuento/'.$mailing->id).'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                return $dt;
            })
            ->make(true);
    }

    public function listarBonos()
    {
        $input = Input::all();
        $mailings = TsBonos::select(['id', 'codigo', 'fechaPago', 'nombre', 'precio', 'codigoDescuento', 'idReserva']);

        if(isset($input['especial']) && !empty($input['especial'])){
            $mailings = $mailings->where('metodoPago','BNN')->where('nombreDestinatario', $input['especial']);
        }

        if(isset($input['mes']) && !empty($input['mes'])){
            $mailings = $mailings->where('metodoPago','BNN')->where(DB::raw('MONTH(created_at)'), '=', $input['mes']);
        }
        if(isset($input['ano']) && !empty($input['ano'])){
            $mailings = $mailings->where('metodoPago','BNN')->where(DB::raw('YEAR(created_at)'), '=', $input['ano']);
        }

        if(isset($input['usados']) && !empty($input['usados'])){
            if($input['usados'] == 1){
                $mailings = $mailings->where('metodoPago','BNN')->where('idReserva', '>=', 0);
            }elseif($input['usados'] == 2){
                $mailings = $mailings->where('metodoPago','BNN')->where('idReserva', null);
            }
        }

        return Datatables::of($mailings)
            ->editColumn('precio', function($reserva){
                return $reserva->precio."&euro;";
            })
            ->editColumn('idReserva', function($reserva){
                if($reserva->idReserva == null){
                    return 'Sin usar';
                }else{
                    return '<a href="'.url('booking/bookings/editar/'.$reserva->idReserva).'">Reserva nº '.$reserva->idReserva.'</a>';
                }
            })
            ->addColumn('estado', function($reserva){
                $nonFechapago = Carbon::parse($reserva->fechaPago)->year;
                if($nonFechapago <= 0 || $reserva->fechaPago == null){
                    return '<p class="btn btn-xs btn-danger">No pagado</p>';
                }else{
                    return '<p class="btn btn-xs btn-success">Pagado</p>';
                }
            })
            ->addColumn('accion', function ($mailing) {
                $dt = '<a class="btn btn-xs btn-primary editar" href="'.url('/booking/bonos/editar/'.$mailing->id).'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                $dt .= ' <a class="btn btn-xs btn-success" target="_new" href="'.url('/booking/bonos/render/'.$mailing->codigo).'">Render código</a>';
                $dt .= ' <a class="btn btn-xs btn-danger" href="'.url('/booking/bonos/sendCode/'.$mailing->id).'">Enviar correo (código)</a>';
                return $dt;
            })
            ->removeColumn('fin')
            ->make(true);
    }
}
