<?php namespace App\Http\Controllers\Booking;

use App\Http\Controllers\Controller;
use App\TsBonos;
use App\TsCjtPrecios;
use App\TsDescuentos;
use App\TsFacturacion;
use App\TsMailing;
use App\TsPrecios;
use App\TsRelFacturacion;
use App\TsReservas;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use yajra\Datatables\Datatables;



class BookingsViewController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function listar()
	{
        return view('booking.bookings', array());
	}

    public function editar($id){
         $idRol = User::find(Auth::id())->toArray();
         $data = TsReservas::find($id)->toArray();
         
         
        
         
       if ((($idRol['idRol'] == 3) & $data['metodoPago'] == ($idRol['name'] )) || ($idRol['idRol'] == 1) ){
                return view('booking.editarReserva', array('input' => $data, 'id' => $id, 'sala' => $data['sala']));
       }else{
                return Redirect::to('booking/bookings')->withErrors(['No tiene aurtorizacion para editar esta reserva']);
            }
    }

    public function borrarReservaDia($id){
        $idRol = User::find(Auth::id())->toArray();
        $data = TsReservas::find($id)->toArray();
        if($data['nPersonas'] == 0){
            if((($idRol['idRol'] == 3) & $data['metodoPago'] == $idRol['name']) || ($idRol['idRol'] == 1)){ 
            TsReservas::find($id)->delete();
            return Redirect::to('booking/bookings')->with('message', 'Reserva interna borrada correctamente');
            }else{
                return Redirect::to('booking/bookings')->withErrors(['No puede borrrar esta reserva']);
            }
        }else{
            return Redirect::to('booking/bookings')->withErrors(['No puede borrrar esta reserva']);
        }
    }

    public function cancelarReservaDia($id){
        $fechaBase = Carbon::parse('0001-01-01 00:00:01')->toDateTimeString();
        $reserva = TsReservas::find($id);
        $reserva->inicio = $fechaBase;
        $reserva->fin = $fechaBase;
        $reserva->recordatorio = true;
        $reserva->save();
        self::sendMailCancelado($reserva->id);

        return Redirect::to('booking/bookings')->with('message', 'Reserva cancelada correctamente');
    }

    public function saveEditar($id){
        $input = Input::all();
        $reserva = TsReservas::find($id);

        if(empty($input['txtFechaIni']) ||
            empty($input['txtTimeIni']) ||
            empty($input['txtTimeFin']) ||
            empty($input['txtNombre']) ||
            empty($input['txtEmail']))
            {
            //Return hay campos basicos que deben ser rellenados
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados']);
        }

        $fechaInicio = $input['txtFechaIni'].' '.$input['txtTimeIni'];
        $fechaFinal = $input['txtFechaIni'].' '.$input['txtTimeFin'];

        $input['inicio'] = $fechaInicio;


        $reservasSolapadas = TsReservas::where('estado','<>', -1)->where('id','<>',$id)->where('fin', '>', Carbon::parse($fechaInicio)->toDateTimeString())->where('inicio', '<', $fechaFinal);
        if($reservasSolapadas->count() > 0 && $input['txtEstado'] != -1){
            return Redirect::back()->withInput(Input::all())->withErrors(['La hora seleccionada ya esta reservada']);
        }

        if(($input['txtDescuento'] == 'bonoregalo' || $input['txtDescuento'] == 'descuento') && empty($input['txtCodigo'])){
            //Return te has olvidado de introducir un codigo valido
            return Redirect::back()->withInput(Input::all())->withErrors(['Has olvidado introducir un codigo valido']);
        }

        if($input['txtDescuento'] == 'carnetjove' && $input['txtCarnetJove'] != $reserva->codigoDescuento && (empty($input['txtFechaNacimiento']) || empty($input['txtCarnetJove']))){
            //Return te has olvidado de introducir el carnet jove o la fecha de nacimiento
            return Redirect::back()->withInput(Input::all())->withErrors(['Hemos encontrado un error!. Revisa la fecha de nacimiento y número de carnet jove']);
        }

    
        
        $extrabono = 0;
        //Check tipus bono regal
        
        
        $sendMsg = false;
        if($reserva->estado != $input['txtEstado'] && $input['txtEstado'] == -1){
            //Mensaje cancelado
            self::sendMailCancelado($reserva->id);
            $reserva->fechaPago = '0000-00-00 00:00:00';
        }elseif($reserva->estado != $input['txtEstado'] && $input['txtEstado'] == 1){
            if(TsRelFacturacion::where('idReserva', $reserva->id)->count() == 0) {
                $fact = new TsRelFacturacion();
                $fact->idReserva = $reserva->id;
                $fact->idBono = null;
                if($reserva->factura){
                    $fact->fechaPago = Carbon::now()->toDateTimeString();
                }else {
                    $fact->fechaPago = Carbon::now()->toDateString();
                }
                $fact->factura = $reserva->factura;
                $fact->save();
            }
            $reserva->fechaPago = Carbon::now()->toDateTimeString();
            $sendMsg = true;
        }
        $idRol = User::find(Auth::id())->toArray();
        $data = TsReservas::find($id)->toArray();    
        $reserva->estado = $input['txtEstado'];
        $reserva->inicio = $fechaInicio;
        $reserva->fin = $fechaFinal;
        $reserva->duracion = Carbon::parse($fechaFinal)->diffInMinutes(Carbon::parse($fechaInicio));
        
     //   $reserva->metodoPago = $input['txtMetodoPago'];
        if(($idRol['idRol'] == 3)){ 
            $reserva->metodoPago = ($idRol['name']);
            $reserva->precio = 0;
            if (strpos($input['txtNombre'], $idRol['name']) === false){
                $reserva->nombre = ($idRol['name'] ) . " - ". $input['txtNombre'];
            }else{        
                  $reserva->nombre = $input['txtNombre'];
            }
        }else{
            $reserva->precio = $input['txtPrecio'];
            $reserva->metodoPago = $input['txtMetodoPago'];
            $reserva->nombre = $input['txtNombre'];
        }
        $reserva->email = $input['txtEmail'];
        if(!empty($input['txtDNI']))
            $reserva->DNI = $input['txtDNI'];
        else
        $reserva->CIF = $input['txtCIF'];
        $reserva->telefono = $input['txtTelefono'];
        $reserva->idioma = $input['txtIdioma'];
        $reserva->direccion = (empty($input['txtDireccion']))?null:$input['txtDireccion'];
        $reserva->descuento = 0;
        $reserva->comentarios = $input['txtComentario'];
       
        $reserva->tipoDescuento = ' ';
        $reserva->codigoDescuento = '';
        $reserva->save();

        //Mensaje confirmado
        if($sendMsg){
            self::sendMailConfirmado($reserva->id);
        }
        return Redirect::to('booking/bookings')->with('message', 'Cambios realizados correctamente');
    }

    private function sendMailConfirmado($idReserva, $admin = true){
        $reservas = TsReservas::find($idReserva)->toArray();
        $contenido = TsMailing::where('nombre', 'confirmacion')->first()->toArray();
        switch(strtolower($reservas['idioma'])){
            case 'english':
                $contenido = $contenido['ingles'];
                break;
            case 'catala':
                $contenido = $contenido['catalan'];
                break;
            case 'frances':
                $contenido = $contenido['frances'];
                break;
            default:
                $contenido = $contenido['contenido'];
                break;
        }

        $data = array('{Name}', '{Timeslots}');
        $replace = array($reservas['nombre'], $reservas['inicio']);
        $contenidoBody =  str_replace($data, $replace, $contenido);


        $mail = new \Mailer();
        $mail->addAddress($reservas['nombre'], $reservas['email']);
        $mail->setContent('Confirmacion de reserva en juego Andorra Quest Room', $contenidoBody);
        $mail->sendMail();

        if($admin) {
            $mailAdmin = new \Mailer();
            $mailAdmin->addAddress('Admin', 'reservas@andorraquestroom.com');
            $mailAdmin->setContent('[Admin] Confirmacion de reserva en juego Andorra Quest Room', $contenidoBody);
            $mailAdmin->sendMail();
        }

        //Mirar si tiene activo el send Factura y enviarsela
        if((bool)$reservas['factura']){
            //Lets send the factura
            $data = TsFacturacion::find(1)->toArray();
            $reserva = TsReservas::find($idReserva)->toArray();
            $idRel = TsRelFacturacion::where('idReserva', $idReserva)->get()->toArray();
            $numF = self::calcNumero(TsRelFacturacion::find($idRel[0]['id']));
            $mpdf = new \mPDF();
            $html = View::make('facturar.genFacturaPersonal', array('data' => $data, 'reserva' => $reserva, 'numeroF' => $numF))->render();
            $mpdf->WriteHTML($html);
            $dttpdf = $mpdf->Output('', 'S');

            $mail = new \Mailer();
            $mail->addAddress($reservas['nombre'], $reservas['email']);
            $mail->setContent('Factura reserva Andorra Quest Room', 'Se le ha adjuntado la factura');
            $mail->addStringAttach($dttpdf, 'Factura.pdf');
            $mail->sendMail();
        }
    }

    private function calcNumero($reserva){
        return count(TsRelFacturacion::groupBy('fechaPago')->where('fechaPago','<',$reserva->fechaPago)->get()->toArray())+1;
    }

    private function sendMailCancelado($idReserva){

        $reservas = TsReservas::find($idReserva)->toArray();
        $contenido = TsMailing::where('nombre', 'cancelacion')->first()->toArray();

        switch(strtolower($reservas['idioma'])){
            case 'english':
                    $titulo = $contenido['titulo_ingles'];
                    $contenido = $contenido['ingles'];
                    break;
                case 'catala':
                    $titulo = $contenido['titulo_catalan'];
                    $contenido = $contenido['catalan'];
                    break;
                case 'frances':
                    $titulo = $contenido['titulo_frances'];
                    $contenido = $contenido['frances'];
                    break;
                default:
                    $titulo = $contenido['titulo_contenido'];
                    $contenido = $contenido['contenido'];
                    break;
        }

        $dataBody = array('{Name}', '{Timeslots}', '{BookingID}', '{Total}');
        $replaceBody = array($reservas['nombre'], $reservas['inicio'], $reservas['id'], round($reservas['precio']-($reservas['precio']*($reservas['descuento']/100)),2));
        $contenidoBody =  str_replace($dataBody, $replaceBody, $contenido);

        $dataTitle = array('{idReserva}');
        $replaceTitle = array($reservas['id']);
        $contenidoTitle = str_replace($dataTitle, $replaceTitle, $titulo);


        $mail = new \Mailer();
        $mail->addAddress($reservas['nombre'], $reservas['email']);
        $mail->setContent($contenidoTitle, $contenidoBody);
        $mail->sendMail();

        $mailAdmin = new \Mailer();
        $mailAdmin->addAddress('Admin', 'reservas@andorraquestroom.com');
        $mailAdmin->setContent('[Admin] Cancelacion Andorra Quest Room', $contenidoBody);
        $mailAdmin->sendMail();
    }

    public function rememberMail($id){
        self::sendMailConfirmado($id, false);
        return Redirect::to('booking/bookings')->with('message', 'Mail enviado correctamente');
    }

    public function listarBookings()
    {   $idRol = User::find(Auth::id())->toArray();
        
        if(($idRol['idRol'] == 3)){
            $mailings = TsReservas::select(['id', 'sala', 'inicio', 'fin', 'nombre', 'precio','estado'])->where('metodoPago','=', $idRol['name'])->where('inicio','<>', '0000-00-00 00:00:00');
        }else{
            $mailings = TsReservas::select(['id','sala', 'inicio', 'fin', 'nombre', 'precio','estado'])->where('nombre','<>', 'interno')->where('inicio','<>', '0000-00-00 00:00:00');
        }
            
        

        return Datatables::of($mailings)
            ->editColumn('inicio', function($reserva){
                $ini = Carbon::parse($reserva->inicio)->format('d/m H:i');
                $end = Carbon::parse($reserva->fin)->format('H:i');
                return $ini.' -> '.$end;
            })
            ->editColumn('precio', function($reserva){
                return $reserva->precio."&euro;";
            })
            ->editColumn('estado', function($reserva){
                if($reserva->estado == 0){
                    return '<p class="btn btn-xs btn-warning">Pendiente</p>';
                }elseif($reserva->estado == 1){
                    return '<p class="btn btn-xs btn-success">Aceptado</p>';
                }else{
                    return '<p class="btn btn-xs btn-danger">Cancelado</p>';
                }
            })
            ->addColumn('accion', function ($mailing) {
                $dt = '<a class="btn btn-xs btn-primary editar" href="'.url('/booking/bookings/editar/'.$mailing->id).'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                $dt .= '<a class="btn btn-xs btn-warning" href="'.url('/booking/bookings/sendRemember/'.$mailing->id).'"><i class="glyphicon glyphicon-envelope"></i> Mail de recordatorio</a>';
                return $dt;
            })
            ->removeColumn('fin')
            ->make(true);
    }
}
