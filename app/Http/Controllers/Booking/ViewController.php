<?php namespace App\Http\Controllers\Booking;

use App\Http\Controllers\Controller;
use App\TsCalendario;
use App\TsReservas;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class ViewController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth');
    
        
    }

    public function calendario($sala = null)
    {
        if($sala==null){
            $sala =  '0';
        }
        Session::put('sala',$sala);
        $session = Session::all();
        if (!isset($session['adminRelativeMetric'])) {
            session(['adminRelativeMetric' => 0]);
            $session = Session::all();
        }
        $carbonDate = Carbon::now()->addDays($session['adminRelativeMetric']);
        $semanaArray = self::generaCalendario($carbonDate);
        $diasSemana = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

        return view('booking.calendario', array('semanaArray' => $semanaArray, 'diasSemana' => $diasSemana));
    }

    public function addReservaView($id, $fecha)
           
            
    {
        
         $session = Session::all();
          $sala= $session['sala'];
        if($id == 0) $id = 7;
        $timetable = TsCalendario::where('idDia', $id) ->where('sala','=', $sala)->get()->toArray();
        $horas = array();
        $ocupacion = array();
        $duracion = array();
        foreach ($timetable as $item) {
            $inicio = Carbon::parse($fecha." ".$item['inicio']);
            $fin = Carbon::parse($fecha." ".$item['fin']);
            while ($inicio->lt($fin)) {
                $horas[] = $inicio->toTimeString();
                $duracion[] = $item['duracion'];
                $ocupacion[] = (TsReservas::where('estado', '<>', -1)
                                            ->where('sala','=', $sala)
                                            ->where('inicio', '>=', $inicio->toDateTimeString())
                                            ->where('fin', '<=', Carbon::parse($inicio->toDateTimeString())->addMinutes($item['duracion'])->toDateTimeString())
                                            ->count() > 0);
                $inicio->addMinutes($item['duracion'] + 1);
            }
        }

        return view('booking.addReserva', array('horas' => $horas, 'ocupacion' => $ocupacion, 'duracion' => $duracion, 'id' => $id, 'fecha' => $fecha));
    }

    public function addInner()
    {
       
        $session = Session::all();
        $sala= $session['sala'];
        
        $input = Input::all();
        $input['horas'] = explode(",", $input['horas']);
        $input['duraciones'] = explode(",", $input['duraciones']);

        foreach($input['horas'] as $k => $hora){
            $cal = TsReservas::where('inicio', '>=', Carbon::parse($input['fechaini'] . ' ' . $hora)->toDateTimeString())
                ->where('fin', '<=', Carbon::parse($input['fechaini'] . ' ' . $hora)->addMinutes($input['duraciones'][$k])->toDateTimeString())
                ->where('estado', '<>', -1)->where('sala','=', $sala);

            if ($cal->count() > 0) {
                return response()->json(['message' => 'No puedes machacar reservas que ya hay hechas'], 404);
            }
        }

        $idRol = User::find(Auth::id())->toArray();
        foreach($input['horas'] as $k => $hora) {
            
            $reserva = new TsReservas();
            $reserva->estado = 1;
            $reserva->sala = $sala;
            $reserva->inicio = Carbon::parse($input['fechaini'] . ' ' . $hora)->toDateTimeString();
            $reserva->fin = Carbon::parse($input['fechaini'] . ' ' . $hora)->addMinutes($input['duraciones'][$k])->toDateTimeString();
            $reserva->duracion = $input['duraciones'][$k];
            $reserva->precio = 0;
            $reserva->nPersonas = 0;
            if ($idRol['idRol'] == 3) {
                $reserva->nombre = ($idRol['name'] );
                $reserva->metodoPago = ($idRol['name'] );
                $reserva->telefono = ($idRol['name'] );
                $reserva->descuento = 100;
                $reserva->tipoDescuento = ($idRol['name'] );
                $reserva->email = ($idRol['email'] );
                $reserva->DNI = ($idRol['name'] );
            }else {
                $reserva->metodoPago = 'interno';
                $reserva->nombre = 'interno';
                $reserva->telefono = 'interno';
                $reserva->descuento = 100;
                $reserva->tipoDescuento = "Interno";
                $reserva->email = ($idRol['email'] );
                $reserva->DNI = 'interno';
            }
  
            $reserva->ip = Request::ip();
            $reserva->save();

            $contenidoBody = "Nom: $reserva->nombre <br> E-mail $reserva->email <br> Data: $reserva->inicio  --  $reserva->fin <br>";
            

            $mailAdmin = new \Mailer();
            $mailAdmin->addAddress('Admin', 'reservas@andorraquestroom.com');
            $mailAdmin->setContent('[Admin] Confirmacion de reserva interna en juego Andorra Quest Room', $contenidoBody);
            $mailAdmin->sendMail();
        }
    
    }

    public function setSessionFechaIni($action)
    {
        $session = Session::all();
        if (!isset($session['adminRelativeMetric'])) {
            session(['adminRelativeMetric' => 0]);
            $session = Session::all();
        }
        if ($action == '+') {
            session(['adminRelativeMetric' => $session['adminRelativeMetric'] + 7]);
        } elseif ($action == '-') {
            session(['adminRelativeMetric' => $session['adminRelativeMetric'] - 7]);
        } else {
            session(['adminRelativeMetric' => 0]);
        }

        return Redirect::to('booking/calendario');
    }

    /**
     * Funciones de ayuda
     */

    private function generaCalendario($weekStart)
    {
        
        $semana = array();
        $baseWeek = clone $weekStart;
        for ($i = 0; $i < 7; ++$i, $weekStart->addDays(1)) {
            //Ahora que sabemos el dia de la semana, añadimos los eventos
            $semana[] = array('fecha' => $weekStart->toDateString(), 'datos' => self::addEventos($weekStart->toDateString(), $baseWeek->toDateString()));
        }
        $semana = self::clearEventos($semana);
        return $semana;
    }

    private function addEventos($date, $iniDate)
            
    {
        
        $session = Session::all();
        $sala= $session['sala'];
        $dayData = array();
        $fecha = Carbon::createFromFormat("Y-m-d", $date);

        $earlierBeginning = $date . " " . TsCalendario::where('festivo', 0)->orderBy('inicio', 'ASC')->first()->toArray()['inicio'];
        $lastestEnding = $date . " " . TsCalendario::where('festivo', 0)->orderBy('fin', 'DESC')->first()->toArray()['fin'];

        

        $durationBetween = 30;//($worst - $best == 0) ? $worst : $worst - $best;
        if($durationBetween == 0) return array();


        $numerbOfDivisions = (int)Carbon::parse($lastestEnding)->diffInMinutes(Carbon::parse($earlierBeginning)) / $durationBetween;
        $earlierBeginning = Carbon::parse($earlierBeginning);

        //Analizamos el dia en tramos de media hora
        $sesion = 0;
        $tiempoAcomulado = 0;
        $idLast = 0;
        for ($i = 0; $i <= $numerbOfDivisions; ++$i) {
            $iniHour = clone $earlierBeginning;
            $iniHour->addMinutes($durationBetween * $i);
            $endHour = clone $iniHour;
            $endHour->addMinutes($durationBetween);

            $data = array();
            $data['ini'] = $iniHour->toDateTimeString();
            $data['end'] = $endHour->toDateTimeString();
            $data['primary'] = false;
            $data['sesion'] = -1;
            $data['rowspan'] = 1;
            $data['reserva'] = false;

            if (TsReservas::where('estado', '<>', -1)->where('sala','=', $sala)->where('fin', '>', $iniHour->toDateTimeString())->where('inicio', '<', $endHour->toDateTimeString())->count() > 0) {
                $reservaDatos = TsReservas::where('estado', '<>', -1)->where('sala','=', $sala)->where('fin', '>', $iniHour->toDateTimeString())->where('inicio', '<', $endHour->toDateTimeString())->first()->toArray();
                if ($sesion != $reservaDatos['id']) {
                    $sesion = $reservaDatos['id'];
                    $data['primary'] = true;
                    $idLast = $i;
                } else {
                    $dayData[$idLast]['rowspan'] += 1;
                }
                $data['reserva'] = true;
                $data['ini'] = $reservaDatos['inicio'];
                $data['end'] = $reservaDatos['fin'];
                $data['nombre'] = $reservaDatos['nombre'];
                $data['id'] = $reservaDatos['id'];
                $data['estado'] = $reservaDatos['estado'];
            } else {
                $data['reserva'] = false;
            }
            $dayData[] = $data;
        }
        return $dayData;
    }

    //Mirar si una hora a lo largo de la semana esta vacio y se borra esa franja horaria
    private function clearEventos($semana)
    {
        $semanaNew = array();
        $arrayDeleteFranjasHorarias = array();
        $delete = true;
        for ($franjaHoraria = 0; $franjaHoraria < count($semana[0]['datos']) && $delete; ++$franjaHoraria) {
            for ($dia = 0; $dia < 7 && $delete; ++$dia) {
                if ($semana[$dia]['datos'][$franjaHoraria]['reserva']) {
                    $delete = false;
                }
            }
            if ($delete) {
                $arrayDeleteFranjasHorarias[] = $franjaHoraria;
            }
        }


        $delete = true;
        for ($franjaHoraria = count($semana[0]['datos'])-1; $franjaHoraria >= 0 && $delete; --$franjaHoraria) {
            for ($dia = 0; $dia < 7 && $delete; ++$dia) {
                if ($semana[$dia]['datos'][$franjaHoraria]['reserva']) {
                    $delete = false;
                }
            }
            if ($delete) {
                $arrayDeleteFranjasHorarias[] = $franjaHoraria;
            }
        }

        for ($dia = 0; $dia < 7; ++$dia) {
            $semanaNew[$dia]['fecha'] = $semana[$dia]['fecha'];
            $semanaNew[$dia]['datos'] = array();
            for ($franjaHoraria = 0; $franjaHoraria < count($semana[0]['datos']); ++$franjaHoraria) {
                if (!in_array($franjaHoraria, $arrayDeleteFranjasHorarias)) {
                    $semanaNew[$dia]['datos'][] = $semana[$dia]['datos'][$franjaHoraria];
                }
            }
        }

        return $semanaNew;
    }
}
