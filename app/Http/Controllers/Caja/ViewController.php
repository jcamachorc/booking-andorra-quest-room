<?php namespace App\Http\Controllers\Caja;

use App\Http\Controllers\Controller;
use App\SysCajaChica;
use App\SysJourney;
use App\SysJourneyComentarios;
use App\SysMailing;
use App\SysMailingMails;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function listar()
	{
        $data = SysCajaChica::orderBy('id', 'ASC')->get()->toArray();
        foreach ($data as $key => $item) {
            if($key == 0){
                $data[$key]['acumulado'] = ($item['ingreso'])?$item['cantidad']:$item['cantidad']*-1;
            }else{
                $data[$key]['acumulado'] = $data[$key-1]['acumulado'] + (($item['ingreso'])?$item['cantidad']:$item['cantidad']*-1);
            }
        }
        return view('caja.listar', array('contabilidad' => $data));
	}

    public function add(){
        $input = Input::all();

        $caja = new SysCajaChica();
        $caja->ingreso = ($input['cantidad'] >= 0);
        $caja->cantidad = ($input['cantidad'] >= 0)?$input['cantidad']:$input['cantidad']*-1;
        $caja->concepto = $input['concepto'];
        $caja->save();

        return redirect('caja');
    }
}
