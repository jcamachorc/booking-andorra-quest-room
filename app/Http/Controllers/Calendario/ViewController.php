<?php

namespace App\Http\Controllers\Calendario;

use App\Http\Controllers\Controller;
use App\SysMailing;
use App\TsBonoDias;
use App\TsBonos;
use App\TsCalendario;
use App\TsConfigs;
use App\TsDescuentos;
use App\TsFacturacion;
use App\TsGrupoPrecios;
use App\TsMailing;
use App\TsPrecios;
use App\TsRelFacturacion;
use App\TsRelGrupoPrecios;
use App\TsSalas;
use App\TsReservas;
use App\TsVacaciones;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use yajra\Datatables\Datatables;

class ViewController extends Controller {

    public function __construct() {
        
    }

    public function index($sala = null) {

        $session = Session::all();

        $sala_session = null;
        if (isset($session['sala'])) {
            $sala_session = $session['sala'];
        }

        if ($sala == null) {
            if ($sala_session != null) {
                $sala = $sala_session;
            } else {
                $sala = '0';
            }
        }

        if($sala!='0' && $sala!='1'){
            $sala = '0';
        }

        Session::put('sala', $sala);
        $session = Session::all();

        $salas = TsSalas::select('id', 'nombre')->get()->toArray();


        if (isset($session['input']) && isset($session['input']['idReserva'])) {
            $input = $session['input'];
            if (TsReservas::where('estado', -1)->where('sala', '=', $sala)->where('id', $input['idReserva'])->count() > 0) {
                Session::forget('input');
            }

            $calendario = TsCalendario::find($input['txtId'])->toArray();
            $fechaFinal = Carbon::parse($input['txtFechaInicio'])->addMinutes($calendario['duracion'])->toDateTimeString();
            if (TsReservas::where('estado', 1)->where('sala', '=', $sala)->where('fin', '>', Carbon::parse($input['txtFechaInicio'])->toDateTimeString())->where('inicio', '<', $fechaFinal)->count() > 0) {
                Session::forget('input');
            }
        }

        if (!isset($session['relativeMetric'])) {
            session(['relativeMetric' => 0]);
            $session = Session::all();
        }

        $carbonDate = Carbon::now()->addDays($session['relativeMetric']);
        $carbonDateCP = clone $carbonDate;
        if (Carbon::now()->gte($carbonDateCP->addDays(7))) {
            return self::setSessionFechaIni('=');
        }

        $semanaArray = self::generaCalendario($carbonDate, $sala);

       
        //$semanaArray = self::generaCalendario(date('d'), date("m"), date("Y"));
        $diasSemana = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');

        return view('calendario.listar', array('semanaArray' => $semanaArray, 'diasSemana' => $diasSemana, 'salas' => $salas));
    }

    public function setSessionFechaIni($action) {
        $session = Session::all();
        if (!isset($session['relativeMetric'])) {
            session(['relativeMetric' => 0]);
            $session = Session::all();
        }
        if ($action == '+') {
            session(['relativeMetric' => $session['relativeMetric'] + 7]);
        } elseif ($action == '-') {
            session(['relativeMetric' => $session['relativeMetric'] - 7]);
        } else {
            session(['relativeMetric' => 0]);
        }

        return Redirect::to('calendario');
    }

    public function reserva($id, $fecha) {
        //Set session for further control
        //Session::put('sala',$sala);

        $session = Session::all();

        $sala = $session['sala'];


        if (isset($session['input'])) {
            $input = $session['input'];
        }
        if (isset($session['input']['idReserva'])) {
            $input['idReserva'] = $session['input']['idReserva'];
        }

        $input['txtId'] = $id;
        $input['txtFechaInicio'] = $fecha;
        // $input['txtSala'] = $sala;


        session(['input' => $input]);

        $calendario = TsCalendario::find($id)->toArray();
        $fechaInicio = $fecha;
        $fechaFinal = Carbon::parse($fecha)->addMinutes($calendario['duracion'])->toDateTimeString();

        //echo "---> " . $sala . "<br>";
        //die();


        $reservasSolapadas = TsReservas::where('estado', '<>', -1)->where('sala', '=', $sala)->where('fin', '>', Carbon::parse($fecha)->toDateTimeString())->where('inicio', '<', $fechaFinal);
        if ($reservasSolapadas->count() > 0) {
            if (isset($session['input']['idReserva']) && $session['input']['idReserva'] == $reservasSolapadas->first()->toArray()['id']) {
                
            } else {
                Session::forget('input');
                return Redirect::to('calendario/')->withErrors(['La hora seleccionada ya esta reservada']);
            }
        }

        //Precio por sala desde BB.DD.
        #$precio = TsSalas::where('id', '=', $sala)->select('precio')->get();

        //Funcionamineto antiguo
        $cjtPrecios = TsRelGrupoPrecios::where('idGrupo', $calendario['idPrecio'])->get()->toArray();
        $precios = array();
        foreach ($cjtPrecios as $itemPrecios) {
            $precios[] = TsPrecios::find($itemPrecios['idPrecio'])->toArray();
        }
        if(TsGrupoPrecios::where('inicio','<=', Carbon::parse($fechaInicio)->toDateString())->where('fin','>=', Carbon::parse($fechaFinal)->toDateString())->count() != 0){
            $gPrec = TsGrupoPrecios::where('inicio','<=', Carbon::parse($fechaInicio)->toDateString())->where('fin','>=', Carbon::parse($fechaFinal)->toDateString())->first()->toArray();
            $cjtPrecios = TsRelGrupoPrecios::where('idGrupo', $gPrec['id'])->get()->toArray();
            $precios = array();
            foreach ($cjtPrecios as $itemPrecios) {
                $precios[] = TsPrecios::find($itemPrecios['idPrecio'])->toArray();
           }
        }
        
        //Chapuza para cambio de precio sala 1
        if($sala==1 && isset($precios)){
            $precios[0]['precio'] += 10;
        }

        //return con precio desde BB.DD.
        #return view('calendario.reserva', array('id' => $id, 'inputs' => (isset($session['input']) ? $session['input'] : null), 'precios' => $precio, 'fechaInicio' => $fechaInicio, 'fechaFinal' => $fechaFinal));
        
        //return con los precios de la manera antigua
        return view('calendario.reserva', array('id' => $id, 'inputs' => (isset($session['input'])?$session['input']:null), 'precios' => $precios, 'fechaInicio' => $fechaInicio, 'fechaFinal' => $fechaFinal));
    }

    public function checkSession() {
        $session_b = Input::all();
        $session_a = Session::all();

        if ($session_b != $session_a['input']) {
            session(['error_checkout' => 'Se ha refrescado la pagina, los datos no coincidian.
            Posiblemente, esto se debe a que se ha iniciado otro proceso de compra paralelo.
            Verifique que los datos sean los correctos']);
            return response()->toJson(['message' => 'Las sesiones no coinciden'], 404);
        }
    }

    public function backReserva() {

        $session = Session::all();

        $sala = $session['sala'];
        //  


        if (!isset($session['input']))
            return Redirect::to('calendario' . '/' . $sala);
        else
            return Redirect::to('calendario/' . $session['input']['txtId'] . '/' . $session['input']['txtFechaInicio'] . '/' . $sala)->withInput($session['input']);
    }

    public function checkout() {

        $input = Input::all();
        $sesion = Session::all();

        $sala = $input['sala'];
        

        if (isset($sesion['error_checkout'])) {
            Session::forget('error_checkout');
            return Redirect::to('calendario/checkout')->withErrors([$sesion['error_checkout']]);
        }

        if (empty($input) && !isset($sesion['input'])) {
            return Redirect::to('calendario')->withErrors(['Error, los datos introducidos se han perdido. Esto puede ser porque el sistema ya le ha llevado a la pasarela de pago. De tener problemas con el pago (y no haberse realizado correctamente), en 15 minutos se liberara automaticamente la session del juego y podra volver a reservar.']);
        }

        if (empty($input)) {
            $input = $sesion['input'];
        }

        //Check campos
        if (empty($input['txtFechaInicio'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtFechaInicio');
        }
        if (empty($input['txtId'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtId');
        }
        if (empty($input['txtNombre'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtNombre');
        }
        if (empty($input['txtEmail'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtEmail');
        }
        if (empty($input['txtTelefono'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtTelefono');
        }
        if (empty($input['txtDescuento'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtDescuento');
        }
        if (empty($input['txtMetodoPago'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Todos los campos deben ser rellenados'])->with('error-input', 'txtMetodoPago');
        }

        # Check if the number of participants are between the min and max for the room
        #$participantes_min = TsSalas::where('id', '=', $sala)->select('minimo_participantes')->get();
        #$participantes_max = TsSalas::where('id', '=', $sala)->select('maximo_participantes')->get();

        if (empty($input['txtPersonas']) || $input['txtPersonas'] < 1 || $input['txtPersonas'] > 5) {
            return Redirect::back()->withInput(Input::all())->withErrors(['El numero de personas debe ser maximo de 5'])->with('error-input', 'txtPersonas');
        }


        //Factura
        if (empty($input['txtNombreF']) && $input['txtFactura'] == 1) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Si deseas una factura, debes rellenar todos los campos'])->with('error-input', 'txtNombreF');
        }
        if (empty($input['txtDNI']) && $input['txtFactura'] == 1) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Si deseas una factura, debes rellenar todos los campos'])->with('error-input', 'txtDNI');
        }
        if (empty($input['txtDireccion']) && $input['txtFactura'] == 1) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Si deseas una factura, debes rellenar todos los campos'])->with('error-input', 'txtDireccion');
        }
        //END Check campos

        $checker = new \CIFNIFcheck();
        if (!$checker->isValidIdNumber($input['txtDNI']) && !empty($input['txtDNI'])) {
            return Redirect::back()->withInput(Input::all())->withErrors(['El campo NIF/NRT no es correcto']);
        }

        $calendario = TsCalendario::find($input['txtId'])->toArray();
        $fechaFinal = Carbon::parse($input['txtFechaInicio'])->addMinutes($calendario['duracion'])->toDateTimeString();

        $reservasSolapadas = TsReservas::where('estado', '<>', -1)
                ->where('fin', '>', Carbon::parse($input['txtFechaInicio'])->toDateTimeString())
                ->where('inicio', '<', $fechaFinal)
                ->where('sala', '=', $sala);

        if ($reservasSolapadas->count() > 0) {
            if (isset($sesion['input']['idReserva']) && $sesion['input']['idReserva'] == $reservasSolapadas->first()->toArray()['id']) {
                
            } else {
                Session::forget('input');
                return Redirect::to('calendario/')->withErrors(['La hora seleccionada ya esta reservada']);
            }
        }

        if (($input['txtDescuento'] == 'bonoregalo' || $input['txtDescuento'] == 'descuento') && empty($input['txtCodigo'])) {
            //Return te has olvidado de introducir un codigo valido
            return Redirect::back()->withInput(Input::all())->withErrors(['Has olvidado introducir un codigo valido']);
        }

        if ($input['txtDescuento'] == 'carnetjove' && (/* empty($input['txtFechaNacimiento']) || */ empty($input['txtCarnetJove']))) {
            //Return te has olvidado de introducir el carnet jove o la fecha de nacimiento
            return Redirect::back()->withInput(Input::all())->withErrors(['Hemos encontrado un error!. Revisa la fecha de nacimiento y número de carnet jove']);
        }

        if ($input['txtDescuento'] == 'clubabc4event' && (empty($input['txtClubABC4event']))) {
            return Redirect::back()->withInput(Input::all())->withErrors(['Hemos encontrado un error! Comprueba que el campo descuento este bien rellenado.']);
        }

        $descuento = 0;
        if ($input['txtDescuento'] == 'carnetjove') {

            $dttt = TsConfigs::find('descuento_carnet_jove')->toArray();
            $descuento = $dttt['value']; //12
        }

        if ($input['txtDescuento'] == 'clubabc4event') {
            $dttt = TsConfigs::find('descuento_clubabc4event')->toArray();
            $descuento = $dttt['value'];
        }

        //Check codigo descuento
        //if fails -> return error aplicando el bono regalo
        if ($input['txtDescuento'] == 'descuento') {
            $valid = TsDescuentos::where('id', $input['txtCodigo'])
                    ->where('fechaCaducidad', '>', Carbon::now()->toDateTimeString())
                    ->where(function($q) {
                        $q->where('idReserva', NULL)
                        ->orWhere('ilimitados', 1);
                    })
                    ->count();
            if ($valid == 0) {
                return Redirect::back()->withInput(Input::all())->withErrors('Error validando el código del bono');
            } else {
                $descuento = TsDescuentos::where('id', $input['txtCodigo'])
                                ->where('fechaCaducidad', '>', Carbon::now()->toDateTimeString())
                                ->where(function($q) {
                                    $q->where('idReserva', NULL)
                                    ->orWhere('ilimitados', 1);
                                })->get()->toArray();
                $descuento = $descuento[0]['descuento'];
            }
        }

        //Check  bono regalo
        //if fails -> return error aplicando el descuento del codigo
        $idBono = -1;
        $bnnBono = false;
        if ($input['txtDescuento'] == 'bonoregalo') {
            if (!isset($sesion['input']['idReserva']) || !isset($sesion['input']['bonoUsed']))
                $sesion['input']['idReserva'] = null;
            if (TsBonos::where('codigo', $input['txtCodigo'])->where('fechaCaducidad', '>', Carbon::now()->toDateTimeString())->whereNotNull('fechaCaducidad')->where('idReserva', $sesion['input']['idReserva'])->count() == 0) {
                return Redirect::back()->withInput(Input::all())->withErrors('Error validando el código del bono');
            } else {
                $input['bonoUsed'] = true;
                $data = TsBonos::where('codigo', $input['txtCodigo'])->where('fechaCaducidad', '>', Carbon::now()->toDateTimeString())->whereNotNull('fechaCaducidad')->where('idReserva', $sesion['input']['idReserva'])->first()->toArray();
                $idBono = $data['id'];
                if ($data['precio'] < $input['txtPrecio']) {
                    $descuento = round(100 * $data['precio'] / $input['txtPrecio'], 2);
                } else {
                    $descuento = 100; //%
                }
                //Especial BNN
                if ($data['metodoPago'] == 'BNN') {
                    $descuento = 100; //%
                    $bnnBono = true;
                }
            }
        }
        //Despues de todo, miramos que el bono sea valido para el dia de la semana a reservar
        $dow = Carbon::parse($input['txtFechaInicio'])->dayOfWeek;
        //Domingo -> 0      || Nosotros domingo -> 6
        //Lunes -> 1        || Nosotros Lunes -> 0
        if ($dow == 0)
            $dow = 6;
        else
            --$dow;

        if (TsBonoDias::where('idBono', $idBono)->where('dia', $dow)->count() == 0 && TsBonoDias::where('idBono', $idBono)->count() != 0) {
            $bono = TsBonos::find($idBono);
            $bono->idReserva = null;
            $bono->save();
            return Redirect::back()->withInput(Input::all())->withErrors('Error validando el código del bono. No valido para este dia de la semana.');
        }

        if ($descuento >= 100) {
            $descuento = 100; //Just in case
            $input['txtFactura'] = 0;
            $input['txtMetodoPago'] = 'paypal';
        }

        $calendario = TsCalendario::find($input['txtId'])->toArray();
        $input['txtFechaFinal'] = Carbon::parse($input['txtFechaInicio'])->addMinutes($calendario['duracion'])->toDateTimeString();
        $input['descuento'] = $descuento;

        //Si todo ha ido bien, introducimos la nueva reserva
        $calendario = TsCalendario::find($input['txtId'])->toArray();

        $reserva = null;
        if (isset($sesion['input']['idReserva'])) {
            $reserva = TsReservas::find($sesion['input']['idReserva']);
            if ($reserva == null) {
                $reserva = new TsReservas();
            }
        } else {
            $reserva = new TsReservas();
        }

        $reserva->estado = 0; //0 => Pendiente || 1 => Ok || -1 => Cancelada
        $reserva->inicio = $input['txtFechaInicio'];
        $reserva->fin = Carbon::parse($input['txtFechaInicio'])->addMinutes($calendario['duracion'])->toDateTimeString();
        $reserva->duracion = $calendario['duracion'];
        $reserva->precio = $input['txtPrecio'];
        $reserva->nPersonas = $input['txtPersonas'];
        if ($bnnBono == 'BNN') {
            $reserva->metodoPago = 'BNN';
        } else {
            $reserva->metodoPago = $input['txtMetodoPago'];
        }
        $reserva->email = $input['txtEmail'];
        $reserva->direccion = $input['txtDireccion'];
        if ($checker->isValidNIFFormat($input['txtDNI']) || $checker->isValidNIEFormat($input['txtDNI'])) {
            $reserva->DNI = $input['txtDNI'];
        } else {
            $reserva->CIF = $input['txtDNI'];
        }

        $reserva->factura = ($input['txtFactura'] == 1);
        $reserva->nombre = $input['txtNombre'];
        $reserva->nombreF = $input['txtNombreF'];
        $reserva->telefono = $input['txtTelefono'];
        $reserva->descuento = $input['descuento'];
        $reserva->tipoDescuento = $input['txtDescuento'];
        $reserva->idioma = $input['txtIdioma'];
        if (!empty($input['txtCarnetJove'])) {
            $reserva->codigoDescuento = $input['txtCarnetJove'];
        } else if (!empty($input['txtClubABC4event'])) {
            $reserva->codigoDescuento = $input['txtClubABC4event'];
        } else {
            $reserva->codigoDescuento = $input['txtCodigo'];
        }
        //$reserva->codigoDescuento = (empty($input['txtCarnetJove']))?$input['txtCodigo']:$input['txtCarnetJove'];
        $reserva->ip = Request::ip();
        $reserva->sala = $sala;
        $reserva->save();

        $input['idReserva'] = $reserva->id;
        $input['precioFinal'] = round(explode('->', $input['txtPrecio'])[0] - (explode('->', $input['txtPrecio'])[0] * ($descuento / 100)), 2);
        ;

        //Marcar bono regalo como gastado
        if ($input['txtDescuento'] == 'bonoregalo') {
            $id = TsBonos::where('codigo', $input['txtCodigo'])->first()->toArray();
            $bono = TsBonos::find($id['id']);
            $bono->idReserva = $reserva->id;
            if ($bono->metodoPago == 'BNN') {
                $bono->DNI = $input['txtDNI'];
                $bono->telefono = $input['txtTelefono'];
                $bono->fechaPago = Carbon::now();
                $bono->nombre = $input['txtNombre'];
                $bono->email = $input['txtEmail'];
            }
            $bono->save();
        } elseif ($input['txtDescuento'] == 'descuento') {
            $dsc = TsDescuentos::find($input['txtCodigo']);
            $dsc->idReserva = $reserva->id;
            if ($dsc->ilimitados == 0)
                $dsc->save();
        }

        //Guardamos los datos en la session
        session(['input' => $input]);
        session(['verify' => md5(serialize($input))]);

        $formPago = null;
        if ($input['txtMetodoPago'] == 'tpv') {
            $tpv = new \TPV();
            $url = url('calendario/tpv.php');
            $formPago = $tpv->generateFormData("0000C" . $reserva->id, //Si se toca, tocar el TPVmerchantURL() -> order substr
                    $input['precioFinal'], getConfig('payment_ok'), getConfig('payment_ko'), $url);
        }
        return view('calendario.checkout', array('input' => $input, 'descuento' => $descuento, 'formPago' => $formPago));
    }

    public function TPVmerchantURL() {
        $input = Input::all();

        $obj = new \RedsysAPI();
        $decod = $obj->decodeMerchantParameters($input['Ds_MerchantParameters']);
        $dt = get_object_vars(json_decode($decod));
        $order = substr($dt['Ds_Order'], 5);

        if ($dt['Ds_Response'] >= 0 && $dt['Ds_Response'] <= 99) {
            //Ok -> confirma and send mail
            $reserva = TsReservas::find($order);
            $reserva->fechaPago = Carbon::now()->toDateTimeString();
            $reserva->estado = 1;
            $reserva->codigoPago = $dt['Ds_Response'];
            $reserva->save();

            if (TsRelFacturacion::where('idReserva', $order)->count() == 0) {
                $fact = new TsRelFacturacion();
                $fact->idBono = null;
                $fact->idReserva = $order;
                if ((bool) $reserva->factura) {
                    $fact->fechaPago = Carbon::now()->toDateTimeString();
                    $fact->factura = 1;
                } else {
                    $fact->fechaPago = Carbon::now()->toDateString();
                }
                $fact->factura = $reserva->factura;
                $fact->save();
            }

            self::sendMailConfirmado($order);
        } else {
            //Error -> guarda error
            $reserva = TsReservas::find($order);
            $reserva->estado = -1;
            $reserva->codigoPago = $dt['Ds_Response'];
            $reserva->save();
        }
    }

    public function cancelar() {
        $sesion = Session::all();
        if (isset($sesion['input'])) {
            $input = $sesion['input'];
            if (isset($input['idReserva'])) {
                $reserva = TsReservas::find($input['idReserva']);
                if ($reserva->estado == 0) {
                    $reserva->estado = -1;
                }
                $reserva->save();

                foreach (TsDescuentos::where('idReserva', $reserva->id)->get()->toArray() as $descuento) {
                    $dsc = TsDescuentos::find($descuento['id']);
                    $dsc->idReserva = null;
                    $dsc->save();
                }

                foreach (TsBonos::where('idReserva', $reserva->id)->get()->toArray() as $descuento) {
                    $dsc = TsBonos::find($descuento['id']);
                    $dsc->idReserva = null;
                    if ($dsc->metodoPago == 'BNN') {
                        $dsc->DNI = "";
                        $dsc->telefono = "";
                        $dsc->nombre = "";
                        $dsc->email = "";
                    }
                    $dsc->save();
                }
            }
            Session::forget('input');
        }
        return Redirect::back();
    }

    public function resetSession() {

        $session = Session::all();
        self::sendMailPrereserva($session['input']);

        if ($session['input']['descuento'] == 100) {
            $reserva = TsReservas::find($session['input']['idReserva']);
            $reserva->estado = 1; //0 => Pendiente || 1 => Ok || -1 => Cancelada
            $reserva->save();
            self::sendMailConfirmado($session['input']['idReserva']);
        }
        Session::forget('input');
    }

    public function pay() {
        $data = Session::all();
        $input = $data['input'];
        Session::forget('input');

        self::sendMailPrereserva($input);

        if ($input['descuento'] == 100) {
            $reserva = TsReservas::find($input['idReserva']);
            $reserva->estado = 1; //0 => Pendiente || 1 => Ok || -1 => Cancelada
            $reserva->save();
            if ($reserva->metodoPago == 'BNN') {
                $data = TsBonos::where('codigo', $reserva->codigoDescuento)->first()->toArray();
                $fact = new TsRelFacturacion();
                $fact->idBono = $data['id'];
                $fact->idReserva = null;
                $fact->fechaPago = Carbon::now()->toDateTimeString();
                $fact->factura = 1;
                $fact->save();
            }
            self::sendMailConfirmado($input['idReserva']);
            return Redirect::away(getConfig('payment_ok')); //Success wordpress
        }

        //Redirect segun pasarela de pago
        if ($input['txtMetodoPago'] == 'paypal') {
            //Pago paypal
            /* $sanboxed = \App\TsConfigs::find('payments_mode_sandboxed')->toArray();
              if($sanboxed['value'] == "1") {
              $paypal = new \Paypal("https://api.sandbox.paypal.com/v1/oauth2/token", "https://api.sandbox.paypal.com/v1/payments/payment");
              }else{ */
            $paypal = new \Paypal("https://api.paypal.com/v1/oauth2/token", "https://api.paypal.com/v1/payments/payment");
            //}

            $paypal->connect();
            $url = $paypal->seconStage(url('calendario/paypal'), getConfig('payment_ko'), $input['precioFinal'], 'Pago del cupón regalo para la experiencia Enigma .- ' . $input['precioFinal'] . '€');

            session(['paypalTkn' => $paypal->getToken()]);
            session(['paypalId' => $input['idReserva']]);

            return Redirect::away($url);
        }
        return Redirect::away(getConfig('payment_ko'));    //Error wordpress
    }

    

    private function sendMailPrereserva($input) {
        $reserva = TsReservas::find($input['idReserva'])->toArray();
        $contenido = TsMailing::where('nombre', 'prereserva')->first()->toArray();
        switch (strtolower($reserva['idioma'])) {
            case 'english':
                $titulo = $contenido['titulo_ingles'];
                $contenido = $contenido['ingles'];
                break;
            case 'catala':
                $titulo = $contenido['titulo_catalan'];
                $contenido = $contenido['catalan'];
                break;
            case 'frances':
                $titulo = $contenido['titulo_frances'];
                $contenido = $contenido['frances'];
                break;
            default:
                $titulo = $contenido['titulo_contenido'];
                $contenido = $contenido['contenido'];
                break;
        }

        $dataBody = array('{Name}', '{Phone}', '{Email}', '{BookingID}', '{Timeslots}', '{PaymentMethod}', '{Total}');
        $replaceBody = array($input['txtNombre'], $input['txtTelefono'], $input['txtEmail'], $input['idReserva'], $input['txtFechaInicio'], $input['txtMetodoPago'], $input['txtPrecio'] - ($input['txtPrecio'] * $input['descuento'] / 100));
        $contenidoTitle = $titulo;
        $contenidoBody = str_replace($dataBody, $replaceBody, $contenido);


        $mail = new \Mailer();
        $mail->addAddress($input['txtNombre'], $input['txtEmail']);
        $mail->setContent($contenidoTitle, $contenidoBody);
        $mail->sendMail();

        $mailAdmin = new \Mailer();
        $adminMail = \App\TsConfigs::find('correo_admin')->toArray();
        //AQUI ESTA EL POLLO--
        $mailAdmin->addAddress('Admin prereserva Quest Room', $adminMail['value']);
        $mailAdmin->setContent('[Admin] Nueva pre-reserva en Quest Room', $contenidoBody);
        $mailAdmin->sendMail();
    }

    private function sendMailConfirmado($idReserva) {
        $reservas = TsReservas::find($idReserva)->toArray();
        $contenido = TsMailing::where('nombre', 'confirmacion')->first()->toArray();
        switch (strtolower($reservas['idioma'])) {
            case 'english':
                $titulo = $contenido['titulo_ingles'];
                $contenido = $contenido['ingles'];
                break;
            case 'catala':
                $titulo = $contenido['titulo_catalan'];
                $contenido = $contenido['catalan'];
                break;
            case 'frances':
                $titulo = $contenido['titulo_frances'];
                $contenido = $contenido['frances'];
                break;
            default:
                $titulo = $contenido['titulo_contenido'];
                $contenido = $contenido['contenido'];
                break;
        }

        $dataTitle = array('{idReserva}');
        $dataBody = array('{Name}', '{Timeslots}', '{idReserva}');
        $replaceTitle = array($idReserva);
        $replaceBody = array($reservas['nombre'], $reservas['inicio'], $idReserva);
        $contenidoTitle = str_replace($dataTitle, $replaceTitle, $titulo);
        $contenidoBody = str_replace($dataBody, $replaceBody, $contenido);


        $mail = new \Mailer();
        $mail->addAddress($reservas['nombre'], $reservas['email']);
        $mail->setContent($contenidoTitle, $contenidoBody);
        $mail->sendMail();

        $mailAdmin = new \Mailer();
        $adminMail = \App\TsConfigs::find('correo_admin')->toArray();
        $mailAdmin->addAddress('Admin Confirmacion Quest Room', $adminMail['value']);
        $mailAdmin->setContent('[Admin] Confirmacion de reserva en Quest Room', $contenidoBody);
        $mailAdmin->sendMail();
    }

    private function calcNumero($reserva) {
        return count(TsRelFacturacion::groupBy('fechaPago')->where('fechaPago', '<', $reserva->fechaPago)->get()->toArray()) + 1;
    }

    public function PayPalOkURL() {
        $input = Session::all();
        Session::forget('paypalTkn');

        $sanboxed = \App\TsConfigs::find('payments_mode_sandboxed')->toArray();
        /* if($sanboxed['value'] == 'true') {
          $paypal = new \Paypal("https://api.sandbox.paypal.com/v1/payments/payment/".$_GET['paymentId']."/execute/",NULL);
          }else{ */
        $paypal = new \Paypal("https://api.paypal.com/v1/payments/payment/" . $_GET['paymentId'] . "/execute/", NULL);
        //}

        $status = $paypal->pay($_GET['PayerID'], $input['paypalTkn']);

        if ($status) {
            //Ok -> confirma and send mail
            $reserva = TsReservas::find($input['paypalId']);
            $reserva->fechaPago = Carbon::now()->toDateTimeString();
            $reserva->estado = 1;
            $reserva->codigoPago = 'Ok';
            $reserva->save();

            if (TsRelFacturacion::where('idReserva', $input['paypalId'])->count() == 0) {
                $fact = new TsRelFacturacion();
                $fact->idBono = null;
                $fact->idReserva = $input['paypalId'];
                if ((bool) $reserva->factura) {
                    $fact->fechaPago = Carbon::now()->toDateTimeString();
                    $fact->factura = 1;
                } else {
                    $fact->fechaPago = Carbon::now()->toDateString();
                }
                $fact->factura = $reserva->factura;
                $fact->save();
            }

            self::sendMailConfirmado($input['paypalId']);

            return Redirect::away(getConfig('payment_ok'));    //Error wordpress
        } else {
            //Error -> guarda error
            $reserva = TsReservas::find($input['paypalId']);
            $reserva->estado = 0;
            $reserva->codigoPago = 'Error en el pago paypal';
            $reserva->save();

            return Redirect::away(getConfig('payment_ko'));    //Error wordpress
        }
    }

    /**
     * Funciones de ayuda
     */
    //private function generaCalendario($dia, $mes, $ano){
    private function generaCalendario($weekStart) {
        $session = Session::all();
        $semana = array();
        $baseWeek = clone $weekStart;
        //$weekStart = Carbon::createFromDate($ano, $mes, $dia)->startOfWeek();
        //$weekStart = Carbon::createFromDate($ano, $mes, $dia)->subDay();

        for ($i = 0; $i < 7; ++$i, $weekStart->addDays(1)) {
            //Ahora que sabemos el dia de la semana, añadimos los eventos
            $semana[] = array('fecha' => $weekStart->toDateString(), 'datos' => self::addEventos($weekStart->toDateString(), $baseWeek->toDateString(), $session['sala']));
        }


        $semana = self::setMargins($semana);
        $semana = self::clearEventos($semana);
        return $semana;
    }

    private function addEventos($date, $iniDate, $sala) {
        $session = Session::all();
        $dayData = array();
        $fecha = Carbon::createFromFormat("Y-m-d", $date);

        $earlierBeginning = $date . " " . TsCalendario::where('festivo', 0)->where('sala', $sala)->orderBy('inicio', 'ASC')->first()->toArray()['inicio'];
        $lastestEnding = $date . " " . TsCalendario::where('festivo', 0)->where('sala', $sala)->orderBy('fin', 'DESC')->first()->toArray()['fin'];

        $worst = TsCalendario::where('festivo', 0)->orderBy('duracion', 'DESC')->first()->toArray()['duracion'];
        $best = TsCalendario::where('festivo', 0)->orderBy('duracion', 'ASC')->first()->toArray()['duracion'];

        $durationBetween = ($worst - $best == 0) ? $worst : $worst - $best;

        while ($durationBetween % 30 != 0) {
            ++$durationBetween;
        }

        $numerbOfDivisions = (int) Carbon::parse($lastestEnding)->diffInMinutes(Carbon::parse($earlierBeginning)) / $durationBetween;
        $earlierBeginning = Carbon::parse($earlierBeginning);

        //Analizamos el dia en tramos de media hora
        $sesion = 0;
        $tiempoAcomulado = 0;
        for ($i = 0; $i <= $numerbOfDivisions; ++$i) {
            $iniHour = clone $earlierBeginning;
            $iniHour->addMinutes($durationBetween * $i);
            $endHour = clone $iniHour;
            $endHour->addMinutes($durationBetween);

            $data = array();
            $data['ini'] = $iniHour->toDateTimeString();
            $data['end'] = $endHour->toDateTimeString();
            $data['primary'] = false;
            $data['sesion'] = -1;
            $data['rowspan'] = 1;
            $data['reserva'] = false;
            $data['actual'] = false;

            if (TsReservas::where('estado', '<>', -1)->where('sala', '=', $sala)->where('fin', '>', $iniHour->toDateTimeString())->where('inicio', '<', $endHour->toDateTimeString())->count() > 0) {
                $data['reserva'] = true;
            }


            //Para cada media hora miramos si es festivo
            if (TsVacaciones::where('fin', '>', $iniHour->toDateTimeString())->where('inicio', '<', $endHour->toDateTimeString())->count() > 0) {
                $data['vacaciones'] = true;
                $tiempoAcomulado = 0;

                //Sino, miramos
            } else {
                $cal = TsCalendario::where('inicio', '<=', $iniHour->toTimeString())
                        ->where('fin', '>=', $endHour->toTimeString())
                        ->where('idDia', ($iniHour->dayOfWeek == 0) ? 7 : $iniHour->dayOfWeek)
                        ->where('festivo', 0);

                //No se ha seteado calendario para ese dia

                if ($cal->count() == 0) {
                    $data['vacaciones'] = true;
                    $tiempoAcomulado = 0;

                    //Hay calendario, vamos a trabajar
                } else {
                    $cal = $cal->get()->toArray()[0];
                    $a = Carbon::parse($date . " " . $cal['fin'])->subMinutes($cal['duracion']);
                    $b = Carbon::parse($iniHour->toDateTimeString());
                    //Si vamos a empezar la actvidad y no hay tiempo para finalizarla, establecer como vacaciones
                    if (!$b->lte($a) && $tiempoAcomulado == 0) {
                        $data['vacaciones'] = true;

                        //Si todo va bien y podemos jugar
                    } else {
                        $data['vacaciones'] = false;
                        $data['id'] = $cal['id'];

                        if ($tiempoAcomulado <= 0) {
                            $data['primary'] = true;
                            ++$sesion;
                            $tiempoAcomulado = $cal['duracion'];
                        }

                        $data['sesion'] = $sesion;
                        $tiempoAcomulado -= $durationBetween;

                        if (isset($session['input']) && $session['input']['txtId'] == $cal['id'] && $session['input']['txtFechaInicio'] == $iniHour) {
                            /* var_dump($cal);
                              echo $session['input']['txtId'] ."<br>";
                              echo $iniHour."<br>";
                              die(); */
                            $data['actual'] = true;
                        }
                    }
                }
            }
            //Asignaciones
            $dayData[] = $data;
        }
        //->where('sala','=', $sala)
        /* echo "------------AQUI:" . $sala;
          echo "------------AQUI:" . $date."<br>";
          echo "------------AQUI:" . $iniDate."<br>";
          var_dump($dayData);
          die(); */
        return $dayData;
    }

    //Borrar (para el primer dia) las horas de margen al principio
    private function setMargins($semana) {

        $maxHour = Carbon::parse($semana[0]['fecha'] . ' ' . getConfig('hora_maxima')); //EJ -> 01:00
        $horaMinimaReserva = Carbon::parse($semana[0]['fecha'] . ' ' . getConfig('hora_minima'));  //EJ -> 11:00
        $delayHora = getConfig('delay_hora_minima');  //EJ -> 3 horas // Antes estaba puesto $delay - 1
        $lastSelected = -1;
        foreach ($semana[0]['datos'] as $key => $primerDia) {
            $fechaInicio = Carbon::parse($primerDia['ini']);

            //Si estamos en las horas de excusion miramos que hacer
            if (Carbon::now()->between($maxHour, $horaMinimaReserva) && $fechaInicio->lte($horaMinimaReserva)) {
                $lastSelected = $primerDia['sesion'];
            }

            //Tambien miramos que no se pueden reservar a 3 horas delay_hora_minima
            if (Carbon::now()->addHours($delayHora)->gt($fechaInicio)) {
                $lastSelected = $primerDia['sesion'];
            }

            //Las horas ya pasadas las borramos
            if ($fechaInicio->lte(Carbon::now())) {
                $lastSelected = $primerDia['sesion'];
            }

            if ($primerDia['sesion'] == $lastSelected && $lastSelected != -1 && !$primerDia['reserva']) {
                $semana[0]['datos'][$key]['primary'] = false;
                $semana[0]['datos'][$key]['sesion'] = -1;
                $semana[0]['datos'][$key]['rowspan'] = 1;
                $semana[0]['datos'][$key]['reserva'] = false;
                $semana[0]['datos'][$key]['actual'] = false;
                $semana[0]['datos'][$key]['vacaciones'] = true;
            } else {
                $lastSelected = -1;
            }
        }

        return $semana;
    }

    //Mirar si una hora a lo largo de la semana esta vacio y se borra esa franja horaria
    private function clearEventos($semana) {
        $semanaNew = array();
        $arrayDeleteFranjasHorarias = array();
        for ($franjaHoraria = 0; $franjaHoraria < count($semana[0]['datos']); ++$franjaHoraria) {
            $delete = true;
            for ($dia = 0; $dia < 7; ++$dia) {
                if ($semana[$dia]['datos'][$franjaHoraria]['primary']) {
                    $delete = false;
                }
            }
            if ($delete) {
                $arrayDeleteFranjasHorarias[] = $franjaHoraria;
            }
        }

        for ($dia = 0; $dia < 7; ++$dia) {
            $semanaNew[$dia]['fecha'] = $semana[$dia]['fecha'];
            $semanaNew[$dia]['datos'] = array();
            for ($franjaHoraria = 0; $franjaHoraria < count($semana[0]['datos']); ++$franjaHoraria) {
                if (!in_array($franjaHoraria, $arrayDeleteFranjasHorarias)) {
                    $semanaNew[$dia]['datos'][] = $semana[$dia]['datos'][$franjaHoraria];
                }
            }
        }

        for ($dia = 0; $dia < 7; ++$dia) {
            $lastSesion = -1;
            $lastfranja = -1;
            foreach ($semanaNew[$dia]['datos'] as $franja => $datos) {
                if ($datos['sesion'] != -1 && $datos['primary']) {
                    $lastfranja = $franja;
                    $lastSesion = $datos['sesion'];
                } elseif ($datos['sesion'] != -1 && !$datos['primary'] && $lastSesion == $datos['sesion']) {
                    $semanaNew[$dia]['datos'][$lastfranja]['rowspan'] += 1;
                }
            }
        }

        return $semanaNew;
    }

}
