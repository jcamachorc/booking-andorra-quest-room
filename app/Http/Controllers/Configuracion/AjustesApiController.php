<?php namespace App\Http\Controllers\Configuracion;

use App\AdmMailings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use yajra\Datatables\Datatables;

class AjustesApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	public function __construct()
	{
        $input = Input::all();
        if(isset($input['access_token'])) {
            $this->beforeFilter('oauth');
        }
        $this->middleware('auth');
	}

    public function listarMail()
    {
        $mailings = AdmMailings::select(['id', 'host', 'username', 'encrypt', 'port']);

        return Datatables::of($mailings)
            ->addColumn('accion', function ($mailing) {
                $dt = '<a class="btn btn-xs btn-primary editar" data-id="'.$mailing->id.'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                return $dt;
            })
            ->removeColumn('id')
            ->make(true);
    }

    /**
     * @api {put} /api/Configuracion/ajustes/mail/editar  Editar un mail
     * @apiName  mail/editar
     * @apiGroup ajustes
     *
     * @apiParam {String}  [txtNombre]              Nombre del mail
     * @apiParam {String}  [txtHost]                Nombre del host
     * @apiParam {String}  [txtUsername]            Username
     * @apiParam {String}  [txtPassword]            Password
     * @apiParam {String}  [txtEncriptacion]        Encriptacion
     * @apiParam {Number}  [txtPuerto]              Puerto
     *
     */
    public function editarMail(){
        $input = Input::all();
        $mailing                = AdmMailings::find(1);
        $mailing->name          = $input['txtNombre'];
        $mailing->host          = $input['txtHost'];
        $mailing->username      = $input['txtUsername'];
        if(!empty($input['txtPassword']))
            $mailing->password      = $input['txtPassword'];
        $mailing->encrypt       = $input['txtEncriptacion'];
        $mailing->port          = $input['txtPuerto'];

        if($mailing->name == '')
            return Response::json(array('message' => 'Error, el nombre no puede estar vacio', 'name' => 'txtNombre'), 400);

        if($mailing->host == '')
            return Response::json(array('message' => 'Error, el host no puede estar vacio', 'name' => 'txtHost'), 400);

        if($mailing->username == '')
            return Response::json(array('message' => 'Error, el nombre de usuario no puede estar vacio', 'name' => 'txtUsername'), 400);

        if($mailing->password == '')
            return Response::json(array('message' => 'Error, el password no puede estar vacio', 'name' => 'txtPassword'), 400);

        if($mailing->encrypt == '')
            return Response::json(array('message' => 'Error, debes seleccionar una encriptacion', 'name' => 'txtEncriptacion'), 400);

        if($mailing->port == '')
            return Response::json(array('message' => 'Error, el puerto  no puede estar vacio', 'name' => 'txtPuerto'), 400);


        $mailing->save();
    }

}
