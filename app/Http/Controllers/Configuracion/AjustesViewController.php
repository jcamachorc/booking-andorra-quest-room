<?php namespace App\Http\Controllers\Configuracion;

use App\AdmMailings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class AjustesViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function index() //Listar
	{
		return view('Configuracion.Ajustes.listar');
	}

    public function editarMail()
    {
        $MailingData = AdmMailings::find(1)->toArray();

        return view('Configuracion.Ajustes.editarMail', array('MailingData' => $MailingData ));
    }
}
