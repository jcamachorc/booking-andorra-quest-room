<?php namespace App\Http\Controllers\Configuracion;


use App\Http\Controllers\Controller;
use App\SysMailsContenido;
use App\TsCalendario;
use App\TsCjtPrecios;
use App\TsConfigs;
use App\TsFacturacion;
use App\TsGrupoPrecios;
use App\TsPrecios;
use App\TsRelGrupoPrecios;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use League\Flysystem\Exception;

class BookingsViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function index()
	{
		return view('Configuracion.Bookings.listar');
	}

    public function addDay($id){
        $idPrecio = TsPrecios::all()->first()->toArray()['id'];
        $new = new TsCalendario();
        $new->idDia = $id;
        $new->idPrecio = $idPrecio;
        $new->save();
        return Redirect::to('configuracion/bookings');
    }

    public function addGrupo(){
        $new = new TsGrupoPrecios();
        $new->nombre = 'Nuevo grupo';
        $new->save();
        return Redirect::to('configuracion/bookings');
    }

    public function delDay($id){
        TsCalendario::find($id)->delete();
        return Redirect::to('configuracion/bookings');
    }

    public function delGrupo($id){
        try {
            TsGrupoPrecios::find($id)->delete();
            return Redirect::to('configuracion/bookings');
        }catch(QueryException $e){
            return Redirect::to('configuracion/bookings')->withErrors(['No puedes borrar un conjunto de precios si este esta vinculado a un dia de la semana.']);
        }
    }

    public function editarDay($id){
        $data = TsCalendario::find($id)->toArray();
        $cjtPrecios = TsGrupoPrecios::all();
        $precios = array();
        foreach ($cjtPrecios as $item) {
            $precios[$item['id']] = $item['nombre'];
        }


        return view('Configuracion.Bookings.editarDias', array('data' => $data, 'precios' => $precios));
    }

    public function editarPrecios($id){
        $dataIds = TsRelGrupoPrecios::where('idGrupo', $id)->get()->toArray();
        $ids = array();
        foreach ($dataIds as $items) {
            $ids[] = $items['idPrecio'];
        }

        $data = TsPrecios::whereIn('id', $ids)->get()->toArray();
        $nombregrupo = TsGrupoPrecios::find($id)->toArray();
        return view('Configuracion.Bookings.editarPrecios', array('id' => $id, 'data' => $data, 'grupo' => $nombregrupo));
    }

    public function editarPreciosPost(){
        $input = Input::all();
        if(count($input['txtPrecio']) == 0)
            return Response::json(array('message' => 'Debes poner un precio', 'name' => 'txtPrecio'), 400);

        $grupo = TsGrupoPrecios::find($input['txtId']);
        $grupo->nombre = $input['txtNombreGrupo'];
        $grupo->inicio = $input['txtIni'];
        $grupo->fin = $input['txtFin'];
        $grupo->save();

        $dataIds = TsRelGrupoPrecios::where('idGrupo', $input['txtId'])->get()->toArray();
        foreach ($dataIds as $items) {
            TsPrecios::find($items['idPrecio'])->delete();
        }

        foreach ($input['txtPrecio'] as $key => $precio) {
            $newPrecio = new TsPrecios();
            $newPrecio->precio = $precio;
            $newPrecio->personas = $input['txtPersonas'][$key];
            $newPrecio->save();

            $relGrupoPrecio = new TsRelGrupoPrecios();
            $relGrupoPrecio->idGrupo = $input['txtId'];
            $relGrupoPrecio->idPrecio = $newPrecio->id;
            $relGrupoPrecio->save();
        }

        return Redirect::to('configuracion/bookings');
    }

    public function editarDayPost(){
        $input = Input::all();

        $ini = Carbon::parse(date('Y-m-d').' '.$input['txtInicio']);
        $fin = Carbon::parse(date('Y-m-d').' '.$input['txtFin']);

        if($fin->lte($ini))
            return Response::json(array('message' => 'No puedes terminar antes de empezar', 'name' => 'txtFin'), 400);

        if($input['txtDuracion'] == 0)
            return Response::json(array('message' => 'No puede durar 0 minutos!!', 'name' => 'txtDuracion'), 400);

        $new = TsCalendario::find($input['txtId']);
        $new->inicio = $input['txtInicio'];
        $new->fin = $input['txtFin'];
        $new->idPrecio = $input['txtPrecio'];
        $new->duracion = $input['txtDuracion'];
        $new->festivo = isset($input['txtFestivo']);
        $new->save();
    }

    public function configEditar($id, $bool = false){
        $data = TsConfigs::find($id)->toArray();
        if($data['encriptado']){
            $data['value'] = Crypt::decrypt($data['value']);
        }
        return view('Configuracion.Bookings.editarConfig', array('data' => $data, 'bool' => $bool));
    }

    public function configEditarPost(){
        $input = Input::all();

        $data = TsConfigs::find($input['txtItem']);
        if($data->encriptado) {
            $data->value = Crypt::encrypt($input['txtValue']);
        }else{
            $data->value = $input['txtValue'];
        }
        $data->save();

        return Redirect::to('configuracion/bookings');
    }

    public function editarFact(){
        $data = TsFacturacion::find(1)->toArray();
        return view('Configuracion.Bookings.editarFact', array('data' => $data));
    }

    public function editarFactPost(){
        $input = Input::all();
        $data = TsFacturacion::find(1);

        $data->cif = $input['txtCIF'];
        $data->iva = $input['txtIVA'];
        $data->compania = $input['txtCompania'];
        $data->direccion = $input['txtDireccion'];
        $data->ciudad = $input['txtCiudad'];
        $data->estado = $input['txtEstado'];
        $data->cp = $input['txtCP'];
        $data->telefono = $input['txtTelefono'];
        $data->fax = $input['txtFAX'];
        $data->email = $input['txtEmail'];
        $data->web = $input['txtWeb'];


        if (Input::file('userImage') != null && Input::file('userImage')->isValid()) {
            $destinationPath = 'images/'; // upload path
            $extension = Input::file('userImage')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            Input::file('userImage')->move($destinationPath, $fileName); // uploading file to given path
            $data->logo = $fileName;
        }

        $data->save();
    }
}
