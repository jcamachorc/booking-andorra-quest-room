<?php namespace App\Http\Controllers\Configuracion;

use App\AdmPermisosOverride;
use App\AdmPermisosRoles;
use App\AdmRoles;
use App\Http\Controllers\Controller;

use App\SysMailingMails;
use App\SysMailsContenido;
use App\TsMailing;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Symfony\Component\Routing\Matcher\ApacheUrlMatcher;
use yajra\Datatables\Datatables;

class MailsApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	public function __construct()
	{
        $input = Input::all();
        if(isset($input['access_token'])) {
            $this->beforeFilter('oauth');
        }
        $this->middleware('auth');

	}

    public function listarts()
    {
        $users = TsMailing::select(['nombre', 'contenido']);
        return Datatables::of($users)
            ->addColumn('accion', function ($user) {
                return '<a class="btn btn-xs btn-primary editar" data-id="'.$user->nombre.'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
            })
            ->make(true);
    }

	public function listar()
	{
        $users = SysMailsContenido::select(['key', 'titulo', 'contenido']);
        return Datatables::of($users)
            ->addColumn('accion', function ($user) {
               return '<a class="btn btn-xs btn-primary editar" data-id="'.$user->key.'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
            })
            ->make(true);
	}

    public function editar()
    {
        $input = Input::all();
        $data = SysMailsContenido::find($input['key']);
        $data->titulo = $input['titulo'];
        $data->contenido = $input['contenido'];
        $data->save();
    }

    public function editarts()
    {
        $input = Input::all();

        $data = TsMailing::find($input['nombre']);
        $data->titulo_contenido = $input['titulo_contenido'];
        $data->contenido = $input['contenido'];
        $data->titulo_ingles = $input['titulo_ingles'];
        $data->ingles = $input['ingles'];
        $data->titulo_catalan = $input['titulo_catalan'];
        $data->catalan = $input['catalan'];
        $data->titulo_frances = $input['titulo_frances'];
        $data->frances = $input['frances'];
        $data->save();
    }

    public function add()
    {
        $input = Input::all();
        $dt = new SysMailsContenido();
        $dt->key = $input['key'];
        $dt->titulo = $input['titulo'];
        $dt->contenido = $input['contenido'];
        $dt->save();
    }

}
