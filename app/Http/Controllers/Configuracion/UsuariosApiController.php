<?php namespace App\Http\Controllers\Configuracion;

use App\AdmPermisosOverride;
use App\AdmPermisosRoles;
use App\AdmRoles;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Symfony\Component\Routing\Matcher\ApacheUrlMatcher;
use yajra\Datatables\Datatables;

class UsuariosApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $input = Input::all();
        if(isset($input['access_token'])) {
            $this->beforeFilter('oauth');
        }
        $this->middleware('auth');

	}


	public function listar()
	{
        $users = User::select(['id', 'name', 'email', 'created_at']);
        return Datatables::of($users)
            ->addColumn('accion', function ($user) {
                $dt = '<a class="btn btn-xs btn-primary editar" data-id="'.$user->id.'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                $dt .= ' <a class="btn btn-xs btn-danger borrar" data-id="'.$user->id.'"><i class="glyphicon glyphicon-trash"></i> Borrar</a>';
                return $dt;
            })
            ->editColumn('email', function($user){
                return '<a href="mailto:'.$user->email.'">'.$user->email.'<a/>';
            })
            ->removeColumn('id')
            ->make(true);
	}

    public function listarRoles()
    {
        $users = AdmRoles::select(['id', 'nombre']);
        return Datatables::of($users)
            ->addColumn('accion', function ($user) {
                $dt = '<a class="btn btn-xs btn-primary editarRol" data-id="'.$user->id.'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                $dt .= ' <a class="btn btn-xs btn-danger borrarRol" data-id="'.$user->id.'"><i class="glyphicon glyphicon-trash"></i> Borrar</a>';
                return $dt;
            })
            ->removeColumn('id')
            ->make(true);
    }

    private function delUserImage($idUser){
        $user = User::find($idUser);
        if(!empty($user->imagen) && File::isFile('uploads/usuarios/'.$user->imagen))
            File::delete('uploads/usuarios/'.$user->imagen);
    }


    /**
     * @api {delete} /api/Configuracion/usuarios/del   Elimina un usuario
     * @apiName  del
     * @apiGroup usuarios
     *
     * @apiParam {Number}  [idUser]              Id del usuario
     *
     */
    public function delUser(){
        $input = Input::all();

        if(!is_numeric($input['idUser']))
            return Response::json(array('message' => 'Error, no se ha pasado el id del usuario correctamente', 'name' => 'idUser'), 400);

        if(!User::existsById($input['idUser']))
            return Response::json(array('message' => 'Error, el usuario no existe', 'name' => 'idUser'), 400);

        if($input['idUser'] == Auth::id())
            return Response::json(array('message' => 'Error, no te puedes borrar a ti mismo', 'name' => 'idUser'), 400);

        //self::delUserImage($input['idUser']);
        $user = User::find($input['idUser']);
        $user->delete();
    }

    /**
     * @api {delete} /api/Configuracion/usuarios/rol/del   Elimina un rol
     * @apiName  rol/del
     * @apiGroup usuarios
     *
     * @apiParam {Number}  [idRol]              Id del rol
     * @apiParam {Number}  [txtAlt]             Id del rol de substitucion
     *
     */
    public function delRol(){
        $input = Input::all();

        if(!is_numeric($input['idRol']))
            return Response::json(array('message' => 'Error, no se ha pasado el id del rol correctamente', 'name' => 'idRol'), 400);

        if(!is_numeric($input['txtAlt']))
            return Response::json(array('message' => 'Error, no se ha seleccionado un rol alternativo', 'name' => 'txtAlt'), 400);

        $usuarios = User::where('idRol', $input['idRol'])->get();

        foreach($usuarios as $user){
            $user->idRol = $input['txtAlt'];
            $user->save();
        }

        $rolToDelete = AdmRoles::find($input['idRol']);
        $rolToDelete->delete();
    }

    /**
     * @api {post} /api/Configuracion/usuarios/rol/add   Añade un rol
     * @apiName  rol/add
     * @apiGroup usuarios
     *
     * @apiParam {String}  [txtNombre]          Nombre del rol
     * @apiParam {Array}   [permisos]           Permisos asignados
     *
     */
    public function addRol(){
        $input = Input::all();

        if(AdmRoles::existsByNombre($input['txtNombre']))
            return Response::json(array('message' => 'Error, el nombre del rol ya existe', 'name' => 'txtNombre'), 400);

        $admRol = new AdmRoles();
        $admRol->nombre = $input['txtNombre'];
        $admRol->save();

        $idAdmRol = $admRol->id;


        foreach($input['permisos'] as $key => $status){
            $admPermisoRol = new AdmPermisosRoles();
            $admPermisoRol->idRol = $idAdmRol;
            $admPermisoRol->permiso = $key ;
            $admPermisoRol->save();
        }

    }

    /**
     * @api {put} /api/Configuracion/usuarios/rol/editar   Edita un rol
     * @apiName  rol/editar
     * @apiGroup usuarios
     *
     * @apiParam {Number}  [idRol]              Id del rol
     * @apiParam {String}  [txtNombre]          Nombre del rol
     * @apiParam {Array}   [permisos]           Permisos asignados
     *
     */
    public function editarRol(){
        $input = Input::all();


        $rolAdmin =  AdmRoles::where('id', $input['idRol'])->first();
        $rolAdmin->nombre = $input['txtNombre'];
        $rolAdmin->save();

        AdmPermisosRoles::where('idRol', $input['idRol'])->delete();
        foreach($input['permisos'] as $key => $status){
            $admPermisoRol = new AdmPermisosRoles();
            $admPermisoRol->idRol = $input['idRol'];
            $admPermisoRol->permiso = $key ;
            $admPermisoRol->save();
        }

    }

    /**
     * @api {post} /api/Configuracion/usuarios/add   Añade un usuario
     * @apiName  add
     * @apiGroup usuarios
     *
     * @apiParam {String}   [txtNombre]              Nombre del usuario
     * @apiParam {String}   [txtCorreo]              Email del usuario
     * @apiParam {String}   [txtPassword]            Password del usuario
     * @apiParam {String}   [txtIMEI]                IMEI del usuario
     * @apiParam {Number}   [txtExtension]           Extension del telefono de la empresa global
     * @apiParam {Number}   [txtRol]                 Id del rol del usuario
     * @apiParam {File}     [userImage]              Imagen de usuario
     * @apiParam {Number}   [txtFranquicia]          Id de la franquicia asignado
     * @apiParam {Array}    [txtFranquicias]         Franquicias de las que se es jefe (Id's)
     * @apiParam {Array}    [permisos]               Permisos que se quitan (Id's)
     *
     *
     */
    public function addUser(){
        $input = Input::all();

        if(User::existsByMail($input['txtCorreo']))
            return Response::json(array('message' => 'Error, el correo ya existe', 'name' => 'txtCorreo'), 400);


        $user = new User();
        $user->name = $input['txtNombre'];
        $user->email = $input['txtCorreo'];
        $user->password = Hash::make($input['txtPassword']);

        $user->idRol = $input['txtRol'];

        $rolPermiso = AdmPermisosRoles::where('idRol', $input['txtRol'])->select('permiso')->get()->toArray();

        if($user->name == '')
            return Response::json(array('message' => 'Error, el nombre no puede estar vacio', 'name' => 'txtNombre'), 400);

        if($user->email == '')
            return Response::json(array('message' => 'Error, el correo no puede estar vacio', 'name' => 'txtCorreo'), 400);

        if($user->password  == '')
            return Response::json(array('message' => 'Error, el password no puede estar vacio', 'name' => 'txtPassword'), 400);


        if (Input::file('userImage') != null && Input::file('userImage')->isValid()) {
            $destinationPath = 'uploads/usuarios'; // upload path
            $extension = Input::file('userImage')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            Input::file('userImage')->move($destinationPath, $fileName); // uploading file to given path
            $user->imagen = $fileName;
        }else{
            $user->imagen = '';
        }

        $user->save();

        $idUser = $user->id;


        foreach($rolPermiso as $item){
            if(!isset($input['permisos'][$item['permiso']])){
                $admPermisoOverride = new AdmPermisosOverride();
                $admPermisoOverride->idUser = $idUser;
                $admPermisoOverride->permiso = $item['permiso'];
                $admPermisoOverride->save();
            }
        }
    }


    /**
     * @api {put} /api/Configuracion/usuarios/editar   Edita un rol
     * @apiName  editar
     * @apiGroup usuarios
     *
     * @apiParam {Number}   [txtId]                  Id usuario
     * @apiParam {String}   [txtNombre]              Nombre del usuario
     * @apiParam {String}   [txtCorreo]              Email del usuario
     * @apiParam {String}   [txtPassword]            Password del usuario
     * @apiParam {String}   [txtIMEI]                IMEI del usuario
     * @apiParam {Number}   [txtExtension]           Extension del telefono de la empresa global
     * @apiParam {Number}   [txtRol]                 Id del rol del usuario
     * @apiParam {File}     [userImage]              Imagen de usuario
     * @apiParam {Number}   [txtFranquicia]          Id de la franquicia asignado
     * @apiParam {Array}    [txtFranquicias]         Franquicias de las que se es jefe (Id's)
     * @apiParam {Array}    [permisos]               Permisos que se quitan (Id's)
     *
     *
     */
    public function editarUser(){
        $input = Input::all();

        if(Input::file('userImage') != null)
            self::delUserImage($input['txtId']);

        $user = User::find($input['txtId']);
        $user->name = $input['txtNombre'];


        if($user->email != $input['txtCorreo'] && User::existsByMail($input['txtCorreo']))
            return Response::json(array('message' => 'Error, el correo ya existe', 'name' => 'txtCorreo'), 400);

        $user->email = $input['txtCorreo'];

        if(!empty($input['txtPassword']))
            $user->password = Hash::make($input['txtPassword']);

        $user->idRol = $input['txtRol'];

        $rolPermiso = AdmPermisosRoles::where('idRol', $input['txtRol'])->select('permiso')->get()->toArray();


        if($user->name == '')
            return Response::json(array('message' => 'Error, el nombre no puede estar vacio', 'name' => 'txtNombre'), 400);

        if($user->email == '')
            return Response::json(array('message' => 'Error, el correo no puede estar vacio', 'name' => 'txtCorreo'), 400);

        if($user->password  == '')
            return Response::json(array('message' => 'Error, el password no puede estar vacio', 'name' => 'txtPassword'), 400);

        if (Input::file('userImage') != null && Input::file('userImage')->isValid()) {
            $destinationPath = 'uploads/usuarios'; // upload path
            $extension = Input::file('userImage')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renameing image
            Input::file('userImage')->move($destinationPath, $fileName); // uploading file to given path
            $user->imagen = $fileName;
        }

        $user->save();

        $idUser = $user->id;

        AdmPermisosOverride::where('idUser', $idUser)->delete();
        foreach($rolPermiso as $item){
            if(!isset($input['permisos'][$item['permiso']])){
                $admPermisoOverride = new AdmPermisosOverride();
                $admPermisoOverride->idUser = $idUser;
                $admPermisoOverride->permiso = $item['permiso'];
                $admPermisoOverride->save();
            }
        }
    }

}
