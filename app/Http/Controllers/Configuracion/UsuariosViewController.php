<?php namespace App\Http\Controllers\Configuracion;

use App\AdmMenu;
use App\AdmPermisos;
use App\AdmPermisosOverride;
use App\AdmPermisosRoles;
use App\AdmRoles;
use App\AdmSubMenu;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use yajra\Datatables\Datatables;

class UsuariosViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function index()
	{
		return view('Configuracion.Usuarios.listar');
	}

    public function nuevo()
    {
        $input = Input::all();

        $roles = AdmRoles::all()->toArray();
        $rolArray = array();
        $rolesPermisos = array();
        foreach($roles as $rol){
            $rolArray[$rol['id']] = $rol['nombre'];
            $rolPermiso = AdmPermisosRoles::where('idRol', $rol['id'])->select('permiso')->get()->toArray();
            $arrayPermisos = array();
            foreach($rolPermiso as $permiso){
                $arrayPermisos[] = $permiso['permiso'];
            }
            $rolesPermisos[$rol['id']] = $arrayPermisos;
        }

        return view('Configuracion.Usuarios.nuevo', array("rolesArray" => $rolArray, "rolesPermisos" => $rolesPermisos, "permisosMenu" => self::getPermisosMenu(), "actualId" => $input['actualID']));
    }

    public function nuevoRol()
    {
        $input = Input::all();

        return view('Configuracion.Usuarios.nuevoRol', array("permisosMenu" => self::getPermisosMenu(), "actualId" => $input['actualID']));
    }

    public function editarRol($id)
    {
        $input = Input::all();
        $rol = AdmRoles::where('id', $id)->get()->toArray()[0];
        $permisosSeleccionados = AdmPermisosRoles::where('idRol', $id)->select('permiso')->get()->toArray();
        $listadoPermisos = array();
        foreach($permisosSeleccionados as $item){
            $listadoPermisos[] = $item['permiso'];
        }
        return view('Configuracion.Usuarios.editarRol', array("rolData" => $rol, "permisosSeleccionados" => $listadoPermisos, "permisosMenu" => self::getPermisosMenu(), "actualId" => $input['actualID']));
    }


    public function editar($id)
    {
        $input = Input::all();

        $user = User::find($id)->toArray();

        $permisosUser = AdmPermisosRoles::where('idRol', $user['idRol'])->select('permiso')->get()->toArray();
        $permisosUserOverride = AdmPermisosOverride::where('idUser', $id)->select('permiso')->get()->toArray();

        $permisosDefinitivos = array();
        foreach($permisosUser as $item){
            $find = false;
            for($k = 0; $k < count($permisosUserOverride) && !$find; ++$k){
                if($permisosUserOverride[$k]['permiso'] == $item['permiso']) $find = true;
            }
            if(!$find)
                $permisosDefinitivos[] = $item['permiso'];
        }

        $roles = AdmRoles::all()->toArray();
        $rolArray = array();
        $rolesPermisos = array();
        foreach($roles as $rol){
            $rolArray[$rol['id']] = $rol['nombre'];
            $rolPermiso = AdmPermisosRoles::where('idRol', $rol['id'])->select('permiso')->get()->toArray();
            $arrayPermisos = array();
            foreach($rolPermiso as $permiso){
                $arrayPermisos[] = $permiso['permiso'];
            }
            $rolesPermisos[$rol['id']] = $arrayPermisos;
        }

        return view('Configuracion.Usuarios.editar', array("usuario" => $user,
                                                            "rolesArray" => $rolArray,
                                                            "rolesPermisos" => $rolesPermisos,
                                                            "permisosMenu" => self::getPermisosMenu(),
                                                            "permisosDef" => $permisosDefinitivos,
                                                            "actualId" => $input['actualID']));
    }

    public function borrar($id)
    {
        $usrs = User::all()->toArray();
        $users = array();
        foreach ($usrs as $user) {
            if($user['id'] != $id)
                $users[$user['id']] = $user['name'];
        }
        return view('Configuracion.Usuarios.borrar', array("idusuario" => $id, "users" => $users));
    }

    public function borrarRol($id)
    {
        $input = Input::all();
        $roles = AdmRoles::where('id', '<>', $id)->orderBy('id', 'DESC')->get()->toArray();
        $listadoRoles = array();
        foreach($roles as $item){
            $listadoRoles[$item['id']] = $item['nombre'];
        }
        return view('Configuracion.Usuarios.borrarRol', array("idRol" => $id, 'listadoRoles' => $listadoRoles, "actualId" => $input['actualID']));
    }

    private function getPermisosMenu(){
        $menu = AdmMenu::all()->toArray();
        $permisosMenu = array();
        foreach($menu as $item){
            $submenu = AdmSubMenu::where('idMenu',$item['id'])->get()->toArray();
            $permisos = array();
            foreach($submenu as $subitem){
                $permisos[self::sanear_string($subitem['nombre'])] = $subitem['permiso'];
            }

            $permisosMenu[self::sanear_string($item['nombre'])] = $permisos;
        }
        return $permisosMenu;
    }

    private function sanear_string($string)
    {

        $string = trim($string);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "-", "~",
                "#", "@", "|", "!", "\"",
                "·", "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "`", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":",
                ".", " "),
            '',
            $string
        );


        return $string;
    }
}
