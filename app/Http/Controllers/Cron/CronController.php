<?php namespace App\Http\Controllers\Cron;

use App\AdmMailings;
use App\Http\Controllers\Controller;
use App\SysMailing;
use App\SysMailingImagenes;
use App\SysMailingMails;
use App\SysMailsContenido;
use App\TsBonos;
use App\TsDescuentos;
use App\TsFacturacion;
use App\TsMailing;
use App\TsRelFacturacion;
use App\TsReservas;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class CronController extends Controller {

	public function __construct(){}

    public function cron(){
        $json_response = array();
        $json_response[] = array("Envio de mensajes" => self::sendMailing());
        $json_response[] = array("Correccion de reservas" => self::clearCalendario());
        $json_response[] = array("Envio de mails de recordatorio" => self::recordatorioCalendario());
        $json_response[] = array("Envio de facturas a las reservas requeridas" => self::enviarFacturas());

        return Response::json($json_response, 200);
    }

    private function enviarFacturas(){
        $data = TsFacturacion::find(1)->toArray();
        $reservas = TsReservas::where('estado',1)->where('factura', true)->get()->toArray();
        foreach ($reservas as $reserva) {
            //Lets send the factura
            $reserva = TsReservas::find($reserva['id'])->toArray();
            $idRel = TsRelFacturacion::where('idReserva', $reserva['id'])->get()->toArray();
            $numF = self::calcNumero(TsRelFacturacion::find($idRel[0]['id']));
            $mpdf = new \mPDF();
            $html = View::make('facturar.genFacturaPersonal', array('data' => $data, 'reserva' => $reserva, 'numeroF' => $numF))->render();
            $mpdf->WriteHTML($html);
            $dttpdf = $mpdf->Output('', 'S');

            $mail = new \Mailer();
            $mail->addAddress($reserva['nombre'], $reserva['email']);
            $mail->setContent('Factura reserva Quest Room', 'Se le ha adjuntado la factura');
            $mail->addStringAttach($dttpdf, 'Factura.pdf');
            $mail->sendMail();

            $rs = TsReservas::find($reserva['id']);
            $rs->factura = false;
            $rs->save();
        }

        $bonos = TsBonos::where('fechaPago','>', Carbon::parse('0001-01-01 00:00:01'))->where('factura', true)->where('metodoPago','<>','BNN')->get()->toArray();
        foreach ($bonos as $reserva) {
            //Lets send the factura
            $reserva = TsBonos::find($reserva['id'])->toArray();
            $idRel = TsRelFacturacion::where('idBono', $reserva['id'])->get()->toArray();
            $numF = self::calcNumero(TsRelFacturacion::find($idRel[0]['id']));
            $mpdf = new \mPDF();
            $html = View::make('facturar.genFacturaPersonal', array('data' => $data, 'reserva' => $reserva, 'numeroF' => $numF))->render();
            $mpdf->WriteHTML($html);
            $dttpdf = $mpdf->Output('', 'S');

            $mail = new \Mailer();
            $mail->addAddress($reserva['nombre'], $reserva['email']);
            $mail->setContent('Factura bono Quest Room', 'Se le ha adjuntado la factura');
            $mail->addStringAttach($dttpdf, 'Factura.pdf');
            $mail->sendMail();

            $rs = TsBonos::find($reserva['id']);
            $rs->factura = false;
            $rs->save();
        }

        $allBonos = TsRelFacturacion::whereNotNull('idBono')->get()->toArray();
        foreach ($allBonos as $item) {
            $bonos2 = TsBonos::where('id', $item['idBono'])->where('fechaPago','>', Carbon::parse('0001-01-01 00:00:01'))->where('factura', true)->where('metodoPago','BNN')->get()->toArray();
            foreach ($bonos2 as $reserva) {
                //Lets send the factura
                $reserva = TsBonos::find($reserva['id'])->toArray();
                $idRel = TsRelFacturacion::where('idBono', $reserva['id'])->get()->toArray();
                $numF = self::calcNumero(TsRelFacturacion::find($idRel[0]['id']));
                $mpdf = new \mPDF();
                $html = View::make('facturar.genFacturaPersonal', array('data' => $data, 'reserva' => $reserva, 'numeroF' => $numF))->render();
                $mpdf->WriteHTML($html);
                $dttpdf = $mpdf->Output('', 'S');

                $mail = new \Mailer();
                $mail->addAddress($reserva['nombreDestinatario'], $reserva['emailDestinatario']);
                $mail->setContent('Factura bono Quest Room', 'Se le ha adjuntado la factura');
                $mail->addStringAttach($dttpdf, 'Factura.pdf');
                $mail->sendMail();

                $rs = TsBonos::find($reserva['id']);
                $rs->factura = false;
                $rs->save();
            }
        }

        return array('message' => 'Todo correcto => '.(count($reservas) + count($bonos)).' facturas enviados');
    }

    private function calcNumero($reserva){
        return count(TsRelFacturacion::groupBy('fechaPago')->where('fechaPago','<',$reserva->fechaPago)->get()->toArray())+1;
    }

    private function recordatorioCalendario(){
        $reservas = TsReservas::where('estado',1)->where('nPersonas', '>', 0)->where('inicio', '<', Carbon::now()->addDays(1)->toDateTimeString())->where('recordatorio', false)->get()->toArray();
        foreach ($reservas as $reserva) {
            try {
                self::sendMailRecordatorio($reserva['id']);
            } catch (phpmailerException $e) {

            } catch (Exception $e) {

            }
            $res = TsReservas::find($reserva['id']);
            $res->recordatorio = true;
            $res->save();
        }
        return array('message' => 'Todo correcto => '.count($reservas).' recordatorios enviados');
    }

    private function sendMailRecordatorio($idReserva){
        $reservas = TsReservas::find($idReserva)->toArray();
        $contenido = TsMailing::where('nombre', 'recordatorio')->first()->toArray();
        switch(strtolower($reservas['idioma'])){
        case 'english':
                $titulo = $contenido['titulo_ingles'];
                $contenido = $contenido['ingles'];
                break;
            case 'catala':
                $titulo = $contenido['titulo_catalan'];
                $contenido = $contenido['catalan'];
                break;
            case 'frances':
                $titulo = $contenido['titulo_frances'];
                $contenido = $contenido['frances'];
                break;
            default:
                $titulo = $contenido['titulo_contenido'];
                $contenido = $contenido['contenido'];
                break;
        }

        $dataBody = array('{Name}', '{Timeslots}');
        $replaceBody = array($reservas['nombre'], $reservas['inicio']);
        $contenidoTitle = $titulo;
        $contenidoBody =  str_replace($dataBody, $replaceBody, $contenido);


        $mail = new \Mailer();
        $mail->addAddress($reservas['nombre'], $reservas['email']);
        $mail->setContent($contenidoTitle, $contenidoBody);
        $mail->sendMail();
    }

    private function clearCalendario(){
        $reservas = TsReservas::where('estado',0)->where('created_at','<',Carbon::now()->subMinutes(15)->toDateTimeString())->get()->toArray();
        foreach($reservas as $reserva){
            $reserva = TsReservas::find($reserva['id']);
            $reserva->estado = -1;
            $reserva->save();


            foreach (TsDescuentos::where('idReserva', $reserva['id'])->get()->toArray() as $descuento) {
                $dsc = TsDescuentos::find($descuento['id']);
                $dsc->idReserva = null;
                $dsc->save();
            }

            foreach (TsBonos::where('idReserva', $reserva['id'])->get()->toArray() as $descuento) {
                $dsc = TsBonos::find($descuento['id']);
                $dsc->idReserva = null;
                if($dsc->metodoPago == 'BNN'){
                    $dsc->nombreDestinatario = "";
                    $dsc->emailDestinatario = "";
                    $dsc->DNI = "";
                    $dsc->telefono = "";
                    $dsc->nombre = "";
                    $dsc->email = "";
                }
                $dsc->save();
            }
        }

        return array('message' => 'Todo correcto => '.count($reservas).' reservas corregidas');
    }

    private function sendMailing(){
        $sendMails = SysMailing::where('envio', '0')->where('revisado', '1')->where('created_at', '<=', Carbon::now()->subDays(1))->get()->toArray();
        foreach ($sendMails as $mails) {
            $mailContenido = SysMailsContenido::find($mails['KeyPlantilla'])->toArray();
            $data = SysMailingMails::where('idMailing', $mails['id'])->get()->toArray();
            $imagenes = SysMailingImagenes::where('idMailing', $mails['id'])->get()->toArray();

            $mail = new \Mailer();
            foreach ($imagenes as $key => $img) {
                $mail->addAttach(public_path('/uploads/imagenes/'.$img['nombre']), $img['nombre']);
            }
            foreach ($data as $destinatarios) {
                $mail->AddBCC($destinatarios['nombre'], $destinatarios['mail']);
            }

            $mail->setContent($mailContenido['titulo'], $mailContenido['contenido']);

            if(!$mail->sendMail()){
                echo Response::json(array('message' => 'Error conectando y enviado', 'name' => 'txtIdNewsletter'), 400);
            }

            $modSend = SysMailing::find($mails['id']);
            $modSend->envio = true;
            $modSend->save();
        }

        return array('message' => 'Todo correcto => '.count($sendMails).' envios');
    }
}
