<?php namespace App\Http\Controllers\Facturar;

use App\Http\Controllers\Controller;
use App\TsBonos;
use App\TsCalendario;
use App\TsFacturacion;
use App\TsRelFacturacion;
use App\TsReservas;
use App\TsVacaciones;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Traits\Macroable;
use League\Flysystem\Exception;
use Symfony\Component\HttpKernel\DataCollector\RequestDataCollector;
use yajra\Datatables\Datatables;

class ViewController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function facturar(){

        return view('facturar.listar');
    }

    public function addFactura(){
        return view('facturar.addFactura', array());
    }

    public function addFacturaNueva(){
        $input = Input::all();

        if($input['txtPrecio'] <= 0){
            return Response::json(array('message' => 'Error, el precio debe ser > 0', 'name' => 'txtPrecio'), 400);
        }

        $data = new TsReservas();
        $data->estado = 1;
        $data->nombre = $input['txtNombre'];
        $data->duracion = 0;
        $data->precio = $input['txtPrecio'];
        $data->email = $input['txtEmail'];
        $data->dni = $input['txtDNI'];
        $data->direccion = (empty($input['txtDireccion'])) ? null : $input['txtDireccion'];
        $data->fechaPago = Carbon::now()->toDateTimeString();
        $data->telefono = $input['txtTelefono'];
        $data->descuento = $input['txtDescuento'];
        $data->save();

        $facturala = new TsRelFacturacion();
        $facturala->idReserva = $data->id;
        $facturala->idBono = null;
        $facturala->fechaPago = Carbon::now()->toDateString();
        $facturala->save();
    }

    public function generaFacturaP($idReserva, $rs){
        $data = TsFacturacion::find(1)->toArray();
        if($rs == 1) {
            $reserva = TsReservas::find($idReserva)->toArray();
            $idRel = TsRelFacturacion::where('idReserva', $idReserva)->get()->toArray();
        }else{
            $reserva = TsBonos::find($idReserva)->toArray();
            $idRel = TsRelFacturacion::where('idBono', $idReserva)->get()->toArray();
        }
        $numF = self::calcNumero(TsRelFacturacion::find($idRel[0]['id']));
        $mpdf = new \mPDF();
        $html = View::make('facturar.genFacturaPersonal', array('data' => $data, 'reserva' => $reserva, 'numeroF' => $numF))->render();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function generaFactura($fecha)
    {
        $data = TsFacturacion::find(1)->toArray();

        self::generarTemporal();

        $relFacturas = TsRelFacturacion::where('fechaPago', $fecha)->get()->toArray();
        $factura['precio'] = 0;
        $factura['fechaPago'] = $fecha;
        $facturas = array();
        if(!($relFacturas[0]['idBono'] == null && $relFacturas[0]['idReserva'] == null)) {
            foreach ($relFacturas as $item) {
                if ($item['idBono'] == null) {
                    $dataTmp = TsReservas::find($item['idReserva'])->toArray();
                    $dataTmp['tipo'] = 'Reserva';
                    $dataTmp['idReserva'] = $item['idReserva'];
                    $dataTmp['rectificativa'] = $item['rectificativa'];
                    $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));

                    $factura['precio'] += $dataTmp['precio'];
                    $facturas[] = $dataTmp;
                } else {
                    $dataTmp = TsBonos::find($item['idBono'])->toArray();
                    $dataTmp['tipo'] = 'Bono';
                    $dataTmp['idBono'] = $item['idBono'];
                    $dataTmp['rectificativa'] = $item['rectificativa'];
                    $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));

                    $factura['precio'] += $dataTmp['precio'];
                    $facturas[] = $dataTmp;
                }
            }
        }

        $relFacturasR = TsRelFacturacion::where('fechaRectificativa', $fecha)->where('rectificativa', 1)->get()->toArray();
        foreach ($relFacturasR as $item) {
            if ($item['idBono'] == null) {
                $dataTmp = TsReservas::find($item['idReserva'])->toArray();
                $dataTmp['tipo'] = 'Reserva';
                $dataTmp['idReserva'] = $item['idReserva'];
                $dataTmp['rectificativa'] = $item['rectificativa'];
                $dataTmp['precio'] = (float)  $dataTmp['precio'] - ($dataTmp['precio']*($dataTmp['descuento']/100));

                $dataTmp['precio'] = -$dataTmp['precio'];
                $factura['precio'] += $dataTmp['precio'];
                $facturas[] = $dataTmp;
            } else {
                $dataTmp = TsBonos::find($item['idBono'])->toArray();
                $dataTmp['tipo'] = 'Bono';
                $dataTmp['idBono'] = $item['idBono'];
                $dataTmp['rectificativa'] = $item['rectificativa'];
                $dataTmp['precio'] = (float)  $dataTmp['precio'] - ($dataTmp['precio']*($dataTmp['descuento']/100));

                $dataTmp['precio'] = -$dataTmp['precio'];
                $factura['precio'] += $dataTmp['precio'];
                $facturas[] = $dataTmp;
            }
        }

        $numF = self::calcNumero(TsRelFacturacion::where('fechaPago', $fecha)->first());
        if(count($facturas) < 10) {
            $mpdf = new \mPDF();
            $html = View::make('facturar.genFactura', array('data' => $data, 'facturas' => $facturas, 'factura' => $factura, 'numeroF' => $numF))->render();
            //exit;
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            $mpdf = new \mPDF();
            $mpdf->SetDisplayMode('fullpage');
            $super = array_chunk($facturas, 15);
            $anterior = 0;
            foreach ($super as $i => $items) {
                if($i) $mpdf->AddPage();
                $html = View::make('facturar.genFacturaM', array('data' => $data, 'facturas' => $items, 'factura' => $factura, 'numeroF' => $numF, 'pagina' => $i, 'paginas' => count($super), 'anterior' => $anterior))->render();
                $mpdf->WriteHTML($html);
                foreach ($items as $precioFact) {
                    $anterior += $precioFact['precio'];
                }
            }
            $mpdf->Output();
        }
        TsRelFacturacion::where('idReserva', NULL)->where('idBono', NULL)->delete();
    }

    public function generaAllFacturas(){
        $input = Input::all();

        $carbInicio = Carbon::parse($input['txtFechaInicio'])->toDateTimeString();
        $carbFinal = Carbon::parse($input['txtFechaFinal'])->toDateTimeString();

        $reservas = TsRelFacturacion::whereBetween('fechaPago', [$carbInicio, $carbFinal])->groupBy('fechaPago')->get()->toArray();
        $reservasR = TsRelFacturacion::WhereBetween('fechaRectificativa', [$carbInicio, $carbFinal])->groupBy('fechaRectificativa')->get()->toArray();
        $rfinal = array();
        foreach ($reservasR as $itemR) {
            $find = false;
            foreach ($reservas as $item) {
                $fecha = Carbon::parse($itemR['fechaRectificativa']);
                $fecha->hour = 0;
                $fecha->minute = 0;
                $fecha->second = 0;
                if($item['fechaPago'] == $fecha->toDateTimeString()){
                    $find = true;
                }
            }
            if(!$find){
                $tsrel = new TsRelFacturacion();
                $tsrel->idReserva = NULL;
                $tsrel->idBono = NULL;
                $tsrel->fechaPago = $itemR['fechaRectificativa'];
                $tsrel->save();
            }
        }

        $reservas = TsRelFacturacion::whereBetween('fechaPago', [$carbInicio, $carbFinal])->groupBy('fechaPago')->get()->toArray();


        $mpdf = new \mPDF();
        $mpdf->SetDisplayMode('fullpage');

        foreach ($reservas as $i => $items) {
            //If not the first payroll then add a new page
            if($i) $mpdf->AddPage();
            if($items['factura']){
                if($items['idReserva'] == null){
                    $idSender = $items['idBono'];
                }else{
                    $idSender = $items['idReserva'];
                }
                $mpdf->WriteHTML(self::generarFacturaMultiP($idSender));
            }else {
                $mpdf->WriteHTML(self::generaFacturaMulti($items['fechaPago']));
            }
        }
        TsRelFacturacion::where('idReserva', NULL)->where('idBono', NULL)->delete();
        $mpdf->Output();
    }

    public function generarFacturaMultiP($idReserva){
        $data = TsFacturacion::find(1)->toArray();
        $reserva = TsReservas::find($idReserva);
        if($reserva == null) {
            $reserva = TsBonos::find($idReserva)->toArray();
            $idRel = TsRelFacturacion::where('idBono', $idReserva)->get()->toArray();
        }else {
            $reserva = $reserva->toArray();
            $idRel = TsRelFacturacion::where('idReserva', $idReserva)->get()->toArray();
        }

        $numF = self::calcNumero(TsRelFacturacion::find($idRel[0]['id']));
        return View::make('facturar.genFacturaPersonal', array('data' => $data, 'reserva' => $reserva, 'numeroF' => $numF))->render();
    }

    public function generaFacturaMulti($fecha){
        $data = TsFacturacion::find(1)->toArray();

        $factura['precio'] = 0;
        $factura['fechaPago'] = $fecha;
        $facturas = array();

        $relFacturas = TsRelFacturacion::where('fechaPago', $fecha)->get()->toArray();
        if(!($relFacturas[0]['idBono'] == null && $relFacturas[0]['idReserva'] == null)){
            foreach ($relFacturas as $item) {
                if ($item['idBono'] == null) {
                    $dataTmp = TsReservas::find($item['idReserva'])->toArray();
                    $dataTmp['tipo'] = 'Reserva';
                    $dataTmp['idReserva'] = $item['idReserva'];
                    $dataTmp['rectificativa'] = $item['rectificativa'];
                    $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));

                    $factura['precio'] += $dataTmp['precio'];
                    $facturas[] = $dataTmp;
                } else {
                    $dataTmp = TsBonos::find($item['idBono'])->toArray();
                    $dataTmp['tipo'] = 'Bono';
                    $dataTmp['idBono'] = $item['idBono'];
                    $dataTmp['rectificativa'] = $item['rectificativa'];
                    $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));

                    $factura['precio'] += $dataTmp['precio'];
                    $facturas[] = $dataTmp;
                }
            }
        }

        $relFacturasR = TsRelFacturacion::where('fechaRectificativa', $fecha)->where('rectificativa', 1)->get()->toArray();
        foreach ($relFacturasR as $item) {
            if ($item['idBono'] == null) {
                $dataTmp = TsReservas::find($item['idReserva'])->toArray();
                $dataTmp['tipo'] = 'Reserva';
                $dataTmp['idReserva'] = $item['idReserva'];
                $dataTmp['rectificativa'] = $item['rectificativa'];
                $dataTmp['precio'] = (float)  $dataTmp['precio'] - ($dataTmp['precio']*($dataTmp['descuento']/100));

                $dataTmp['precio'] = -$dataTmp['precio'];
                $factura['precio'] += $dataTmp['precio'];
                $facturas[] = $dataTmp;
            } else {
                $dataTmp = TsBonos::find($item['idBono'])->toArray();
                $dataTmp['tipo'] = 'Bono';
                $dataTmp['idBono'] = $item['idBono'];
                $dataTmp['rectificativa'] = $item['rectificativa'];
                $dataTmp['precio'] = (float)  $dataTmp['precio'] - ($dataTmp['precio']*($dataTmp['descuento']/100));

                $dataTmp['precio'] = -$dataTmp['precio'];
                $factura['precio'] += $dataTmp['precio'];
                $facturas[] = $dataTmp;
            }
        }

        $numF = self::calcNumero(TsRelFacturacion::where('fechaPago', $fecha)->first());

        return View::make('facturar.genFactura', array('data' => $data, 'facturas' => $facturas, 'factura' => $factura, 'numeroF' => $numF))->render();
    }

    private function calcNumero($reserva){
        if($reserva->fechaPago == null){
            if($reserva->idBono == null){
                $data = TsReservas::find($reserva->idReserva)->toArray();
            }else{
                $data = TsBonos::find($reserva->idBono)->toArray();
            }
            if($data['fechaPago'] != null && $data['fechaPago'] != '0000-00-00 00:00:00'){
                $dt = TsRelFacturacion::find($reserva->id);
                if($reserva->factura) {
                    $dt->fechaPago = Carbon::parse($data['fechaPago'])->toDateTimeString();
                }else{
                    $dt->fechaPago = Carbon::parse($data['fechaPago'])->toDateString();
                }
                $dt->save();
                $reserva->fechaPago = $dt->fechaPago;
            }else{
                return "Error en la fecha de pago";
            }
        }

        return (count(TsRelFacturacion::groupBy('fechaPago')->where('fechaPago','<',$reserva->fechaPago)->get()->toArray())+1);
    }

    public function editarFacturaB($id){
        $data = TsReservas::find($id)->toArray();
        return view('facturar.editarFacturaB', array('input' => $data, 'id' => $id));
    }

    public function saveFacturaB($id){
        $input = Input::all();
        $reserva = TsReservas::find($id);

        $reserva->precio = $input['txtPrecio'];
        $reserva->email = $input['txtEmail'];
        if(!empty($input['txtDNI']))
            $reserva->DNI = $input['txtDNI'];
        else
            $reserva->CIF = $input['txtCIF'];
        $reserva->nombre = $input['txtNombre'];
        $reserva->telefono = $input['txtTelefono'];
        $reserva->direccion = (empty($input['txtDireccion']))?null:$input['txtDireccion'];
        $reserva->descuento = $input['txtDescuento'];
        $reserva->save();

        return Redirect::to('facturar')->with('message', 'Cambios realizados correctamente');
    }

    public function editarFacturaBono($id){
        $data = TsBonos::find($id)->toArray();
        return view('facturar.editarFacturaBono', array('input' => $data, 'id' => $id));
    }

    public function saveFacturaBono($id){
        $input = Input::all();
        $bono = TsBonos::find($id);
        $bono->email = $input['txtEmail'];
        $bono->DNI = $input['txtDNI'];
        $bono->nombre = $input['txtNombre'];
        $bono->telefono = $input['txtTelefono'];
        $bono->precio = $input['txtPrecio'];
        $bono->save();

        return Redirect::to('facturar')->with('message', 'Cambios realizados correctamente');
    }

    public function generaFacturaRectificativa($id){
        $fr = TsRelFacturacion::find($id);
        $fr->rectificativa = true;
        $fr->fechaRectificativa = Carbon::now()->toDateString();
        $fr->save();

        /*if($fr->idReserva != null){
            $fechaBase = Carbon::parse('0001-01-01 00:00:01')->toDateTimeString();
            $reserva = TsReservas::find($fr->idReserva);
            $reserva->inicio = $fechaBase;
            $reserva->fin = $fechaBase;
            $reserva->recordatorio = true;
            $reserva->save();
        }*/

        return Redirect::to('/facturar')->with('message', 'Cambios realizados correctamente');
    }

    public function listarFacturas()
    {
        //Generamos factuas temporales para que salgan las rectificativas en dias que no hay facturas normales
        self::generarTemporal();

        $mailings = TsRelFacturacion::select(['id', 'idBono','idReserva', 'rectificativa','fechaPago','factura'])->groupBy('fechaPago');
        $dataTables = Datatables::of($mailings)
            ->addColumn('numF', function($reserva){
                return self::calcNumero($reserva);
            })
            ->addColumn('nombre', function($reserva){
                //Items facturados
                $fecha = $reserva->fechaPago;
                $dt = '';
                $items = TsRelFacturacion::where('fechaPago', $fecha)->get()->toArray();
                if(!($items[0]['idBono'] == null && $items[0]['idReserva'] == null)){
                    foreach ($items as $item) {
                        $precio = 0;
                        if ($item['idBono'] == null)
                            $data = TsReservas::find($item['idReserva'])->toArray();
                        else
                            $data = TsBonos::find($item['idBono'])->toArray();

                        if(!empty($data['nombreF']))
                            $dt .= $data['nombreF'];
                        else
                            $dt .= $data['nombre'];
                        if ($item['idBono'] != null) {
                            $precio = TsBonos::find($item['idBono'])->toArray()['precio'];
                            $dt .= ' <a class="btn btn-xs btn-primary" href="' . url('/facturar/bonos/editar/' . $item['idBono']) . '"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                        } else {
                            $precio = TsReservas::find($item['idReserva'])->toArray()['precio'];
                            $dt .= ' <a class="btn btn-xs btn-primary" href="' . url('/facturar/bookings/editar/' . $item['idReserva']) . '"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                        }
                        if ($precio > 0) {
                            if ((bool)$item['rectificativa']) {
                                $dt .= ' <a class="btn btn-xs btn-danger">Rectificativa generada</a>';
                            } else {
                                $dt .= ' <a class="btn btn-xs btn-warning" href="' . url('/facturar/generaRectificativa/' . $item['id']) . '"><i class="glyphicon glyphicon-share"></i> Genera rectificativa</a>';
                            }
                        }
                        $dt .= '<br>';
                    }
                }

                //if(Carbon::parse($fecha)->hour == 0 && Carbon::parse($fecha)->minute == 0 && Carbon::parse($fecha)->second == 0) {
                    $items = TsRelFacturacion::where('fechaRectificativa', $fecha)->where('rectificativa', 1)->get()->toArray();
                    foreach ($items as $item){
                        if ($item['idBono'] == null)
                            $data = TsReservas::find($item['idReserva'])->toArray();
                        else
                            $data = TsBonos::find($item['idBono'])->toArray();
                        $dt .= $data['nombre'];
                        $dt .= ' <a class="btn btn-xs btn-danger">Factura rectificativa del ' . Carbon::parse($item['fechaPago'])->toDateString() . '</a>';
                        $dt .= '<br>';
                    }
                //}

                return $dt;
            })
            ->addColumn('precio', function($reserva){
                $fecha = $reserva->fechaPago;
                $precio = 0;
                $items = TsRelFacturacion::where('fechaPago', $fecha)->get()->toArray();
                if(!($items[0]['idBono'] == null && $items[0]['idReserva'] == null)) {
                    foreach ($items as $item) {
                        if ($item['idBono'] != null) {
                            $dataTmp = TsBonos::find($item['idBono'])->toArray();
                            $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));
                            $precio += $dataTmp['precio'];
                        } else {
                            $dataTmp = TsReservas::find($item['idReserva'])->toArray();
                            $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));
                            $precio += $dataTmp['precio'];
                        }
                    }
                }

                //$items = TsRelFacturacion::whereBetween('fechaRectificativa', [$fecha, Carbon::parse($fecha)->addDay()])->where('rectificativa', 1)->get()->toArray();
                $items = TsRelFacturacion::where('fechaRectificativa', $fecha)->where('rectificativa', 1)->get()->toArray();
                foreach ($items as $item) {
                    if ($item['idBono'] != null) {
                        $dataTmp = TsBonos::find($item['idBono'])->toArray();
                        $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));
                        $precio -= $dataTmp['precio'];
                    } else {
                        $dataTmp = TsReservas::find($item['idReserva'])->toArray();
                        $dataTmp['precio'] = (float)$dataTmp['precio'] - ($dataTmp['precio'] * ($dataTmp['descuento'] / 100));
                        $precio -= $dataTmp['precio'];
                    }
                }

                return $precio."&euro;";
            })
            ->editColumn('id', function($reserva){
                return Carbon::parse($reserva->fechaPago)->toDateString();
            })
            ->addColumn('accion', function ($reserva) {
                if($reserva->idBono == null){
                    $id = $reserva->idReserva;
                    $rs = 1;
                }else{
                    $id = $reserva->idBono;
                    $rs = 0;
                }

                if($reserva->factura)
                    return ' <a class="btn btn-xs btn-success" target="_new" href="' . url('/facturarP/' . $id . '/' . $rs) . '"><i class="glyphicon glyphicon-eye-open"></i> Ver</a>';
                else
                    return ' <a class="btn btn-xs btn-success" target="_new" href="' . url('/facturar/' . $reserva->fechaPago) . '"><i class="glyphicon glyphicon-eye-open"></i> Ver</a>';
            })
            ->removeColumn('rectificativa')
            ->make(true);

        TsRelFacturacion::where('idReserva', NULL)->where('idBono', NULL)->delete();

        return $dataTables;
    }

    private function generarTemporal(){
        TsRelFacturacion::where('idReserva', NULL)->where('idBono', NULL)->delete();

        $reservas = TsRelFacturacion::groupBy('fechaPago')->get()->toArray();
        $reservasR = TsRelFacturacion::groupBy('fechaRectificativa')->get()->toArray();
        foreach ($reservasR as $itemR) {
            $find = false;
            foreach ($reservas as $item) {
                $fecha = Carbon::parse($itemR['fechaRectificativa']);
                $fecha->hour = 0;
                $fecha->minute = 0;
                $fecha->second = 0;
                if($item['fechaPago'] == $fecha->toDateTimeString()){
                    $find = true;
                }
            }
            if(!$find && $itemR['fechaRectificativa'] != null){
                $tsrel = new TsRelFacturacion();
                $tsrel->idReserva = NULL;
                $tsrel->idBono = NULL;
                $tsrel->fechaPago = $itemR['fechaRectificativa'];
                $tsrel->save();
            }
        }
    }
}