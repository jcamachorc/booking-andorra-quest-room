<?php namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\SysMailing;
use App\User;
use Bugsnag\BugsnagLaravel\BugsnagFacade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use yajra\Datatables\Datatables;

class ViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function listar()
	{
		return view('home.listar', array());
	}

    public function getListMailing()
    {

        $idRol = User::find(Auth::id())->toArray();
        if($idRol['idRol'] == 1) {
            $mailings = SysMailing::select(['id', 'revisado','envio', 'created_at']);
        }else{
            $mailings = SysMailing::select(['id', 'revisado','envio', 'created_at'])->where('idUsuario', Auth::id());
        }

        return Datatables::of($mailings)
            ->addColumn('accion', function ($mailing) {

                if($mailing->revisado)
                    $dt = '<a class="btn btn-xs btn-warning" href="'.url('api/home/validar/'.$mailing->id.'/0').'"><i class="glyphicon glyphicon-remove"></i> Parar envío</a>';
                else {
                    $dt = '<a class="btn btn-xs btn-success" href="' . url('api/home/validar/' . $mailing->id . '/1') . '"><i class="glyphicon glyphicon-ok"></i> Validar</a>';
                }

                if($mailing->envio){
                    $dt = ' <p class="btn btn-xs btn-danger">Enviado</p>';
                }
                $dt .= ' <a class="btn btn-xs btn-warning" href="'.url('mailing/editar/'.$mailing->id).'"><i class="glyphicon glyphicon-edit"></i> Editar</a>';
                return $dt;
            })
            ->addColumn('grupo', function ($mailing) {
                $idGrupo = SysMailing::where('id', '<', $mailing->id)->count();
                return ++$idGrupo;
            })
            ->editColumn('envio', function ($mailing) {
                if($mailing->envio)
                    return "Si";
                else
                    return "No";
            })
            ->editColumn('revisado', function ($mailing) {
                if($mailing->revisado)
                    return "Si";
                else
                    return "No";
            })
            ->make(true);
    }

    public function validar($id, $status){
        $envio = SysMailing::find($id);
        $envio->revisado = ($status == 1);
        $envio->save();

        return Redirect::back()->with('success', 'Validado y preaprado para su envio');
    }
}
