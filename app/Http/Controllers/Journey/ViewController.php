<?php namespace App\Http\Controllers\Journey;

use App\Http\Controllers\Controller;
use App\SysJourney;
use App\SysJourneyComentarios;
use App\SysMailing;
use App\SysMailingMails;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

    public function listar()
    {
        $modo = 0;
        $comentario = '';
        $revision = false;
        $correo = false;
        $redes = false;
        $comentarios = array();
        $id = -1;
        //
        $id_user = Auth::id();
        //$jornada = SysJourney::all();
        $jornada = SysJourney::where('idUsuario', '=', $id_user)->orderBy('inicio', 'DESC')->get();
        //$name = Auth::email()->get();      
        $name_user = User::find($id_user)->toArray()['email'];
        //
        $com_jornada = SysJourneyComentarios::all();

        $jd = SysJourney::where('idUsuario', Auth::id())->where('inicio','>=', Carbon::now()->startOfDay())->count();
        if($jd == 1){
            $modo = 1;
            $data = SysJourney::where('idUsuario', Auth::id())->where('inicio','>=', Carbon::now()->startOfDay())->get()->toArray();
            $revision = $data[0]['revision'];
            $correo = $data[0]['correo'];
            $redes = $data[0]['redes'];
            $id = $data[0]['id'];
            $comentarios = SysJourneyComentarios::where('created_at','>=', Carbon::now()->subDays(2))->orderBy('created_at', 'DESC')->get()->toArray();
            if($data[0]['final'] != 0){
                $modo = 2;
            }
        }
        return view('journey.listar', array('id' => $id, 'modo' => $modo, 'revision' => $revision, 'comentarios' => $comentarios, 'correo' => $correo, 'redes' => $redes, 'libros' => $jornada, 'name_user' => $name_user, 'com_jornada' => $com_jornada));
    }

    public function start()
    {
        $jr = new SysJourney();
        $jr->idUsuario = Auth::id();
        $jr->inicio = Carbon::now();
        $jr->save();

        return redirect('journey');
    }

    public function finalizar($id)
    {
        $jr = SysJourney::find($id);
        $jr->final = Carbon::now();
        $jr->save();

        return redirect('journey');
    }

    public function continuar($id)
    {
        $jr = SysJourney::find($id);
        $jr->final = 0;
        $jr->save();

        return redirect('journey');
    }

    public function update($id)
    {
        $input = Input::all();
        $jr = SysJourney::find($id);
        $jr->correo = isset($input['correos']);
        $jr->revision = isset($input['revision']);
        $jr->redes = isset($input['redes']);
        $jr->save();

        return redirect('journey');
    }

    public function addComentario($id){
        $input = Input::all();
        if(empty($input['comentario'])) return redirect('journey');
        $coment = new SysJourneyComentarios();
        $coment->idJourney = $id;
        $coment->comentario = $input['comentario'];
        $coment->type = ($input['type'] == 'comentario');
        $coment->save();

        return redirect('journey');
    }

    public function delComentario($id){
        $idJourn = SysJourneyComentarios::find($id)->toArray();
        $idUser = SysJourney::find($idJourn['idJourney'])->toArray();
        if($id < 0 || $idUser['idUsuario'] != Auth::id()) return redirect('journey');
        else {
            SysJourneyComentarios::find($id)->delete();
            return redirect('journey');
        }
    }
}
