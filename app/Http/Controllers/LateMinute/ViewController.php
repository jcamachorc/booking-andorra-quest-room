<?php namespace App\Http\Controllers\LateMinute;

use App\AdmMailings;
use App\Http\Controllers\Controller;
use App\SysCajaChica;
use App\SysJourney;
use App\SysJourneyComentarios;
use App\SysLateMinute;
use App\SysMailing;
use App\SysMailingMails;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class ViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth', ['except' => ['add', 'del']]);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function listar()
	{
        $data = SysLateMinute::orderBy('id', 'DESC')->get()->toArray();
        return view('lateminute.listar', array('datos' => $data));
	}

    public function del($id, $correo){
        if(SysLateMinute::where('id', $id)->where('correo', $correo)->count() > 0){
            SysLateMinute::find($id)->delete();
        }
        return Redirect::back();
    }

    public function add(){
        $input = Input::all();

        $contar = SysLateMinute::where('correo', $input['correo'])->count();
        if($contar == 0) {
            $caja = new SysLateMinute();
            $caja->correo = $input['correo'];
            $caja->save();
        }
        return Redirect::back();
    }

    public function enviar(){
        $input = Input::all();
        $sendMails = SysLateMinute::all();

        $mailServer = AdmMailings::find(1)->toArray();
        try {
            $mail = new \PHPMailer(false);
            //$mail->SMTPDebug  = 1;
            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = "utf-8"; // set charset to utf8
            $mail->SMTPAuth = true;  // use smpt auth
            $mail->SMTPSecure = $mailServer['encrypt']; // or ssl
            $mail->Host = $mailServer['host'];
            $mail->Port = $mailServer['port'];
            $mail->Username = $mailServer['username'];
            $mail->Password = $mailServer['password'];
            $mail->setFrom($mailServer['username'], $mailServer['name']);
            $mail->Subject = 'Last minute Juego enigma!!';

            foreach ($sendMails as $destinatarios) {
                $lopd = '<div style="font-size: 11px; margin-top: 150px">Des-suscribirse de los correos automáticos clique <a href="'.url('/lateminute/del/'.$destinatarios['id'].'/'.$destinatarios['correo']).'">aqui</a></div>';
                $mail->MsgHTML(nl2br($input['mensaje'].$lopd));

                $mail->AddBCC($destinatarios['correo'], 'Late minute user');
            }

            if(!$mail->send()){
                //return redirect('lateminute')->with('error', 'Error conectando y enviado');
            }
        } catch (phpmailerException $e) {
            return redirect('lateminute')->with('error', 'Error interno');
        } catch (Exception $e) {
            return redirect('lateminute')->with('error', 'Error interno');
        }

        return redirect('lateminute')->with('success', 'Mails enviados correctamente');
    }
}
