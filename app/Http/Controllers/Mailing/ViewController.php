<?php namespace App\Http\Controllers\Mailing;

use App\Http\Controllers\Controller;
use App\SysMailing;
use App\SysMailingImagenes;
use App\SysMailingMails;
use App\SysMailsContenido;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class ViewController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function listar()
	{
        $donde = array('recomendacion' => 'recomendacion',
                        'internet' => 'internet',
                        'tripadvisor' => 'tripadvisor',
                        'bono' => 'bono',
                        'otro room' => 'otro room',
                        'regalo' => 'regalo',
                        'bloggers' => 'bloggers');

        $idioma = array('castellano' => 'castellano',
                        'catala' => 'catalan',
                        'frances' => 'frances',
                        'english' => 'ingles');

        $nivel = array('0' => '0',
                        '1 a 2' => '1 a 2',
                        '3 a 5' => '3 a 5',
                        '6 a 11' => '6 a 11',
                        '12 o mas' => '12 o mas');

        $p = SysMailsContenido::all()->toArray();
        $persona = array();
        foreach ($p as $item) {
            $persona[$item['key']] = $item['key'];
        }

        $idGrupo = SysMailing::orderBy('created_at', 'DESC')->first();
        if($idGrupo != null)
            $idGrupo = $idGrupo->id;
        else
            $idGrupo = 0;

        return view('mailing.listar', array('donde' => $donde, 'idioma' => $idioma, 'nivel' => $nivel, 'persona' => $persona, 'MyId' => Auth::id(), 'idGrupo' => ++$idGrupo));
	}

    public function editar($id)
    {
        $donde = array('recomendacion' => 'recomendacion',
            'internet' => 'internet',
            'tripadvisor' => 'tripadvisor',
            'bono' => 'bono',
            'otro room' => 'otro room',
            'regalo' => 'regalo',
            'bloggers' => 'bloggers');

        $idioma = array('castellano' => 'castellano',
                        'catala' => 'catalan',
                        'frances' => 'frances',
                        'english' => 'ingles');

        $nivel = array('0' => '0',
            '1 a 2' => '1 a 2',
            '3 a 5' => '3 a 5',
            '6 a 11' => '6 a 11',
            '12 o mas' => '12 o mas');

        $p = SysMailsContenido::all()->toArray();
        $persona = array();
        foreach ($p as $item) {
            $persona[$item['key']] = $item['key'];
        }

        $idGrupo = SysMailing::orderBy('created_at', 'DESC')->first();
        if($idGrupo != null)
            $idGrupo = $idGrupo->id;
        else
            $idGrupo = 0;

        $dataMailing = SysMailing::find($id)->toArray();
        $dataImg = SysMailingImagenes::where('idMailing', $id)->get()->toArray();
        $dataCorreos = SysMailingMails::where('idMailing', $id)->get()->toArray();

        return view('mailing.editar', array('dataImg' => $dataImg, 'dataCorreos' => $dataCorreos, 'mailing' => $dataMailing, 'id' => $id, 'donde' => $donde, 'idioma' => $idioma, 'nivel' => $nivel, 'persona' => $persona, 'MyId' => Auth::id(), 'idGrupo' => ++$idGrupo));
    }

    public function saveEdit(){
        $input = Input::all();

        SysMailing::find($input['id'])->delete();

        if(empty($input['imagenes'])){
            return redirect('mailing')->withInput($input)->with('error', 'Hay datos vacios');
        }

        $newMailing = new SysMailing();
        $newMailing->id = $input['id'];
        $newMailing->donde = $input['donde'];
        $newMailing->idioma = $input['idioma'];
        $newMailing->nivel = $input['nivel'];
        $newMailing->KeyPlantilla = $input['persona'];
        $newMailing->idUsuario = Auth::id();
        $newMailing->comentario = $input['comentario'];
        $newMailing->envio = false;
        $newMailing->save();

        $cjtImg = explode(',', $input['imagenes']);
        foreach ($cjtImg as $imagen) {
            if(!empty($imagen)) {
                $newImagensMail = new SysMailingImagenes();
                $newImagensMail->idMailing = $newMailing->id;
                $newImagensMail->nombre = $imagen;
                $newImagensMail->save();
            }
        }


        foreach ($input['correo'] as $key => $correo) {
            if(empty($correo)) continue;
            $newMailingMails = new SysMailingMails();
            $newMailingMails->nombre = $input['nombre'][$key];
            $newMailingMails->mail = $correo;
            $newMailingMails->idMailing = $newMailing->id;
            $newMailingMails->save();
        }

        return redirect('home')->with('success', 'Datos introducidos correctamente');
    }

    public function saveMailing()
    {
        $input = Input::all();

        if(empty($input['imagenes'])){
            return redirect('mailing')->withInput($input)->with('error', 'Hay datos vacios');
        }

        /*if(!isset($input['checkbox'])){
            return redirect('mailing')->withInput($input)->with('error', 'Falta aceptar LOPD');
        }*/

        $newMailing = new SysMailing();
        $newMailing->donde = $input['donde'];
        $newMailing->idioma = $input['idioma'];
        $newMailing->nivel = $input['nivel'];
        $newMailing->KeyPlantilla = $input['persona'];
        $newMailing->idUsuario = Auth::id();
        $newMailing->comentario = $input['comentario'];
        $newMailing->envio = false;
        $newMailing->save();

        $cjtImg = explode(',', $input['imagenes']);
        foreach ($cjtImg as $imagen) {
            if(!empty($imagen)) {
                $newImagensMail = new SysMailingImagenes();
                $newImagensMail->idMailing = $newMailing->id;
                $newImagensMail->nombre = $imagen;
                $newImagensMail->save();
            }
        }


        foreach ($input['correo'] as $key => $correo) {
            if(empty($correo)) continue;
            $newMailingMails = new SysMailingMails();
            $newMailingMails->nombre = $input['nombre'][$key];
            $newMailingMails->mail = $correo;
            $newMailingMails->idMailing = $newMailing->id;
            $newMailingMails->save();
        }

        return redirect('mailing')->with('success', 'Datos introducidos correctamente');
    }

    public function upload()
    {
        if (Input::file('file') != null) {
            $destinationPath = 'uploads/imagenes'; // upload path
            //$extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = Input::file('file')->getClientOriginalName(); // renameing image
            $uniqueName = time().$fileName;
            Input::file('file')->move(public_path().'/uploads/imagenes/', $uniqueName); // uploading file to given path
            return $uniqueName;
        }
        return -1;
    }

    public function delete()
    {
        $input = Input::all();
        $name = $input['nombre'];

        SysMailingImagenes::where('nombre', $name)->delete();

        File::delete('uploads/imagenes/'.$name);
        if(File::exists(public_path().'/uploads/imagenes/'.$name)){
            return Response::json(array('message' => 'Error no se ha podido borrar'), 400);
        }else{
            return Response::json(array('message' => 'Todo correcto'), 200);
        }
    }
}
