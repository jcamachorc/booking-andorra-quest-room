<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Form;

class ModalesController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function create()
	{
        $id = Input::get('actualID');
        $titulo = Input::get('titulo');
        $contenido = Input::get('contenido');
        $viewContenido = Input::get('showContenido');

        $btnAccept = Input::get('botonAceptar');
        $txtAccept = Input::get('textoAceptar');

        $btnCancel = Input::get('botonCancelar');
        $txtCancel = Input::get('textoCancelar');

        $reloadTable = Input::get('tableReload');

		return view('partials.modales', array('id_modal' => $id,
                                                'titulo' => $titulo,
                                                'viewContenido' => $viewContenido,
                                                'contenido' => $contenido,
                                                'botonAceptar'=> $btnAccept,
                                                'textoAceptar' => $txtAccept,
                                                'botonCancelar'=> $btnCancel,
                                                'textoCancelar' => $txtCancel,
                                                'tableReload' => $reloadTable));
	}

    public function createAlt(){
        $input = Input::all();
        return view('partials.loaderModalAlt', array('redirect' => $input['from'],
                                                        'titulo' => $input['titulo'],
                                                        'viewContenido' => $input['showContenido'],
                                                        'textoAceptar'=> $input['botonAceptar'],
                                                        'botonAceptar' => $input['textoAceptar'],
                                                        'botonCancelar'=> $input['botonCancelar'],
                                                        'textoCancelar' => $input['textoCancelar']));
    }
}
