<?php namespace App\Http\Middleware;

use App\SysUserRegister;
use Carbon\Carbon;
use Closure;
use App\Http\Controllers\LanguageController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Autorizacion {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(Auth::guest()){
            return $next($request);
        }

        $user = User::find(Auth::id());
        LanguageController::setLang();

        if(!Auth::guest()){
            //Guardamos el login o hacemos update a la bbdd para saber su ultimo acceso
            $data = SysUserRegister::where('idUsuario', Auth::id())->whereBetween('created_at', [Carbon::today()->toDateTimeString(), Carbon::tomorrow()->toDateTimeString()])->count();
            if($data == 0){
                $newRegister = new SysUserRegister();
                $newRegister->idUsuario = Auth::id();
                $newRegister->save();
            }else{
                $data = SysUserRegister::where('idUsuario', Auth::id())->whereBetween('created_at', [Carbon::today()->toDateTimeString(), Carbon::tomorrow()->toDateTimeString()])->first()->toArray();
                $oldRegister = SysUserRegister::find($data['id']);
                $oldRegister->updated_at = Carbon::now();
                $oldRegister->save();
            }
        }

        if(!Auth::guest() && !$user->routeAllowed()){
            abort(403, 'Unauthorized action');
        }else {
            return $next($request);
        }
	}

}
