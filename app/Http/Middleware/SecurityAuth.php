<?php namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class SecurityAuth {

    public function handle($request, Closure $next)
    {
        /*if(!Auth::guest()){
            $id = Auth::id();
            $pcid = $_SERVER['HTTP_USER_AGENT'];
            $ip = Request::getClientIp();


            $data = AdmSecurity::where('idUsuario', $id)->where('idUsuario', $id)->where('ip', $ip)->where('pcid', $pcid)->where('ultima', '>=', Carbon::now()->subMinutes(15))->get()->toArray(); //60*15 => 15 minutos (en segundos)
            if(!isset($data[0]['id'])){
                $nuevaEntrada = new AdmSecurity();
                $nuevaEntrada->idUsuario = Auth::id();
                $nuevaEntrada->iniciado = \Carbon\Carbon::now()->toDateTimeString();
                $nuevaEntrada->ultima = \Carbon\Carbon::now()->toDateTimeString();
                $nuevaEntrada->ip = $ip;
                $nuevaEntrada->pcid = $pcid;

                try{
                    $reader = new Reader(app_path('GeoLite2-City.mmdb'));
                    $record = $reader->city($ip);
                    $nuevaEntrada->location = $record->city->name.','.$record->country->name;
                }catch(AddressNotFoundException $e){
                    $nuevaEntrada->location = '';
                }

                $nuevaEntrada->device = $pcid;
                $nuevaEntrada->save();
            }else{
                $nuevaEntrada = AdmSecurity::find($data[0]['id']);
                $nuevaEntrada->ultima = \Carbon\Carbon::now()->toDateTimeString();
                $nuevaEntrada->save();
            }
        }*/

        return $next($request);

    }

}
