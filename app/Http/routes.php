<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Basics -> For language, login and modal-views
 */
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
Route::any('api/modal', 'ModalesController@create');

/**
 * Cargador de ventanas modales alternativas (para movil, las carga como si no fuesen modales)
 */
Route::any('api/modalalt', 'ModalesController@createAlt');

/**
 * Error pages
 */
Route::get('Error/403', function(){ return view('errors.403'); });

/**
 * Sistema cron
 */
Route::get('/cron', 'Cron\CronController@cron');

/**
 * Modulo facturar
 */
Route::get('/facturar', 'Facturar\ViewController@facturar');
Route::get('/facturar/listar', 'Facturar\ViewController@listarFacturas');
Route::get('/facturar/listarR', 'Facturar\ViewController@listarFacturasR');
Route::post('/facturar/generar', 'Facturar\ViewController@generaAllFacturas');
Route::post('/facturar/generarR', 'Facturar\ViewController@generaAllFacturasR');

Route::get('/facturar/bookings/editar/{id}', 'Facturar\ViewController@editarFacturaB');
Route::get('/facturar/bonos/editar/{id}', 'Facturar\ViewController@editarFacturaBono');

Route::post('/facturar/bookings/editar/{id}', 'Facturar\ViewController@saveFacturaB');
Route::post('/facturar/bonos/editar/{id}', 'Facturar\ViewController@saveFacturaBono');

Route::get('/facturar/addFactura', 'Facturar\ViewController@addFactura');
Route::post('/facturar/addFactura', 'Facturar\ViewController@addFacturaNueva');
Route::get('/facturar/generaRectificativa/{id}', 'Facturar\ViewController@generaFacturaRectificativa');
Route::get('/facturarR/{id}', 'Facturar\ViewController@generaFacturaR');
Route::get('/facturar/{id}', 'Facturar\ViewController@generaFactura');
Route::get('/facturarP/{id}/{rs}', 'Facturar\ViewController@generaFacturaP');

/**
 * Modulo booking (admin)
 */
Route::get('/booking/calendario/{sala?}', 'Booking\ViewController@calendario');
Route::get('/booking/calendario/moveDays/{action}', 'Booking\ViewController@setSessionFechaIni');
Route::get('/booking/calendario/addReserva/{id}/{fecha}', 'Booking\ViewController@addReservaView');
Route::post('/booking/calendario/addReserva', 'Booking\ViewController@addInner');

Route::get('/booking/bookings', 'Booking\BookingsViewController@listar');
Route::get('/booking/bookings/listar', 'Booking\BookingsViewController@listarBookings');
Route::get('/booking/bookings/editar/{id}/{sala?}', 'Booking\BookingsViewController@editar');
Route::post('/booking/bookings/editar/{id}/{sala?}', 'Booking\BookingsViewController@saveEditar');
Route::get('/booking/bookings/cancelarReservaDia/{id}', 'Booking\BookingsViewController@cancelarReservaDia');
Route::get('/booking/bookings/borrarReservaDia/{id}', 'Booking\BookingsViewController@borrarReservaDia');


Route::get('/booking/bonos', 'Booking\BonosViewController@listar');
Route::get('/booking/bonos/listar', 'Booking\BonosViewController@listarBonos');
Route::get('/booking/bonos/editar/{id}', 'Booking\BonosViewController@editar');
Route::post('/booking/bonos/editar/{id}', 'Booking\BonosViewController@saveEditar');

Route::get('/booking/bonos/listarDesc', 'Booking\BonosViewController@listarDescuentos');
Route::get('/booking/bonos/addBono', 'Booking\BonosViewController@addBono');
Route::post('/booking/bonos/addBono', 'Booking\BonosViewController@addBonoPost');

Route::get('/booking/bonos/addBonoNuevo', 'Booking\BonosViewController@addBonoNuevo');
Route::post('/booking/bonos/addBonoNuevo', 'Booking\BonosViewController@addBonoNuevoPost');

Route::get('/booking/bonos/render/{code}', 'Booking\BonosViewController@renderCode');
Route::get('/booking/bonos/sendCode/{id}', 'Booking\BonosViewController@sendCode');


Route::get('/booking/bonos/editarDescuento/{id}', 'Booking\BonosViewController@editarDescuento');
Route::post('/booking/bonos/editarDescuento/{id}', 'Booking\BonosViewController@saveDescuentoEditar');

Route::get('/booking/bookings/sendRemember/{id}', 'Booking\BookingsViewController@rememberMail');

Route::get('/booking/bonos/addBonoMasivo','Booking\BonosViewController@addBonoMasivo');
Route::post('/booking/bonos/addBonoMasivo','Booking\BonosViewController@addBonoMasivoPost');

Route::get('/booking/bonos/del/{name}/{mes}/{ano}', 'Booking\BonosViewController@delMasivo');



/**
 * Modulo calendario
 */
Route::get('/calendario/{sala?}', 'Calendario\ViewController@index')->where(['sala' => '[0-1]+']);
Route::get('/calendario/backReserva/{sala?}', 'Calendario\ViewController@backReserva');
Route::get('/calendario/cancelar/{sala?}', 'Calendario\ViewController@cancelar');
Route::any('/calendario/checkout', 'Calendario\ViewController@checkout');
Route::post('/calendario/pay', 'Calendario\ViewController@pay');
Route::get('/calendario/paypal', 'Calendario\ViewController@PayPalOkURL');
Route::any('/calendario/tpv.php', 'Calendario\ViewController@TPVmerchantURL');
Route::get('/calendario/resetsession/{sala?}', 'Calendario\ViewController@resetSession');
Route::POST('/calendario/checkSession/{sala?}', 'Calendario\ViewController@checkSession');
Route::get('/calendario/moveDays/{action}', 'Calendario\ViewController@setSessionFechaIni');
Route::get('/calendario/{id}/{fecha}/{sala?}', 'Calendario\ViewController@reserva');


/**
 * Modulo Bono Regalo
 */
Route::get('/bonoregalo', 'BonoRegalo\ViewController@index');
Route::post('/bonoregalo/checkout', 'BonoRegalo\ViewController@checkout');
Route::any('/bonoregalo/codigo/{id}', 'BonoRegalo\ViewController@codigo');
Route::get('/bonoregalo/renderCodigo/{id}', 'BonoRegalo\ViewController@renderCode');

Route::any('/bonoregalo/payTPV', 'BonoRegalo\ViewController@payTPV');
Route::post('/bonoregalo/pay', 'BonoRegalo\ViewController@pay');
Route::get('/bonoregalo/paypal', 'BonoRegalo\ViewController@PayPalOkURL');
Route::any('/bonoregalo/tpv.php', 'BonoRegalo\ViewController@TPVmerchantURL');

/**
 * Modulo home
 */
Route::get('/home', 'Home\ViewController@listar');
Route::get('/', 'Home\ViewController@listar');

Route::get('api/home/getListMailing', 'Home\ViewController@getListMailing');

Route::get('api/home/validar/{id}/{status}', 'Home\ViewController@validar');


/**
 * Modulo journey
 */
Route::get('/journey', 'Journey\ViewController@listar');
Route::post('/journey/start', 'Journey\ViewController@start');
Route::post('/journey/finalizar/{id}', 'Journey\ViewController@finalizar');
Route::post('/journey/continuar/{id}', 'Journey\ViewController@continuar');
Route::post('/journey/update/{id}', 'Journey\ViewController@update');
Route::post('/journey/comentario/add/{id}', 'Journey\ViewController@addComentario');
Route::get('/journey/comentario/del/{id}', 'Journey\ViewController@delComentario');

/**
 * Modulo caja chica
 */
Route::get('/caja', 'Caja\ViewController@listar');
Route::post('/caja/add', 'Caja\ViewController@add');

/**
 * Modulo mailing
 */
Route::get('/mailing', 'Mailing\ViewController@listar');
Route::get('/mailing/editar/{id}', 'Mailing\ViewController@editar');
Route::post('/mailing/edit', 'Mailing\ViewController@saveEdit');
Route::post('/mailing/save', 'Mailing\ViewController@saveMailing');

Route::post('/mailing/upload', 'Mailing\ViewController@upload');
Route::post('/mailing/delete', 'Mailing\ViewController@delete');

/**
 * Modulo late minute
 */
Route::get('/lateminute', 'LateMinute\ViewController@listar');
Route::post('/lateminute/add', 'LateMinute\ViewController@add');
Route::post('/lateminute/enviar', 'LateMinute\ViewController@enviar');
Route::get('/lateminute/del/{id}/{correo}', 'LateMinute\ViewController@del');


/**
 * Modulo Configuración
 */
    /**
     * Submodulo Usuarios
     */
    Route::get('configuracion/usuarios', 'Configuracion\UsuariosViewController@index');
    Route::get('configuracion/usuarios/nuevoRol', 'Configuracion\UsuariosViewController@nuevoRol');
    Route::get('configuracion/usuarios/nuevo', 'Configuracion\UsuariosViewController@nuevo');
    Route::get('configuracion/usuarios/delRol/{id}', 'Configuracion\UsuariosViewController@borrarRol');
    Route::get('configuracion/usuarios/del/{id}', 'Configuracion\UsuariosViewController@borrar');
    Route::get('configuracion/usuarios/editar/{id}', 'Configuracion\UsuariosViewController@editar');
    Route::get('configuracion/usuarios/editarRol/{id}', 'Configuracion\UsuariosViewController@editarRol');

    Route::get('api/Configuracion/usuarios/listar', 'Configuracion\UsuariosApiController@listar'); //Datatable List users
    Route::get('api/Configuracion/usuarios/listarRoles', 'Configuracion\UsuariosApiController@listarRoles'); //Datatable List Roles
    Route::post('api/Configuracion/usuarios/add', 'Configuracion\UsuariosApiController@addUser'); //Add new user
    Route::post('api/Configuracion/usuarios/rol/add', 'Configuracion\UsuariosApiController@addRol'); //Add new Rol
    Route::delete('api/Configuracion/usuarios/del', 'Configuracion\UsuariosApiController@delUser'); //Delete user
    Route::delete('api/Configuracion/usuarios/rol/del', 'Configuracion\UsuariosApiController@delRol'); //Delete Rol
    Route::put('api/Configuracion/usuarios/editar', 'Configuracion\UsuariosApiController@editarUser'); //Edit user
    Route::put('api/Configuracion/usuarios/rol/editar', 'Configuracion\UsuariosApiController@editarRol'); //Edit rol

    /**
     * Submodulo Ajustes
     */
    Route::get('configuracion/ajustes', 'Configuracion\AjustesViewController@index');
    Route::get('configuracion/ajustes/nueva', 'Configuracion\AjustesViewController@nueva');
    Route::get('configuracion/ajustes/del/{id}', 'Configuracion\AjustesViewController@borrar');
    Route::get('configuracion/ajustes/editarSegmento/{id}', 'Configuracion\AjustesViewController@editarSegmento');
    Route::get('configuracion/ajustes/editarMail', 'Configuracion\AjustesViewController@editarMail');

    Route::get('api/Configuracion/ajustes/listarSegmentos', 'Configuracion\AjustesApiController@listarSegmentos'); //Datatable List
    Route::get('api/Configuracion/ajustes/listarMail', 'Configuracion\AjustesApiController@listarMail'); //Datatable List
    Route::post('api/Configuracion/ajustes/add', 'Configuracion\AjustesApiController@addSegmento'); //Add new
    Route::delete('api/Configuracion/ajustes/del', 'Configuracion\AjustesApiController@delSegmento'); //Delete
    Route::delete('api/Configuracion/ajustes/imagen/del', 'Configuracion\AjustesApiController@delSegmentoImage'); //Delete
    Route::put('api/Configuracion/ajustes/segmento/editar', 'Configuracion\AjustesApiController@editarSegmento'); //Edit
    Route::put('api/Configuracion/ajustes/mail/editar', 'Configuracion\AjustesApiController@editarMail'); //Edit
    Route::post('api/Configuracion/ajustes/icono/add', 'Configuracion\AjustesApiController@newicono'); //Add new icono

    /**
     * Submodulo Mails
     */
    Route::get('configuracion/mails', 'Configuracion\MailsViewController@index');
    Route::get('configuracion/mails/editar/{id}', 'Configuracion\MailsViewController@editar');
    Route::get('configuracion/mails/editarts/{id}', 'Configuracion\MailsViewController@editarts');
    Route::get('configuracion/mails/add', 'Configuracion\MailsViewController@add');

    Route::get('api/Configuracion/mails/listarts', 'Configuracion\MailsApiController@listarts'); //Datatable List users
    Route::get('api/Configuracion/mails/listar', 'Configuracion\MailsApiController@listar'); //Datatable List users
    Route::put('api/Configuracion/mails/editar', 'Configuracion\MailsApiController@editar'); //Edit
    Route::put('api/Configuracion/mails/editarts', 'Configuracion\MailsApiController@editarts'); //Edit
    Route::put('api/Configuracion/mails/add', 'Configuracion\MailsApiController@add'); //Add

    /**
     * Submodulo booking
     */
    Route::get('configuracion/bookings', 'Configuracion\BookingsViewController@index');
    Route::post('configuracion/bookings/editar', 'Configuracion\BookingsViewController@editarDayPost');
    Route::get('configuracion/bookings/editarPrecios/{id}', 'Configuracion\BookingsViewController@editarPrecios');
    Route::post('configuracion/bookings/editarPrecios', 'Configuracion\BookingsViewController@editarPreciosPost');
    Route::get('configuracion/bookings/delGrupo/{id}', 'Configuracion\BookingsViewController@delGrupo');
    Route::get('configuracion/bookings/addGrupo', 'Configuracion\BookingsViewController@addGrupo');
    Route::get('configuracion/bookings/add/{idDay}', 'Configuracion\BookingsViewController@addDay');
    Route::get('configuracion/bookings/del/{id}', 'Configuracion\BookingsViewController@delDay');
    Route::get('configuracion/bookings/editar/{id}', 'Configuracion\BookingsViewController@editarDay');

    Route::get('configuracion/bookings/configEditar/{id}', 'Configuracion\BookingsViewController@configEditar');
    Route::get('configuracion/bookings/configEditar/{id}/{bool}', 'Configuracion\BookingsViewController@configEditar');
    Route::post('configuracion/bookings/configEditar', 'Configuracion\BookingsViewController@configEditarPost');

    Route::get('configuracion/bookings/editarFacturacion', 'Configuracion\BookingsViewController@editarFact');
    Route::post('configuracion/bookings/editarFacturacion', 'Configuracion\BookingsViewController@editarFactPost');
