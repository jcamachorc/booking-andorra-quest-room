<?php
class Mailer{

    private $mail;

    function __construct() {
        $mailServer = \App\AdmMailings::find(1)->toArray();
        $this->mail = new PHPMailer();
        $this->mail->setFrom($mailServer['username'], $mailServer['name']);
        $this->mail->CharSet = "utf-8"; // set charset to utf8
        $this->mail->isHTML(true);

        // $mail->isSendMail();// Para 1 & 1
    }

    public function setContent($titulo, $contenido){
        $this->mail->Subject = $titulo;
        $this->mail->MsgHTML(nl2br($contenido));
    }

    public function addAddress($nombre, $correo){
        $this->mail->addAddress($correo, $nombre);
    }
    public function addBCC($nombre, $correo){
        $this->mail->addBCC($correo, $nombre);
    }

    public function addAttach($path){
        $this->mail->AddAttachment($path);
    }

    public function addStringAttach($string,$filename){
        $this->mail->AddStringAttachment($string,$filename);
    }

    public function sendMail(){
 
        if(!$this->mail->Send()) {
            //echo $this->mail->ErrorInfo;
            return false;
        }else{
            return true;
        }
    }
}
?>