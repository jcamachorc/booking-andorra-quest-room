<?php
class PayPal{
    private $uri;
    private $pay;
    private $token;

    function __construct($uri, $pay) {
        $this->uri = $uri;
        $this->pay = $pay;
    }

    function connect(){
        /*$sanboxed = \App\TsConfigs::find('payments_mode_sandboxed')->toArray();
        if($sanboxed['value'] == "1") {
            $client_id = getConfig('paypal_sandboxed_client_id');
            $client_secret = getConfig('paypal_sandboxed_client_secret');
            $userpwd = $client_id.':'.$client_secret;
        }else{*/
            $client_id = getConfig('paypal_client_id');
            $client_secret = getConfig('paypal_client_secret');
            $userpwd = $client_id.':'.$client_secret;
        //}

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->uri);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        $result = curl_exec($ch);
        if(empty($result))die("Error: No response.");
        $json = json_decode($result);

        $this->token = $json->access_token;
        return $this->token;
    }

    function getToken(){
        return $this->token;
    }

    function seconStage($okUri, $errorUri, $precio, $desc){
        $ch = curl_init();
        $data = '{
		  "intent":"sale",
		  "redirect_urls":{
			"return_url":"'.$okUri.'",
			"cancel_url":"'.$errorUri.'"
		  },
		  "payer":{
			"payment_method":"paypal"
		  },
		  "transactions":[
			{
			  "amount":{
				"total":"'.number_format($precio, 2, '.', '').'",
				"currency":"EUR"
			  },
			  "description":"'.$desc.'"
			}
		  ]
		}';
        curl_setopt($ch, CURLOPT_URL, $this->pay);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "Authorization: Bearer ".$this->token)
        );
        $result = curl_exec($ch);
        $json = json_decode($result, true);

        return $json['links'][1]['href'];
    }

    function pay($payer_id, $token){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->uri);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{ "payer_id" : "'.$payer_id.'" }');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "Authorization: Bearer ".$token)
        );
        $result = curl_exec($ch);
        $json = json_decode($result, true);

        return ($json['state'] == 'approved');
    }
}
?>