<?php namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use App\User;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{

        view()->composer('partials.nav', function($view){
            //Aqui pasamos los datos del menu
            $user = User::find(Auth::id());
            $view->with("menu", $user->getMenu());
            $view->with("menuType", $user->getMenuType());
        });

        view()->composer('partials.tabs', function($view){
            //Aqui pasamos los datos de las tabs
            $user = User::find(Auth::id());
            $view->with("tabs", $user->getSubMenu());
            $view->with("actualMenu", $user->getActualMenu());
        });


	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);

	}

}
