<?php namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes;

	protected $table = 'users';

    protected $dates = ['deleted_at'];

	protected $fillable = ['name', 'email', 'password', 'imei','idRol','extension'];

	protected $hidden = ['password', 'remember_token'];

    public function idRol()
    {
        return $this->hasOne('idRol');
    }

    public static function getName(){
        $id = Auth::id();
        return User::find($id)->toArray()['name'];
    }

    public static function getNameById($id){
        return User::find($id)->toArray()['name'];
    }

    public static function getImage(){
        $id = Auth::id();
        $userImage = User::find($id)->toArray()['imagen'];

				if(empty($userImage))
            $userImage = 'default.png';

        return asset('/uploads/usuarios/'.$userImage); //Le decimos que nos formatee la url
    }

    public static function getImageById($id){
        $userImg = User::find($id)->toArray()['imagen'];
        if(empty($userImg))
            $img = 'default.png';
        else
            $img = $userImg;

        return asset('/uploads/usuarios/'.$img); //Le decimos que nos formatee la url
    }

    public function getMenuType(){
        $id = Auth::id();
        return User::find($id)->toArray()['menu'];
    }

    private function getPermisos(){
        $id = Auth::id();
        $idRol = User::find($id)->idRol;
        $permisos = AdmPermisosRoles::where('idRol', $idRol)->select('permiso')->get()->toArray();
        $result = array();
        foreach($permisos as $item){
            $result[] = $item['permiso'];
        }
        return $result;
    }

    private function getPermisosOverride(){
        $id = Auth::id();
        $permisos = AdmPermisosOverride::where('idUser', $id)->select('permiso')->get()->toArray();
        $result = array();
        foreach($permisos as $item){
            $result[] = $item['permiso'];
        }
        return $result;
    }

    private function getPermisosDefinitivos(){
        $permisos = self::getPermisos();
        $permisosoverride = self::getPermisosOverride();
        return array_diff($permisos, $permisosoverride);
    }

    public function getMenu(){
        $rtl = self::getActualMenu();
        if(isset($rtl[1])) $rtl = $rtl[0]."/".$rtl[1];
        else $rtl = $rtl[0];

        $ruta = Request::path();
        $menu = DB::table('adm_menus')
                ->join('adm_sub_menus', 'adm_sub_menus.idMenu', '=', 'adm_menus.id')
                ->select('adm_menus.nombre as nombre', 'adm_menus.id as id', 'adm_menus.icon as icon')
                ->GroupBy('adm_menus.nombre')
                ->OrderBy('adm_menus.id')
                ->get();

        $meunAllowed = self::getPermisosDefinitivos();

        $menuDefinitivo = array();
        foreach($menu as $key => $item) {
            $submenu = AdmSubMenu::where('idMenu', $item->id)->whereIn('permiso', $meunAllowed)->select('nombre', 'ruta', 'nochildren')->get()->toArray();

            for($i = 0; $i < count($submenu); ++$i){
                if($submenu[$i]['ruta'] == $rtl)
                    $submenu[$i]['active'] = true;
            }

            if(isset($submenu[0]))
                $menuDefinitivo[$key] = array(  "nombre" => $item->nombre,
                                            "ruta" => $submenu[0]['ruta'],
                                            "icon" => $item->icon,
                                            'selected' => (explode("/", $ruta)[0] == explode("/", $submenu[0]['ruta'])[0]),
                                            "SubMenu" => ($submenu[0]['nochildren']) ? NULL : $submenu);
        }
        array_unshift($menuDefinitivo, array("nombre" => 'Dashboard', "ruta" => '/home', 'icon' => 'home' ,"SubMenu" => NULL, 'selected' => ($ruta == '/') ));

        return $menuDefinitivo;
    }

    public function getSubMenu(){
        $ruta = explode("/",Request::path())[0];
        if($ruta == '') return null;

        $rtl = self::getActualMenu();
        if(isset($rtl[1])) $rtl = $rtl[0]."/".$rtl[1];
        else $rtl = $rtl[0];

        $meunAllowed = self::getPermisosDefinitivos();
        $submenu = AdmSubMenu::where('ruta', 'like', '%'.$ruta.'%')->whereIn('permiso', $meunAllowed)->select('nombre', 'ruta')->get()->toArray();
        foreach($submenu as $key => $item){
            if($item['ruta'] == $rtl)
                $submenu[$key]['active'] = true;
        }
        return $submenu;
    }

    public function getActualMenu(){
        $var = explode("/", Request::path());
        return (end($var) == '')? array('Dashboard') : $var;
    }

    public static function getAgendaView(){
        return User::find(Auth::id())->agenda;
    }

    public function routeAllowed(){
        $rutasEspeciales = array("error/",
                                "modal",
                                "calendario",
                                "uploads/",
                                "lang",
                                "lang/",
                                "bonoregalo",
                                "bonoregalo/",
                                "cron",
                                "cron/",
                                "registro",
                                "modalalt",
                                'auth/',
                                'vendor',
                                'images',
                                'fonts',
                                'css',
                                'js',
                                'home');
        $id = Auth::id();
        $ruta = strtolower(Request::path());
        $ruta = str_replace("api/",'',$ruta);

        //Dashboard abierto para todos
        if($ruta == '/'){
            return true;
        }

        //Excepciones que tambien estan abiertas a todos
        foreach($rutasEspeciales as $special){
            if(strpos($ruta, $special) !== false)
                return true;
        }

        $permisos = self::getPermisosDefinitivos();
        $permisosRuta = AdmSubMenu::whereIn('permiso', $permisos)->get()->toArray();
        foreach ($permisosRuta as $permisoRuta) {
            if(stripos($ruta, $permisoRuta['ruta']) !== false){
                return true;
            }
        }
        return false;
    }

    public function checkPermiso($permiso){
        $permisos = self::getPermisosDefinitivos();
        if(!in_array($permiso, $permisos)){
            return response()->view('errors.403');
        }
    }


    public static function existsById($id){
        try {
            $user = User::where('id', $id)->count();
            return ($user != 0);
        } catch (ErrorException $e) {
            return false;
        }
    }

    public static function existsByMail($correo){
        try {
            $user = User::where('email', $correo)->count();
            return ($user != 0);

        } catch (ErrorException $e) {
            return false;
        }
    }

    public function isAdmin(){
        $id = Auth::id();
        $idRol = User::find($id)->idRol;
        return ($idRol == 1);
    }

    public function isJefe(){
        $id = Auth::id();
        $isJefe = User::find($id)->jefe;
        return ($isJefe == 1);
    }
}
