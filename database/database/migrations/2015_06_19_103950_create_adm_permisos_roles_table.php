<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmPermisosRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_permisos_roles', function(Blueprint $table)
		{
			$table->integer('idRol')->unsigned();
            $table->string('permiso');
			$table->timestamps();

            $table->primary(array('idRol','permiso'));
            $table->foreign('idRol')->references('id')->on('adm_roles')->onDelete('cascade');
            $table->foreign('permiso')->references('permiso')->on('adm_permisos')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_permisos_roles');
	}

}
