<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmMailingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_mailings', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('host');
            $table->string('username');
            $table->string('password');
            $table->string('encrypt');
            $table->integer('port');
            $table->string('name');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_mailings');
	}

}
