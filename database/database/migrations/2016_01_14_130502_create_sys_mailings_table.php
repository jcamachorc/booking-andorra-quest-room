<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMailingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_mailings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('donde');
            $table->string('idioma');
            $table->string('nivel');
            $table->integer('idUsuario')->unsigned();
            $table->string('KeyPlantilla');
            $table->boolean('revisado')->default(false);
            $table->boolean('envio')->default(false); //Si se ha realizado el envio
			$table->timestamps();

            $table->foreign('idUsuario')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('KeyPlantilla')->references('key')->on('sys_mails_contenidos')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_mailings');
	}

}
