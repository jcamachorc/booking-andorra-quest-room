<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMailingImagenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_mailing_imagenes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('idMailing')->unsigned();
            $table->string('nombre');
			$table->timestamps();

            $table->foreign('idMailing')->references('id')->on('sys_mailings')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_mailing_imagenes');
	}

}
