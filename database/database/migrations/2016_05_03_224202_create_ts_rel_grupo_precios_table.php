<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsRelGrupoPreciosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_rel_grupo_precios', function(Blueprint $table)
		{
			$table->integer('idGrupo')->unsigned();
            $table->integer('idPrecio')->unsigned();
			$table->timestamps();

            $table->foreign('idGrupo')->references('id')->on('ts_grupo_precios')->onDelete('cascade');
            $table->foreign('idPrecio')->references('id')->on('ts_precios')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_rel_grupo_precios');
	}

}
