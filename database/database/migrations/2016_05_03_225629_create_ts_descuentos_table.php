<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsDescuentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_descuentos', function(Blueprint $table)
		{
			$table->string('id');
            $table->dateTime('fechaCaducidad');
            $table->integer('descuento');
            $table->integer('idReserva')->unsigned()->nullable();
			$table->timestamps();

            $table->primary('id');
            $table->foreign('idReserva')->references('id')->on('ts_reservas')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_descuentos');
	}

}
