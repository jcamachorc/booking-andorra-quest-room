<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsRelFacturacionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_rel_facturacions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('idBono')->unsigned()->nullable()->unique();
            $table->integer('idReserva')->unsigned()->nullable()->unique();
			$table->timestamps();

            $table->foreign('idBono')->references('id')->on('ts_bonos')->onDelete('cascade');
            $table->foreign('idReserva')->references('id')->on('ts_reservas')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_rel_facturacions');
	}

}
