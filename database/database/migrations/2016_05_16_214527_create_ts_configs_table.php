<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_configs', function(Blueprint $table)
		{
			$table->string('item');
            $table->longText('value');
            $table->string('descripcion');
            $table->boolean('encriptado');
			$table->timestamps();

            $table->primary('item');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_configs');
	}

}
