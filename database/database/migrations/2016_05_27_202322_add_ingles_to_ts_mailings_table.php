<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInglesToTsMailingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_mailings', function(Blueprint $table)
		{
            $table->longText('ingles');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_mailings', function(Blueprint $table)
		{
			$table->dropColumn('ingles');
		});
	}

}
