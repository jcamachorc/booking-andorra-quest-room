<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNombreFacturaToTsReservasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_reservas', function(Blueprint $table)
		{
            $table->string('nombreF');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_reservas', function(Blueprint $table)
		{
            $table->dropColumn('nombreF');
		});
	}

}
