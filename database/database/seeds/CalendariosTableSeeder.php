<?php

use Illuminate\Database\Seeder;

class CalendariosTableSeeder extends Seeder {

    public function run() {
        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(8,0,0),
            'fin' => \Carbon\Carbon::createFromTime(20,0,0),
            'duracion' => 90,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 1
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(8,0,0),
            'fin' => \Carbon\Carbon::createFromTime(12,0,0),
            'duracion' => 90,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 2
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(15,0,0),
            'fin' => \Carbon\Carbon::createFromTime(20,0,0),
            'duracion' => 90,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 2
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(8,0,0),
            'fin' => \Carbon\Carbon::createFromTime(20,0,0),
            'duracion' => 90,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 3
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(8,0,0),
            'fin' => \Carbon\Carbon::createFromTime(20,0,0),
            'duracion' => 120,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 4
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(8,0,0),
            'fin' => \Carbon\Carbon::createFromTime(20,0,0),
            'duracion' => 90,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 5
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(10,0,0),
            'fin' => \Carbon\Carbon::createFromTime(23,59,59),
            'duracion' => 120,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 6
        ] );

        \App\TsCalendario::create( [
            'inicio' => \Carbon\Carbon::createFromTime(8,0,0),
            'fin' => \Carbon\Carbon::createFromTime(23,59,59),
            'duracion' => 120,
            'idPrecio' => 1,
            'festivo' => false,
            'idDia' => 7
        ] );
    }
}