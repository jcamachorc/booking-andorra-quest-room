<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        $this->call('RolesTableSeeder');
		$this->call('UserTableSeeder');
        $this->call('PermisosTableSeeder');
        $this->call('PermisoRolTableSeeder');
        $this->call('MenuTableSeeder');
        $this->call('SubMenuTableSeeder');

        $this->call('PreciosTableSeeder');
        $this->call('DiasTableSeeder');
        $this->call('CalendariosTableSeeder');
        $this->call('VacacionesTableSeeder');
        $this->call('TsMailingTableSeeder');
        $this->call('FacturacionTableSeeder');
	}

}
