<?php

use Illuminate\Database\Seeder;

class DiasTableSeeder extends Seeder {

    public function run() {
        \App\TsDias::create( [
            'id' => 1,
            'nombre' => 'Lunes',
        ] );
        \App\TsDias::create( [
            'id' => 2,
            'nombre' => 'Martes',
        ] );
        \App\TsDias::create( [
            'id' => 3,
            'nombre' => 'Miercoles',
        ] );
        \App\TsDias::create( [
            'id' => 4,
            'nombre' => 'Jueves',
        ] );
        \App\TsDias::create( [
            'id' => 5,
            'nombre' => 'Viernes',
        ] );
        \App\TsDias::create( [
            'id' => 6,
            'nombre' => 'Sabado',
        ] );
        \App\TsDias::create( [
            'id' => 7,
            'nombre' => 'Domingo',
        ] );
    }
}