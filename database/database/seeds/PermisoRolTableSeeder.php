<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\AdmPermisosRoles as Permisos;

class PermisoRolTableSeeder extends Seeder {

    public function run() {
        $db = array(
            [
                'permiso' => 'config_usuarios',
                'idRol' => 1
            ],[
                'permiso' => 'config_ajustes',
                'idRol' => 1
            ],[
                'permiso' => 'config_bookings',
                'idRol' => 1
            ],[
                'permiso' => 'config_mails',
                'ioRol' => 1
            ],[
                'permiso' => 'sys_mails',
                'idRol' => 1
            ],[
                'permiso' => 'sys_journey',
                'idRol' => 1
            ],[
                'permiso' => 'sys_caja',
                'idRol' => 1
            ],[
                'permiso' => 'sys_lateminute',
                'idRol' => 1
            ],[
                'permiso' => 'sys_facturar',
                'idRol' => 1
            ],[
                'permiso' => 'booking_calendario',
                'idRol' => 1
            ],[
                'permiso' => 'booking_booking',
                'idRol' => 1
            ],[
                'permiso' => 'booking_bonos',
                'idRol' => 1
            ]
        );
        DB::table('adm_permisos_roles')->insert($db);

        $db = array(
            [
                'permiso' => 'sys_mails',
                'idRol' => 2
            ],[
                'permiso' => 'sys_journey',
                'idRol' => 2
            ],[
                'permiso' => 'sys_caja',
                'idRol' => 2
            ]
        );
        DB::table('adm_permisos_roles')->insert($db);
    }
}