<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\AdmPermisos as Permisos;

class PermisosTableSeeder extends Seeder {

    public function run() {
        $db = array(
            [
                'permiso' => 'config_usuarios',
                'descripcion' => ''
            ],[
                'permiso' => 'config_ajustes',
                'descripcion' => ''
            ],[
                'permiso' => 'config_mails',
                'descripcion' => ''
            ],[
                'permiso' => 'sys_mails',
                'descripcion' => ''
            ],[
                'permiso' => 'sys_journey',
                'descripcion' => ''
            ],[
                'permiso' => 'sys_caja',
                'descripcion' => ''
            ],[
                'permiso' => 'sys_lateminute',
                'descripcion' => ''
            ],[
                'permiso' => 'sys_facturar',
                'descripcion' => ''
            ],[
                'permiso' => 'booking_calendario',
                'descripcion' => ''
            ],[
                'permiso' => 'booking_booking',
                'descripcion' => ''
            ],[
                'permiso' => 'booking_bonos',
                'descripcion' => ''
            ],[
                'permiso' => 'config_bookings',
                'descripcion' => ''
            ]
        );

        //// Uncomment the below to run the seeder
        DB::table('adm_permisos')->insert($db);
    }
}