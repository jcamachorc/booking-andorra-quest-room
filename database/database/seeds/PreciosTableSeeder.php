<?php

use Illuminate\Database\Seeder;

class PreciosTableSeeder extends Seeder {

    public function run() {
        \App\TsPrecios::create( [
            'id' => 1,
            'personas' => 5,
            'precio' => 60
        ]);

        \App\TsPrecios::create( [
            'id' => 2,
            'personas' => 4,
            'precio' => 40
        ]);

        \App\TsPrecios::create( [
            'id' => 3,
            'personas' => 5,
            'precio' => 70
        ]);

        \App\TsPrecios::create( [
            'id' => 4,
            'personas' => 4,
            'precio' => 50
        ]);

        \App\TsGrupoPrecios::create([
            'id' => 1,
            'nombre' => 'Precios Semana'
        ]);

        \App\TsGrupoPrecios::create([
            'id' => 2,
            'nombre' => 'Precios finde'
        ]);

        \App\TsRelGrupoPrecios::create([
            'idGrupo' => 1,
            'idPrecio' => 1
        ]);

        \App\TsRelGrupoPrecios::create([
            'idGrupo' => 1,
            'idPrecio' => 2
        ]);

        \App\TsRelGrupoPrecios::create([
            'idGrupo' => 2,
            'idPrecio' => 3
        ]);

        \App\TsRelGrupoPrecios::create([
            'idGrupo' => 2,
            'idPrecio' => 4
        ]);

        \App\TsConfigs::create([
            'item' => 'precio_bono',
            'value' => '60',
            'descripcion' => 'Precio del bono',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'descuento_carnet_jove',
            'value' => '16',
            'descripcion' => 'Descuento en % del carnet jove',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'payments_mode_sandboxed',
            'value' => true,
            'descripcion' => 'Activar pagos de pruebas (desactiva pagos normales)',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'paypal_sandboxed_client_id',
            'value' => \Illuminate\Support\Facades\Crypt::encrypt('ATMegiqIkLF3SbiofXSDgfFjRGxGozFxsK_KJVykyZPUQqGjZ1JbX45fwn-m_nG3MzloUMYNxsoXgFeK'),
            'descripcion' => 'Client id (Paypal) -> Sandbox mode',
            'encriptado' => true
        ]);

        \App\TsConfigs::create([
            'item' => 'paypal_sandboxed_client_secret',
            'value' => \Illuminate\Support\Facades\Crypt::encrypt('EKWCnda9VLDxvpJG-qtIet4ys3n6Ew5NEHJG8rGfPUv8GfuUdMLMeTaLmUQPOWTVH7nnE46tc8sOtSqR'),
            'descripcion' => 'Client secret (Paypal) -> Sandbox mode',
            'encriptado' => true
        ]);

        \App\TsConfigs::create([
            'item' => 'paypal_client_id',
            'value' => \Illuminate\Support\Facades\Crypt::encrypt(''),
            'descripcion' => 'Client id (Paypal) -> Normal mode',
            'encriptado' => true
        ]);

        \App\TsConfigs::create([
            'item' => 'paypal_client_secret',
            'value' => \Illuminate\Support\Facades\Crypt::encrypt(''),
            'descripcion' => 'Client secret (Paypal) -> Normal mode',
            'encriptado' => true
        ]);

        \App\TsConfigs::create([
            'item' => 'correo_admin',
            'value' => 'reservas@juegoenigmamadrid.es',
            'descripcion' => 'Correo de administrador para notificar reservas',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'tpv_clave_cliente',
            'value' => \Illuminate\Support\Facades\Crypt::encrypt('53KzbMDX1FRV5suN0hoDQ9SKW2qdi7jQ'),
            'descripcion' => 'Clave del cliente TPV',
            'encriptado' => true
        ]);

        \App\TsConfigs::create([
            'item' => 'tpv_codigo_comercio',
            'value' => \Illuminate\Support\Facades\Crypt::encrypt('336097324'),
            'descripcion' => 'Codigo de comercio del TPV',
            'encriptado' => true
        ]);

        \App\TsConfigs::create([
            'item' => 'payment_ok',
            'value' => url('http://juegoenigmamadrid.es/es/gracias/'),
            'descripcion' => 'Url al realizar un pago correcto',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'payment_ko',
            'value' => url('http://juegoenigmamadrid.es/es/error/'),
            'descripcion' => 'Url al realizar un pago incorrecto',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'cj_enable',
            'value' => false,
            'descripcion' => 'Activar carnet jove',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'tpv_enable',
            'value' => true,
            'descripcion' => 'Activar pagos TPV',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'paypal_enable',
            'value' => true,
            'descripcion' => 'Activar pagos PayPal',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'hora_maxima',
            'value' => '01:00:00',
            'descripcion' => 'A partir de esta hora no se dejara reservar hasta la hora minima',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'hora_minima',
            'value' => '11:00:00',
            'descripcion' => 'Hora a partir de la cual se deja resevar (llegados a la hora maxima de la noche)',
            'encriptado' => false
        ]);

        \App\TsConfigs::create([
            'item' => 'delay_hora_minima',
            'value' => '3',
            'descripcion' => 'Horas de delay para la reserva mas inmediata',
            'encriptado' => false
        ]);

    }
}