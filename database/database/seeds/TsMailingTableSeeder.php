<?php
use Illuminate\Database\Seeder;

class TsMailingTableSeeder extends Seeder {

    public function run() {
        \App\TsMailing::create( [
            'nombre' => 'cancelacion',
            'contenido' => '¡Hola {Name}!

La reserva nº {BookingID}, para el día {Timeslots}
Pendiente de pago: {Total}

Ha sido anulada.

Para cualquier duda o aclaración contacte con nosotros.

¡Gracias!

Juego Enigma
adn@juegoenigma.es
C/ de les Acàcies 38, bajos
08027 Barcelona
Tel. 93 142 66 38
www.juegoenigma.es'
        ] );

        \App\TsMailing::create( [
            'nombre' => 'confirmacion',
            'contenido' => 'Juego Enigma - Room Escape
-------------------------------------

¡Gracias por reservar {Name}!

¡Oficialmente ya sois agentes!

Queda confirmada la misión para el día {Timeslots}

Os esperamos a ti y a tu equipo para realizar la misión secreta, que se os comunicará en cuanto lleguéis al punto de encuentro en c/ de les Acàcies nº 38, 08027 Barcelona.

Tendréis que resolver todas las pruebas, conseguir salir en menos de 60 minutos y recibiréis vuestra recompensa.

Recordar llegar 5 minutos antes de la hora en punto, ya que antes y después de la misión, hay varios extras que precisan de vuestro tiempo.

Atentamente,

El Coronel.


Juego Enigma
adn@juegoenigma.es
C/ de les Acàcies 38, bajos
08027 Barcelona
Tel. 93 142 66 38
www.juegoenigma.es'
        ] );

        \App\TsMailing::create( [
            'nombre' => 'recordatorio',
            'contenido' => 'Recordatorio Juego Enigma
-------------------------------------

¡Hola Agente {Name}!

Le recordamos que tiene una misión a realizar con Juego Enigma.

Día y hora: {Timeslots}

Por favor recordar llegar 5 minutos antes de la hora en punto.

¡Te esperamos con tu equipo para jugar al room escape en vivo!

Gracias.


Juego Enigma
adn@juegoenigma.es
C/ de les Acàcies 38, bajos
08027 Barcelona
Tel. 93 142 66 38
www.juegoenigma.es'
        ] );

        \App\TsMailing::create( [
            'nombre' => 'prereserva',
            'contenido' => 'Pre-Reserva Juego Enigma
-------------------------------------

- Nombre:  {Name}
- Teléfono: {Phone}
- Email:     {Email}

- ID Reserva: {BookingID}
- Día reserva: {Timeslots}

- Método de pago: {PaymentMethod}
- Total: {Total}

IMPORTANTE: La reserva no está pagada, este es un mail informativo conforme se ha bloqueado la sesión durantes 15 minutos.

Este correo electrónico ha sido generado automáticamente. Por favor no responda a esta cuenta de correo.

Juego Enigma
adn@juegoenigma.es
C/ de les Acàcies 38, bajos
08027 Barcelona
Tel. 93 142 66 38
www.juegoenigma.es'
        ] );
    }
}