<?php

use Illuminate\Database\Seeder;
use App\User as User;

class UserTableSeeder extends Seeder {

    public function run() {
        User::create( [
            'email' => 'oscar.pallarols@gmail.com' ,
            'password' => \Illuminate\Support\Facades\Hash::make('cristian'),
            'name' => 'Cristian Rodriguez Crespo',
            'idRol'=> 1
        ] );

        \App\AdmMailings::create( [
            'host' => 'smtp.google.com' ,
            'username' => 'oscar.pallarols@gmail.com',
            'password' => 'aDm123aDm123',
            'encrypt'=> 'tls',
            'port'=> '587',
            'name'=> 'Juego Enigma Room Escape'
        ] );

        \App\SysMailsContenido::create([
            'titulo' => '',
            'key' => 'mails_imagen_grupo',
            'contenido' => ''
        ]);
    }
}