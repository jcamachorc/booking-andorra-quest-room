<?php

use Illuminate\Database\Seeder;

class VacacionesTableSeeder extends Seeder {

    public function run() {
        \App\TsVacaciones::create( [
            'id' => 1,
            'inicio' => \Carbon\Carbon::create('2016','05','08','15','0','0'),
            'fin' => \Carbon\Carbon::create('2016','05','09','15','0','0')
        ] );
    }
}