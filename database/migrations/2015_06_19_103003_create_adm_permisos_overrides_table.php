<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmPermisosOverridesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_permisos_overrides', function(Blueprint $table)
		{
            $table->integer('idUser')->unsigned();
            $table->string('permiso');
			$table->timestamps();

            $table->primary(array('idUser','permiso'));
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('permiso')->references('permiso')->on('adm_permisos')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_permisos_overrides');
	}

}
