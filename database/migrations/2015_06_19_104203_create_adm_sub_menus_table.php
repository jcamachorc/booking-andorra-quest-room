<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmSubMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_sub_menus', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('idMenu')->unsigned();
            $table->string('nombre');
            $table->string('ruta');
            $table->string('permiso');
            $table->boolean('nochildren')->default(false);
			$table->timestamps();

            $table->foreign('idMenu')->references('id')->on('adm_menus')->onDelete('cascade');
            $table->foreign('permiso')->references('permiso')->on('adm_permisos')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_sub_menus');
	}

}
