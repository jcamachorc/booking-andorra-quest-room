<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMailsContenidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_mails_contenidos', function(Blueprint $table)
		{
            $table->string('key');
            $table->string('titulo');
            $table->longText('contenido');
			$table->timestamps();

            $table->primary('key');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_mails_contenidos');
	}

}
