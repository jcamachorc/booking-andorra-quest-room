<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysJourneysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_journeys', function(Blueprint $table)
		{
			$table->increments('id');
            $table->timestamp('inicio');
            $table->timestamp('final');
            $table->boolean('revision');
            $table->boolean('correo');
            $table->boolean('redes');
            $table->integer('idUsuario')->unsigned();
			$table->timestamps();

            $table->foreign('idUsuario')->references('id')->on('users')->onDelete('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_journeys');
	}

}
