<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysJourneyComentariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_journey_comentarios', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('idJourney')->unsigned();
            $table->longText('comentario');
            $table->boolean('type')->default(false); //False -> incidencia || True -> comentario
			$table->timestamps();

            $table->foreign('idJourney')->references('id')->on('sys_journeys')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_journey_comentarios');
	}

}
