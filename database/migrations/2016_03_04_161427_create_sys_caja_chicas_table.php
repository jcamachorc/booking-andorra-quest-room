<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysCajaChicasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sys_caja_chicas', function(Blueprint $table)
		{
			$table->increments('id');
            $table->boolean('ingreso');
            $table->double('cantidad',5,2);
            $table->string('concepto');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sys_caja_chicas');
	}

}
