<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsReservasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_reservas', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('estado');
            $table->dateTime('inicio');
            $table->dateTime('fin');
            $table->integer('duracion');
            $table->float('precio');
            $table->integer('nPersonas');
            $table->string('metodoPago');
            $table->string('email');
            $table->string('DNI'); //NIF -> DNI + letra
            $table->string('nombre');
            $table->string('telefono');
            $table->integer('descuento');
            $table->string('tipoDescuento');
            $table->timestamp('fechaPago');
            $table->string('codigoPago');
            $table->string('codigoDescuento');
            $table->string('ip');
            $table->string('idioma');
            $table->string('comentarios');
            $table->boolean('recordatorio')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_reservas');
	}

}
