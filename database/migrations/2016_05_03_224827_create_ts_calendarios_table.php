<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsCalendariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_calendarios', function(Blueprint $table)
		{
			$table->increments('id');
            $table->time('inicio');
            $table->time('fin');
            $table->integer('duracion');
            $table->integer('idPrecio')->unsigned();
            $table->boolean('festivo');
            $table->integer('idDia')->unsigned();
			$table->timestamps();

            $table->foreign('idPrecio')->references('id')->on('ts_grupo_precios')->onDelete('restrict');
            $table->foreign('idDia')->references('id')->on('ts_dias')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_calendarios');
	}

}
