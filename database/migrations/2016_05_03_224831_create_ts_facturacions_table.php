<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsFacturacionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_facturacions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('IVA');
            $table->string('compania');
            $table->string('cif');
            $table->string('direccion');
            $table->string('ciudad');
            $table->string('estado');
            $table->string('cp');
            $table->string('telefono');
            $table->string('fax');
            $table->string('email');
            $table->string('logo');
            $table->string('web');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_facturacions');
	}

}
