<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsBonosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ts_bonos', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('codigo')->unique();
            $table->timestamp('fechaPago')->nullable();
            $table->string('metodoPago');
            $table->float('precio');
            $table->integer('descuento');
            $table->string('codigoDescuento');
            $table->string('nombre');
            $table->string('nombreDestinatario');
            $table->string('email');
            $table->string('emailDestinatario');
            $table->boolean('sendMailDest');
            $table->string('DNI');
            $table->string('telefono');
            $table->dateTime('fechaCaducidad');
            $table->integer('idReserva')->unsigned()->nullable();
            $table->string('codigoPago');
			$table->timestamps();

            $table->foreign('idReserva')->references('id')->on('ts_reservas')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ts_bonos');
	}

}
