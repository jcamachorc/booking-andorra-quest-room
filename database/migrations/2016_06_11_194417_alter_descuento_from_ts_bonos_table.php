<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDescuentoFromTsBonosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_bonos', function(Blueprint $table)
		{
            DB::statement('ALTER TABLE '.\Illuminate\Support\Facades\DB::getTablePrefix().'ts_bonos MODIFY COLUMN descuento float');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_bonos', function(Blueprint $table)
		{

		});
	}

}
