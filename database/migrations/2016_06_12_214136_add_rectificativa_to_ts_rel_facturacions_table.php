<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRectificativaToTsRelFacturacionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_rel_facturacions', function(Blueprint $table)
		{
            $table->boolean('rectificativa');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_rel_facturacions', function(Blueprint $table)
		{
            $table->dropColumn('rectificativa');
		});
	}

}
