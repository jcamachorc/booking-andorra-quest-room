<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIlimitadosToTsDescuentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_descuentos', function(Blueprint $table)
		{
            $table->boolean('ilimitados');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_descuentos', function(Blueprint $table)
		{
            $table->dropColumn('ilimitados');
		});
	}

}
