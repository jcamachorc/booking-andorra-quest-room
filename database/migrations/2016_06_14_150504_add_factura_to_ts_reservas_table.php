<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacturaToTsReservasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_rel_facturacions', function(Blueprint $table)
		{
            $table->boolean('factura');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_rel_facturacions', function(Blueprint $table)
		{
            $table->dropColumn('factura');
		});
	}

}
