<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToTsBonosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ts_bonos', function(Blueprint $table)
		{
            $table->string('nombreF');
            $table->boolean('factura');
            $table->string('direccion');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ts_bonos', function(Blueprint $table)
		{
            $table->dropColumn('nombreF');
            $table->dropColumn('factura');
            $table->dropColumn('direccion');
		});
	}

}
