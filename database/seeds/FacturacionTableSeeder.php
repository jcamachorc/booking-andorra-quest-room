<?php

use Illuminate\Database\Seeder;

class FacturacionTableSeeder extends Seeder {

    public function run() {
        \App\TsFacturacion::create([
            'IVA' => 21,
            'compania' => 'Corporation S.A',
            'direccion' => 'C/ de los carmenes 27',
            'ciudad' => 'Barcelona',
            'estado' => 'España',
            'cp' => '08027',
            'telefono' => '933529008',
            'fax' => '658787858',
            'email' => 'corp@hts.com',
            'logo' => 'logo.png',
            'web' => 'www.google.es',
            'cif' => '454689j'
        ]);
    }
}