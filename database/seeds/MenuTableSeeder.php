<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenuTableSeeder extends Seeder {

    public function run() {
        $menu = array(
            [
                'id'     => 1,
                'nombre' => 'Booking',
                'icon'   => 'calendar'
            ],[
                'id'     => 2,
                'nombre' => 'Mailing',
                'icon'   => 'paper-plane-o'
            ],[
                'id'    => 3,
                'nombre' => 'Journey',
                'icon'  => 'book'
            ],[
                'id'    => 4,
                'nombre' => 'Caja chica',
                'icon'  => 'euro'
            ],[
                'id' => 5,
                'nombre' => 'Last minute',
                'icon' => 'tint'
            ],[
                'id' => 6,
                'nombre' => 'Facturar',
                'icon' => 'print'
            ],[
                'id'     => 7,
                'nombre' => 'Configuración',
                'icon'   => 'cogs'
            ]
        );

        //// Uncomment the below to run the seeder
        DB::table('adm_menus')->insert($menu);
    }
}