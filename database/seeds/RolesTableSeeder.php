<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\AdmRoles as AdmRoles;

class RolesTableSeeder extends Seeder {

    public function run() {
        AdmRoles::create( [
            'id' => 1,
            'nombre' => 'Admin'
        ] );
        AdmRoles::create( [
            'id' => 2,
            'nombre' => 'Usuario'
        ] );
    }
}