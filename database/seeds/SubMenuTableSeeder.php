<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\AdmSubMenu as Menu;

class SubMenuTableSeeder extends Seeder {

    public function run() {
        $menu = array(
            [
                'idMenu'    => 7,
                'nombre'    => 'Usuarios',
                'ruta'      => 'configuracion/usuarios',
                'Permiso'   => 'config_usuarios',
            ],[
                'idMenu'     => 7,
                'nombre'     => 'Ajustes',
                'ruta'       => 'configuracion/ajustes',
                'Permiso'    => 'config_ajustes',
            ],[
                'idMenu'     => 7,
                'nombre'     => 'Contenido mails',
                'ruta'       => 'configuracion/mails',
                'Permiso'    => 'config_mails',
            ],[
                'idMenu'     => 7,
                'nombre'     => 'Bookings',
                'ruta'       => 'configuracion/bookings',
                'Permiso'    => 'config_bookings'
            ],[
                'idMenu'     => 1,
                'nombre'     => 'Calendario',
                'ruta'       => 'booking/calendario',
                'Permiso'    => 'booking_calendario',
            ],[
                'idMenu'     => 1,
                'nombre'     => 'Bookings',
                'ruta'       => 'booking/bookings',
                'Permiso'    => 'booking_booking',
            ],[
                'idMenu'     => 1,
                'nombre'     => 'Bonos',
                'ruta'       => 'booking/bonos',
                'Permiso'    => 'booking_bonos',
            ]
        );


        //// Uncomment the below to run the seeder
        DB::table('adm_sub_menus')->insert($menu);


        Menu::create( [
            'idMenu'     => 2,
            'nombre'     => 'Mailing',
            'ruta'       => 'mailing',
            'Permiso'    => 'sys_mails',
            'nochildren' => 1
        ] );

        Menu::create( [
            'idMenu'     => 3,
            'nombre'     => 'Journey',
            'ruta'       => 'journey',
            'Permiso'    => 'sys_journey',
            'nochildren' => 1
        ] );

        Menu::create( [
            'idMenu'     => 4,
            'nombre'     => 'Caja Chica',
            'ruta'       => 'caja',
            'Permiso'    => 'sys_caja',
            'nochildren' => 1
        ] );

        Menu::create( [
            'idMenu'     => 5,
            'nombre'     => 'Last Minute',
            'ruta'       => 'lateminute',
            'Permiso'    => 'sys_lateminute',
            'nochildren' => 1
        ] );

        Menu::create( [
            'idMenu'     => 6,
            'nombre'     => 'Facturar',
            'ruta'       => 'facturar',
            'Permiso'    => 'sys_facturar',
            'nochildren' => 1
        ] );
    }
}