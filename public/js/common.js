
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

/*function MuestraHoraCabecera(){
    var fecha = new Date();
    var hora = fecha.getHours();
    var minuto = fecha.getMinutes();
    var meridiano = " AM";
    if(hora > 12){hora -= 12; meridiano = " PM";}
    if (hora < 10) {hora = hora;}
    if (minuto < 10) {minuto = "0" + minuto;}

    cad = hora +":"+ minuto +"<div class='pull-right' id='meridiano'>"+ meridiano +"</div>";
    $("#hora-head").html(cad);
    setTimeout("MuestraHoraCabecera()",1000);
}
MuestraHoraCabecera();*/

$("#filters").hide();
$(".filters, .filters-sec").click(function(){
    if(!$("#filters").is(":visible")) {
        $("#filters").show(400);
        $(".filters").hide(100);
    }else{
        $("#filters").hide(200);
        $(".filters").show(100);

        $("#filters select").each(function() {
            $(this).val(0);
        });
        var lis = $("#filters input");
        len = lis.length;
        $("#filters input").each(function(index) {
            $(this).val('');
            if(index === len-1){
                $(this).trigger("change");
            }
        });

    }

});

$(".dropdown-toggle-side").click(function(){
    $(".sidebar").getNiceScroll().resize();
    $('.dropdown-items').each(function( index ) {
        $(this).hide(300);
    });

    if($(this).siblings('.dropdown-items').is(':visible'))
        $(this).siblings('.dropdown-items').hide(300);
    else
        $(this).siblings('.dropdown-items').show(300);
});

jQuery(function ($) {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié;', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$(".datepicker").datepicker();

$(".user-name-side").click(function(){
    if($("#sub-menu-user").is(':visible'))
        $("#sub-menu-user").hide(300);
    else
        $("#sub-menu-user").show(300);
});

function normal(){
    $(".menu-hidden-minimum").show(300);
    $('.sidebar').removeClass('readyHover');
    $('.main').removeClass('readyHoverMain');
    $('.sidebar').addClass('anchoSidebar');
}

function minimum(){
    $(".menu-hidden-minimum").hide(300);
    $('.dropdown-items').each(function( index ) {
        $(this).hide(300);
    });
    $('.sidebar').addClass('readyHover');
    $('.main').addClass('readyHoverMain');
    $('.sidebar').removeClass('anchoSidebar');
}

function tabletMode(){
    $('.sidebar').removeClass('anchoSidebar');
    $('.sidebar').removeClass('readyHover');
    $('.main').removeClass('readyHoverMain');
    $(".menu-hidden-minimum").hide(300);
}

$(".menu-toggle").click(function(){
    if($(".menu-hidden-minimum").is(':visible')){
        minimum();
    }else{
        normal();
    }
});

if(jQuery.browser.mobile) {
    //minimum();
}else{
    $(".sidebar").niceScroll().resize();
    $("body").niceScroll().resize();
}

$(".magicScroll").niceScroll().resize();
$(".side-cal").niceScroll().resize();

$("#items-search").niceScroll().resize();
$("#contenedor-search").niceScroll().resize();

$("#agendaConteDia").niceScroll().resize();

$(window).resize(function() {
    if($(window).width() > 1000 && !$(".profile").hasClass('minimal-sidebar')){
        normal();

    }else if($(window).width() < 767){
        minimum();
        if($(".profile").hasClass('minimal-sidebar')){
            $(".sidebar").removeClass('minimal-menu-sidebar');
            $(".main").removeClass('minimal-menu-main');
        }

    }else if(!$(".profile").hasClass('minimal-sidebar')){
        tabletMode();

    }else if($(".profile").hasClass('minimal-sidebar')){
        $(".sidebar").addClass('minimal-menu-sidebar');
        $(".main").addClass('minimal-menu-main');
        $(".sidebar").removeClass('readyHover');
    }
});

$(document).mouseup(function (e){
    var container = $(".search-system");
    if (!container.is(e.target) && container.has(e.target).length === 0)
        container.hide();
});

$(document).keyup(function(e) {
    if (e.keyCode == 27){
        $(".search-system").hide();
    }
});

$(".search-system").hide();
$('#search-system').click(function(e){
    e.stopPropagation();
    $(".search-system").show();
    $('#search-bar').focus();
});


var lastSearchItemSelected = null;
$(".search-system").on( "click", '.separador-item', function() {
    if(lastSearchItemSelected != null)
        lastSearchItemSelected.removeClass('focus-item');
    $(this).addClass('focus-item');
    lastSearchItemSelected = $(this);
});

var itemsSearch = [];
$('#search-bar').on('input', function() {
    var val = $(this).val();
    itemsSearch = [];
    if(val != '') {
        $(".search-system").removeClass('min-alto-search');
        $('#search-image-datos').attr('src', '');
        $('#search-nombre-datos').html('');
        $('#contenedor-search-extra-data').html('');

        $.ajax({
            dataType: "json",
            cache: false,
            method: "POST",
            url: baseUrl+'/api/headSearch',
            data: {
                _token: token,
                term: val
            }
        })
        .done(function( json ) {
            $("#items-search").html("");
            var lastCategory = null;
            $.each(json, function(index, item) {
                itemsSearch.push(item);
                if(lastCategory != item.category){
                    lastCategory = item.category;
                    $("#items-search").append('<div class="col-md-12 separador-search">'+ item.category +'</div>');
                }

                $("#items-search").append('<div class="col-md-12 separador-item item-search-list" data-id="' + item.id + '">'
                +'<img src="' + item.img + '" />'
                +'<span>' + item.label + '</span>'
                +'</div>');

            });
        });
    }else{
        $("#items-search").html("");
        $(".search-system").addClass('min-alto-search');

        $('#search-image-datos').attr('src', '');
        $('#search-nombre-datos').html('');
        $('#contenedor-search-extra-data').html('');
    }
});

$(".search-system").on('click', '.item-search-list', function(){
    var id = $(this).attr('data-id');
    $('#search-image-datos').attr('src', itemsSearch[id].img);
    $('#search-nombre-datos').html(itemsSearch[id].label);

    $('#contenedor-search-extra-data').html('');
    $.each(itemsSearch[id].extraData, function(index, value){
        var dts = '<div class="contenedor-datos-search-titulo">' + value.label + '</div>';
        $.each(value.data, function(index, innverVal){
            dts += "<div>" + innverVal + "</div>";
        });
        $('#contenedor-search-extra-data').append('<div class="contenedor-datos-search">' + dts + '</div>');
    });
});



$.extend({
    redirectPost: function(location, args)
    {
        var form = '';
        $.each( args, function( key, value ) {
            form += '<input type="hidden" name="'+key+'" value="'+value+'">';
        });
        $('<form action="'+location+'" method="POST">'+form+'</form>').appendTo('body').submit();
    }
});

if($(".profile").hasClass('minimal-sidebar')){
    $(".sidebar").addClass('minimal-menu-sidebar');
    $(".main").addClass('minimal-menu-main');
}

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    $($(this).attr('data-hide')).hide();
    $(target).show();
});

$('.hidden-menu-lat').click(function(){
    if($("#quickTask").is(":visible")) {
        $("#quickTask").hide(100);
        $("#icon-menu-hidden-lat").removeClass('fa-indent').addClass('fa-outdent');
    }else{
        $("#quickTask").show(250);
        $("#icon-menu-hidden-lat").addClass('fa-indent').removeClass('fa-outdent');
    }

});

$(document).on("mouseover", 'a img', function() {
    $(this).tooltip({
        selector: '[rel=tooltip]'
    });
});

wizardEnable = false;

/*$(document).on('click', '.btn', function(){
    var objeto = $(this).html();
    $.ajax({
        method: "POST",
        url: baseUrl+'/api/stats',
        data: {
            _token: token,
            from: window.location.pathname,
            objeto: objeto
        }
    });
});*/