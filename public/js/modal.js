/**
 * Created by cristian on 16/06/2015.
 */

function loaderAltModal(toPage, Titulo, showContenido, Aceptar, TextoAceptar, Cancelar, TextoCancelar){
    if(jQuery.browser.mobile) {
        if(toPage != ''){
            $.redirectPost(baseUrl + '/api/modalalt', {_token: token,
                                                        from: document.URL,
                                                        to: toPage,
                                                        titulo: Titulo,
                                                        showContenido: showContenido,
                                                        botonAceptar: Aceptar,
                                                        textoAceptar: TextoAceptar,
                                                        botonCancelar: Cancelar,
                                                        textoCancelar: TextoCancelar});
            return true;
        }
    }
    return false;
}

var nextModal = 0;
var superObject = [];

function loadTemporal(){
    var temporalLoading = '<div style="width: 400px !important; max-width: 100% !important; margin: auto; z-index: 9999999;" id="modal-'+nextModal+'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><img id="imagenLoading'+nextModal+'" style="max-width:100%; max-height:100%;" src="'+assetLoading+'" alt=""/> </div></div></div>';
    $('body').prepend(temporalLoading);
    $('#modal-' + nextModal).modal({show: true, keyboard: false});
    if(!jQuery.browser.mobile) {
        $('.container-fluid').addClass('blur');
    }

    return nextModal;
}

function deleteTemporal(id){
    $('#modal-' + id).modal('hide');
    $('#modal-' + id).remove();
}

function loadErrorLoading(id, img){
    $("#imagenLoading"+id).attr('src', baseUrl+"/images/"+img);

    $("#imagenLoading"+id).click(function(){
        deleteTemporal(id);
    });
}

function newModalRefresh(Titulo, URLcontenido, showContenido, Aceptar, TextoAceptar, Cancelar, TextoCancelar, URLmodal, token, extraWidth, reloadTable, fromModal) {
    if(loaderAltModal(URLcontenido, showContenido, Aceptar, TextoAceptar, Cancelar, TextoCancelar))
        return true;

    if (typeof fromModal === "undefined" || fromModal === null) {
        fromModal = -1;
    }

    superObject[nextModal] = {'Titulo' : Titulo,
        'URLcontenido' : URLcontenido,
        'showContenido' : showContenido,
        'Aceptar' : Aceptar,
        'TextoAceptar' : TextoAceptar,
        'Cancelar' : Cancelar,
        'TextoCancelar' : TextoCancelar,
        'URLmodal' : URLmodal,
        'token' : token,
        'extraWidth' : extraWidth,
        'reloadTable' :reloadTable,
        'fromModal' : fromModal,
        'special' : true
    };

    var tmp = loadTemporal();
    $.ajax({
        method: "GET",
        url: URLcontenido,
        data: {
            _token: token,
            actualID: nextModal
        },
        statusCode: {
            403: function(xhr) {
                loadErrorLoading(tmp, "error.403.png");
            },
            500: function(xhr) {
                loadErrorLoading(tmp, "error.500.png");
            }
        }
    })
    .done(function (dataContent) {
        $.ajax({
            method: "POST",
            url: URLmodal,
            data: {
                _token: token,
                actualID: nextModal,
                titulo: Titulo,
                contenido: dataContent,
                showContenido: showContenido,
                botonAceptar: Aceptar,
                textoAceptar: TextoAceptar, //o texto "Enviar Form" o "Guardar" ...
                botonCancelar: Cancelar,
                textoCancelar: TextoCancelar,
                tableReload: reloadTable
            }
        })
        .done(function (msg) {
            deleteTemporal(tmp);
            $('body').prepend(msg);
            $('#modal-' + nextModal).modal({show: true, keyboard: false});

            var maxWidth = $(window).width() - 170;
            if(extraWidth > maxWidth){
                extraWidth = maxWidth;
            }

            if(extraWidth > 600)
                $('#modal-' + nextModal).attr('style', 'width: '+extraWidth+'px !important; margin: auto; z-index: '+ (1050 + nextModal + 1)+';');
            else
                $('#modal-' + nextModal).attr('style', 'width: 600px !important; margin: auto; z-index: '+ (1050 + nextModal + 1)+';');

            $('#backdrop-modal-' + nextModal).attr('style', 'z-index: '+ (1050 + nextModal)+';');
            ++nextModal;
        })
        .fail(function() {
                loadErrorLoading(tmp, "error.png");
        });
    });
}

//newModal('Ejemplo de titulo', '{{ url('home/api/test') }}', true, true, "Aceptar", true, "Cerrar", '{{ url('/api/modal') }}','{{ csrf_token() }}' );
function newModal(Titulo, URLcontenido, showContenido, Aceptar, TextoAceptar, Cancelar, TextoCancelar, URLmodal, token, extraWidth, reloadTable, fromModal) {
    if(loaderAltModal(URLcontenido, showContenido, Aceptar, TextoAceptar, Cancelar, TextoCancelar))
        return true;

    if (typeof fromModal === "undefined" || fromModal === null) {
        fromModal = -1;
    }

    superObject[nextModal] = {'Titulo' : Titulo,
                        'URLcontenido' : URLcontenido,
                        'showContenido' : showContenido,
                        'Aceptar' : Aceptar,
                        'TextoAceptar' : TextoAceptar,
                        'Cancelar' : Cancelar,
                        'TextoCancelar' : TextoCancelar,
                        'URLmodal' : URLmodal,
                        'token' : token,
                        'extraWidth' : extraWidth,
                        'reloadTable' :reloadTable,
                        'fromModal' : fromModal
                    };

    var tmp = loadTemporal();
    $.ajax({
        method: "GET",
        url: URLcontenido,
        data: {
            _token: token,
            actualID: nextModal
        },
        statusCode: {
            403: function(xhr) {
                loadErrorLoading(tmp, "error.403.png");
            },
            500: function(xhr) {
                loadErrorLoading(tmp, "error.500.png");
            }
        }
    })
    .done(function (dataContent) {
        $.ajax({
            method: "POST",
            url: URLmodal,
            data: {
                _token: token,
                actualID: nextModal,
                titulo: Titulo,
                contenido: dataContent,
                showContenido: showContenido,
                botonAceptar: Aceptar,
                textoAceptar: TextoAceptar, //o texto "Enviar Form" o "Guardar" ...
                botonCancelar: Cancelar,
                textoCancelar: TextoCancelar,
                tableReload: reloadTable
            }
        })
        .done(function (msg) {
            deleteTemporal(tmp);
            $('body').prepend(msg);
            $('#modal-' + nextModal).modal({show: true, keyboard: false});

            var maxWidth = $(window).width() - 170;
            if(extraWidth > maxWidth){
                extraWidth = maxWidth;
            }

            if(extraWidth > 600)
                $('#modal-' + nextModal).attr('style', 'width: '+extraWidth+'px !important; margin: auto; z-index: '+ (1050 + nextModal + 1)+';');
            else
                $('#modal-' + nextModal).attr('style', 'width: 600px !important; margin: auto; z-index: '+ (1050 + nextModal + 1)+';');

            $('#backdrop-modal-' + nextModal).attr('style', 'z-index: '+ (1050 + nextModal)+';');
            ++nextModal;
        }).fail(function() {
                loadErrorLoading(tmp, "error.png");
        });
    })/*.fail(function() {
        loadErrorLoading(tmp);
    });*/
}

function newModalContent(Titulo, Contenido,showContenido, Aceptar, TextoAceptar, Cancelar, TextoCancelar, URLmodal, token, reloadTable) {
    $.ajax({
        method: "POST",
        url: URLmodal,
        data: {
            _token: token,
            actualID: nextModal,
            titulo: Titulo,
            contenido: Contenido,
            showContenido: showContenido,
            botonAceptar: Aceptar,
            textoAceptar: TextoAceptar, //o texto "Enviar Form" o "Guardar" ...
            botonCancelar: Cancelar,
            textoCancelar: TextoCancelar,
            tableReload: reloadTable
        }
    })
    .done(function (msg) {
        $('body').prepend(msg);
        $('#modal-' + nextModal).modal({show: true, keyboard: false});
        $('#modal-' + nextModal).attr('style', 'z-index: '+ (1050 + nextModal)+';');
        ++nextModal;
    });
}