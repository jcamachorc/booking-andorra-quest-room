/*Validador de campos*/
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}

function validLenght(data, maxLenght, minLenght){
    var status = true;
    if(minLenght != -1 && data.length < minLenght) status = false;
    else if(maxLenght != -1 && data.length > maxLenght) status = false;
    return status;
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/*
 * Tiene que recibir el cif sin espacios ni guiones
 */
function validateCIF(cif)
{
    //Quitamos el primer caracter y el ultimo digito
    var valueCif=cif.substr(1,cif.length-2);

    var suma=0;

    //Sumamos las cifras pares de la cadena
    for(i=1;i<valueCif.length;i=i+2)
    {
        suma=suma+parseInt(valueCif.substr(i,1));
    }

    var suma2=0;

    //Sumamos las cifras impares de la cadena
    for(i=0;i<valueCif.length;i=i+2)
    {
        result=parseInt(valueCif.substr(i,1))*2;
        if(String(result).length==1)
        {
            // Un solo caracter
            suma2=suma2+parseInt(result);
        }else{
            // Dos caracteres. Los sumamos...
            suma2=suma2+parseInt(String(result).substr(0,1))+parseInt(String(result).substr(1,1));
        }
    }

    // Sumamos las dos sumas que hemos realizado
    suma=suma+suma2;

    var unidad = String(suma).substr(String(suma).length - 1, 1);
    unidad=10-parseInt(unidad);

    var primerCaracter=cif.substr(0,1).toUpperCase();

    if(primerCaracter.match(/^[FJKNPQRSUVW]$/))
    {
        //Empieza por .... Comparamos la ultima letra
        if(String.fromCharCode(64+unidad).toUpperCase()==cif.substr(cif.length-1,1).toUpperCase())
            return true;
    }else if(primerCaracter.match(/^[XYZ]$/)){
        //Se valida como un dni
        var newcif;
        if(primerCaracter=="X")
            newcif=cif.substr(1);
        else if(primerCaracter=="Y")
            newcif="1"+cif.substr(1);
        else if(primerCaracter=="Z")
            newcif="2"+cif.substr(1);
        return validateDNI(newcif);
    }else if(primerCaracter.match(/^[ABCDEFGHLM]$/)){
        //Se revisa que el ultimo valor coincida con el calculo
        if(unidad==10)
            unidad=0;
        if(cif.substr(cif.length-1,1)==String(unidad))
            return true;
    }else{
        //Se valida como un dni
        return validateDNI(cif);
    }
    return false;
}

/*
 * Tiene que recibir el dni sin espacios ni guiones
 */
function validateDNI(dni)
{
    var lockup = 'TRWAGMYFPDXBNJZSQVHLCKE';
    var valueDni=dni.substr(0,dni.length-1);
    var letra=dni.substr(dni.length-1,1).toUpperCase();

    if(lockup.charAt(valueDni % 23)==letra)
        return true;
    return false;
}

/*Fin validador de campos*/

/*Ayuda de campos*/
function checkInput(input){
    var ancho = input.parent().width();
    $('.helper-modal').remove();
    var help = '';
    var error = false;
    if(isNumber(input.attr('data-min-length'))){
        if(validLenght(input.val(), -1, input.attr('data-min-length')))
            classe = 'glyphicon-ok-circle';
        else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }

        help = '<span class="glyphicon '+classe+'" aria-hidden="true"></span> Input minimo de ' + input.attr('data-min-length') + ' caracteres de longitud';
    }

    if(isNumber(input.attr('data-max-length'))){
        if(help != '') help += '<br>';

        if(validLenght(input.val(), input.attr('data-max-length'), -1))
            classe = 'glyphicon-ok-circle';
        else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }

        help += '<span class="glyphicon '+classe+'" aria-hidden="true"></span> Input maximo de ' + input.attr('data-max-length') + ' caracteres de longitud';
    }

    /*if(input.attr('type') == 'email'){
        if(isValidEmailAddress(input.val() )){
            classe = 'glyphicon-ok-circle';
        }else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }
        help += '<span class="glyphicon '+classe+'" aria-hidden="true"></span> El input debe ser de formato correo';
    }*/

    if(input.attr('data-numeric')){
        if(isNumber(input.val() )){
            classe = 'glyphicon-ok-circle';
        }else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }
        help += '<span class="glyphicon '+classe+'" aria-hidden="true"></span> El input debe ser numerico';
    }

    if(input.attr('data-numeric-inf')){
        if(isNumber(input.val()) || input.val() == '#'){
            classe = 'glyphicon-ok-circle';
        }else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }
        help += '<span class="glyphicon '+classe+'" aria-hidden="true"></span> El input debe ser numerico o #';
    }

    if(input.attr('data-cif')){
        if(validateCIF(input.val())){
            classe = 'glyphicon-ok-circle';
        }else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }
        help += '<span class="glyphicon '+classe+'" aria-hidden="true"></span> El input debe ser un cif';
    }

    if(input.attr('data-din')){
        if(validateDNI(input.val() )){
            classe = 'glyphicon-ok-circle';
        }else{
            error = true;
            classe = 'glyphicon-exclamation-sign';
        }
        help += '<span class="glyphicon '+classe+'" aria-hidden="true"></span> Formato DNI';
    }

    var offset = input.parent().offset();
    var posY = offset.top - $(window).scrollTop();
    var posX = offset.left - $(window).scrollLeft();
    var alto = 2*input.height();

    if(error)
        alert = 'danger';
    else
        alert = 'success';

    if(help != '')
        $('body').append('<div class="helper-modal alert alert-'+alert+'" style="z-index: 9999 !important; width: ' + ancho + 'px !important; position: absolute; top: '+(posY+alto) +'px; left: '+(posX) +'px;" role="alert">'+help+'</div>');

    return error;
}

$(document).on('keypress', 'input', function(e) {
    if($(this).attr('data-numeric') == 1){
        if ((e.keyCode >= 48 && e.keyCode <= 57)) { //Si es numero
            return true;
        }else{
            e.preventDefault();
        }
    }
});

$(document).on('keyup', 'input',function() {
    checkInput($(this));
});

$(document).on('click', 'input',function() {
    checkInput($(this));
});

$(document).on('blur', 'input',function() {
    $('.helper-modal').remove();
});



var scroll = false;
function wizard(){
    if(wizardEnable != true)
        return false;

    $('.ayuda-wizard').each(function(index){

        var alert = $(this).attr('data-alert');

        $(this).addClass('alert-'+alert);

        if($(this).attr('data-pad') != 'none') {
            $(this).css('padding', '10px 0px');
            $(this).css('margin', '5px 0px');
        }

        var offset = $(this).offset();
        var posY = offset.top - $(window).scrollTop();

        if(posY-70>= 0 && $(".modal-body").height()+$(".modal-body").offset().top >= posY) { //Miramos que este dentro
            var posX = offset.left - $(window).scrollLeft();
            if ($(this).attr('data-pad') != 'none') {
                var alto = $(this).height() + 20;
            } else {
                var alto = $(this).height();
            }
            var help = $(this).attr('data-wizard');

            if ($(this).attr('data-align') == 'right') {
                posX += $(this).width();
            }

            if($(this).attr('data-align') == 'left')
                var dtIg = 'border-right: 0;';
            else
                var dtIg = 'border-left: 0;';

            $('body').append('<div id="helper-' + index + '" class="helper-wizard alert alert-' + alert + '" style="display: table; ' + dtIg + ' z-index: 9999 !important; min-height: ' + alto + 'px !important; max-width: 350px !important; position: absolute; top: ' + (posY) + 'px; left: ' + (posX) + 'px;" role="alert"><span style="display: table-cell; vertical-align: middle;">' + help + '</span></div>');

            if ($(this).attr('data-align') == 'left') {
                var ancho = $('#helper-' + index).width();
                $('#helper-' + index).css('left', posX - ancho - 20);
            }
        }
    });

    if(!scroll) {
        scroll = true;
        $(".modal-body").scroll(function () {
            $('.helper-wizard').remove();
            wizard();
        });
    }
}

$(window).resize(function(){
    $('.helper-wizard').remove();
    wizard();
});
