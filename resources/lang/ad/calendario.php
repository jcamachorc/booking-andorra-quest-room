<?php

return [
    'bigtitle' => 'Reserva a Andorra Quest Room',

    'titulobono' => 'Compra val regal',
    'tipobono' => ' ',
    'b1' => 'Bono Regal contrabandistes 70€',
    'b2' => 'Bono Regal + Collar 1 Clau Andorra  85€',
    'b3' => 'Bono Regal + Collar les 7 Claus Andorra 98€',
    'b4' => 'Bono Regal + Clauer 7 Claus Andorra 98€',
    'b5' => 'Bono amic invisible 15€',

    
    'titulo' => 'Hores seleccionades sala Contrabandistes',
    'titulo2' => 'Hores seleccionades sala Quest bank',
    'subtitulo' => 'El preu per activitat és de 70 €, només grups de 2 a 5 persones. En promoció a 65 € de dilluns a dijous!',
    'subtitulo2' => 'El preu per activitat és de 80 €, només grups de 2 a 5 persones. En promoció a 75 € de dilluns a dijous!',
    'back' => '<< Anteriors 7 dies',
    'hoy' => 'Hoy',
    'further' => 'Següents 7 dies >> ',
    'horas' => 'Hores',

    'dom' => 'Dom',
    'lun' => 'Lun',
    'mar' => 'Mar',
    'mie' => 'Mie',
    'jue' => 'Jue',
    'vie' => 'Vie',
    'sab' => 'Sab',

    'cancelar' => 'Cancelar',

    'fecha' => 'Data',
    'ini' => 'Hora d`inici',
    'fin' => 'Hora de finalització',
    
    'Sala' => 'Reserva para juego',
    'Sala1' => 'CONTRABANDISTES', 
    'Sala2' => 'QUEST BANK', 
    'form' => 'Formulari de reserva',
    'facturacion' => 'Dades de facturació',
    'nombre' => 'Nom i cognoms',
    'selbono' => 'Val',
    'nombreF' => 'Nom factura',
    'DNI' => 'DNI/NRT/CIF',
    'direccion' => 'Direcció',
    'facturar' => 'Vols factura?',
    'si' => 'Si',
    'no' => 'No',
    'correo' => 'E-mail',
    'telefono' => 'Telèfon',
    'idioma' => 'Idioma',
    'n_personas' => 'Nº de participants',
    'descuento' => 'Descompte/Bono',
    'none' => 'Cap',
    'bono' => 'Val Regal',
    'cj' => 'Carnet jove',
    'ABC' => 'ABC4students',
    'nacimiento' => 'Data de naixement',
    'ncj' => 'Nº carnet jove',
    'nABC' => 'Nº carnet ABC4students',
    'codigo' => 'Codi',
    'precio' => 'Preu',
    'metodo' => 'Mètode de pagament',
    'paypal' => 'Paypal',
    'tj' => 'Targeta de crèdit',
    'goback' => 'Tornar',
    'checkout' => 'Checkout',

    'preciofinal' => 'Preu final',
    'nper' => 'Nº persones',
    'pay' => 'pagar',

    'warning' => '<p>En només tres senzills passos podràs reservar dia i hora.<p>
    				<p> <p>
    				<p>Pas 1, Escull el joc.<p>
                    <p> <p>
                    <p>Pas 2, al calendari, pots triar el dia i l`hora que et vagi millor, les de color vermell ja no estan disponibles.<p>
                    <p> <p>
                    <p> Pas 3, introdueix les teves dades per finalitzar la reserva amb targeta de crèdit. En cas de necessitar factura, indica-ho en aquest pas, i completa la resta de dades.<p>
                    <p> Rebràs un correu electrònic amb la confirmació al completar-se el pagament, i ja només quedarà esperar que arribi, ¡el dia escollit!<p>',
    
    'warningSala2' => '<p> <p>
                       <p> <p>
                       <p> <p>
                       <p> <p>
                       <p>Ho sentim molt !, pero en aquest moments , per problemes tecnics no tenim disponiblitad per aquesta sala.<p>
                       <p> <p>
                       <p> Esperem estar disponibles en breu. Si volss mes informació sobre aquest desafiament, contacta amb nosaltres en aquesta direcció.<p>
                       <p> <a href="mailto:info@andorraquestroom.com?Subject=Informacion%20QuestBank" target="_top">Adorraquestroom</a> <p>',
    
    'nomdest' => 'Nom destinatari',
    'correodest' => 'E-mail destinatari',
    'sendmail' => 'Vols que li vam enviar un correu notificant el regal?',
];
