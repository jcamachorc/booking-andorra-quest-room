<?php

return [
    'bigtitle' => 'Book Andorra Quest Room',

    'titulobono' => 'Buy a gift voucher',
    'tipobono' => ' ',
    'b1' => "Gift voucher smuggler's gang 70€",
    'b2' => 'Gift voucher + Necklace 1 key of Andorra  85€',
    'b3' => 'Gift voucher + Necklace 7 keys of Andorra 98€',
    'b4' => 'Gift voucher + key chain 7 keys of Andorra 98€',
    'b5' => 'Gift voucher invisible friend/Secret Pal 15€',

    'titulo' => 'Date and Time Selection',
    'titulo2' => 'Date and Time Selection',
    'subtitulo' => 'The price per activity is 70€, only groups of two to five people. Discounted to 65€ from Monday to Thursday!',
    'subtitulo2' => 'The price per activity is 80€, only groups of two to five people. Discounted to 75€ from Monday to Thursday!',
    
    'back' => '<< Previous 7 days',
    'hoy' => 'Today',
    'further' => 'Next 7 days >> ',
    'horas' => 'Hours',

    'dom' => 'Sun',
    'lun' => 'Mon',
    'mar' => 'Tue',
    'mie' => 'Wed',
    'jue' => 'Thu',
    'vie' => 'Fri',
    'sab' => 'Sat',

    'cancelar' => 'Cancel',

    'fecha' => 'Date',
    'ini' => 'Start Time',
    'fin' => 'Ending Time',
    
    'Sala' => 'Game',
    'Sala1' => 'SMUGGLERS', 
    'Sala2' => 'QUEST BANK', 
    'form' => 'Booking form and billing information',
    'nombre' => 'Name and Surname',
    'selbono' => 'Gift voucher',
    'DNI' => 'DNI/NRT',
    'direccion' => 'Direction (optional if you require a bill))',
    'facturar' => 'You want to recieve the bill on your mail?',
    'si' => 'Yes',
    'no' => 'No',
    'correo' => 'E-mail',
    'telefono' => 'Telephone',
    'idioma' => 'Language',
    'descuento' => 'Discount/Bonus',
    'n_personas' => 'Nº of participants',
    'none' => 'None',
    'bono' => 'Gift Voucher',
    'cj' => 'Carnet jove',
    'ABC' => 'ABC4students',
    'nacimiento' => 'Born Date',
    'ncj' => 'Nº carnet jove',
    'nABC' => 'Nº carnet ABC4students',
    'codigo' => 'Code',
    'precio' => 'Price',
    'metodo' => 'Payment Method',
    'paypal' => 'Paypal',
    'tj' => 'Credit Card',
    'goback' => 'Go back',
    'checkout' => 'Checkout',

    'preciofinal' => 'Final Price',
    'nper' => 'Number of people',
    'pay' => 'Pay',

    'warning' => '<p>In just three simple steps you can book day and time.</p>
                    <p> <p>
    				<p>Step 1, Choose de game.<p>
                    <p> <p>
                    <p>Step 2, on the calendar, you can choose the day and time that suits you best, the red are no longer available.</p>
                    <p> <p>
                    <p> Step 3, enter your details to complete the booking by credit card. In case you need invoice, please indicate in this step, and complete the remaining data.</p>
                    <p> You will receive an email confirmation upon completion of payment, and it will only wait for, I chose the day!</p>',
    
    'warningSala2' => '<p> <p>
                       <p> <p>
                       <p> <p>
                       <p> <p>
                       <p>LWe are very sorry!, But at the moment, due to technical problems we do not have availability for this room.<p>
                       <p> <p>
                       <p> We hope to be available soon. If you want more information about this challenge, contact us at this mail.<p>
                       <p> <a href="mailto:info@andorraquestroom.com?Subject=Informacion%20QuestBank" target="_top">Andorraquestroom</a> <p>',
    
    'nomdest' => 'Destination Name',
    'correodest' => 'Destination E-mail',
    'sendmail' => 'You want to recieve an e-mail notifying you the gift?',
];
