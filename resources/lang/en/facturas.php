<?php

return [
    'factura' => 'Bill',
    'facturaR' => 'Amendment Bill',
    'facturaO' => 'Original Bill',
    'fecha' => 'Date',
    'simplificada' => 'Simplifed',
    'de' => 'From',
    'para' => 'To',
    'direccion' => 'Direction',
    'cifPropio' => 'NRT',
    'dni' => 'DNI',
    'cif' => 'NRT',
    'telefono' => 'Telephone',
    'descripcion' => 'Description',
    'precio' => 'Price',
    'bonoTexto' => 'Voucher for a session at Quest Room',
    'sesionTexto' => 'Quest Room Session',
    'iva' => 'IGI',
    'subtotal' => 'Subotal',
    'descuento' => 'Discount',
    'total' => 'Total',
    'fax' => 'FAX',
    'email' => 'E-mail',
    'web' => 'Web'
];
