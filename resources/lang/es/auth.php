<?php

return [
    'problema' => 'Hay algun problema en los datos introducidos',
    'problemaSSL' => 'No puedes entrar en el sistema si no estas bajo una conexión segura',
    'clicaRedireccion' => 'Clica aqui para redirección',
    'bienvenido' => 'Bienvenido',
    'forgotten' => 'Has olvidado la contraseña?',
    'sendReset' => 'Proceder',
    'resetPWD'  => 'Resetear contraseña'
];
