<?php

return [
    'bigtitle' => 'Reserva en Andorra Quest Room',

    'titulobono' => 'Compra bono regalo',
    'tipobono' => '',
    'b1' => 'Bono Regalo contrabandistas 70€',
    'b2' => 'Bono Regalo + Collar 1 Llave Andorra  85€',
    'b3' => 'Bono Regalo + Collar les 7 Llaves Andorra 98€',
    'b4' => 'Bono Regalo + Llavero 7 Llaves Andorra 98€',
    'b5' => 'Bono amigo invisible 15€',
    
    'titulo' => 'Horas seleccionadas Contrabandistas',
    'titulo2' => 'Horas seleccionadas Quest bank',
    'subtitulo' => 'El precio por actividad es de 70€, sólo grupos de 2 a 5 personas. ¡En promoción a 65€ de lunes a jueves!',
    'subtitulo2' => 'El precio por actividad es de 80€, sólo grupos de 2 a 5 personas. ¡En promoción a 75€ de lunes a jueves!',
    'back' => '<< Anteriores 7 dias',
    'hoy' => 'Hoy',
    'further' => 'Siguientes 7 dias >> ',
    'horas' => 'Horas',

    'dom' => 'Dom',
    'lun' => 'Lun',
    'mar' => 'Mar',
    'mie' => 'Mie',
    'jue' => 'Jue',
    'vie' => 'Vie',
    'sab' => 'Sab',

    'cancelar' => 'Cancelar',

    'fecha' => 'Fecha',
    'ini' => 'Hora de inicio',
    'fin' => 'Hora de finalización',

    'Sala' => 'Reserva para juego',
    'Sala1' => 'CONTRABANDISTAS', 
    'Sala2' => 'QUEST BANK', 
    'form' => 'Formulario de reserva',
    'facturacion' => 'Datos de facturación',
    'selbono' => 'Bono',
    'nombre' => 'Nombre y apellidos',
    'nombreF' => 'Nombre factura',
    'DNI' => 'DNI/NRT/CIF',
    'direccion' => 'Dirección',
    'facturar' => '¿Quieres factura?',
    'si' => 'Si',
    'no' => 'No',
    'correo' => 'E-mail',
    'telefono' => 'Telefono',
    'idioma' => 'Idioma',
    'descuento' => 'Descuento/Bono',
    'n_personas' => 'Nº de participantes',
    'none' => 'Ninguno',
    'bono' => 'Bono regalo',
    'cj' => 'Carnet jove',
    'ABC' => 'ABC4students',
    'nacimiento' => 'Fecha de nacimiento',
    'ncj' => 'Nº carnet jove',
    'nABC' => 'Nº carnet ABC4students',
    'codigo' => 'Código',
    'precio' => 'Precio',
    'metodo' => 'Método de pago',
    'paypal' => 'Paypal',
    'tj' => 'Tarjeta de credito',
    'goback' => 'Volver',
    'checkout' => 'Checkout',

    'preciofinal' => 'Precio final',
    'nper' => 'Nº personas',
    'pay' => 'pagar',

    'warning' => '<p>En solo tres sencillos pasos podrás reservar día y hora.<p>
    				<p> <p>
    				<p>Paso 1, Elige el juego.<p>
                    <p> <p>
                    <p>Paso 2, en el calendario, puedes elegir el día y la hora que te vaya mejor, las de color rojo ya no están disponibles.<p>
                    <p> <p>
                    <p> Paso 3, introduce tus datos para finalizar la reserva mediante tarjeta de crédito. En caso de necesitar factura, indícalo en éste paso, y completa el resto de datos.<p>
                    <p> Recibirás un correo electrónico con la confirmación al completarse el pago, y ya solo quedará esperar a que llegue, ¡el día elegido!<p>',
    'warningSala2' => '<p> <p>
                       <p> <p>
                       <p> <p>
                       <p> <p>
                       <p>Lo sentimos mucho !, pero en estos momentos, por problemas tecnicos no tenemos disponiblidad para esta sala.<p>
                       <p> <p>
                       <p> Esperamos volver a estar disponibles en breve. Si deseas mas información sobre este desafio, contacta con nosotros en esta dirección.<p>
                       <p> <a href="mailto:info@andorraquestroom.com?Subject=Informacion%20QuestBank" target="_top">Andorraquestroom</a> <p>',
    'nomdest' => 'Nombre destinatario',
    'correodest' => 'E-mail destinatario',
    'sendmail' => '¿Deseas que le enviemos un correo notificandole el regalo?',
];
