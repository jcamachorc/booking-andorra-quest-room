<?php

return [
    'tituloSegmentacion' => 'Listado de segmentacion',
    'icono' => 'Icono',
    'nombre' => 'Nombre',
    'min' => 'Cantidad de ventas mínima',
    'max' => 'Cantidad de ventas máxima',
    'opciones' => 'Opciones',

    'tituloServer' =>   'Configuración Servidor Mailing',
    'host' => 'Host',
    'username' => 'Username',
    'encrypt' => 'Encriptacion',
    'puerto' => 'Puerto',
    'password' => 'Contraseña',

    'nuevo' =>'Nuevo segmento',
    'borrarConfirmar' => '¿Esta seguro de que desea borrar este Segmento?',
    'editarSegmento'    => 'Editar segmento',
    'editarMailing'     => 'Editar mailing',

    'great' => 'Genial',
    'accessHist' => 'Historial de accesos',
    'lastH' => 'Hora del ultimo acceso',
    'ubicacion' => 'Ubicación',
    'dispositivo' => 'Dispositivo',
    'accessHistSosp' => 'Historial de accesos sospechosos',
    'multiaccess' => 'Multiples accesos el ',
    'ajustesBasicos' => 'Ajustes básicos del sistema',
    'idioma' => 'Idioma',
    'tipomenu' => 'Tipo de menú',
    'completo' => 'completo',
    'minimalista' => 'minimalista',
    'borrar' => 'Borrar',
    'borrarCorreo' => 'Borrar correo',
    'infoApi' => '<h3>Sistema de autentificacion Oauth para la API</h3>
                        <p>Disponemos de un sistema de API para la utilización de la plataforma usando un servicio RESTFull.</p>
                        <p>Dicho sistema esta protegido por una autentificación de credenciales de la plataforma Oauth V2.0</p>
                        <p>Para la utilización del sistema, es preciso conocer una serie de claves y autorizaciones</p>',
    'enableapi' => 'Activar el sistema Oauth para este usuario',
    'infouseapi' => 'Si utilizas nuestra aplicación para moviles, simplemente debes escanear este cádigo con la misma',
    'infoadv' => 'Ver información avanzada',
    'infoavanzada' => 'Información avanzada',
    'getToken' => 'Obtención del token',
    'urlGetToken' => ' Url para la obtención del token',
    'method' => 'Método:',
    'neededParam' => 'Parametros necesarios. Deben ser enviados en las cabeceras',
    'renoveToken' => 'Renovación del token',
    'urlToken' => 'Url para la renovación del token',
];
