<?php

return [
    'titulo' => 'Listado de usuarios',

    'nombre' => 'Nombre',
    'email' => 'Email',
    'imei' => 'IMEI',
    'created_at' => 'Fecha de creación',
    'opciones' => 'Opciones',

    'addnew' => 'Añadir nuevo usuario',
    'deluser' => 'Borrar usuario?',
    'edituser' => 'Editar Usuario',

    'nombrecompleto' => 'Nombre completo',
    'correo' => 'Correo',
    'contrasena' => 'Contraseña',
    'extension' => 'extension',
    'selfile' => 'Seleccionar imagen',
    'franquicia' => 'Franquicia a la que pertenece',
    'jefeseccion' => 'Es jefe de sección?',
    'queseccion' => '¿De qué sección?',
    'browse'    => 'Browse …',
    'roles' =>'Roles',
    'newrol' => 'Nuevo Rol',

    'errorDelAdminRol' => 'Error, no se puede borrar el rol Admin',
    'delSusbsRol'   => 'Seleccionar el Rol de subtitución',
    'notaDelRol'    => 'Nota: Los usuarios que tengan el rol a borrar, se les cambiara al seleccionado',

    'errorEditAdminRol' => 'Error, no se puede editar el rol Admin',
    'nombreRol' => 'Nombre del rol',
    'permisosRol'   => 'Permisos del rol',

    'systemRols' => 'Roles del sistema',
    'rol'   => 'Rol',

    'confirmDelRol' => 'Confirmar para borrar rol',
    'editRol'   => 'Editar el rol',

];
