<?php

return [
    'factura' => 'Factura',
    'facturaR' => 'Factura rectificativa',
    'facturaO' => 'Factura original',
    'fecha' => 'Fecha',
    'simplificada' => 'simplificada',
    'de' => 'De',
    'para' => 'Para',
    'direccion' => 'Dirección',
    'cifPropio' => 'NRT',
    'dni' => 'DNI',
    'cif' => 'NRT',
    'telefono' => 'Telefono',
    'descripcion' => 'Descripcion',
    'precio' => 'Precio',
    'bonoTexto' => 'Bono para una sesión de juego quest room',
    'sesionTexto' => 'Sesión de juego quest room',
    'iva' => 'IGI',
    'subtotal' => 'Subotal',
    'descuento' => 'Descuento',
    'total' => 'Total',
    'fax' => 'FAX',
    'email' => 'E-mail',
    'web' => 'Web'
];
