<?php

return [
    /*Tablas*/
    "sProcessing"   =>      "Procesando...",
    "sLengthMenu"   =>      "Mostrar _MENU_ registros",
    "sZeroRecords"  =>      "No se encontraron resultados",
    "sEmptyTable"   =>      "Ningún dato disponible en esta tabla",
    "sInfo"         =>      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
"sInfoEmpty"        =>      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered" =>      "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix"  =>      "",
    "sSearch"       =>      "Buscar:",
    "sUrl"          =>      "",
    "sInfoThousands"=>      ",",
    "sLoadingRecords"   =>  "Cargando...",
    "sFirst"        =>      "<i class='glyphicon glyphicon-fast-backward'></i>",
    "sLast"         =>      "<i class='glyphicon glyphicon-fast-forward'></i>",
    "sNext"         =>      "<i class='glyphicon glyphicon-step-forward'></i>",
    "sPrevious"     =>      "<i class='glyphicon glyphicon-step-backward'></i>",
    "sSortAscending"=>      ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending"   =>  ":Activar para ordenar la columna de manera descendente",

    /*Otros*/

    "new" => "Nuevo",
    "add" => "Añadir",
    "cerrar" => "Cerrar",
    "cancelar" => "Cancelar",
    "guardar" => "Guardar",
    "borrar" => "Borrar",
    'aceptar' => 'Aceptar',

    "search" => 'Buscar en el sistema...',

    'filtros' => 'Filtros',


];
