<?php

return [
    'bigtitle' => 'Réserve Andorra Quest Room',

    'titulobono' => 'Achete Chèque-Cadeau',
    'tipobono' => ' ',
    'b1' => 'Chèque-Cadeau contrebandiers 70€',
    'b2' => 'Chèque-Cadeau + collier 1 clé Andorre 85€',
    'b3' => 'Chèque-Cadeau + collier le 7 Clés Andorre 98€',
    'b4' => 'Chèque-Cadeau + porte-clés 7 Clés Andorre 98€',
    'b5' => 'Chèque-Cadeau ami invisible 15€',
    
    'titulo' => 'Heures selectionées',
    'titulo2' => 'Heures selectionées',
    'subtitulo' => 'Le prix par activité est de 70€, seuls les groupes de deux à cinq personnes. À prix réduits à 65€ du lundi au jeudi!',
    'subtitulo2' => 'Le prix par activité est de 80€, seuls les groupes de deux à cinq personnes. À prix réduits à 75€ du lundi au jeudi!',
    'back' => '<< Précédent 7 jours',
    'hoy' => 'Aujourd hui',
    'further' => '7 prochains jours >> ',
    'horas' => 'Heures',

    'dom' => 'Dimanche',
    'lun' => 'Lundi',
    'mar' => 'Mardi',
    'mie' => 'Mercredi',
    'jue' => 'Jeudi',
    'vie' => 'Vendredi',
    'sab' => 'Samedi',

    'cancelar' => 'annuler',

    'fecha' => 'Date',
    'ini' => 'heure de début',
    'fin' => 'End time',
    
    'Sala' => 'Réservation pour jeux',
    'Sala1' => 'CONTREBANDIERS', 
    'Sala2' => 'QUEST BANK', 
    'form' => 'Réservation et facturation',
    'nombre' => 'Prénom et noms',
    'selbono' => 'Chèque-Cadeau',
    'DNI' => 'Carte d identitée et CIF',
    'direccion' => 'Direction (Optionel Facturation)',
    'facturar' => 'Voulez-vous la facture par Email?',
    'si' => 'Oui',
    'no' => 'Nom',
    'correo' => 'Email',
    'telefono' => 'Téléphone',
    'idioma' => 'Langue',
    'descuento' => 'Réduction/Pass',
    'n_personas' => 'Nº de participants',
    'none' => 'Aucun',
    'bono' => 'Chèque-Cadeau',
    'cj' => 'Carte Jeune',
    'ABC' => 'ABC4students',
    'nacimiento' => 'Date de naissance',
    'ncj' => 'Nº de la carte jeune',
    'nABC' => 'Nº de la carte ABC4students',
    'codigo' => 'Code ',
    'precio' => 'Prix',
    'metodo' => 'Moyen de Paiement',
    'paypal' => 'Paypal',
    'tj' => 'Carte de crédit',
    'goback' => 'Revenir ',
    'checkout' => 'Checkout',

    'preciofinal' => 'Prix final',
    'nper' => 'Nº personnes',
    'pay' => 'payer',

    'warning' => '<p>En seulement trois étapes simples, vous pouvez réserver le jour et l\'heure.</p>
    <p> <p>
    				<p>Étape 1, Choisissez le jeu.<p>
                    <p> <p>
                    <p>Étape 2, sur le calendrier, vous pouvez choisir le jour et l\'heure qui vous convient le mieux, le rouge ne sont plus disponibles.</p>
                    <p>Étape 3, entrez vos informations pour compléter la réservation par carte de crédit. Dans le cas où vous avez besoin facture, s\'il vous plaît indiquer dans cette étape, et de compléter les données restantes.</p>
                    <p>Vous recevrez un email de confirmation à la fin de paiement, et il sera seulement attendre, je choisis la journée!</p>',

    'warningSala2' => '<p> <p>
                       <p> <p>
                       <p> <p>
                       <p> <p>
                       <p>Nous sommes désolé !, Mais maintenant, en raison de problèmes techniques, nous avons pas disponibilité pour cette chambre.<p>
                       <p> <p>
                       <p> Nous espérons être bientôt disponible. Si vous voulez plus informations sur ce défi, contactez-nous à cette adresse.<p>
                       <p> <a href="mailto:info@andorraquestroom.com?Subject=Informacion%20QuestBank" target="_top">Andorraquestroom</a> <p>',
    
    'nomdest' => 'Prénom du destinataire',
    'correodest' => 'Email destinataire',
    'sendmail' => 'Souhaitez-vous que nous lui envoyons un Email en lui dissant le cadeau?',
];
