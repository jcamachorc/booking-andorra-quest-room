<?php

return [
    'factura' => 'Facture',
    'facturaR' => 'Facture modifié',
    'facturaO' => 'Facture originale',
    'fecha' => 'Date',
    'simplificada' => 'simplifié',
    'de' => 'De',
    'para' => 'Pour',
    'direccion' => 'Direction',
    'cifPropio' => 'CIFpropre',
    'dni' => 'Carte d indentité',
    'cif' => 'CIF',
    'telefono' => 'Téléphone',
    'descripcion' => 'Description',
    'precio' => 'Prix',
    'bonoTexto' => 'Chèque cadeau pour une Quest Room de session',
    'sesionTexto' => 'Session de Quest Room',
    'iva' => 'TVA',
    'subtotal' => 'Subtotal',
    'descuento' => 'Réduction',
    'total' => 'Total',
    'fax' => 'FAX',
    'email' => 'E-mail',
    'web' => 'Web'
];
