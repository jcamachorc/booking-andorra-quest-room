<div class="row-fluid" style="...">
    {!! Form::open(array('url' => 'api/Configuracion/ajustes/mail/editar', 'method' => 'put', 'files'=> true)) !!}
        <div class="col-md-12">

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_usuarios.nombrecompleto');?> <span style="color:red;">* </span></span>
                             {!! Form::hidden('txtId', $MailingData['id']) !!}
                            {!! Form::text('txtNombre', $MailingData['name'], array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>



                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.host');?><span style="color:red;">* </span></span>
                            {!! Form::text('txtHost',  $MailingData['host'], array('class' => 'form-control', 'placeholder' => 'eMail', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.username');?><span style="color:red;">* </span></span>
                            {!! Form::text('txtUsername',  $MailingData['username'], array('class' => 'form-control', 'placeholder' => 'Dirección', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.password');?></span>
                                {!! Form::password('txtPassword', array('class' => 'form-control', 'placeholder' => 'Password', 'aria-describedby' => 'basic-addon1')) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.encrypt');?><span style="color:red;">* </span></span>
                            {!! Form::select('txtEncriptacion', array ('tls' => 'tls', 'ssl' => 'ssl'), $MailingData['encrypt'], array('class' => 'form-control', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            {!! Form::hidden('txtPuerto',  $MailingData['port'], array('class' => 'form-control', 'placeholder' => 'Puerto', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            <div class="clearfix"></div>
        </div>
    {!! Form::close() !!}
</div>

<script>
     $( 'select[name="txtEncriptacion"]' ).change(function() {
        var valor = $(this).val();

        if (valor == 'tls'){
            $('input[name="txtPuerto"]').val('587');
        }else{
             $('input[name="txtPuerto"]').val('25');
        }
     });
</script>