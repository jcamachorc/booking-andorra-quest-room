@extends('app')

@section('header')
    {!! Minify::stylesheet(asset('/css/segmentacion.css')) !!}
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-*-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				    <i class="glyphicon glyphicon-th-list"></i>
				    <?=Lang::get('configuracion_ajustes.tituloServer');?>
				</div>

				<div class="panel-body">
					<table id="mailing-table" class="table table-striped table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th><?=Lang::get('configuracion_ajustes.host');?></th>
                                <th><?=Lang::get('configuracion_ajustes.username');?></th>
                                <th><?=Lang::get('configuracion_ajustes.encrypt');?></th>
                                <th><?=Lang::get('configuracion_ajustes.puerto');?></th>
                                <th><?=Lang::get('configuracion_ajustes.opciones');?></th>
                            </tr>
                        </thead>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection




@section('footer')
<script>
var mailingtable;
$(function() {
      mailingtable = $('#mailing-table').DataTable({
            sPaginationType: "full_numbers",
            oLanguage:
            {
                "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
                "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
                "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
                "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
                "sInfo":           "<?=Lang::get('general.sInfo');?>",
                "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
                "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
                "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
                "sSearch":         "<?=Lang::get('general.sSearch');?>",
                "sUrl":            "<?=Lang::get('general.sUrl');?>",
                "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
                "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
                "oPaginate": {
                    "sFirst":    "<?=Lang::get('general.sFirst');?>",
                    "sLast":     "<?=Lang::get('general.sLast');?>",
                    "sNext":     "<?=Lang::get('general.sNext');?>",
                    "sPrevious": "<?=Lang::get('general.sPrevious');?>"
                },
                "oAria": {
                    "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                    "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
                }
            },
            sDom: 't',
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '<?= url('api/Configuracion/ajustes/listarMail') ?>',
            columns: [
                {data: 'host', name: 'host'},
                {data: 'username', name: 'username'},
                {data: 'encrypt', name: 'encrypt'},
                {data: 'port', name: 'port'},
                {data: 'accion', name: 'accion'},
            ]
        });


        //Gestion de edicion, creacion y borrado

        $("#mailing-table").on( "click", '.editar', function() {
            newModal('<?=Lang::get('configuracion_ajustes.editarMailing');?>',      //Titulo de la ventana
                        '{{ url('configuracion/ajustes/editarMail') }}',        //Url del contenido de la ventana -> La vista
                        true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        900,                                                    //Ancho de la ventana modal (0 -> Default;)
                        'mailingtable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

    });
</script>

@endsection