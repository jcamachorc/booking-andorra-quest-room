<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/ajustes/add', 'method' => 'post', 'files'=> true)) !!}
        <div class="col-md-12">

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.nombre');?> <span style="color:red;">* </span></span>

                           {!! Form::text('txtNombre', '', array('class' => 'form-control', 'placeholder' => 'Nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.min');?> <span style="color:red;">* </span> </span>
                            {!! Form::text('txtMin', '', array('class' => 'form-control', 'placeholder' => 'Mimino', 'aria-describedby' => 'basic-addon1', 'required', 'data-numeric')) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_ajustes.max');?> <span style="color:red;">* </span> </span>
                            {!! Form::text('txtMax', '', array('class' => 'form-control', 'placeholder' => 'Maximo', 'aria-describedby' => 'basic-addon1', 'required', 'data-numeric')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>



                <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                  {!! Form::hidden('txtIcono', '', array('class' => 'form-control', 'placeholder' => 'textIcono ', 'aria-describedby' => 'basic-addon1', 'required', 'data-error-msg' => 'Debe seleccionar una imagen')) !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                </div>


            <div class="clearfix"></div>
        </div>

        <div>
            <div class="form-group">
                <div class="col-md-12">
                    <?php foreach ($listadoIconos as $name){ ?>
                       <div class="iconos-selectables" data-img="<?=$name?>">
                            <div class="glyphicon glyphicon-trash del-image segmentacion-inner" aria-hidden="true"></div>
                            <div class="glyphicon glyphicon-ok ok-image segmentacion-inner" aria-hidden="true"></div>
                            <div class="glyphicon glyphicon-remove cross-image segmentacion-inner" aria-hidden="true"></div>
                            <div class="img" alt="" style="background-image: url('<?=$name?>'); background-size: cover;"></div>
                       </div>
                    <?php  }?>
                    <div class="clearfix"></div>
                 </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <br />
        <div class="dropzone" id="uploadIcono" style="width: 100%;"></div>
    {!! Form::close() !!}
</div>

<script>

/*Sistema de upload de imagenes drag'n'drop*/
var tk = '{{ csrf_token() }}';
var dt = '{{ url('api/Configuracion/ajustes/icono/add?_token=') }}';
var myDropzone = new Dropzone("div#uploadIcono", { url: dt + tk});
myDropzone.on("success", function(file, responseText) {
    $('.iconos-selectables').parent().prepend('<div class="iconos-selectables" data-img="/uploads/segmentacion/' + responseText +'">'+
                                                '<div class="glyphicon glyphicon-trash del-image segmentacion-inner" aria-hidden="true"></div>'+
                                                '<div class="glyphicon glyphicon-ok ok-image segmentacion-inner" aria-hidden="true"></div>'+
                                                '<div class="glyphicon glyphicon-remove cross-image segmentacion-inner" aria-hidden="true"></div>'+
                                                '<div class="img" alt="" style="background-image: url(/uploads/segmentacion/' + responseText +'); background-size: cover;"></div></div>');
    myDropzone.removeFile(file);
});
/*FIN Sistema de upload de imagenes drag'n'drop*/


var last = -1;
$(document).on("click", ".ok-image", function() {
    var url = $(this).parent().attr('data-img');
    $('input[name="txtIcono"]').val(url);

    if(last != -1){
        last.parent().removeClass('selected-image-segmentacion');
    }

    $(this).parent().addClass('selected-image-segmentacion');
    last = $(this);
});

$(document).on("click", ".cross-image", function() {
    $('input[name="txtIcono"]').val('');
    $(this).parent().removeClass('selected-image-segmentacion');
    last = -1;
});

$(document).on("click", ".del-image", function() {
    var thisDeleting = $(this).parent();
    var url = $(this).parent().attr('data-img');
    $.ajax({
      method: "delete",
      url: '{{ url("api/Configuracion/ajustes/imagen/del")  }}',
      data: { img: url, _token: '{{ csrf_token() }}'}
    })
    .done(function( msg ) {
        thisDeleting.remove();
    });
});

</script>