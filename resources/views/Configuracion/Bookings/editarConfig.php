<div class="row-fluid" style="...">
    <?= Form::open(array('url' => 'configuracion/bookings/configEditar', 'method' => 'post', 'files'=> false)) ?>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=$data['item']?> <span style="color:red;">* </span></span>
                    <?= Form::hidden('txtItem', $data['item']) ?>
                    <?php
                    if($bool){
                    ?>
                        <select name="txtValue" class="form-control">
                            <option value="1" <?=($data['value'])?'selected':''?>>Activar</option>
                            <option value="0" <?=(!$data['value'])?'selected':''?>>Desactivar</option>
                        </select>
                    <?php
                    }else{
                        echo Form::text('txtValue', $data['value'], array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) ;
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <?= Form::close() ?>
</div>