<div class="row-fluid" style="...">
    {!! Form::open(array('url' => 'configuracion/bookings/editar', 'method' => 'post', 'files'=> false)) !!}
        <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Inicio <span style="color:red;">* </span></span>
                             {!! Form::hidden('txtId', $data['id']) !!}
                            {!! Form::text('txtInicio', $data['inicio'], array('class' => 'form-control timepicker', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Fin <span style="color:red;">* </span></span>
                            {!! Form::text('txtFin', $data['fin'], array('class' => 'form-control timepicker', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Duración <span style="color:red;">* </span></span>
                            {!! Form::text('txtDuracion', $data['duracion'], array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Precio <span style="color:red;">* </span></span>
                            {!! Form::select('txtPrecio', $precios ,$data['idPrecio'], array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">¿Es festivo?</span>
                            {!! Form::checkbox('txtFestivo', $data['festivo'], ($data['festivo']==1), array('class' => 'form-control', 'style' => 'width: 50px!important;','placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <div class="clearfix"></div>
        </div>
    {!! Form::close() !!}
</div>

<script>
$(function() {
    $('.timepicker').timepicker({timeFormat:  "H:i"});
});
</script>