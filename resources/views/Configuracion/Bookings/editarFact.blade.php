<div class="row-fluid" style="...">
    {!! Form::open(array('url' => 'configuracion/bookings/editarFacturacion', 'method' => 'post', 'files'=> true)) !!}
        <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Nombre <span style="color:red;">* </span></span>
                            {!! Form::text('txtCompania', $data['compania'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">NRT <span style="color:red;">* </span></span>
                            {!! Form::text('txtCIF', $data['cif'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                 <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">IGI <span style="color:red;">* </span></span>
                            {!! Form::text('txtIVA', $data['IVA'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Dirección <span style="color:red;">* </span></span>
                            {!! Form::text('txtDireccion', $data['direccion'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Ciudad <span style="color:red;">* </span></span>
                            {!! Form::text('txtCiudad', $data['ciudad'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Estado <span style="color:red;">* </span></span>
                            {!! Form::text('txtEstado', $data['estado'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">CP <span style="color:red;">* </span></span>
                            {!! Form::text('txtCP', $data['cp'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Telefono</span>
                            {!! Form::text('txtTelefono', $data['telefono'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">FAX</span>
                            {!! Form::text('txtFAX', $data['fax'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Web <span style="color:red;">* </span></span>
                            {!! Form::text('txtWeb', $data['web'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Email <span style="color:red;">* </span></span>
                            {!! Form::text('txtEmail', $data['email'], array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                            <label class="control-label"><?=Lang::get('configuracion_usuarios.selfile');?></label>
                            <span class="file-input file-input-new">
                                <div class="file-preview ">
                                    <div class="close fileinput-remove">×</div>
                                        <div class="">
                                            <div class="file-preview-thumbnails">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                </div>
                                <div class="kv-upload-progress hide"></div>
                                <div class="input-group ">
                                   <div tabindex="-1" class="form-control file-caption  kv-fileinput-caption">
                                   <span class="file-caption-ellipsis">…</span>
                                   <div class="file-caption-name" id="filePath"></div>
                                </div>
                                <div class="input-group-btn">
                                    <div class="btn btn-primary btn-file"> <i class="glyphicon glyphicon-folder-open"></i> &nbsp;<?=Lang::get('configuracion_usuarios.browse');?>
                                    <input id="userImage" name="userImage" type="file" class="file"></div>
                                </div>
                            </div>
                        </span>
                        <?php
                        if(!empty($data['logo'])){
                        ?>
                            <div style="text-align: center">
                                <img src="<?= asset('images/'.$data['logo']); ?>"  alt="" style="width: 150px;">
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

            <div class="clearfix"></div>
        </div>
    {!! Form::close() !!}
</div>

<script>
$('#userImage').change( function(event) {
    $("#filePath").html(event.target.files[0].name);
});
</script>