<div class="row-fluid" style="...">
    {!! Form::open(array('url' => 'configuracion/bookings/editarPrecios', 'method' => 'post', 'files'=> false)) !!}
        {!! Form::hidden('txtId', $id) !!}
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-4">
                    Añadir nuevo precio <span class="glyphicon glyphicon-plus btn-xs btn-success addPrecioItem" style="cursor: pointer" aria-hidden="true"></span>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Nombre grupo <span style="color:red;">* </span></span>
                        {!! Form::text('txtNombreGrupo', $grupo['nombre'], array('class' => 'form-control', 'placeholder' => 'Nombre Grupo', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                    </div>
                </div>
            </div>
            <br /><br />
            <div class="form-group">
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Inicio</span>
                        {!! Form::text('txtIni', $grupo['inicio'], array('class' => 'form-control datepicker', 'placeholder' => 'Inicio', 'aria-describedby' => 'basic-addon1')) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Fin</span>
                        {!! Form::text('txtFin', $grupo['fin'], array('class' => 'form-control datepicker', 'placeholder' => 'Fin', 'aria-describedby' => 'basic-addon1')) !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <br /><br />
        </div>
        <div class="col-md-12" id="contenedor-items-precios">
            @if(count($data) == 0)
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Precio <span style="color:red;">* </span></span>
                            {!! Form::text('txtPrecio[]', null, array('class' => 'form-control', 'placeholder' => 'Precio', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                            <span class="input-group-addon" id="basic-addon1">&euro;</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Nº pers. <span style="color:red;">* </span></span>
                            {!! Form::text('txtPersonas[]', null, array('class' => 'form-control', 'placeholder' => 'Nº personas', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
            @foreach($data as $item)
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Precio <span style="color:red;">* </span></span>
                            {!! Form::text('txtPrecio[]', $item['precio'], array('class' => 'form-control', 'placeholder' => 'Precio', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                            <span class="input-group-addon" id="basic-addon1">&euro;</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Nº pers. <span style="color:red;">* </span></span>
                            {!! Form::text('txtPersonas[]', $item['personas'], array('class' => 'form-control', 'placeholder' => 'Nº personas', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    {!! Form::close() !!}
</div>

<script>
$('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
$('.addPrecioItem').click(function(){
    $('#contenedor-items-precios div:first').clone().find(':input').val('').end().appendTo('#contenedor-items-precios');
});
</script>