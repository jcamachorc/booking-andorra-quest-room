@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
    @if($errors->any())
        <div class="alert alert-danger">{{$errors->first()}}</div>
    @endif
	<div class="row">
	    <h3>Horarios de la semana</h3>
        <table class="table table-striped" id="diasTable">
            <thead>
                <th>Dia de la semana</th>
                <th>Inicio</th>
                <th>Fin</th>
                <th>Duracion</th>
                <th>Festivo</th>
                <th>Opciones</th>
            </thead>
            <tbody>
            <?php
            $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
            foreach ($dias as $k => $dia) {
                $data = \App\TsCalendario::where('idDia', $k+1)->get()->toArray();
            ?>
                @foreach($data as $j => $slots)
                    <tr>
                        <td>
                            <?php
                                echo $dia;
                                if(isset($data[$j-1]) && $data[$j-1]['idDia'] == $data[$j]['idDia']){
                                ?>
                                    <a href="<?=url('configuracion/bookings/del/'.$data[$j]['id'])?>">
                                        <span class="glyphicon glyphicon-minus btn-xs btn-danger" style="cursor: pointer" aria-hidden="true"></span>
                                    </a>
                                <?php
                                }else{
                                ?>
                                    <a href="<?=url('configuracion/bookings/add/'.$data[$j]['idDia'])?>">
                                        <span class="glyphicon glyphicon-plus btn-xs btn-success" style="cursor: pointer" aria-hidden="true"></span>
                                    </a>
                                <?php
                                }
                            ?>
                        </td>
                        <td><?=$slots['inicio']?></td>
                        <td><?=$slots['fin']?></td>
                        <td><?=$slots['duracion']?></td>
                        <td><?=($slots['festivo']==0)?'No':'Si'?></td>
                        <td>
                            <span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$data[$j]['id']?>" style="cursor: pointer" aria-hidden="true"></span>
                        </td>
                    </tr>
                @endforeach
            <?php
            }
            ?>
            </tbody>
        </table>
        <br>

        <h3>
            Listado de precios
            <a href="<?=url('configuracion/bookings/addGrupo/')?>">
                <span class="glyphicon glyphicon-plus btn-xs btn-success" style="cursor: pointer" aria-hidden="true"></span>
            </a>
        </h3>
        <table class="table table-bordered" id="preciosTable">
            <thead>
                <th>Nombre</th>
                <th>Opciones</th>
            </thead>
            <tbody>
                <?php
                $precios = \App\TsGrupoPrecios::all();
                foreach ($precios as $k => $precio) {
                ?>
                    <tr>
                        <td><?=$precio['nombre']?></td>
                        <td>
                            <span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$precio['id']?>" style="cursor: pointer" aria-hidden="true"></span>
                            <a href="<?=url('configuracion/bookings/delGrupo/'.$precio['id'])?>">
                                <span class="glyphicon glyphicon-minus btn-xs btn-danger" style="cursor: pointer" aria-hidden="true"></span>
                            </a>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <br />

        <h3> Configuración general </h3>
        <h4>Carnet jove</h4>
        <table class="table table-bordered configTable">
            <thead>
                <th width="60%">Descripción</th>
                <th width="20%">Valor</th>
                <th width="20%">Opciones</th>
            </thead>
            <tbody>
                <tr>
                    <?php $item = \App\TsConfigs::find('cj_enable')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['value'])?'Activo':'Inactivo'?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" data-special="bool" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('descuento_carnet_jove')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
            </tbody>
        </table>

        <h4>TPV</h4>
        <table class="table table-bordered configTable">
            <thead>
                <th width="60%">Descripción</th>
                <th width="20%">Valor</th>
                <th width="20%">Opciones</th>
            </thead>
            <tbody>
                <tr>
                    <?php $item = \App\TsConfigs::find('tpv_enable')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['value'])?'Activo':'Inactivo'?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" data-special="bool" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('tpv_clave_cliente')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('tpv_codigo_comercio')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
            </tbody>
        </table>

        <h4>PayPal</h4>
        <table class="table table-bordered configTable">
            <thead>
                <th width="60%">Descripción</th>
                <th width="20%">Valor</th>
                <th width="20%">Opciones</th>
            </thead>
            <tbody>
                <tr>
                    <?php $item = \App\TsConfigs::find('paypal_enable')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['value'])?'Activo':'Inactivo'?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" data-special="bool" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('paypal_client_id')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('paypal_client_secret')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
            </tbody>
        </table>

        <h4>Precio Bono</h4>
        <table class="table table-bordered configTable">
            <thead>
                <th width="60%">Descripción</th>
                <th width="20%">Valor</th>
                <th width="20%">Opciones</th>
            </thead>
            <tbody>
                <tr>
                    <?php $item = \App\TsConfigs::find('precio_bono')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
            </tbody>
        </table>

        <h4>General</h4>
        <table class="table table-bordered configTable">
            <thead>
                <th width="60%">Descripción</th>
                <th width="20%">Valor</th>
                <th width="20%">Opciones</th>
            </thead>
            <tbody>
                <tr>
                    <?php $item = \App\TsConfigs::find('payment_ko')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('payment_ok')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('hora_maxima')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('hora_minima')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('delay_hora_minima')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                <tr>
                    <?php $item = \App\TsConfigs::find('correo_admin')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                @if(\App\TsConfigs::find('lopd') != null)
                <tr>
                    <?php $item = \App\TsConfigs::find('lopd')->toArray(); ?>
                    <td><?=$item['descripcion']?></td>
                    <td><?=($item['encriptado'])?'<span class="btn btn-xs btn-danger">**-Datos encriptados-**</span>':$item['value']?></td>
                    <td><span class="glyphicon glyphicon-pencil btn-xs btn-warning editar" data-id="<?=$item['item']?>" style="cursor: pointer" aria-hidden="true"></span></td>
                </tr>
                @endif
            </tbody>
        </table>
        <br/>
        <h3>
            Configuración de facturación
            <a class="editar-facturacion">
                <span class="glyphicon glyphicon-pencil btn-xs btn-success" style="cursor: pointer" aria-hidden="true"></span>
            </a>
        </h3>
        <br/><br/><br/><br/>
	</div>
</div>
@endsection

@section('footer')
<script>
$(".configTable").on( "click", '.editar', function() {
    var id = $(this).attr('data-id');
    var special = $(this).attr('data-special');
    if(special == 'bool'){
        newModal('Editar configuracion',      //Titulo de la ventana
                    '{{ url('configuracion/bookings/configEditar/') }}' + '/' + id + '/true',        //Url del contenido de la ventana -> La vista
                    true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                    true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                    "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                    true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                    "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                    '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                    '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                    900,                                                    //Ancho de la ventana modal (0 -> Default;)
                    null                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                );
    }else{
        newModal('Editar configuracion',      //Titulo de la ventana
                    '{{ url('configuracion/bookings/configEditar/') }}' + '/' + id,        //Url del contenido de la ventana -> La vista
                    true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                    true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                    "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                    true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                    "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                    '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                    '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                    900,                                                    //Ancho de la ventana modal (0 -> Default;)
                    null                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                );
    }
});

$("#diasTable").on( "click", '.editar', function() {
    var id = $(this).attr('data-id');
    newModal('Editar dia',      //Titulo de la ventana
                '{{ url('configuracion/bookings/editar/') }}' + '/' + id,        //Url del contenido de la ventana -> La vista
                true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                900,                                                    //Ancho de la ventana modal (0 -> Default;)
                null                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
            );
});

$("#preciosTable").on( "click", '.editar', function() {
    var id = $(this).attr('data-id');
    newModal('Editar dia',      //Titulo de la ventana
                '{{ url('configuracion/bookings/editarPrecios/') }}' + '/' + id,        //Url del contenido de la ventana -> La vista
                true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                900,                                                    //Ancho de la ventana modal (0 -> Default;)
                null                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
            );
});

$(".editar-facturacion").on( "click", function() {
    newModal('Editar facturacion',      //Titulo de la ventana
                '{{ url('configuracion/bookings/editarFacturacion/') }}',        //Url del contenido de la ventana -> La vista
                true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                900,                                                    //Ancho de la ventana modal (0 -> Default;)
                null                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
            );
});
</script>
@endsection