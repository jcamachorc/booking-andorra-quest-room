<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/mails/add', 'method' => 'put')) !!}
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Key</span>
                {!! Form::text('key', null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Titulo</span>
                {!! Form::text('titulo', null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Contenido</span>
                {!! Form::textarea('contenido', null, array('class' => 'form-control')) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>

<script>

</script>