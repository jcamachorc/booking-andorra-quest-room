<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/mails/editar', 'method' => 'put')) !!}
        <input type="hidden" value="<?=$data->key?>" name="key">
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Titulo</span>
                {!! Form::text('titulo', $data->titulo, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Contenido</span>
                {!! Form::textarea('contenido', $data->contenido, array('class' => 'form-control contenido', 'id' => 'contenido')) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>

<script language="javascript" type="text/javascript" src="<?=asset('/js/tinymce.min.js')?>"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
    setup: function (editor) {
        editor.on('change', function () {
        editor.save();
        });
    },
    mode: "textareas",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
    + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
    height:"350px"
});
</script>