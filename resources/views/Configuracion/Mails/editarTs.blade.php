<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/mails/editarts', 'method' => 'put')) !!}
        <input type="hidden" value="<?=$data->nombre?>" name="nombre">
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Titulo</span>
                {!! Form::text('titulo_contenido', $data->titulo_contenido, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Contenido</span>
                {!! Form::textarea('contenido', nl2br($data->contenido), array('class' => 'form-control contenido', 'id' => 'contenido')) !!}
            </div>
        </div>
        <br></br>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Titulo</span>
                {!! Form::text('titulo_ingles', $data->titulo_ingles, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Ingles</span>
                {!! Form::textarea('ingles', nl2br($data->ingles), array('class' => 'form-control contenido')) !!}
            </div>
        </div>


        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Titulo</span>
                {!! Form::text('titulo_catalan', $data->titulo_catalan, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Catalan</span>
                {!! Form::textarea('catalan', nl2br($data->catalan), array('class' => 'form-control contenido')) !!}
            </div>
        </div>

        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Titulo</span>
                {!! Form::text('titulo_frances', $data->titulo_frances, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Frances</span>
                {!! Form::textarea('frances', nl2br($data->frances), array('class' => 'form-control contenido')) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>

<script language="javascript" type="text/javascript" src="<?=asset('/js/tinymce.min.js')?>"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
    setup: function (editor) {
        editor.on('change', function () {
        editor.save();
        });
    },
    mode: "textareas",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
    + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
    image_advtab: true,
    forced_root_block : "",
    force_br_newlines : true,
    force_p_newlines : false,
    height:"350px"
});
</script>
