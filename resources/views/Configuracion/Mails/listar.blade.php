@extends('app')

@section('header')
    {!! Minify::stylesheet(asset('/css/segmentacion.css')) !!}
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-*-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				    <i class="glyphicon glyphicon-th-list"></i>
				    <?=Lang::get('configuracion_ajustes.tituloServer');?>
				    <a class="btn btn-xs btn-success add"><i class="glyphicon glyphicon-plus-sign"></i> Nuevo</a>
				</div>

				<div class="panel-body">
					<table id="mailing-table" class="table table-striped table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Titulo</th>
                                <th><?=Lang::get('configuracion_ajustes.opciones');?></th>
                            </tr>
                        </thead>
                    </table>
				</div>
			</div>
		</div>

		<div class="col-*-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-th-list"></i>
                    Mailing para Reservas
                </div>

                <div class="panel-body">
                    <table id="mailingts-table" class="table table-striped table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th><?=Lang::get('configuracion_ajustes.opciones');?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
	</div>
</div>
@endsection




@section('footer')
<script>
var mailingtable;
var mailingtstable;
$(function() {
        mailingtstable = $('#mailingts-table').DataTable({
            sPaginationType: "full_numbers",
            oLanguage:
            {
                "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
                "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
                "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
                "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
                "sInfo":           "<?=Lang::get('general.sInfo');?>",
                "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
                "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
                "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
                "sSearch":         "<?=Lang::get('general.sSearch');?>",
                "sUrl":            "<?=Lang::get('general.sUrl');?>",
                "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
                "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
                "oPaginate": {
                    "sFirst":    "<?=Lang::get('general.sFirst');?>",
                    "sLast":     "<?=Lang::get('general.sLast');?>",
                    "sNext":     "<?=Lang::get('general.sNext');?>",
                    "sPrevious": "<?=Lang::get('general.sPrevious');?>"
                },
                "oAria": {
                    "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                    "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
                }
            },
            sDom: 't',
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '<?= url('api/Configuracion/mails/listarts') ?>',
            columns: [
                {data: 'nombre', name: 'nombre'},
                {data: 'accion', name: 'accion'}
            ]
        });

        $("#mailingts-table").on( "click", '.editar', function() {
            var id = $(this).attr("data-id");
            newModal('Editar contenido de los correos',                         //Titulo de la ventana
                        '{{ url('configuracion/mails/editarts/') }}/' + id,        //Url del contenido de la ventana -> La vista
                        true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        900,                                                    //Ancho de la ventana modal (0 -> Default;)
                        'mailingtstable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

      mailingtable = $('#mailing-table').DataTable({
            sPaginationType: "full_numbers",
            oLanguage:
            {
                "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
                "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
                "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
                "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
                "sInfo":           "<?=Lang::get('general.sInfo');?>",
                "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
                "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
                "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
                "sSearch":         "<?=Lang::get('general.sSearch');?>",
                "sUrl":            "<?=Lang::get('general.sUrl');?>",
                "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
                "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
                "oPaginate": {
                    "sFirst":    "<?=Lang::get('general.sFirst');?>",
                    "sLast":     "<?=Lang::get('general.sLast');?>",
                    "sNext":     "<?=Lang::get('general.sNext');?>",
                    "sPrevious": "<?=Lang::get('general.sPrevious');?>"
                },
                "oAria": {
                    "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                    "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
                }
            },
            sDom: 't',
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '<?= url('api/Configuracion/mails/listar') ?>',
            columns: [
                {data: 'key', name: 'key'},
                {data: 'titulo', name: 'titulo'},
                {data: 'accion', name: 'accion'}
            ]
        });


        //Gestion de edicion, creacion y borrado

        $("#mailing-table").on( "click", '.editar', function() {
            var id = $(this).attr("data-id");
            newModal('Editar contenido de los correos',                         //Titulo de la ventana
                        '{{ url('configuracion/mails/editar/') }}/' + id,        //Url del contenido de la ventana -> La vista
                        true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        900,                                                    //Ancho de la ventana modal (0 -> Default;)
                        'mailingtable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

        $(".add").click(function() {
            newModal('Añadir contenido de los correos',                         //Titulo de la ventana
                        '{{ url('configuracion/mails/add') }}',                 //Url del contenido de la ventana -> La vista
                        true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        900,                                                    //Ancho de la ventana modal (0 -> Default;)
                        'mailingtable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

    });
</script>

@endsection