<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/usuarios/del', 'method' => 'delete', 'files'=> true)) !!}
        {!! Form::hidden('idUser', $idusuario) !!}
        <div class="form-group" style="display: none">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Asignar a:<span style="color:red;"> *</span></span>
                    {!! Form::select('txtAlt', $users, '', array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>