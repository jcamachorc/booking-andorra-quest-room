<div class="row-fluid" style="height:100%;">
    <?php
    if($idRol == 1){
    ?>
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span id="inner-error"><?=Lang::get('configuracion_usuarios.errorDelAdminRol');?></span>
        </div>
        <script>
            $('#modal-<?=$actualId?> .btn-success').remove();
        </script>
    <?php
    }else{
    ?>
    {!! Form::open(array('url' => 'api/Configuracion/usuarios/rol/del', 'method' => 'delete', 'files'=> true)) !!}
        {!! Form::hidden('idRol', $idRol) !!}
        <div class="form-group">
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_usuarios.delSubsRol');?><span style="color:red;"> *</span></span>
                    {!! Form::select('txtAlt', $listadoRoles, '', array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                </div>
            </div>
            <div class="col-md-12">
                <?=Lang::get('configuracion_usuarios.notaDelRol');?>
            </div>
        </div>
    {!! Form::close() !!}
    <?php
    }
    ?>
</div>