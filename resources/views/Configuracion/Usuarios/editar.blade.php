<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/usuarios/editar', 'method' => 'put', 'files'=> true)) !!}
        <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_usuarios.nombrecompleto');?> <span style="color:red;">* </span></span>
                            {!! Form::hidden('txtId', $usuario['id']) !!}
                            {!! Form::text('txtNombre', $usuario['name'], array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon2"><?=Lang::get('configuracion_usuarios.correo');?> <span style="color:red;">* </span></span>
                            {!! Form::email('txtCorreo', $usuario['email'], array('class' => 'form-control', 'placeholder' => 'correo', 'aria-describedby' => 'basic-addon2', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon2"><?=Lang::get('configuracion_usuarios.contrasena');?></span>
                            {!! Form::password('txtPassword', array('class' => 'form-control', 'placeholder' => 'password', 'aria-describedby' => 'basic-addon2', 'data-max-length' => 12, 'data-min-length' => 6)) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                            <label class="control-label"><?=Lang::get('configuracion_usuarios.selfile');?></label>
                            <span class="file-input file-input-new">
                                <div class="file-preview ">
                                    <div class="close fileinput-remove">×</div>
                                        <div class="">
                                            <div class="file-preview-thumbnails">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                </div>
                                <div class="kv-upload-progress hide"></div>
                                <div class="input-group ">
                                   <div tabindex="-1" class="form-control file-caption  kv-fileinput-caption">
                                   <span class="file-caption-ellipsis">…</span>
                                   <div class="file-caption-name" id="filePath"></div>
                                </div>
                                <div class="input-group-btn">
                                    <div class="btn btn-primary btn-file"> <i class="glyphicon glyphicon-folder-open"></i> &nbsp;<?=Lang::get('configuracion_usuarios.browse');?>
                                    <input id="userImage" name="userImage" type="file" class="file"></div>
                                </div>
                            </div>
                        </span>

                        <?php if(empty($usuario['imagen'])) $usuario['imagen'] = 'default.png'; ?>

                        <img src="<?= asset('uploads/usuarios/'.$usuario['imagen']); ?>"  alt="" style="width: 70%; margin: 10px 15%;">
                    </div>
                    <div class="clearfix"></div>
                </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon2"><?=Lang::get('configuracion_usuarios.roles');?><span style="color:red;">* </span></span>
                        {!! Form::select('txtRol', $rolesArray, $usuario['idRol'], array('class' => 'form-control', 'id' => 'txtRol')); !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <?php
            foreach($permisosMenu as $key => $item){
                echo '<h4><b>'.$key.'</b></h4>';
                echo '<div class="form-group">';
                foreach($item as $key => $subpermisos){
            ?>
                <div class="col-md-6">
                    {!! Form::checkbox('permisos['.$subpermisos.']', 1, in_array($subpermisos, $permisosDef), array('id' => $subpermisos)) !!} <label><?=$key?></label>
                </div>
            <?php
                }
                echo '<div class="clearfix"></div></div>';
            }
            ?>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    {!! Form::close() !!}
</div>

<script>
$('#modal-<?=$actualId?> #userImage').change( function(event) {
    $("#modal-<?=$actualId?> #filePath").html(event.target.files[0].name);
});

<?php
$js_array = json_encode($rolesPermisos);
echo "var javascript_array = ". $js_array . ";\n";
?>

function updatePermisos(){
    var id = $("#modal-<?=$actualId?> #txtRol").val();
    var dataPermisos = javascript_array[id];

    $('#modal-<?=$actualId?> input[type="checkbox"]').each(function() {
        $(this).prop( "checked", false );
        $(this).parent().hide();
    });

    for(var item in dataPermisos){
        $("#modal-<?=$actualId?> #"+dataPermisos[item]).prop( "checked", true );
        $("#modal-<?=$actualId?> #"+dataPermisos[item]).parent().show();
    }
}

$('#modal-<?=$actualId?> #txtRol').change( function() {
    updatePermisos();

    var id = $("#modal-<?=$actualId?> #txtRol").val();
    if(id != 1){
        $('#jefeTxt').show();
    }else{
        $('#jefeTxt').hide();
    }
});

updatePermisos();
</script>