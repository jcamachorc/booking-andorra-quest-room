@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-*-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				    <i class="glyphicon glyphicon-th-list"></i>
				    <?=Lang::get('configuracion_usuarios.titulo');?>
				    <a class="btn btn-xs btn-success addUser"><i class="glyphicon glyphicon-plus-sign"></i> <?=Lang::get('general.new');?></a>
				</div>

				<div class="panel-body">
					<table id="users-table" class="table table-striped table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th><?=Lang::get('configuracion_usuarios.nombre');?></th>
                                <th><?=Lang::get('configuracion_usuarios.email');?></th>
                                <th><?=Lang::get('configuracion_usuarios.created_at');?></th>
                                <th><?=Lang::get('configuracion_usuarios.opciones');?></th>
                            </tr>
                        </thead>
                    </table>
				</div>
			</div>
			<div class="panel panel-default">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-th-list"></i>
                    <?=Lang::get('configuracion_usuarios.systemRols');?>
                    <a class="btn btn-xs btn-success addRol"><i class="glyphicon glyphicon-plus-sign"></i> <?=Lang::get('general.new');?></a>
                </div>

                <div class="panel-body">
                    <div class="panel-body">
                        <table id="roles-table" class="table table-striped table-hover dt-responsive">
                            <thead>
                                <tr>
                                    <th><?=Lang::get('configuracion_usuarios.rol');?></th>
                                    <th><?=Lang::get('configuracion_usuarios.opciones');?></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
@endsection


@section('footer')
<script>
var usersTable;
var rolesTable;
$(function() {
        usersTable = $('#users-table').DataTable({
            sPaginationType: "full_numbers",
            oLanguage:
            {
                "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
                "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
                "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
                "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
                "sInfo":           "<?=Lang::get('general.sInfo');?>",
                "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
                "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
                "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
                "sSearch":         "<?=Lang::get('general.sSearch');?>",
                "sUrl":            "<?=Lang::get('general.sUrl');?>",
                "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
                "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
                "oPaginate": {
                    "sFirst":    "<?=Lang::get('general.sFirst');?>",
                    "sLast":     "<?=Lang::get('general.sLast');?>",
                    "sNext":     "<?=Lang::get('general.sNext');?>",
                    "sPrevious": "<?=Lang::get('general.sPrevious');?>"
                },
                "oAria": {
                    "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                    "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
                }
            },
            order: [[3,'desc']],
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '<?= url('api/Configuracion/usuarios/listar') ?>',
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'accion', name: 'Opciones', orderable: false, searchable: false, sWidth: "160px"}
            ]
        });

        rolesTable = $('#roles-table').DataTable({
            sPaginationType: "full_numbers",
            oLanguage:
            {
                "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
                "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
                "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
                "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
                "sInfo":           "<?=Lang::get('general.sInfo');?>",
                "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
                "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
                "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
                "sSearch":         "<?=Lang::get('general.sSearch');?>",
                "sUrl":            "<?=Lang::get('general.sUrl');?>",
                "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
                "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
                "oPaginate": {
                    "sFirst":    "<?=Lang::get('general.sFirst');?>",
                    "sLast":     "<?=Lang::get('general.sLast');?>",
                    "sNext":     "<?=Lang::get('general.sNext');?>",
                    "sPrevious": "<?=Lang::get('general.sPrevious');?>"
                },
                "oAria": {
                    "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                    "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
                }
            },
            autoWidth: false,
            processing: true,
            serverSide: true,
            ajax: '<?= url('api/Configuracion/usuarios/listarRoles') ?>',
            columns: [
                {data: 'nombre', name: 'nombre'},
                {data: 'accion', name: 'Opciones', orderable: false, searchable: false, sWidth: "160px"}
            ]
        });

        $(".addRol").on( "click", function() {
            newModal('<?=Lang::get('configuracion_usuarios.newrol');?>',    //Titulo de la ventana
                        '{{ url('configuracion/usuarios/nuevoRol') }}',        //Url del contenido de la ventana -> La vista
                         true,                                              //Se debe mostrar el contenido que acabamos de cargar
                         true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                         "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                         true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                         "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                         '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                         '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                         750,                                               //Ancho de la ventana modal (0 -> Default;)
                         'rolesTable'                                       //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                     );
        });

        $("#roles-table").on( "click", '.borrarRol', function() {
            var id = $(this).attr("data-id");
            newModal('<?=Lang::get('configuracion_usuarios.confirmDelRol');?>',       //Titulo de la ventana
                        '{{ url('configuracion/usuarios/delRol/') }}'+ '/'+id,     //Url del contenido de la ventana -> La vista
                        true,                                                  //Se debe mostrar el contenido que acabamos de cargar -> En borrar no pq podrian modificar el id
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.borrar');?>",                    //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        0,                                                      //Ancho de la ventana modal (0 -> Default;)
                        'rolesTable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

        $("#roles-table").on( "click", '.editarRol', function() {
            var id = $(this).attr("data-id");
            newModal('<?=Lang::get('configuracion_usuarios.editRol');?>',      //Titulo de la ventana
                        '{{ url('configuracion/usuarios/editarRol/') }}'+ '/'+id,  //Url del contenido de la ventana -> La vista
                        true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        900,                                                    //Ancho de la ventana modal (0 -> Default;)
                        'rolesTable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

        //Gestion de edicion, creacion y borrado
        $(".addUser").on( "click", function() {
            newModal('<?=Lang::get('configuracion_usuarios.addnew');?>',    //Titulo de la ventana
                        '{{ url('configuracion/usuarios/nuevo') }}',        //Url del contenido de la ventana -> La vista
                         true,                                              //Se debe mostrar el contenido que acabamos de cargar
                         true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                         "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                         true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                         "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                         '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                         '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                         900,                                               //Ancho de la ventana modal (0 -> Default;)
                         'usersTable'                                       //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                     );
        });

        $("#users-table").on( "click", '.borrar', function() {
            var id = $(this).attr("data-id");
            newModal('<?=Lang::get('configuracion_usuarios.deluser');?>',       //Titulo de la ventana
                        '{{ url('configuracion/usuarios/del/') }}'+ '/'+id,     //Url del contenido de la ventana -> La vista
                        true,                                                  //Se debe mostrar el contenido que acabamos de cargar -> En borrar no pq podrian modificar el id
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.borrar');?>",                    //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        0,                                                      //Ancho de la ventana modal (0 -> Default;)
                        'usersTable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

        $("#users-table").on( "click", '.editar', function() {
            var id = $(this).attr("data-id");
            newModal('<?=Lang::get('configuracion_usuarios.edituser');?>',      //Titulo de la ventana
                        '{{ url('configuracion/usuarios/editar/') }}'+ '/'+id,  //Url del contenido de la ventana -> La vista
                        true,                                                   //Se debe mostrar el contenido que acabamos de cargar
                        true,                                                   //Debe aparecer el boton de Aceptar/Añadir...
                        "<?=Lang::get('general.guardar');?>",                   //Texto del boton Aceptar/Añadir...
                        true,                                                   //Debe aparecer el boton de Cancelar/Cerrar..
                        "<?=Lang::get('general.cancelar');?>",                  //Texto del boton Cancelar/Cerrar..
                        '{{ url('/api/modal') }}',                              //Url del diseño de la ventana modal -> Siempre es el mismo
                        '{{ csrf_token() }}',                                   //Token de seguridad -> Seimpre es el mismo
                        900,                                                    //Ancho de la ventana modal (0 -> Default;)
                        'usersTable'                                            //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                    );
        });

    });
</script>
@endsection