<div class="row-fluid" style="height:100%;">
    <div class="form-group">
        <div class="col-md-12">
            <div class="pull-right"><a class="btn btn-xs btn-success addRol"><i class="glyphicon glyphicon-tower"></i> Nuevo rol</a></div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    {!! Form::open(array('url' => 'api/Configuracion/usuarios/add', 'method' => 'post', 'files'=> true)) !!}
        <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_usuarios.nombrecompleto');?> <span style="color:red;">* </span></span>
                            {!! Form::text('txtNombre', '', array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon2"><?=Lang::get('configuracion_usuarios.correo');?> <span style="color:red;">* </span></span>
                            {!! Form::email('txtCorreo', '', array('class' => 'form-control', 'placeholder' => 'correo', 'aria-describedby' => 'basic-addon2', 'required')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon2"><?=Lang::get('configuracion_usuarios.contrasena');?> <span style="color:red;">* </span></span>
                            {!! Form::password('txtPassword', array('class' => 'form-control', 'placeholder' => 'password', 'aria-describedby' => 'basic-addon2', 'required', 'data-max-length' => 12, 'data-min-length' => 6)) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                            <label class="control-label"><?=Lang::get('configuracion_usuarios.selfile');?></label>
                            <span class="file-input file-input-new">
                                <div class="file-preview ">
                                    <div class="close fileinput-remove">×</div>
                                        <div class="">
                                            <div class="file-preview-thumbnails">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                </div>
                                <div class="kv-upload-progress hide"></div>
                                <div class="input-group ">
                                   <div tabindex="-1" class="form-control file-caption  kv-fileinput-caption">
                                   <span class="file-caption-ellipsis">…</span>
                                   <div class="file-caption-name" id="filePath"></div>
                                </div>
                                <div class="input-group-btn">
                                    <div class="btn btn-primary btn-file"> <i class="glyphicon glyphicon-folder-open"></i> &nbsp;<?=Lang::get('configuracion_usuarios.browse');?>
                                    <input id="userImage" name="userImage" type="file" class="file"></div>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon2"><?=Lang::get('configuracion_usuarios.roles');?><span style="color:red;">* </span></span>
                        {!! Form::select('txtRol', $rolesArray, null, array('class' => 'form-control', 'id' => 'txtRol')); !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <?php
            foreach($permisosMenu as $key => $item){
                echo '<h4><b>'.$key.'</b></h4>';
                echo '<div class="form-group">';
                foreach($item as $key => $subpermisos){
            ?>
                <div class="col-md-6 col-xs-3">
                    {!! Form::checkbox('permisos['.$subpermisos.']', 1, null, array('id' => $subpermisos)) !!} <label><?=$key?></label>
                </div>
            <?php
                }
                echo '<div class="clearfix"></div></div>';
            }
            ?>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    {!! Form::close() !!}
</div>

<script>
$('#modal-<?=$actualId?> #userImage').change( function(event) {
    $("#modal-<?=$actualId?> #filePath").html(event.target.files[0].name);
});

<?php
$js_array = json_encode($rolesPermisos);
echo "var javascript_array = ". $js_array . ";\n";
?>

function updatePermisos(){
    var id = $("#modal-<?=$actualId?> #txtRol").val();
    var dataPermisos = javascript_array[id];

    $('#modal-<?=$actualId?> input[type="checkbox"]').each(function() {
        $(this).prop( "checked", false );
        $(this).parent().hide();
    });

    for(var item in dataPermisos){
        $("#modal-<?=$actualId?> #"+dataPermisos[item]).prop( "checked", true );
        $("#modal-<?=$actualId?> #"+dataPermisos[item]).parent().show();
    }
}

$('#modal-<?=$actualId?> #txtRol').change( function() {
    updatePermisos();
    var id = $("#modal-<?=$actualId?> #txtRol").val();
    if(id != 1){
        $('#jefeTxt').show();
    }else{
        $('#jefeTxt').hide();
    }
});

updatePermisos();


$("#modal-<?=$actualId?> .addRol").on( "click", function() {
    newModal('<?=Lang::get('configuracion_usuarios.newrol');?>',    //Titulo de la ventana
                '{{ url('configuracion/usuarios/nuevoRol') }}',     //Url del contenido de la ventana -> La vista
                 true,                                              //Se debe mostrar el contenido que acabamos de cargar
                 true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                 "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                 true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                 "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                 '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                 '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                 750,                                               //Ancho de la ventana modal (0 -> Default;)
                 'rolesTable',                                              //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                 '<?=$actualId?>'                            //Referral modal
             );
});

</script>