<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => 'api/Configuracion/usuarios/rol/add', 'method' => 'post', 'files'=> true)) !!}
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><?=Lang::get('configuracion_usuarios.nombreRol');?><span style="color:red;">* </span></span>
                        {!! Form::text('txtNombre', '', array('class' => 'form-control', 'placeholder' => 'nombre', 'aria-describedby' => 'basic-addon1', 'required')) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <h3><?=Lang::get('configuracion_usuarios.permisosRol');?></h3><br/>
                    <?php
                    foreach($permisosMenu as $key => $item){
                        echo '<h4><b>'.$key.'</b></h4>';
                        echo '<div class="form-group">';
                        foreach($item as $key => $subpermisos){
                    ?>
                        <div class="col-md-3">
                            {!! Form::checkbox('permisos['.$subpermisos.']', 1, null, array('id' => $subpermisos)) !!} <label><?=$key?></label>
                        </div>
                    <?php
                        }
                        echo '<div class="clearfix"></div></div>';
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    {!! Form::close() !!}
</div>