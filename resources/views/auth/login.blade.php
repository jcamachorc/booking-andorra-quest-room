<?php $type = 'login'; ?>
@extends('app')

@section('header')
{!! Minify::stylesheet(array(asset('/css/login.css'))) !!}
@endsection

@section('content')
<div class="container-fluid">

    <div class="container col-xlg-2 col-lg-3 col-md-6 col-sm-10 col-xs-11">
        <div id="logo-img">
            <img src="{{ asset('images/logo.png')  }}" alt="logo" />
        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> <?=Lang::get('auth.problema');?><br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <?php
            if(!Request::secure() && false){
        ?>
            <br><br>
            <div class="alert alert-danger">
                <h2><strong>Whoops!</strong></h2>
                <h3><?=Lang::get('auth.problemaSSL');?></h3>
                <h4><a href="<?=secure_url("/auth/login")?>"><?=Lang::get('auth.clicaRedireccion');?></a></h4>
            </div>
        <?php
            }else{
        ?>
        <h1>Admin Panel</h1>
        {!! Form::open(array('url' => '/auth/login', 'method' => 'post')) !!}
            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::email('email', old('email'), array('class' => 'form-control', 'placeholder' => 'correo')) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'password')) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-5" style="text-align: right; padding-top: 5px;">
                    {!! Form::checkbox('remember', '', array('class' => 'form-control')) !!}
                </div>
                <div class="col-xs-6 remember-me" style="text-align: left;">
                    Recuerdame
                </div>
            </div>
            <br/><br/>
            <div class="form-group">
                <div class="col-md-12">
                    {!!  Form::submit('Login') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <a class="btn btn-link forget-pwd" href="{{ url('/password/email') }}"><?=Lang::get('auth.forgotten');?></a>
                </div>
            </div>
        {!! Form::close() !!}

        <?php
        }
        ?>
    </div>
</div>


<ul class="bg-bubbles">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
</ul>
@endsection
