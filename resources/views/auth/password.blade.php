@extends('app')

@section('header')
{!! Minify::stylesheet(array(asset('/css/login.css'))) !!}
@endsection

@section('content')
<div class="container-fluid">
    <div class="container col-xlg-2 col-lg-3 col-md-4 col-sm-4 col-xs-10">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> <?=Lang::get('auth.problema');?><br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <h1>Reset password</h1>
        {!! Form::open(array('url' => '/password/email', 'method' => 'post')) !!}
            <div class="form-group">
                <div class="col-md-12">
                    <input type="email" placeholder="E-Mail Address" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        <?=Lang::get('auth.sendReset');?>
                    </button>
                </div>
            </div>
        {!! Form::close() !!}


    </div>
</div>


<ul class="bg-bubbles">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
</ul>
@endsection