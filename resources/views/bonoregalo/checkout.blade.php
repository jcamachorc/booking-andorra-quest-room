@extends('calendario')

@section('contenido')
<div class="col-md-12">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title"><?=Lang::get('calendario.titulobono');?></h3>
        </div>
        <div class="panel-body">
        {!! Form::open(array('url' => 'bonoregalo/pay', 'method' => 'post', 'id' => "bigform")) !!}
            <input type="hidden" name="txtBono" value="<?=$input['txtBono']?>">
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtBono')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.selbono');?></span>
                <select name="txtBono" class="form-control" disabled>
                    <option value="0" {{ ($input['txtBono'] == '0' ? "selected":"") }}><?=Lang::get('calendario.b1');?></option>
                    <option value="1" {{ ($input['txtBono'] == '1' ? "selected":"") }}><?=Lang::get('calendario.b2');?></option>
                    <option value="2" {{ ($input['txtBono'] == '2' ? "selected":"") }}><?=Lang::get('calendario.b3');?></option>
                    <option value="3" {{ ($input['txtBono'] == '3' ? "selected":"") }}><?=Lang::get('calendario.b4');?></option>
                    <option value="4" {{ ($input['txtBono'] == '4' ? "selected":"") }}><?=Lang::get('calendario.b5');?></option>
                </select>
            </div>
            <br/> 
        
            <div class="input-group">
                <input type="hidden" name="tipobono" value="<?=$input['tipobono']?>">
                <input type="hidden" name="precioFinal" value="<?=$input['precioFinal']?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombre');?></span>
                <input type="text" class="form-control" name="txtNombre" value="<?=$input['txtNombre']?>" readonly placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtFactura')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.facturar');?></span>
                <select name="txtFactura" class="form-control" disabled>
                    <option value="0" {{ $input['txtFactura'] == '0'?'selected':'' }}><?=Lang::get('calendario.no');?></option>
                    <option value="1" {{ $input['txtFactura'] == '1'?'selected':'' }}><?=Lang::get('calendario.si');?></option>
                </select>
                <input type="hidden" name="txtFactura" value="<?=$input['txtFactura']?>">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.correo');?></span>
                <input type="email" class="form-control" name="txtEmail" value="<?=$input['txtEmail']?>" readonly placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.telefono');?></span>
                <input type="text" class="form-control" name="txtTelefono" value="<?=$input['txtTelefono']?>" readonly placeholder="Telefono" aria-describedby="basic-addon1">
            </div>
            <br/>
        
            <input type="hidden" name="txtMetodoPago" value="<?=$input['txtMetodoPago']?>">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.metodo');?></span>
                <select name="txtMetodoPago" class="form-control" disabled>
                    <option value="paypal" {{ ($input['txtMetodoPago'] == 'paypal' ? "selected":"") }}><?=Lang::get('calendario.paypal');?></option>
                    <option value="tpv" {{ ($input['txtMetodoPago'] == 'tpv' ? "selected":"") }}><?=Lang::get('calendario.tj');?></option>
                </select>
            </div>
            <hr />
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nomdest');?></span>
                <input type="text" class="form-control" name="txtNombreDest" value="{{ $input['txtNombreDest'] }}" readonly placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.correodest');?></span>
                <input type="email" class="form-control" name="txtEmailDest" readonly value="{{ $input['txtEmailDest'] }}" placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
     
                <input type="hidden" name="txtSendDest" value="<?=(isset($input['txtSendDest']))?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.sendmail');?></span>
                <span class="input-group-addon"><?=(isset($input['txtSendDest'])?'SI':'NO')?></span>
            </div>
        </div>

        <div class="panel-heading bg-primary factura-place">
            <h3 class="panel-title"><?=Lang::get('calendario.facturacion');?></h3>
        </div>
        <?php
            $display = $input['txtFactura'] == '0'?'none':'block';
        ?>
        <div class="panel-body bg-info factura-place" style="display: <?= $display?>">
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtNombreF')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombreF');?></span>
                <input type="text" class="form-control" name="txtNombreF" value="{{ isset($input['txtNombreF'])?$input['txtNombreF']:old('txtNombreF') }}" placeholder="Nombre factura" aria-describedby="basic-addon1" readonly>
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDNI')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.DNI');?></span>
                <input type="text" class="form-control" name="txtDNI" value="{{ isset($input['txtDNI'])?$input['txtDNI']:old('txtDNI') }}" placeholder="DNI/NRT/CIF" aria-describedby="basic-addon1" readonly>
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDireccion')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.direccion');?></span>
                <input type="text" class="form-control" name="txtDireccion" value="{{ isset($input['txtDireccion'])?$input['txtDireccion']:old('txtDireccion') }}" placeholder="Direccion" aria-describedby="basic-addon1" readonly>
            </div>
        </div>

        <div class="panel-footer">
            <a href="<?=url('bonoregalo')?>"><input type="button" class="btn btn-default pull-left" value="<?=Lang::get('calendario.goback');?>" /></a>
            <input type="submit" class="btn btn-success pull-right" id="payButton" value="<?=Lang::get('calendario.pay');?>" />
            <button type="button" class="btn btn-success pull-right" id="buttonLoading" style="display: none">
                <img src="http://klear.com/images/new/loader.gif" height="15px" />
            </button>
            <div class="clearfix"></div>
        </div>
        {!! Form::close() !!}

        @if($input['txtMetodoPago'] == 'tpv')
        <!-- PRUEBAS -->
          <!--<form action="https://sis-t.redsys.es:25443/sis/realizarPago" method="post" id="sbdll_send">  --> 

        <!-- PRODUCCION 
           <form action="https://sis.redsys.es/sis/realizarPago" method="post" id="sbdll_send"> 
        -->
            <form action="<?php echo REDSYS_URL; ?>" method="post" id="sbdll_send"> 
                <input type="hidden" value="HMAC_SHA256_V1" name="Ds_SignatureVersion" id="Ds_SignatureVersion">
                <input type="hidden" value="" id="Ds_MerchantParameters" name="Ds_MerchantParameters">
                <input type="hidden" value="" id="Ds_Signature" name="Ds_Signature">
                <input type="hidden" value="Bono regalo" name="Ds_Merchant_ProductDescription"/>
                <input type="hidden" value="Quest Room" name="Ds_Merchant_Titular"/>
                <div class="clearfix"></div>
            </form>
        @endif
    </div>
</div>
@endsection

@section('js')
<script>
$('#bigform').submit( function(event) {
    $('#payButton').hide();
    $('#buttonLoading').show();
    if($('#Ds_SignatureVersion').length != 0){
        event.preventDefault();
        $.ajax({
            method: "POST",
            url: "<?=url('/bonoregalo/payTPV/')?>",
            data: $('#bigform').serialize() + "& _token=<?=csrf_token()?>"
        }).done(function(response){

            $("#Ds_MerchantParameters").val(response['params']);
            $("#Ds_Signature").val(response['sign']);
            $("#sbdll_send").submit();
        }).error(function(){
            $('#buttonLoading').hide();
            $('#payButton').show();
 
        });
    }else{
        $('#bigform').submit();
    }
});
</script>
@endsection
