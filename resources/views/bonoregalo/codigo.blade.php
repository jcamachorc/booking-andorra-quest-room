<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bono Regalo Quest Room</title>
<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
<style>
	*{
		padding: 0;
		margin: 0;
		border: none;
	}

	#code{
	    margin-top: 70px;
	}

	#code p{
	    color: #26778C;
	    font-size: 30px;
        padding-bottom: 15px;
        text-align: center;
        font-weight: lighter;
        font-family: 'Open Sans', sans-serif;
	}

	@font-face {
		font-family: "LCD";
		src: url(<?=asset('fonts/lcd.ttf')?>) format("truetype");
	}

	body{
        background: #F0F3F2;
    }
    .main_slider{
        position:relative;
        text-align: center;
        height: 500px;
        width: 100%;
        overflow: hidden;
        background-color: rgba(0,0,0,.9);
        /*background: url(http://www.andorraquestroom.com/tt1.png) no-repeat rgba(0,0,0,.9) center;*/
        background: url(<?php echo APP_URL_BASE; ?>/tt1.png) no-repeat rgba(0,0,0,.9) center;
        background-position-y: -230px;
        background-attachment: fixed!important;
    }
    .main_slider img {
        margin-top: 40px;
        width: 350px;
    }
    .arrowUp {
        margin: 0 !important;
        padding: 0 !important;
        width: 100% !important;
        position: absolute;
        bottom: 0;
        left: 0;
    }
    #lcd {
        font-family: "LCD", Verdana, Tahoma;
        color: rgba(255,0,0,.7);
        text-align: center;
        font-size: 40px;
    }
    .standard-padding{
        padding: 20px 150px 50px 150px;
    }
    .notPrint{
        margin-top: 25px;
    }
    .notPrint a{
        color: #777777;
        border-top: 3px dashed #e9b415;
        border-bottom: 3px dashed #e9b415;
        text-decoration: none;
    }
    h2{
        color: #26778C;
    }
    @media screen and (max-width: 700px) {
     .standard-padding{
         padding: 0px !important;
     }
    }
</style>
</head>
<body>

    <div id="code">
    	<p>Has sido invitado para una fantastica aventura en el Quest Room Andorra</p>
        <p>Este es tu código</p>
        <div style="text-align: center">
            <span id="lcd"><?= $codigo ?></span><br /><br />
        </div>
        <!--<p class="notPrint"><a target="new" href="http://calendario.andorraquestroom.com/calendario">Intoducelo aquí y disfrutarás de una auténtica aventura</a></p>-->
        <p class="notPrint"><a target="new" href="<?php echo APP_URL_BASE; ?>/calendario">Intoducelo aquí y disfrutarás de una auténtica aventura</a></p>
        
    </div>
</body>
</html>