@extends('calendario')

@section('contenido')
<br />
<div class="col-md-12">
    @if($errors->any())
    <div class="alert alert-danger">{{$errors->first()}}</div>
    @endif

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title"><?=Lang::get('calendario.titulobono');?></h3>
        </div>
        <div class="panel-body">
            {!! Form::open(array('url' => 'bonoregalo/checkout', 'method' => 'post')) !!}
            <div class="panel-body">
                <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtBono')?'has-error':''?>">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.selbono');?></span>
                    <select name="txtBono" class="form-control">
                        <option value="0" {{ old('txtBono') == '0'?'selected':'' }}><?=Lang::get('calendario.b1');?></option>
                        <option value="1" {{ old('txtBono') == '1'?'selected':'' }}><?=Lang::get('calendario.b2');?></option>
                        <option value="2" {{ old('txtBono') == '2'?'selected':'' }}><?=Lang::get('calendario.b3');?></option>
                        <option value="3" {{ old('txtBono') == '3'?'selected':'' }}><?=Lang::get('calendario.b4');?></option>
                        <option value="4" {{ old('txtBono') == '4'?'selected':'' }}><?=Lang::get('calendario.b5');?></option>
                    </select>
                </div>
                <br/>
                
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombre');?></span>
                    <input type="text" class="form-control" name="txtNombre" value="{{ old('txtNombre') }}" placeholder="Nombre completo" aria-describedby="basic-addon1">
                </div>
                <br/>
                <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtFactura')?'has-error':''?>">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.facturar');?></span>
                    <select name="txtFactura" class="form-control">
                        <option value="0" {{ old('txtFactura') == '0'?'selected':'' }}><?=Lang::get('calendario.no');?></option>
                        <option value="1" {{ old('txtFactura') == '1'?'selected':'' }}><?=Lang::get('calendario.si');?></option>
                    </select>
                </div>
                <br/>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.correo');?></span>
                    <input type="email" class="form-control" name="txtEmail" value="{{ old('txtEmail') }}" placeholder="E-mail" aria-describedby="basic-addon1">
                </div>
                <br/>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.telefono');?></span>
                    <input type="number" class="form-control" name="txtTelefono" value="{{ old('txtTelefono') }}" placeholder="Telefono" aria-describedby="basic-addon1">
                </div>
                <br/>
      
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.metodo');?></span>
                    <select name="txtMetodoPago" class="form-control">
                        @if(getConfig('paypal_enable'))
                            <option value="paypal" {{ old("txtMetodoPago") == 'paypal' ? "selected":"" }}><?=Lang::get('calendario.paypal');?></option>
                        @endif
                        @if(getConfig('tpv_enable'))
                            <option value="tpv" {{ old("txtMetodoPago") == 'tpv' ? "selected":"" }}><?=Lang::get('calendario.tj');?></option>
                        @endif
                    </select>
                </div>
                <hr />
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nomdest');?></span>
                    <input type="text" class="form-control" name="txtNombreDest" value="{{ old('txtNombreDest') }}" placeholder="Nombre completo" aria-describedby="basic-addon1">
                </div>
                <br/>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.correodest');?></span>
                    <input type="email" class="form-control" name="txtEmailDest" value="{{ old('txtEmailDest') }}" placeholder="E-mail" aria-describedby="basic-addon1">
                </div>
                <br/>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.sendmail');?></span>
                    <span class="input-group-addon">
                        <input type="checkbox" name="txtSendDest" />
                    </span>
                </div>
            </div>

            <div class="panel-heading bg-primary factura-place">
                <h3 class="panel-title"><?=Lang::get('calendario.facturacion');?></h3>
            </div>
            <div class="panel-body bg-info factura-place">
                <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtNombreF')?'has-error':''?>">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombreF');?></span>
                    <input type="text" class="form-control" name="txtNombreF" value="{{ isset($inputs['txtNombreF'])?$inputs['txtNombreF']:old('txtNombreF') }}" placeholder="Nombre factura" aria-describedby="basic-addon1">
                </div>
                <br/>
                <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDNI')?'has-error':''?>">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.DNI');?></span>
                    <input type="text" class="form-control" name="txtDNI" value="{{ isset($inputs['txtDNI'])?$inputs['txtDNI']:old('txtDNI') }}" placeholder="DNI/NRT/CIF" aria-describedby="basic-addon1">
                </div>
                <br/>
                <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDireccion')?'has-error':''?>">
                    <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.direccion');?></span>
                    <input type="text" class="form-control" name="txtDireccion" value="{{ isset($inputs['txtDireccion'])?$inputs['txtDireccion']:old('txtDireccion') }}" placeholder="Direccion" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" class="btn btn-success pull-right" value="<?=Lang::get('calendario.pay');?>" />
                <div class="clearfix"></div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php $fc = old('txtFactura'); echo ((empty($fc))?"$('.factura-place').hide(0);":'');?>
<?php $fc = old('txtFactura'); echo (($fc == 1)?"$('.factura-place').show(0);":'');?>

$('select[name="txtBono"]').on('change', function() {
    selectedItem(this.value);
});

$(function() {
    selectedItem($('select[name="txtBono"]').val());
});

    
    $('select[name="txtFactura"]').on('change', function() {
    if(this.value == '0'){
        $('.factura-place').hide(500);
    }else{
        $('.factura-place').show(250);
    }
});

//$('select[name="txtDescuento"]').on('change', function() {
//    selectedItem(this.value);
//});

//function selectedItem(value){
//    $('input[name="txtFechaNacimiento"]').parent().hide();
//    $('input[name="txtCarnetJove"]').parent().hide();
//    if(value == 'carnetjove'){
//        $('input[name="txtFechaNacimiento"]').parent().show();
//        $('input[name="txtCarnetJove"]').parent().show();
//    }
//}

//$(function() {
//    selectedItem($('select[name="txtDescuento"]').val());
//    $('#datepicker').datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date(1991, 01, 01), changeYear: true});
//});
</script>
@endsection
