<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => '/booking/bonos/addBono', 'method' => 'post')) !!}
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Descuento</span>
                {!! Form::input('number','txtDescuento', 0, array('class' => 'form-control')) !!}
                <span class="input-group-addon">%</span>
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Código</span>
                {!! Form::text('txtCodigo', $codigo, array('class' => 'form-control', 'readonly' => 'readonly')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Fecha caducidad</span>
                {!! Form::text('txtCaducidad', $caducidad, array('class' => 'form-control datepicker1')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Ilimitado</span>
                {!! Form::checkbox('txtIlimitado', false, null, array('class' => 'form-control')) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>

<script>
$(function() {
    $('.datepicker1').datepicker({dateFormat: 'yy-mm-dd', timeFormat:  "hh:mm:ss", changeYear: true});
});

</script>
<style>
.ui-datepicker-year{
    color: #000;
}
</style>