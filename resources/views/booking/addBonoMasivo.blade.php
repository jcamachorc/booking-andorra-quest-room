<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => '/booking/bonos/addBonoMasivo', 'method' => 'post')) !!}
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Nombre identificativo</span>
                {!! Form::input('text', 'txtNombre', null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Correo a enviar factura (correo hotel)</span>
                {!! Form::input('mail', 'txtCorreo', null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Cantidad</span>
                {!! Form::input('number', 'txtCantidad', null, array('class' => 'form-control', 'step' => 1,'required')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Precio</span>
                {!! Form::input('number', 'txtPrecio', null, array('class' => 'form-control', 'step' => 'any', 'required')) !!}
                <span class="input-group-addon">&euro;</span>
            </div>
        </div>
        <hr />
        <h3>Días validos</h3>
        <?php
        $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
        foreach ($dias as $k => $dia) {
        ?>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span><?=$dia?></span>
                {!! Form::input('checkbox', 'txt'.$dia, null, array('class' => 'pull-left', 'step' => 'any', 'required')) !!}
            </div>
        </div>
        <?php
        }
        ?>
    {!! Form::close() !!}
</div>