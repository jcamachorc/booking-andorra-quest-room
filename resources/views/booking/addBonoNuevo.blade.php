<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => '/booking/bonos/addBonoNuevo', 'method' => 'post')) !!}
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Nombre destinatario</span>
                {!! Form::text('txtNombre', null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Correo destinatario</span>
                {!! Form::text('txtCorreo', null, array('class' => 'form-control', 'required')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">DNI (facturación)</span>
                {!! Form::text('txtDNI', null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Telefono (facturación)</span>
                {!! Form::text('txtTelefono', null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Nombre (facturación)</span>
                {!! Form::text('txtNombreF', null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Precio</span>
                {!! Form::text('txtPrecio', null, array('class' => 'form-control', 'step' => 'any', 'required')) !!}
                <span class="input-group-addon">&euro;</span>
            </div>
        </div>
        <!--div class="form-group has-feedback col-md-12">
          <div class"form-group has-feedback col-md-12">
            <span class="input-group-addon">Idioma</span>
              <select id="txtIdioma" name="txtIdioma" class="form-control">
                <option value="contenido">español</option>
                <option value="catala">catala</option>
                <option value="english">english</option>
                <option value="frances">français</option>
              </select>
          </div>
        </div>-->
        <div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Código</span>
                {!! Form::text('txtCodigo', $codigo, array('class' => 'form-control', 'readonly' => 'readonly')) !!}
            </div>
        </div>
        <!--<div class="form-group has-feedback col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Descuento al ser aplicado</span>
                {!! Form::input('number', 'txtDescuento', 0, array('class' => 'form-control', 'step' => 'any', 'required')) !!}
                <span class="input-group-addon">&euro;</span>
            </div>
        </div>-->
    {!! Form::close() !!}
</div>
