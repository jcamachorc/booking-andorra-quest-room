<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => '/booking/calendario/addReserva', 'method' => 'post')) !!}
        {!! Form::hidden('idDia', $id, array('class' => 'form-control')) !!}
        {!! Form::hidden('fechaini', $fecha, array('class' => 'form-control')) !!}
        {!! Form::hidden('horas', null, array('class' => 'form-control', 'id' => 'horas')) !!}
        {!! Form::hidden('duraciones', null, array('class' => 'form-control', 'id' => 'duraciones')) !!}
        <table class="table table-striped" style="text-align: center">
            <thead>
                <th style="text-align: center">Horas</th>
                <th style="text-align: center">Reserva</th>
            </thead>
            <tbody>
                @foreach($horas as $i => $hora)
                <tr>
                    <td><?=$hora?></td>
                    <td>
                    @if($ocupacion[$i])
                        <div class="tsWeeklyIconOcupado"></div></td>
                    @else
                        <div class="tsWeeklyIcon cambiable" data-id="<?=$hora?>" data-duracion="<?=$duracion[$i]?>" style="cursor: pointer"></div>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    {!! Form::close() !!}
</div>

<script>
$('.cambiable').click(function(){
    if($(this).hasClass('tsWeeklyIcon')){
        $(this).removeClass('tsWeeklyIcon');
        $(this).addClass('tsWeeklyIconActual');
    }else{
        $(this).addClass('tsWeeklyIcon');
        $(this).removeClass('tsWeeklyIconActual');
    }
    //Update listados
    var dataId = '';
    var dataDuracion = '';
    $('.cambiable').each(function() {
        if($(this).hasClass('tsWeeklyIconActual')){
            if(dataId == ""){
                dataId = $(this).attr('data-id');
            }else{
                dataId += "," + $(this).attr('data-id');
            }
            if(dataDuracion == ""){
                dataDuracion = $(this).attr('data-duracion');
            }else{
                dataDuracion += "," + $(this).attr('data-duracion');
            }
        }
    });
    $("#horas").val(dataId);
    $("#duraciones").val(dataDuracion);
});
</script>