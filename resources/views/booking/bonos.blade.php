@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
	<div class="row">

	    @if($errors->any())
        <div class="alert alert-danger">{{ $errors->first()}}</div>
        @endif

        @if(Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}}</div>
        @endif

        <div class="panel panel-warning" id="filters">
            <div class="panel-heading" style="background: #ec971f !important;">
                <i class="glyphicon glyphicon-filter"></i>
                Filtros
                <a class="btn btn-xs btn-danger pull-right filters-sec" data-id="1"><i class="glyphicon glyphicon-refresh"></i> Reset</a>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-md-2">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Destinatario</span>
                            {!! Form::select('txtEspecial', $especial, null, array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Mes</span>
                            <?php $meses = array(0 => 'Todos'); for($i = 1; $i < 13; ++$i) $meses[$i] = $i; ?>
                            {!! Form::select('txtMes', $meses, null, array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Año</span>
                            <?php $thisYear = \Carbon\Carbon::now()->year ?>
                            {!! Form::select('txtAno', array(0 => 'Todos', $thisYear-1 => $thisYear-1, $thisYear => $thisYear, $thisYear+1 => $thisYear+1), null, array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Usados</span>
                            {!! Form::select('txtUsados', array(0 => 'Todos', 1 => 'Si', 2 => 'No'), null, array('class' => 'form-control', 'placeholder' => '', 'aria-describedby' => 'basic-addon1')) !!}
                        </div>
                    </div>
                    <a href="" class="btn btn-danger pull-right" id="del-special" style="display: none" onclick="return confirm('Estas seguro de querer borrar TODOS estos bonos no usados?');">Borrar los no utilizados del destinatario</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

	    <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-th-list"></i>
                Listado de bonos
                <a class="btn btn-xs btn-success addBono"><i class="glyphicon glyphicon-plus-sign"></i> Nuevo bono</a>
                <a class="btn btn-xs btn-warning filters"><i class="glyphicon glyphicon-filter"></i> Filtros</a>

                <a class="btn btn-xs btn-warning genBono pull-right">Generar bonos masivos</a>
            </div>
            <div class="panel-body">
                <table id="reservas-table" class="table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Reserva</th>
                            <th>Fecha pago</th>
                            <th>Nombre</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>



        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-th-list"></i>
                Listado de descuentos
                <a class="btn btn-xs btn-success addDescuento"><i class="glyphicon glyphicon-plus-sign"></i> Nuevo descuento</a>
            </div>

            <div class="panel-body">
                <table id="descuentos-table" class="table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Reserva</th>
                            <th>Fecha caducidad</th>
                            <th>Descuento</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
	</div>
</div>
@endsection


@section('footer')
<script>
var ReservasTable, descuentosTable;
$(function() {
    ReservasTable = $('#reservas-table').DataTable({
        sPaginationType: "full_numbers",
        oLanguage:
        {
            "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
            "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
            "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
            "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
            "sInfo":           "<?=Lang::get('general.sInfo');?>",
            "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
            "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
            "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
            "sSearch":         "<?=Lang::get('general.sSearch');?>",
            "sUrl":            "<?=Lang::get('general.sUrl');?>",
            "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
            "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
            "oPaginate": {
                "sFirst":    "<?=Lang::get('general.sFirst');?>",
                "sLast":     "<?=Lang::get('general.sLast');?>",
                "sNext":     "<?=Lang::get('general.sNext');?>",
                "sPrevious": "<?=Lang::get('general.sPrevious');?>"
            },
            "oAria": {
                "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
            }
        },
        order: [[2,'desc']],
        autoWidth: false,
        processing: true,
        serverSide: true,
        /*ajax: '<?= url('booking/bonos/listar') ?>',*/
        ajax: {
            url: '<?= url('booking/bonos/listar') ?>',
            data: function (d) {
                d.especial = $('select[name="txtEspecial"]').val();
                d.mes = $('select[name="txtMes"]').val();
                d.ano = $('select[name="txtAno"]').val();
                d.usados = $('select[name="txtUsados"]').val();
            }
        },
        columns: [
            {data: 'codigo', name: 'codigo'},
            {data: 'idReserva', name: 'idReserva'},
            {data: 'fechaPago', name: 'fechaPago'},
            {data: 'nombre', name: 'nombre'},
            {data: 'precio', name: 'precio'},
            {data: 'accion', name: 'Opciones', orderable: false, searchable: false, sWidth: "160px"}
        ]
    });

     $('select[name="txtEspecial"], select[name="txtAno"], select[name="txtMes"], select[name="txtUsados"]').change(function() {
        ReservasTable.draw();

        if($(this).val() != 0){
            $('#del-special').show();
            $('#del-special').attr('href', '<?=url('booking/bonos/del/')?>/' + $('select[name="txtEspecial"]').val() + '/' + $('select[name="txtMes"]').val() + '/' + $('select[name="txtAno"]').val());
        }else{
            $('#del-special').hide();
            $('#del-special').attr('href', '');
        }
    });

    $('.filters-sec').click(function(){
        $('select').val(0);
        ReservasTable.draw();
        $('#del-special').hide().attr('href', '');
    });

    descuentosTable = $('#descuentos-table').DataTable({
        sPaginationType: "full_numbers",
        oLanguage:
        {
            "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
            "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
            "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
            "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
            "sInfo":           "<?=Lang::get('general.sInfo');?>",
            "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
            "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
            "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
            "sSearch":         "<?=Lang::get('general.sSearch');?>",
            "sUrl":            "<?=Lang::get('general.sUrl');?>",
            "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
            "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
            "oPaginate": {
                "sFirst":    "<?=Lang::get('general.sFirst');?>",
                "sLast":     "<?=Lang::get('general.sLast');?>",
                "sNext":     "<?=Lang::get('general.sNext');?>",
                "sPrevious": "<?=Lang::get('general.sPrevious');?>"
            },
            "oAria": {
                "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
            }
        },
        order: [[2,'desc']],
        autoWidth: false,
        processing: true,
        serverSide: true,
        ajax: '<?= url('booking/bonos/listarDesc') ?>',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'idReserva', name: 'idReserva'},
            {data: 'fechaCaducidad', name: 'fechaCaducidad'},
            {data: 'descuento', name: 'descuento'},
            {data: 'accion', name: 'Opciones', orderable: false, searchable: false, sWidth: "160px"}
        ]
    });

    $(".genBono").on( "click", function() {
            newModal('Añadir Bonos Masivos',    //Titulo de la ventana
                        '{{ url('/booking/bonos/addBonoMasivo') }}',        //Url del contenido de la ventana -> La vista
                         true,                                              //Se debe mostrar el contenido que acabamos de cargar
                         true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                         "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                         true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                         "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                         '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                         '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                         750,                                               //Ancho de la ventana modal (0 -> Default;)
                         'ReservasTable'                                    //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                     );
            });

    $(".addBono").on( "click", function() {
            newModal('Añadir bono',    //Titulo de la ventana
                        '{{ url('/booking/bonos/addBonoNuevo') }}',        //Url del contenido de la ventana -> La vista
                         true,                                              //Se debe mostrar el contenido que acabamos de cargar
                         true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                         "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                         true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                         "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                         '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                         '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                         750,                                               //Ancho de la ventana modal (0 -> Default;)
                         'ReservasTable'                                    //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                     );
        });

    $(".addDescuento").on( "click", function() {
        newModal('Añadir descuento',    //Titulo de la ventana
                    '{{ url('/booking/bonos/addBono') }}',        //Url del contenido de la ventana -> La vista
                     true,                                              //Se debe mostrar el contenido que acabamos de cargar
                     true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                     "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                     true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                     "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                     '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                     '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                     750,                                               //Ancho de la ventana modal (0 -> Default;)
                     'DescuentosTable'                                    //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                 );
    });
});
</script>
@endsection