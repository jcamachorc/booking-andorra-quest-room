@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
	<div class="row">
	    <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-th-list"></i>
                Listado de reservas
            </div>

            <div class="panel-body">
                <table id="reservas-table" class="table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Sala</th>
                            <th>Inicio</th>
                            <th>Detalles</th>
                            <th>Total</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
	</div>
</div>
@endsection


@section('footer')
<script>
var ReservasTable;
$(function() {
    ReservasTable = $('#reservas-table').DataTable({
        sPaginationType: "full_numbers",
        oLanguage:
        {
            "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
            "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
            "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
            "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
            "sInfo":           "<?=Lang::get('general.sInfo');?>",
            "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
            "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
            "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
            "sSearch":         "<?=Lang::get('general.sSearch');?>",
            "sUrl":            "<?=Lang::get('general.sUrl');?>",
            "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
            "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
            "oPaginate": {
                "sFirst":    "<?=Lang::get('general.sFirst');?>",
                "sLast":     "<?=Lang::get('general.sLast');?>",
                "sNext":     "<?=Lang::get('general.sNext');?>",
                "sPrevious": "<?=Lang::get('general.sPrevious');?>"
            },
            "oAria": {
                "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
            }
        },
        order: [[0,'desc']],
        autoWidth: false,
        processing: true,
        serverSide: true,
        ajax: '<?= url('booking/bookings/listar') ?>',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'sala', name: 'sala'},
            {data: 'inicio', name: 'inicio'},
            {data: 'nombre', name: 'nombre'},
            {data: 'precio', name: 'precio'},
            {data: 'estado', name: 'estado'},
            {data: 'accion', name: 'Opciones', orderable: false, searchable: false, sWidth: "160px"}
        ]
    });
});
</script>
@endsection