@extends('app')

@section('header')
    {!! Minify::stylesheet(asset('/css/cal.css')) !!}
@endsection

@section('content')
<?php
    $session = Session::all();
    $sala= $session['sala'];
    //var_dump($session);
?>
<div class="container">
        
	<div class="row">
        <div class="panel panel-default">
           <div class="panel-heading">
                <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtSala')?'has-error':''?>">
                    <span class="input-group-addon" id="basic-addon1">SALA</span>
                    <select name="txtSala" class="form-control">
                        <option value="0" <?php echo ($sala=='0') ? 'selected' : ''; ?> ><?=Lang::get('calendario.Sala1');?></option>
                        <option value="1" <?php echo ($sala=='1') ? 'selected' : ''; ?>><?=Lang::get('calendario.Sala2');?></option>
                    </select>
                </div> 
                <div class="panel-heading sala1-place">   
                    <h3 class="panel-title"><?php echo ($sala=='0') ? 'CONTRABANDISTAS' : 'QUEST BANK';?></h3>
                </div>
                <i class="glyphicon glyphicon-th-list"></i>
                Calendario
                <span class="pull-right header-color">
                    <a href="<?=url('/booking/calendario/moveDays/-')?>" class="btn btn-xs btn-warning"> << Anteriores 7 dias</a>
                    <a href="<?=url('/booking/calendario/moveDays/=')?>" class="btn btn-xs btn-success">Hoy</a>
                    <a href="<?=url('/booking/calendario/moveDays/+')?>" class="btn btn-xs btn-warning">Siguientes 7 dias >> </a>
                </span>
            </div>
            <div class="panel-body">
                <div class="divTableSize">
                    <div class="tableInnerDivTop" style="height: 52px;">Horas</div>
                    <?php
                    if(isset($semanaArray[0]) && isset($semanaArray[0]['datos'][0]['ini'])){
                        $first = \Carbon\Carbon::parse($semanaArray[0]['datos'][0]['ini']);
                        $lastestEnding = \Carbon\Carbon::parse($semanaArray[0]['datos'][count($semanaArray[0]['datos'])-1]['end']);
                        while($first->minute%30 != 0)$first->addMinutes(1);
                        while($lastestEnding->minute%30 != 0)$lastestEnding->addMinutes(1);

                        /*$worst = \App\TsReservas::where('estado','<>', -1)->orderBy('duracion', 'DESC')->first()->toArray()['duracion'];
                        $best = \App\TsReservas::where('estado','<>', -1)->orderBy('duracion', 'ASC')->first()->toArray()['duracion'];*/

                        $durationBetween = 30;//($worst - $best == 0) ? $worst : $worst - $best;
                        while($lastestEnding->gt($first)){
                        ?>
                            <div class="tableInnerDivHoras">
                                <?=$first->format('H:i')?>
                            </div>
                        <?php
                            $Fhoraria[] = $first->format('H:i');
                            $first->addMinutes($durationBetween);
                        }
                    }
                    ?>
                </div>
                <?php
                $arrayDias = array('Dom','Lun','Mar','Mie','Jue','Vie','Sab');
                ?>
                @for($k = 0; $k < 7; ++$k)
                    <div class="divTableSize">
                        <div class="tableInnerDivTop" style="height: 52px;">
                            <?=$arrayDias[\Carbon\Carbon::parse($semanaArray[$k]['fecha'])->dayOfWeek]." ".\Carbon\Carbon::parse($semanaArray[$k]['fecha'])->format("d/m")?>
                            <a class="btn btn-xs btn-success addReserva" data-fecha="<?=\Carbon\Carbon::parse($semanaArray[$k]['fecha'])->format('Y-m-d')?>" data-id="<?=\Carbon\Carbon::parse($semanaArray[$k]['fecha'])->dayOfWeek;?>"><i class="glyphicon glyphicon-plus-sign"></i></a>
                        </div>
                        <?php
                        $keysDatos = array_keys($semanaArray[$k]['datos']);

                        for($franjaHoraria = 0; $franjaHoraria < count($semanaArray[$k]['datos']); ++$franjaHoraria){
                            
                            $keyActual = $keysDatos[$franjaHoraria];
                            if($semanaArray[$k]['datos'][$keyActual]['reserva']){
                                if($semanaArray[$k]['datos'][$keyActual]['primary']){
                                    $ini = \Carbon\Carbon::parse($semanaArray[$k]['datos'][$keyActual]['ini']);
                                    $end = \Carbon\Carbon::parse($semanaArray[$k]['datos'][$keyActual]['end']);
                            
                                    while($ini->minute%30 != 0)$ini->addMinutes(1);
                                    while($end->minute%30 != 0)$end->addMinutes(1);
                                    $duracion = $end->diffInMinutes($ini);
                                ?>
                                    <div class="<?=($semanaArray[$k]['datos'][$keyActual]['estado'] == 0)?'alert-warning':''?><?=($semanaArray[$k]['datos'][$keyActual]['estado'] == 1)?'alert-success':''?> tableInnerDiv-<?=$semanaArray[$k]['datos'][$keyActual]['rowspan']?>" style="height: <?=($duracion/$durationBetween)*50?>px; padding-top: <?=(($duracion/$durationBetween)-1)*25?>px;">
                                        <p><a href="<?=url('booking/bookings/editar/'.$semanaArray[$k]['datos'][$keyActual]['id'])?>"><?=$semanaArray[$k]['datos'][$keyActual]['nombre']?></a></p>
                                        <p><?=\Carbon\Carbon::parse($semanaArray[$k]['datos'][$keyActual]['ini'])->format("H:i")?> - <?=\Carbon\Carbon::parse($semanaArray[$k]['datos'][$keyActual]['end'])->format("H:i")?></p>
                                    </div>
                                <?php
                                }
                            }else{
                                echo '<div class="tableInnerDiv-1">---</div>';
                            }
                        }
                        ?>
                    </div>
                @endfor
                 
            </div>
        </div>
	</div>
</div>
@endsection

@section('footer')

<script>
    
<?= (isset($inputs['txtSala'])?$inputs['txtSala']:old('txtSala')) == '0'?"$('.sala1-place').show(0);":'' ?>
<?= !isset($inputs['txtSala'])?"$('.sala2-place').show(0);":'' ?>
    
$('select[name="txtSala"]').on('change', function() {
      selectedItem(this.value);

});


function selectedItem(value) {
    if(value === '1'){
        $(window.location="<?php echo APP_URL_BASE; ?>/public/index.php/booking/calendario/1");
        
        
     }else{
        $(window.location="<?php echo APP_URL_BASE; ?>/public/index.php/booking/calendario/0");
     }    
 }    
    
$(".addReserva").on( "click", function() {
    var id = $(this).attr('data-id');
    var fecha = $(this).attr('data-fecha');
    newModal('Añadir reserva en el calendario',                     //Titulo de la ventana
                '{{ url('/booking/calendario/addReserva') }}' + '/'+id+'/'+fecha,        //Url del contenido de la ventana -> La vista
                 true,                                              //Se debe mostrar el contenido que acabamos de cargar
                 true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                 "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                 true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                 "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                 '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                 '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                 750,                                               //Ancho de la ventana modal (0 -> Default;)
                 null                                               //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
             );
});
</script>
@endsection