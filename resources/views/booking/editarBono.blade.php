@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    {!! Form::open(array('url' => '/booking/bonos/editar/'.$id, 'method' => 'post')) !!}
    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Resumen</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-center-text">
                <thead>
                    <th>Fecha de compra</th>
                    <th>Código descuento</th>
                    <th>Reserva</th>
                    <th>Metodo de pago</th>
                    <th>Código de pago</th>
                    <th>Fecha caducidad</th>
                    <th>Descuento</th>
                    <th>Codigo CJ</th>
                    <th>Precio final</th>
                </thead>
                <tbody>
                    <tr>
                        <td><?=\Carbon\Carbon::parse($input['fechaPago'])->format("d/m/Y H:i");?></td>
                        <td><?=$input['codigo']?></td>
                        <?php
                        if($input['idReserva'] == null){
                        ?>
                            <td>Sin reserva</td>
                        <?php
                        }else{
                        ?>
                            <td><a href="<?=url('booking/bookings/editar/'.$input['idReserva'])?>">Reserva nº <?=$input['idReserva']?></a></td>
                        <?php
                        }
                        ?>
                        <td><?=$input['metodoPago']?></td>
                        <td><?=$input['codigoPago']?></td>
                        <td><?=\Carbon\Carbon::parse($input['fechaCaducidad'])->format("d/m/Y H:i");?></td>
                        <td><?=$input['descuento']?>%</td>
                        <td><?=$input['codigoDescuento']?></td>
                        <td><?=round($input['precio']-($input['precio']*($input['descuento']/100)),2)?>&euro;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de facturación</h3>
        </div>
        <div class="panel-body">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Nombre y apellidos</span>
                <input type="text" class="form-control" name="txtNombre" value="{{ $input['nombre'] }}" placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">DNI</span>
                <input type="text" class="form-control" name="txtDNI" value="{{ $input['DNI']  }}" placeholder="DNI" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">E-mail</span>
                <input type="email" class="form-control" name="txtEmail" value="{{ $input['email'] }}" placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Telefono</span>
                <input type="number" class="form-control" name="txtTelefono" value="{{ $input['telefono'] }}" placeholder="Telefono" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Descuento</span>
                <select name="txtDescuento" class="form-control">
                    <option value="none" {{ $input['codigoDescuento'] == '' ? "selected":"" }}>Ninguno</option>
                    <option value="carnetjove" {{ $input['codigoDescuento'] != '' ? "selected":"" }}>Carnet jove</option>
                </select>
            </div>
            <div class="input-group" style="display: none; margin-top: 15px;">
                <span class="input-group-addon" id="basic-addon1">Nº carnet jove</span>
                <input type="text" class="form-control" name="txtCarnetJove" value="{{ $input['codigoDescuento'] }}"  placeholder="Nº carnet jove" aria-describedby="basic-addon1">
            </div>
            </br>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Método de pago</span>
                <select name="txtMetodoPago" class="form-control">
                    <option value="paypal" {{ $input['metodoPago'] == 'paypal' ? "selected":"" }}>Paypal</option>
                    <option value="tpv" {{ $input['metodoPago'] == 'tpv' ? "selected":"" }}>Tarjeta de credito</option>
                </select>
            </div>
            <hr />
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Nombre destinatario</span>
                <input type="text" class="form-control" name="txtNombreDest" value="{{ $input['nombreDestinatario'] }}" placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">E-mail destinatario</span>
                <input type="email" class="form-control" name="txtEmailDest" value="{{ $input['emailDestinatario'] }}" placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Envía correo</span>
                <select name="txtSendDest" class="form-control" disabled>
                    <option value="1" {{ $input['sendMailDest'] == '1' ? "selected":"" }}>Si</option>
                    <option value="0" {{ $input['sendMailDest'] == '0' ? "selected":"" }}>No</option>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <input type="submit" class="btn btn-success pull-right" value="Guardar cambios" />
            <div class="clearfix"></div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection


@section('footer')
<script>
$('select[name="txtDescuento"]').on('change', function() {
    selectedItem(this.value);
});

function selectedItem(value){
    $('input[name="txtCodigo"]').parent().hide();
    $('input[name="txtFechaNacimiento"]').parent().hide();
    $('input[name="txtCarnetJove"]').parent().hide();
    if(value == 'carnetjove'){
        $('input[name="txtFechaNacimiento"]').parent().show();
        $('input[name="txtCarnetJove"]').parent().show();
    }else if(value == 'descuento' || value == 'bonoregalo'){
        $('input[name="txtCodigo"]').parent().show();
    }
}

$(function() {
    selectedItem($('select[name="txtDescuento"]').val());
    $('.datepicker').datepicker({dateFormat: 'yy-mm-dd', timeFormat:  "hh:mm:ss", changeYear: true});
    $('.timepicker').timepicker({'timeFormat': 'H:i' });
    $('.datepickercj').datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date(1991, 01, 01), changeYear: true});
});

var oldClass = null;
function checkEstado(){
    var data = $('option:selected', '#txtEstado').attr('class');
    if(oldClass != null){
        $('#txtEstado').removeClass(oldClass);
    }
    $('#txtEstado').addClass(data);
    oldClass = data;

}
$('#txtEstado').change(function(){
    checkEstado();
});
checkEstado();
</script>
<style>
.ui-datepicker-year{
    color: #000;
}
</style>
@endsection
