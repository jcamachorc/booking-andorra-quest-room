@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
    @if(Session::has('error'))
    <div class="alert alert-danger">{{ Session::get('error')}}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    {!! Form::open(array('url' => '/booking/bonos/editarDescuento/'.$id, 'method' => 'post')) !!}
    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Resumen</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-center-text">
                <thead>
                    <th>Fecha de creacion</th>
                    <th>Código descuento</th>
                    <th>Reserva</th>
                    <th>Descuento</th>
                </thead>
                <tbody>
                    <tr>
                        <td><?=\Carbon\Carbon::parse($input['created_at'])->format("d/m/Y H:i");?></td>
                        <td><?=$input['id']?></td>
                        <?php
                        if($input['idReserva'] == null){
                        ?>
                            <td>Sin reserva</td>
                        <?php
                        }else{
                        ?>
                            <td><a href="<?=url('booking/bookings/editar/'.$input['idReserva'])?>">Reserva nº <?=$input['idReserva']?></a></td>
                        <?php
                        }
                        ?>
                        <td><?=$input['descuento']?>%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de descuento</h3>
        </div>
        <div class="panel-body">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Código</span>
                <input type="hidden" name="txtId" value="<?=$input['id']?>">
                <input type="text" class="form-control" name="txtCodigo" value="{{ $input['id'] }}" placeholder="Código" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Descuento</span>
                <input type="text" class="form-control" name="txtDescuento" value="{{ $input['descuento']  }}" placeholder="Decuento" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">Fecha cadu   cidad</span>
                    {!! Form::text('txtCaducidad', \Carbon\Carbon::parse($input['fechaCaducidad'])->toDateString(), array('class' => 'form-control datepicker1')) !!}
                </div>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon">Ilimitado</span>
                {!! Form::checkbox('txtIlimitado', null, $input['ilimitados'], array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="panel-footer">
            <input type="submit" class="btn btn-success pull-right" value="Guardar cambios" />
            <div class="clearfix"></div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('footer')
<script>
$(function() {
    $('.datepicker1').datepicker({dateFormat: 'yy-mm-dd', timeFormat:  "hh:mm:ss", changeYear: true});
});

</script>
<style>
.ui-datepicker-year{
    color: #000;
}
</style>
@endsection
