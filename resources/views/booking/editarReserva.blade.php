@extends('app')

@section('header')
@endsection

@section('content')
<?php
    $session = Session::all();
    $sala= $session['sala'];
    //var_dump($session);
?>
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    {!! Form::open(array('url' => '/booking/bookings/editar/'.$id.'/'.$sala, 'method' => 'post')) !!}
    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">   
                    <h3 class="panel-title"><?php echo ($sala=='0') ? 'CONTRABANDISTAS' : 'QUEST BANK';?></h3>
        </div>
        <div class="panel-heading">
            <h3 class="panel-title">Resumen</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-center-text">
                <thead>
                    <th>Fecha</th>
                    <th>Hora de inicio</th>
                    <th>Hora de finalización</th>
                    <th>Duracion</th>
                    <th>Personas</th>
                    <th>Descuento</th>
                    <th>Precio final</th>
                </thead>
                <tbody>
                    <tr>
                        <td><?=\Carbon\Carbon::parse($input['inicio'])->format("d/m/Y");?></td>
                        <td><?=\Carbon\Carbon::parse($input['inicio'])->format("H:i");?></td>
                        <td><?=\Carbon\Carbon::parse($input['fin'])->format("H:i");?></td>
                        <td><?=$input['duracion']?></td>
                        <td><?=$input['nPersonas']?></td>
                        <td><?=$input['descuento']?>%</td>
                        <td><?=round(explode('->',$input['precio'])[0]-(explode('->',$input['precio'])[0]*($input['descuento']/100)),2)?>&euro;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de generales</h3>
        </div>
        <div class="panel-body">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Estado</span>
                <select name="txtEstado" id="txtEstado" class="form-control" {{ ($input['estado'] == '1' ? "disabled":"") }}>
                    <option value="-1" class="alert-danger" {{ ($input['estado'] == '-1' ? "selected":"") }}>Cancelado</option>
                    <option value="0" class="alert-warning" {{ ($input['estado'] == '0' ? "selected":"") }}>Pendiente</option>
                    <option value="1" class="alert-success" {{ ($input['estado'] == '1' ? "selected":"") }}>Aceptado</option>
                </select>
                <?php
                if($input['estado'] == '1'){
                ?>
                    <input name="txtEstado" type="hidden" value="<?= $input['estado'];?>"/>
                <?php
                }
                ?>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Fecha</span>
                <input type="text" class="form-control datepicker1" name="txtFechaIni" value="<?=\Carbon\Carbon::parse($input['inicio'])->format("Y-m-d");?>" placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Hora inicio</span>
                <input type="time" class="form-control timepicker" name="txtTimeIni" value="<?=\Carbon\Carbon::parse($input['inicio'])->format("H:i");?>" placeholder="DNI" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Hora finalizacion</span>
                <input type="time" class="form-control timepicker" name="txtTimeFin" value="<?=\Carbon\Carbon::parse($input['fin'])->format("H:i");?>" placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Precio base (sin descuento)</span>
                <input type="number" class="form-control" name="txtPrecio" step="any" value="<?=$input['precio']?>" placeholder="" aria-describedby="basic-addon1">
            </div>
        </div>
    </div>

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de facturación</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Nombre y apellidos</span>
                    <input type="text" class="form-control" name="txtNombre" value="<?=$input['nombre']?>" placeholder="Nombre completo" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=empty($input['DNI'])?'CIF':'DNI'?></span>
                    <input type="text" class="form-control" name="<?=empty($input['DNI'])?'txtCIF':'txtDNI'?>" value="<?=empty($input['DNI'])?$input['CIF']:$input['DNI']?>" placeholder="DNI" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">E-mail</span>
                    <input type="email" class="form-control" name="txtEmail" value="<?=$input['email']?>" placeholder="E-mail" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Telefono</span>
                    <input type="number" class="form-control" name="txtTelefono" value="<?=$input['telefono']?>" placeholder="Telefono" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Idioma</span>
                    <select name="txtIdioma" class="form-control">
                        <option value="catala" {{ ($input['idioma'] == 'catala') ? "selected":"" }}>Catalán</option>
                        <option value="castellano" {{ ($input['idioma'] == 'castellano') ? "selected":"" }}>Castellano</option>
                        <option value="frances" {{ ($input['idioma'] == 'frances') ? "selected":"" }}>Français</option>
                        <option value="english" {{ ($input['idioma'] == 'english') ? "selected":"" }}>English</option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Descuento</span>
                    <select name="txtDescuento" class="form-control">
                        <option value="none" {{ ($input['tipoDescuento'] == 'none' ? "selected":"") }}>Ninguno</option>
                        <option value="bonoregalo" {{ ($input['tipoDescuento'] == 'bonoregalo' ? "selected":"") }}>Bono regalo</option>
                        <option value="descuento" {{ ($input['tipoDescuento'] == 'descuento' ? "selected":"") }}>Descuento</option>
                        <option value="carnetjove" {{ ($input['tipoDescuento'] == 'carnetjove' ? "selected":"") }}>Carnet jove</option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group" style="display: none;">
                    <span class="input-group-addon" id="basic-addon1">Nº carnet jove</span>
                    <input type="text" class="form-control" name="txtCarnetJove" value="{{ isset($input['codigoDescuento'])?$input['codigoDescuento']:old('txtCarnetJove') }}"  placeholder="Nº carnet jove" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group" style="display: none;">
                    <span class="input-group-addon" id="basic-addon1">Código</span>
                    <input type="text" class="form-control" name="txtCodigo" value="{{ isset($input['codigoDescuento'])?$input['codigoDescuento']:old('txtCodigo') }}" placeholder="Código" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Método de pago</span>
                    <select name="txtMetodoPago" class="form-control">
                        <option value="paypal" {{ ($input['metodoPago'] == 'paypal' ? "selected":"") }}>Paypal</option>
                        <option value="tpv" {{ ($input['metodoPago'] == 'tpv' ? "selected":"") }}>Tarjeta de credito</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Código de pago</span>
                    <input class="form-control" disabled value="<?=$input['codigoPago']?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Fecha de pago</span>
                    <input class="form-control" disabled value="<?=$input['fechaPago']?>" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Dirección (opcional)</span>
                    <input type="text" class="form-control" name="txtDireccion" value="{{ isset($input['direccion'])?$input['direccion']:old('txtDireccion') }}" placeholder="" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Comentarios</span>
                    <textarea class="form-control" name="txtComentario">{{ isset($input['comentarios'])?$input['comentarios']:old('txtComentario') }}</textarea>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            @if($input['estado'] == 1 && $input['nPersonas'] > 0)
                <a href="<?=url('/booking/bookings/cancelarReservaDia/'.$input['id'])?>" class="pull-left btn btn-danger">Cancelar Reserva</a>
            @endif
            @if($input['nPersonas'] == 0)
                <a href="<?=url('/booking/bookings/borrarReservaDia/'.$input['id'])?>" class="pull-left btn btn-danger">Borrar Reserva Interna</a>
            @endif
            
            <input type="submit" class="btn btn-success pull-right" value="Guardar cambios" />
            
            <div class="clearfix"></div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection


@section('footer')
<script>
$('select[name="txtDescuento"]').on('change', function() {
    selectedItem(this.value);
});

function selectedItem(value){
    $('input[name="txtCodigo"]').parent().hide();
    $('input[name="txtFechaNacimiento"]').parent().hide();
    $('input[name="txtCarnetJove"]').parent().hide();
    if(value == 'carnetjove'){
        $('input[name="txtFechaNacimiento"]').parent().show();
        $('input[name="txtCarnetJove"]').parent().show();
    }else if(value == 'descuento' || value == 'bonoregalo'){
        $('input[name="txtCodigo"]').parent().show();
    }
}

$(function() {
    selectedItem($('select[name="txtDescuento"]').val());
    $('.datepicker1').datepicker({dateFormat: 'yy-mm-dd', timeFormat:  "hh:mm:ss", changeYear: true});
    $('.timepicker').timepicker({'timeFormat': 'H:i' });
    $('.datepickercj').datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date(1991, 01, 01), changeYear: true});
});

var oldClass = null;
function checkEstado(){
    var data = $('option:selected', '#txtEstado').attr('class');
    if(oldClass != null){
        $('#txtEstado').removeClass(oldClass);
    }
    $('#txtEstado').addClass(data);
    oldClass = data;

}
$('#txtEstado').change(function(){
    checkEstado();
});
checkEstado();
</script>
<style>
.ui-datepicker-year{
    color: #000;
}
</style>
@endsection
