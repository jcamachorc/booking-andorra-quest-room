@extends('app')

@section('header')

@endsection

@section('content')
<div class="container">
	<div class="row">
        {!! Form::open(array('url' => '/caja/add/', 'method' => 'POST')) !!}
            <div class="form-group has-feedback col-md-12">
                <div class="input-group">
                    <span class="input-group-addon">Cantidad</span>
                    <input type="number" class="form-control" step="any" id="cantidad" name="cantidad">
                </div>
            </div>
            <div class="form-group has-feedback col-md-12">
                <div class="input-group">
                    <span class="input-group-addon">Concepto</span>
                    <input type="text" class="form-control" id="concepto" name="concepto">
                </div>
            </div>
            <button type="submit" class="btn btn-success">Introducir</button>
        {!! Form::close() !!}
        <div class="clearfix"></div>
        <hr>

        <table class="table table-bordered">
            <thead>
                <th>Tipo</th>
                <th>Concepto</th>
                <th>Cantidad</th>
                <th>Fecha</th>
                <th>Balance parcial</th>
            </thead>
            <tbody>
                <?php
                for($i = count($contabilidad) - 1; $i >= max(count($contabilidad) - 20, 0); --$i){
                ?>
                <tr class="<?=(!$contabilidad[$i]['ingreso'])? (($contabilidad[$i]['acumulado'] < 0)?'btn-danger':'btn-warning') : (($contabilidad[$i]['acumulado'] < 0)?'btn-danger':'btn-success')?>">
                    <td><?=($contabilidad[$i]['ingreso'])? 'Ingreso' : 'Pago'?></td>
                    <td><?=$contabilidad[$i]['concepto']?></td>
                    <td><?=$contabilidad[$i]['cantidad']?> &euro;</td>
                    <td><?=date("d/m/y h:i", strtotime($contabilidad[$i]['created_at']))?></td>
                    <td><?=$contabilidad[$i]['acumulado']?> &euro;</td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
	</div>
</div>
@endsection


@section('footer')
@endsection