<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0">
    <meta http-equiv="expires" content="Sat, 31 Oct 2014 00:00:00 GMT">
    <meta http-equiv="pragma" content="no-cache">

    <!-------------------------------------------------------------------->
	<title>Andorra Quest Room - reservas</title>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100' rel='stylesheet' type='text/css'>
	{!! Minify::stylesheet(array(
	                        asset('css/bootstrap.min.css'),
	                        asset('css/font-awesome.min.css'),
	                        asset('css/cal.css'),
	                        asset('css/modal.css'),
	                        asset('css/dataTables.bootstrap.css'),
	                        asset('css/dataTables.responsive.css'),
	                        asset('css/fileinput.min.css'),
	                        asset('css/dropzone.min.css'),
	                        asset('css/basic.drop.min.css'),
	                        asset('css/bootstrap-select.min.css'),
	                        asset('css/jquery-ui.min.css'),
	                        asset('css/token-input.css'),
	                        asset('css/token-input-facebook.css')
	                        )) !!}
	<!-- Fonts -->
	<!--<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>-->
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
	    body{
	        background-color: #F0F3F2;
    	}

        .arrowUp {
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
            position: absolute;
            bottom: 0;
            left: 0;
        }
        .standard-padding{
            padding: 20px 150px 50px 150px;
        }
        h2{
            color: #26778C;
        }
        @media screen and (max-width: 700px) {
         .standard-padding{
             padding: 0px !important;
         }
        }
	</style>
</head>
<body>
    

    <div class="contenido-main standard-padding">
        <div class="col-md-12">
            <h2>
                <?=Lang::get('calendario.bigtitle');?>
                <div class="pull-right" style="margin-right: 25px">
                    <a href="<?=url('lang/ad')?>">
                        <img src="<?=asset('images/andorra.png')?>" alt="" height="35px"/>
                    </a>
                    <a href="<?=url('lang/es')?>">
                        <img src="<?=asset('images/spanish.png')?>" alt="" height="35px"/>
                    </a>
                    <a href="<?=url('lang/fr')?>">
                        <img src="<?=asset('images/france.png')?>" alt="" height="35px"/>
                    </a>
                    <a href="<?=url('lang/en')?>">
                        <img src="<?=asset('images/english.png')?>" alt="" height="35px"/>
                    </a>
                </div>
            </h2>
            <div class="text-main">
                <?=Lang::get('calendario.warning');?>
            </div>
        </div>
        @yield('contenido')
        <div class="clearfix"></div>
    </div>

	{!! Minify::javascript(array(asset('js/jquery-2.1.4.min.js'),
	                        asset('js/bootstrap.min.js'),
	                        asset('js/jquery-ui.min.js'),
	                        asset('js/jquery.tokeninput.js'),
	                        asset('js/jquery.nicescroll.min.js'),
	                        asset('js/moment.min.js'),
	                        asset('js/jquery.complexify.js'),
	                        asset('js/common.js')
	                        )) !!}
    @yield('js')
</body>
</html>