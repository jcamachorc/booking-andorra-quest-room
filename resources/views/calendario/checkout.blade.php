@extends('calendario')

@section('contenido')

<?php
    $session = Session::all();
    $sala= $session['sala'];
    //var_dump($session);
?>

<div class="col-md-12">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    <div class="panel panel-default panel-rounded">
         <div class="panel-heading">
            <h3 class="panel-title"><?php echo ($sala=='0') ? Lang::get('calendario.titulo') : Lang::get('calendario.titulo2');?></h3>
        </div> 
        <div class="panel-body">
            <table class="table table-striped table-center-text">
                <thead>
                    <th><?=Lang::get('calendario.fecha');?></th>
                    <th><?=Lang::get('calendario.ini');?></th>
                    <th><?=Lang::get('calendario.fin');?></th>
                    <th><?=Lang::get('calendario.descuento');?></th>
                    <th><?=Lang::get('calendario.preciofinal');?></th>
                </thead>
                <tbody>
                    <tr>
                        <td><?=\Carbon\Carbon::parse($input['txtFechaInicio'])->format("d/m/Y");?></td>
                        <td><?=\Carbon\Carbon::parse($input['txtFechaInicio'])->format("H:i");?></td>
                        <td><?=\Carbon\Carbon::parse($input['txtFechaFinal'])->format("H:i");?></td>
                        <td><?=$descuento?>%</td>
                        <td><?=round(explode('|',$input['txtPrecio'])[0]-(explode('|',$input['txtPrecio'])[0]*($descuento/100)),2)?>&euro;</td>
                    </tr>
                </tbody>
            </table>

            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombre');?></span>
                <input type="text" class="form-control" name="txtNombre" value="<?=$input['txtNombre']?>" readonly placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.facturar');?></span>
                <select name="txtFactura" disabled class="form-control">
                    <option value="0" {{ (isset($input['txtFactura'])?$input['txtFactura']:old('txtFactura')) == '0'?'selected':'' }}><?=Lang::get('calendario.no');?></option>
                    <option value="1" {{ (isset($input['txtFactura'])?$input['txtFactura']:old('txtFactura')) == '1'?'selected':'' }}><?=Lang::get('calendario.si');?></option>
                </select>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.correo');?></span>
                <input type="email" class="form-control" name="txtEmail" value="<?=$input['txtEmail']?>" readonly placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.telefono');?></span>
                <input type="text" class="form-control" name="txtTelefono" value="<?=$input['txtTelefono']?>" readonly placeholder="Telefono" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.idioma');?></span>
                <select name="txtIdioma" class="form-control" disabled>
                    <option value="catala" {{ ($input['txtIdioma'] == 'catala') ? "selected":"" }}>Catalán</option>
                    <option value="castellano" {{ ($input['txtIdioma'] == 'castellano') ? "selected":"" }}>Castellano</option>
                    <option value="english" {{ ($input['txtIdioma'] == 'english') ? "selected":"" }}>English</option>
                </select>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.descuento');?></span>
                <select name="txtDescuento" class="form-control" disabled>
                    <option value="none" {{ ($input['txtDescuento'] == 'none' ? "selected":"") }}><?=Lang::get('calendario.none');?></option>
                    <option value="bonoregalo" {{ ($input['txtDescuento'] == 'bonoregalo' ? "selected":"") }}><?=Lang::get('calendario.bono');?></option>
                    <option value="descuento" {{ ($input['txtDescuento'] == 'descuento' ? "selected":"") }}><?=Lang::get('calendario.descuento');?></option>
                    <option value="carnetjove" {{ ($input['txtDescuento'] == 'carnetjove' ? "selected":"") }}><?=Lang::get('calendario.cj');?></option>
                    <option value="clubabc4event" {{ ($input['txtDescuento'] == 'clubabc4event' ? "selected":"") }}>ClubABC4event</option>
                </select>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.metodo');?></span>
                <select name="txtMetodoPago" class="form-control" disabled>
                    <option value="paypal" {{ ($input['txtMetodoPago'] == 'paypal' ? "selected":"") }}><?=Lang::get('calendario.paypal');?></option>
                    <option value="tpv" {{ ($input['txtMetodoPago'] == 'tpv' ? "selected":"") }}><?=Lang::get('calendario.tj');?></option>
                </select>
            </div>
            <br />
         <!--   <div class="input-group has-warning">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nper');?></span>
                <input style="background-color: #fcf8e3; color: #8a6d3b" type="text" class="form-control" value="<?=explode('|',$input['txtPrecio'])[1]?>" name="nPer" readonly />
            </div> -->
        </div>
        <div class="panel-heading bg-primary factura-place">
            <h3 class="panel-title"><?=Lang::get('calendario.facturacion');?></h3>
        </div>
        <div class="panel-body bg-info factura-place">
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtNombreF')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombreF');?></span>
                <input type="text" class="form-control" name="txtNombreF" disabled value="{{ isset($input['txtNombreF'])?$input['txtNombreF']:old('txtNombreF') }}" placeholder="Nombre factura" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.DNI');?></span>
                <input type="text" class="form-control" name="txtDNI" value="<?=$input['txtDNI']?>" readonly placeholder="DNI" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.direccion');?></span>
                <input type="text" class="form-control" name="txtDireccion" value="{{ isset($input['txtDireccion'])?$input['txtDireccion']:old('txtDireccion') }}" readonly placeholder="Direccion" aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="panel-footer">
            <a href="<?=url("calendario/backReserva/$sala")?>"><input type="button" class="btn btn-default pull-left" value="<?=Lang::get('calendario.goback');?>" /></a>
            @if($input['txtMetodoPago'] == 'tpv')
                <!-- PRUEBA -->
                <!--<form action="https://sis-t.redsys.es:25443/sis/realizarPago" method="post" id="sbdll_send">-->

                <!-- PRODUCCION 
                <form action="https://sis.redsys.es/sis/realizarPago" method="post" id="sbdll_send">
                   --> 
                <form action="<?php echo REDSYS_URL; ?>" method="post" id="sbdll_send">
                    
                    <input type="hidden" value="HMAC_SHA256_V1" name="Ds_SignatureVersion">
                    <input type="hidden" value="<?=$formPago['params']?>" name="Ds_MerchantParameters">
                    <input type="hidden" value="<?=$formPago['sign']?>" name="Ds_Signature">
                    <input type="hidden" value="Reserva del juego" name="Ds_Merchant_ProductDescription"/>
                    <input type="hidden" value="Quest Room" name="Ds_Merchant_Titular"/>
                    <input type="submit" class="btn btn-success pull-right payButton" id="pagartpv" value="Pagar" />
                    <button type="button" class="btn btn-success pull-right" id="buttonLoading" style="display: none">
                        <img src="http://klear.com/images/new/loader.gif" height="15px" />
                    </button>
                    <div class="clearfix"></div>
                </form>
            @else
            {!! Form::open(array('url' => 'calendario/pay', 'method' => 'post')) !!}
            <input type="submit" class="btn btn-success pull-right payButton" value="<?=Lang::get('calendario.pay');?>" />
            <button type="button" class="btn btn-success pull-right" id="buttonLoading" style="display: none">
                <img src="http://klear.com/images/new/loader.gif" height="15px" />
            </button>
            <div class="clearfix"></div>
            {!! Form::close() !!}
            @endif

        </div>
    </div>
</div>
@endsection

@section('js')
<?php $sesionActual = \Illuminate\Support\Facades\Session::all();?>
<script>
@if($input['txtFactura'] == '0')
    $('.factura-place').hide(0);
@endif

$('form').submit( function(event) {
    var form = this;
    event.preventDefault();
    $('.payButton').hide();
    $('#buttonLoading').show();
    $.ajax({
        method: "POST",
        url: "<?=url('/calendario/checkSession/$sala')?>",
        data: <?=json_encode($sesionActual['input'])?>
    }).done(function(){

        if($('#pagartpv').length != 0){
            $.ajax({
                method: "GET",
                url: "<?=url('/calendario/resetsession/$sala')?>"
            });
        }
        form.submit();
    }).error(function(){
        document.location = document.location;
    });
});
</script>
@endsection
