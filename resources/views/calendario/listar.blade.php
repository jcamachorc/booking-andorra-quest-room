@extends('calendario')

@section('contenido')
<?php
    $session = Session::all();
    $sala= $session['sala'];
    //var_dump($session);
?>


<div class="col-md-12">
    @if($errors->any())
    <div class="alert alert-danger">{{$errors->first()}}</div>
    @endif
    <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtSala')?'has-error':''?>">
                       
            <span class="input-group-addon" id="basic-addon1"><b><?=Lang::get('calendario.Sala');?></b></span>
              <select name="txtSala" class="form-control">
                @foreach ($salas as $_sala)
                  <option value="{{ $_sala['id'] }}" <?php if($sala == $_sala['id']) echo 'selected'; ?>>{{$_sala['nombre']}}</option>
                @endforeach
              </select>

    </div> 
    <div class="panel panel-default panel-rounded">
        
        <div class="panel-heading sala1-place">   
            <h3 class="panel-title"><?php echo ($sala=='0') ? Lang::get('calendario.titulo') : Lang::get('calendario.titulo2');?></h3>
            <br>
            <p><i><?php echo ($sala=='0') ? Lang::get('calendario.subtitulo') : Lang::get('calendario.subtitulo2');?></i></p>
        </div>
  
        <div class="panel-body">
            <span class="pull-right header-color">
                <a href="<?=url('/calendario/moveDays/-')?>" class="btn btn-xs btn-primary"><?=Lang::get('calendario.back');?></a>
                <a href="<?=url('/calendario/moveDays/=')?>" class="btn btn-xs btn-success"><?=Lang::get('calendario.hoy');?></a>
                <a href="<?=url('/calendario/moveDays/+')?>" class="btn btn-xs btn-primary"><?=Lang::get('calendario.further');?></a>
            </span>
            <br><br>
            <div class="divTableSize">
                <div class="tableInnerDivTop" style="height: 52px;"><?=Lang::get('calendario.horas');?></div>
                <?php $k = 0; ?>
                @foreach($semanaArray[0]['datos'] as $j => $item)
                <div class="tableInnerDivHoras">
                    <?=\Carbon\Carbon::parse($semanaArray[0]['datos'][$j]['ini'])->format('H:i')?>

                    <!--
                    @if(isset(array_keys($semanaArray[0]['datos'])[$k+1]))
                        <?=\Carbon\Carbon::parse($semanaArray[0]['datos'][array_keys($semanaArray[0]['datos'])[$k+1]]['ini'])->format('H:i')?>
                @else
                        <?=\Carbon\Carbon::parse($semanaArray[0]['datos'][$j]['end'])->format('H:i')?>
                    @endif
                    -->
                    <?php ++$k; ?>
                </div>
                @endforeach
            </div>
            <?php
                $arrayDias = array(Lang::get('calendario.dom'),
                                    Lang::get('calendario.lun'),
                                    Lang::get('calendario.mar'),
                                    Lang::get('calendario.mie'),
                                    Lang::get('calendario.jue'),
                                    Lang::get('calendario.vie'),
                                    Lang::get('calendario.sab'));
            ?>
            @for($k = 0; $k < 7; ++$k)
                <div class="divTableSize">
                    <div class="tableInnerDivTop" style="height: 52px;"><?=$arrayDias[\Carbon\Carbon::parse($semanaArray[$k]['fecha'])->dayOfWeek]." ".\Carbon\Carbon::parse($semanaArray[$k]['fecha'])->format("d/m")?></div>
                    <?php
                    $keysDatos = array_keys($semanaArray[$k]['datos']);
                    for($franjaHoraria = 0; $franjaHoraria < count($semanaArray[$k]['datos']); ++$franjaHoraria){
                        $keyActual = $keysDatos[$franjaHoraria];
                        if(!$semanaArray[$k]['datos'][$keyActual]['vacaciones']){
                            if($semanaArray[$k]['datos'][$keyActual]['primary']){
                                $sesion = \Illuminate\Support\Facades\Session::all();
                                if($semanaArray[$k]['datos'][$keyActual]['reserva'] && !(isset($sesion['input']['txtId']) && $sesion['input']['txtId'] == $semanaArray[$k]['datos'][$keyActual]['id'])){
                                ?>
                                <div class="tableInnerDiv-<?=$semanaArray[$k]['datos'][$keyActual]['rowspan']?>" style="padding-top: 10px">
                                    <div class="tsWeeklyIconOcupado"></div>
                                </div>
                                <?php
                                }else{
                                ?>
                                <div class="tableInnerDiv-<?=$semanaArray[$k]['datos'][$keyActual]['rowspan']?>" style="padding-top: 10px">
                                    <a href="<?=url('/calendario/'.$semanaArray[$k]['datos'][$keyActual]['id'].'/'.$semanaArray[$k]['datos'][$keyActual]['ini'])?>">
                                        <?php
                                        if($semanaArray[$k]['datos'][$keyActual]['actual']){
                                        ?>
                                        <div class="tsWeeklyIconActual"></div>
                                        <?php
                                        }else{
                                        ?>
                                        <div class="tsWeeklyIcon"></div>
                                        <?php
                                        }
                                        ?>
                                    </a>
                                </div>
                                <?php
                                }
                            }
                        }else{
                            echo '<div class="tableInnerDiv-1">---</div>';
                        }
                    }
                    ?>
                </div>
            @endfor
        </div>
        
  
        <div class="panel-footer">

            <?php
        $sesion = \Illuminate\Support\Facades\Session::all();
        if(isset($sesion['input'])){
        ?>
    
            <a href="<?=url('calendario/cancelar/$sala')?>"><input type="button" class="btn btn-danger pull-left" value="<?=Lang::get('calendario.cancelar');?>" /></a>
        <?php
        }
        ?>
            <div class="clearfix"></div>
        
        
        </div>
    </div>
</div>    


@endsection

@section('js')
<script>
    
<?= (isset($inputs['txtSala'])?$inputs['txtSala']:old('txtSala')) == '0'?"$('.sala1-place').show(0);":'' ?>
<?= !isset($inputs['txtSala'])?"$('.sala2-place').show(0);":'' ?>
    
$('select[name="txtSala"]').on('change', function() {
      selectedItem(this.value);

});


function selectedItem(value) {
	$(window.location="<?php echo APP_URL_BASE; ?>/public/index.php/calendario/" + value);
    
 }

</script>
@endsection
