@extends('calendario')

@section('contenido')

<?php
    $session = Session::all();
    $sala= $session['sala'];
    //var_dump($session);
?>
<div class="col-md-12">
    @if($errors->any())
    <div class="alert alert-danger">{{$errors->first()}}</div>
    @endif
    
    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
             <h3 class="panel-title"><?php echo ($sala=='0') ? Lang::get('calendario.titulo') : Lang::get('calendario.titulo2');?></h3>
       </div>
        <div class="panel-body">
            <table class="table table-striped table-center-text">
                <thead>
                    <th><?=Lang::get('calendario.fecha');?></th>
                    <th><?=Lang::get('calendario.ini');?></th>
                    <th><?=Lang::get('calendario.fin');?></th>
                </thead>
                <tbody>
                    <tr>
                        <td><?=\Carbon\Carbon::parse($fechaInicio)->format("d/m/Y");?></td>
                        <td><?=\Carbon\Carbon::parse($fechaInicio)->format("H:i");?></td>
                        <td><?=\Carbon\Carbon::parse($fechaFinal)->format("H:i");?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title"><?=Lang::get('calendario.form');?></h3>
        </div>
        {!! Form::open(array('url' => 'calendario/checkout', 'method' => 'post')) !!}
        <input type="hidden" name="txtFechaInicio" value="<?=$fechaInicio?>"/>
        <input type="hidden" name="txtId" value="<?=$id?>"/>
        <input type="hidden" name="sala" value="<?=$sala?>"/>
        <div class="panel-body">
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtNombre')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombre');?></span>
                <input type="text" class="form-control" name="txtNombre" value="{{ isset($inputs['txtNombre'])?$inputs['txtNombre']:old('txtNombre') }}" placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtFactura')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.facturar');?></span>
                <select name="txtFactura" class="form-control">
                    <option value="0" {{ (isset($inputs['txtFactura'])?$inputs['txtFactura']:old('txtFactura')) == '0'?'selected':'' }}><?=Lang::get('calendario.no');?></option>
                    <option value="1" {{ (isset($inputs['txtFactura'])?$inputs['txtFactura']:old('txtFactura')) == '1'?'selected':'' }}><?=Lang::get('calendario.si');?></option>
                </select>
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtEmail')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.correo');?></span>
                <input type="email" class="form-control" name="txtEmail" value="{{ isset($inputs['txtEmail'])?$inputs['txtEmail']:old('txtEmail') }}" placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtTelefono')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.telefono');?></span>
                <input type="number" class="form-control" name="txtTelefono" value="{{ isset($inputs['txtTelefono'])?$inputs['txtTelefono']:old('txtTelefono') }}" placeholder="Telefono" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtIdioma')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.idioma');?></span>
                <select name="txtIdioma" class="form-control">
                    <option value="castellano" {{ (isset($inputs['txtIdioma'])?$inputs['txtIdioma']:old("txtIdioma")) == 'castellano' ? "selected":"" }}>Castellano</option>
                    <option value="english" {{ (isset($inputs['txtIdioma'])?$inputs['txtIdioma']:old("txtIdioma")) == 'english' ? "selected":"" }}>English</option>
                    <option value="frances" {{ (isset($inputs['txtIdioma'])?$inputs['txtIdioma']:old("txtIdioma")) == 'frances' ? "selected":"" }}>Français</option>
                    <option value="catala" {{ (isset($inputs['txtIdioma'])?$inputs['txtIdioma']:old("txtIdioma")) == 'catala' ? "selected":"" }}>Catalán</option>
                </select>
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDescuento')?'has-error':''?>">
              <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.descuento');?></span>
                
              <select name="txtDescuento" class="form-control">
                <option value="none" {{ (isset($inputs['txtDescuento'])?$inputs['txtDescuento']:old("txtDescuento")) == 'none' ? "selected":"" }}><?=Lang::get('calendario.none');?></option>
                <option value="bonoregalo" {{ (isset($inputs['txtDescuento'])?$inputs['txtDescuento']:old("txtDescuento")) == 'bonoregalo' ? "selected":"" }}><?=Lang::get('calendario.bono');?></option>
                <option value="descuento" {{ (isset($inputs['txtDescuento'])?$inputs['txtDescuento']:old("txtDescuento")) == 'descuento' ? "selected":"" }}><?=Lang::get('calendario.descuento');?></option>
                 @if(getConfig('cj_enable'))
                   <option value="carnetjove" {{ (isset($inputs['txtDescuento'])?$inputs['txtDescuento']:old("txtDescuento")) == 'carnetjove' ? "selected":"" }}><?=Lang::get('calendario.cj');?></option>
                @endif
                    
                @if(getConfig('ABC_enable'))
                  <option value="clubabc4event" {{ (isset($inputs['txtDescuento'])?$inputs['txtDescuento']:old("txtDescuento")) == 'clubabc4event' ? "selected":"" }}>ClubABC4event</option>
                @endif
              </select>
            </div>

            <!--<div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtFechaNacimiento')?'has-error':''?>" style="display: none; margin-top: 15px;">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nacimiento');?></span>
                <input type="datetime" class="form-control" value="{{ isset($inputs['txtFechaNacimiento'])?$inputs['txtFechaNacimiento']:old('txtFechaNacimiento') }}" id="datepicker" name="txtFechaNacimiento" placeholder="Fecha de nacimiento" aria-describedby="basic-addon1">
            </div>-->

            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtCarnetJove')?'has-error':''?>" style="display: none; margin-top: 15px;">
              <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.ncj');?></span>
              <input type="text" class="form-control" name="txtCarnetJove" value="{{ isset($inputs['txtCarnetJove'])?$inputs['txtCarnetJove']:old('txtCarnetJove') }}"  placeholder="Nº carnet jove" aria-describedby="basic-addon1">
            </div>
            
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtClubABC4event')?'has-error':''?>" style="display: none; margin-top: 15px;">
              <span class="input-group-addon" id="basic-addon1">Nº ClubABC4event</span>
              <input type="text" class="form-control" name="txtClubABC4event" value="{{ isset($inputs['txtClubABC4event'])?$inputs['txtClubABC4event']:old('txtClubABC4event') }}"  placeholder="Nº ClubABC4event" aria-describedby="basic-addon1">
            </div>

            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtCodigo')?'has-error':''?>" style="display: none; margin-top: 15px;">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.codigo');?></span>
                <input type="text" class="form-control" name="txtCodigo" value="{{ isset($inputs['txtCodigo'])?$inputs['txtCodigo']:old('txtCodigo') }}" placeholder="Código" aria-describedby="basic-addon1">
            </div>
            
            <div style="display: none;">
                <div class="input-group <?(Session::has('error-input') && Session::get('error-input') == 'txtPrecio')?'has-error':''?>">
                  <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.n_personas');?></span>
                <!--
                  <input type="text" class="form-control" name="txtPersonas" value="{{ isset($inputs['txtPersonas'])?$inputs['txtPersonas']:old('txtPersonas') }}" placeholder="<?= Lang::get('calendario.n_personas'); ?>" aria-describedby="basic-addon1">
                -->
                     <input type="hidden" name="txtPersonas" value="5" />
                </div>
            </div>
            
            <br>

            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtPrecio')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.precio');?></span>
                @if(count($precios) == 1)
                    <input type="hidden" name="txtPrecio" value="<?=$precios[0]['precio']?>|<?=$precios[0]['personas']?>">
                    <select name="txtPrecio" class="form-control" disabled>
                        <option value="<?=$precios[0]['precio']?>|<?=$precios[0]['personas']?>" {{ (isset($inputs['txtPrecio'])?$inputs['txtPrecio']:old("txtPrecio")) == $precios[0]['precio'] ? "selected":"" }}><?=$precios[0]['precio']?>&euro;</option>
                    </select>
                @else
                    <select name="txtPrecio" class="form-control">
                    @foreach($precios as $precio)
                        <option value="<?=$precio['precio']?>|<?=$precio['personas']?>" {{ (isset($inputs['txtPrecio'])?$inputs['txtPrecio']:old("txtPrecio")) == $precio['precio'] ? "selected":"" }}><?=$precio['precio']?>&euro; (<?=$precio['personas']?> personas)</option>
                    @endforeach
                    </select>
                @endif
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtMetodoPago')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.metodo');?></span>
                <select name="txtMetodoPago" class="form-control">
                    @if(getConfig('paypal_enable'))
                    <option value="paypal" {{ (isset($inputs['txtMetodoPago'])?$inputs['txtMetodoPago']:old("txtMetodoPago")) == 'paypal' ? "selected":"" }}><?=Lang::get('calendario.paypal');?></option>
                    @endif
                    @if(getConfig('tpv_enable'))
                    <option value="tpv" {{ (isset($inputs['txtMetodoPago'])?$inputs['txtMetodoPago']:old("txtMetodoPago")) == 'tpv' ? "selected":"" }}><?=Lang::get('calendario.tj');?></option>
                    @endif
                </select>
            </div>
        </div>

        <div class="panel-heading bg-primary factura-place">
            <h3 class="panel-title"><?=Lang::get('calendario.facturacion');?></h3>
        </div>
        <div class="panel-body bg-info factura-place">
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtNombreF')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.nombreF');?></span>
                <input type="text" class="form-control" name="txtNombreF" value="{{ isset($inputs['txtNombreF'])?$inputs['txtNombreF']:old('txtNombreF') }}" placeholder="Nombre factura" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDNI')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.DNI');?></span>
                <input type="text" class="form-control" name="txtDNI" value="{{ isset($inputs['txtDNI'])?$inputs['txtDNI']:old('txtDNI') }}" placeholder="DNI/NRT/CIF" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group <?=(Session::has('error-input') && Session::get('error-input') == 'txtDireccion')?'has-error':''?>">
                <span class="input-group-addon" id="basic-addon1"><?=Lang::get('calendario.direccion');?></span>
                <input type="text" class="form-control" name="txtDireccion" value="{{ isset($inputs['txtDireccion'])?$inputs['txtDireccion']:old('txtDireccion') }}" placeholder="Direccion" aria-describedby="basic-addon1">
            </div>
        </div>
        <div class="panel-footer">
            <a href="<?=url('calendario')?>"><input type="button" class="btn btn-default pull-left" value="<?=Lang::get('calendario.goback');?>" /></a>

            <input type="submit" class="btn btn-success pull-right" value="<?=Lang::get('calendario.checkout');?>" />
            <div class="clearfix"></div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('js')
<script>
$('select[name="txtDescuento"]').on('change', function() {
    selectedItem(this.value);
});


<?= (isset($inputs['txtFactura'])?$inputs['txtFactura']:old('txtFactura')) == '0'?"$('.factura-place').hide(0);":'' ?>
<?= !isset($inputs['txtFactura'])?"$('.factura-place').hide(0);":'' ?>

$('select[name="txtFactura"]').on('change', function() {
    if(this.value == '0'){
        $('.factura-place').hide(500);
    }else{
        $('.factura-place').show(250);
    }
});

function selectedItem(value){
      $('input[name="txtCodigo"]').parent().hide();
      $('input[name="txtFechaNacimiento"]').parent().hide();
      $('input[name="txtCarnetJove"]').parent().hide();
      $('input[name="txtClubABC4event"]').parent().hide();
      
      if(value == 'carnetjove'){
        $('input[name="txtFechaNacimiento"]').parent().show();
        $('input[name="txtCarnetJove"]').parent().show();
      }else if(value =='clubabc4event'){
        $('input[name="txtClubABC4event"]').parent().show();
      }else if(value == 'descuento' || value == 'bonoregalo'){
        $('input[name="txtCodigo"]').parent().show();
      }
    }

$(function() {
    selectedItem($('select[name="txtDescuento"]').val());
    $('#datepicker').datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date(1991, 01, 01), changeYear: true});
});
</script>
@endsection
