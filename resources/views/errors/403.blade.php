<?php
header("HTTP/1.0 403 Forbidden");
?>

@extends('app')

@section('header')
{!! Minify::stylesheet(array(asset('/css/error.css'))) !!}
@endsection

@section('content')
<div class="container-fluid">
    <div class="container col-xlg-2 col-lg-3 col-md-6 col-sm-10 col-xs-11">
        <h1>Error 403! <br/> No tienes permisos para acceder a este modulo</h1>
    </div>
</div>
@endsection