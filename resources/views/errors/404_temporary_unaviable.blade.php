<?php
header("HTTP/1.0 404 Not Found");
?>

@extends('app')

@section('header')
{!! Minify::stylesheet(array(asset('/css/error.css'))) !!}
@endsection

@section('content')
@include('partials.topNav')
<div class="container-fluid">
    <div class="container col-xlg-2 col-lg-3 col-md-6 col-sm-10 col-xs-11">
        <h1>Error 404! <br/> Pagina no encontrada</h1>
    </div>
</div>

@endsection