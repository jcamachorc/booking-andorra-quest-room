<?php
header("HTTP/1.0 503 service unavailable");
?>
@extends('app')

@section('header')
{!! Minify::stylesheet(array(asset('/css/error.css'))) !!}
@endsection

@section('content')
<div class="container-fluid">
    <div class="container col-xlg-2 col-lg-3 col-md-6 col-sm-10 col-xs-11">
        <h1>Sistema en operaciones de mantenimiento</h1>
    </div>
</div>
@endsection
