<div class="row-fluid" style="height:100%;">
    {!! Form::open(array('url' => '/facturar/addFactura', 'method' => 'post')) !!}
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Nombre y apellidos</span>
            <input type="text" class="form-control" name="txtNombre" value="" placeholder="Nombre completo" aria-describedby="basic-addon1">
        </div>
        <br/>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">DNI/CIF</span>
            <input type="text" class="form-control" name="txtDNI" value="" placeholder="DNI" aria-describedby="basic-addon1">
        </div>
        <br/>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">E-mail</span>
            <input type="email" class="form-control" name="txtEmail" value="" placeholder="E-mail" aria-describedby="basic-addon1">
        </div>
        <br/>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Telefono</span>
            <input type="text" class="form-control" name="txtTelefono" value="" placeholder="Telefono" aria-describedby="basic-addon1">
        </div>
        <br/>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Descuento</span>
            <input type="text" class="form-control" name="txtDescuento" value="" placeholder="descuento en %" aria-describedby="basic-addon1">
            <span class="input-group-addon" id="basic-addon1">%</span>
        </div>
        <br/>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Precio</span>
            <input type="text" class="form-control" name="txtPrecio" value="" placeholder="precio" aria-describedby="basic-addon1">
            <span class="input-group-addon" id="basic-addon1">&euro;</span>
        </div>
        <br/>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Direccion</span>
            <input type="text" class="form-control" name="txtDireccion" value="" placeholder="Direccion" aria-describedby="basic-addon1">
        </div>
    {!! Form::close() !!}
</div>