@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    {!! Form::open(array('url' => '/facturar/bookings/editar/'.$id, 'method' => 'post')) !!}
    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de generales</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Descuento</span>
                    <input type="text" class="form-control" name="txtDescuento" value="<?=$input['descuento'];?>" placeholder="E-mail" aria-describedby="basic-addon1">
                    <span class="input-group-addon" id="basic-addon1">%</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Precio base (sin descuento)</span>
                    <input type="number" class="form-control" name="txtPrecio" step="any" value="<?=$input['precio']?>" placeholder="" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de facturación</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Nombre y apellidos</span>
                    <input type="text" class="form-control" name="txtNombre" value="<?=$input['nombre']?>" placeholder="Nombre completo" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><?=empty($input['DNI'])?'CIF':'DNI'?></span>
                    <input type="text" class="form-control" name="<?=empty($input['DNI'])?'txtCIF':'txtDNI'?>" value="<?=empty($input['DNI'])?$input['CIF']:$input['DNI']?>" placeholder="DNI" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">E-mail</span>
                    <input type="email" class="form-control" name="txtEmail" value="<?=$input['email']?>" placeholder="E-mail" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Telefono</span>
                    <input type="number" class="form-control" name="txtTelefono" value="<?=$input['telefono']?>" placeholder="Telefono" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Dirección (opcional)</span>
                    <input type="text" class="form-control" name="txtDireccion" value="{{ isset($input['direccion'])?$input['direccion']:old('txtDireccion') }}" placeholder="" aria-describedby="basic-addon1">
                </div>
            </div>

            <div class="clearfix"></div>
            <br/>

            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">Comentarios</span>
                    <textarea class="form-control" name="txtComentario">{{ isset($input['comentarios'])?$input['comentarios']:old('txtComentario') }}</textarea>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <input type="submit" class="btn btn-success pull-right" value="Guardar cambios" />
            <div class="clearfix"></div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection


@section('footer')
@endsection
