@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
    @if($errors->any())
    <div class="alert alert-danger">{{ $errors->first()}}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    {!! Form::open(array('url' => '/facturar/bonos/editar/'.$id, 'method' => 'post')) !!}
    <div class="panel panel-default panel-rounded">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de facturación</h3>
        </div>
        <div class="panel-body">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Nombre y apellidos</span>
                <input type="text" class="form-control" name="txtNombre" value="{{ $input['nombre'] }}" placeholder="Nombre completo" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">DNI</span>
                <input type="text" class="form-control" name="txtDNI" value="{{ $input['DNI']  }}" placeholder="DNI" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">E-mail</span>
                <input type="email" class="form-control" name="txtEmail" value="{{ $input['email'] }}" placeholder="E-mail" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Telefono</span>
                <input type="number" class="form-control" name="txtTelefono" value="{{ $input['telefono'] }}" placeholder="Telefono" aria-describedby="basic-addon1">
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Precio</span>
                <input type="number" class="form-control" name="txtPrecio" value="{{ $input['precio'] }}" aria-describedby="basic-addon1">
                <span class="input-group-addon" id="basic-addon1">&euro;</span>
            </div>
        </div>
        <div class="panel-footer">
            <input type="submit" class="btn btn-success pull-right" value="Guardar cambios" />
            <div class="clearfix"></div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection


@section('footer')
<script>
$('select[name="txtDescuento"]').on('change', function() {
    selectedItem(this.value);
});

function selectedItem(value){
    $('input[name="txtCodigo"]').parent().hide();
    $('input[name="txtFechaNacimiento"]').parent().hide();
    $('input[name="txtCarnetJove"]').parent().hide();
    if(value == 'carnetjove'){
        $('input[name="txtFechaNacimiento"]').parent().show();
        $('input[name="txtCarnetJove"]').parent().show();
    }else if(value == 'descuento' || value == 'bonoregalo'){
        $('input[name="txtCodigo"]').parent().show();
    }
}

$(function() {
    selectedItem($('select[name="txtDescuento"]').val());
    $('.datepicker').datepicker({dateFormat: 'yy-mm-dd', timeFormat:  "hh:mm:ss", changeYear: true});
    $('.timepicker').timepicker({'timeFormat': 'H:i' });
    $('.datepickercj').datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date(1991, 01, 01), changeYear: true});
});

var oldClass = null;
function checkEstado(){
    var data = $('option:selected', '#txtEstado').attr('class');
    if(oldClass != null){
        $('#txtEstado').removeClass(oldClass);
    }
    $('#txtEstado').addClass(data);
    oldClass = data;

}
$('#txtEstado').change(function(){
    checkEstado();
});
checkEstado();
</script>
<style>
.ui-datepicker-year{
    color: #000;
}
</style>
@endsection
