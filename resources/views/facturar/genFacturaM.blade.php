<!DOCTYPE html>
<html lang="en">
    <body style="padding: 15px 50px; font-family: sans-serif;">
        <table width="100%">
            <tr>
                <td width="50%" style="text-align: left">
                    <img src="<?=asset('images/'.$data['logo'])?>" height="80px" />
                </td>
                <td width="50%" style="text-align: right">
                    <p><b><?=Lang::get('facturas.factura');?> nº</b> <span style="color: #017ebc"><?=$numeroF?></span></p>
                    <p><b>Página nº <span style="color: #017ebc"><?=$pagina+1?></span> de <span style="color: #017ebc"><?=$paginas?></span></b></p>
                    <p><b><?=Lang::get('facturas.fecha');?>:</b> <span style="color: #017ebc"><?=\Carbon\Carbon::parse($factura['fechaPago'])->format("d/m/Y");?></span></p>
                </td>
            </tr>
        </table>

        <table width="100%" style="margin-top: 7px">
            <tr>
                <td width="40%" style="text-align: left; background-color: #f5f5f5; padding: 15px; border: 1px solid #ddd;">
                    <?=Lang::get('facturas.de');?>: <span style="color: #017ebc"><?=$data['compania']?></span>
                </td>
                <td></td>
                <td width="40%" style="text-align: left; background-color: #f5f5f5; padding: 15px; border: 1px solid #ddd;"></td>
            </tr>
            <tr>
                <td width="40%" style="text-align: left; padding: 15px; border: 1px solid #ddd;">
                    <p><b><?=Lang::get('facturas.cifPropio');?>:</b> <?=$data['cif']?></p>
                    <p><b><?=Lang::get('facturas.direccion');?>:</b> <?=$data['direccion']?></p>
                    <p><?=$data['cp']?>, <?=$data['ciudad']?></p>
                </td>
                <td></td>
                <td width="40%" style="text-align: left; padding: 15px; border: 1px solid #ddd;" valign="top"></td>
            </tr>
        </table>
        <table style="margin-top: 20px; width: 100%">
            <tr>
                <td style="border-bottom: 2px solid #ddd;"><b><?=Lang::get('facturas.descripcion');?></b></td>
                <td style="border-bottom: 2px solid #ddd;"><b><?=Lang::get('facturas.precio');?></b></td>
            </tr>
            <?php $preciofinal = 0; $precionegativo = 0;?>
            @foreach($facturas as $item)
                <tr>
                    <td><?=($item['precio'] < 0)?'Rectificación de ':''?><?=(!isset($item['idReserva']))?Lang::get("facturas.bonoTexto"):Lang::get("facturas.sesionTexto")?> id: <?=(isset($item['idReserva'])?$item['idReserva']:$item['idBono'])?></td>
                    <td><?=round($item['precio']/(1+($data['IVA']/100)),2)?>&euro;</td>
                </tr>
                <?php $preciofinal += $item['precio']; if($item['precio'] < 0) $precionegativo += $item['precio']; ?>
            @endforeach
            <?php $factura['precio'] = $preciofinal;?>
        </table>
        <table style="margin-top: 45px; width: 100%">
            <tr>
                <td width="50%"></td>
                <td width="30%" style="text-align: right">
                    <p><b><?=Lang::get("facturas.iva")?>:</b> <?=round(($factura['precio']-$precionegativo)-(($factura['precio']-$precionegativo)/(1+($data['IVA']/100))), 2)?>&euro;</p>
                    @if($precionegativo != 0)
                    <p><b><?=Lang::get("facturas.iva")?> Rectificado:</b> <?=round($precionegativo - ($precionegativo/(1+($data['IVA']/100))), 2)?>&euro;</p>
                    @endif
                    <p><b><?=Lang::get("facturas.subtotal")?>:</b> <?=round($factura['precio']/(1+($data['IVA']/100)),2)?>&euro;</p>
                    @if($paginas != ($pagina+1))
                        <p><b><?=Lang::get("facturas.total")?> página:</b> <?=round($factura['precio'],2)?>&euro;</p>
                        @if($pagina != 0)
                        <p><b><?=Lang::get("facturas.total")?> acumulado:</b> <?=$anterior+round($factura['precio'],2)?>&euro;</p>
                        @endif
                    @else
                        <p><b><?=Lang::get("facturas.total")?> página:</b> <?=round($factura['precio'],2)?>&euro;</p>
                        <br/><br/>
                        <p><b><?=Lang::get("facturas.total")?> factura:</b> <?=$anterior+round($factura['precio'],2)?>&euro;</p>
                    @endif
                </td>
                <td width="20%"></td>
            </tr>
        </table>

        <table style="margin-top: 5cm; width: 80%; margin-left: 10%; border-top: 2px solid #ddd;">
            <tr>
                <td width="50%">
                    <p><b><?=Lang::get("facturas.telefono")?>:</b> <?=$data['telefono']?></p>
                    <p><b><?=Lang::get("facturas.fax")?>:</b> <?=$data['fax']?></p>
                </td>
                <td width="50%" style="text-align: right">
                    <p><b><?=Lang::get("facturas.email")?>:</b> <?=$data['email']?></p>
                    <p><b><?=Lang::get("facturas.web")?>:</b> <?=$data['web']?></p>
                </td>
            </tr>
        </table>
    </body>
</html>