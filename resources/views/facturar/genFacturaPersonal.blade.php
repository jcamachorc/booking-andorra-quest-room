<!DOCTYPE html>
<html lang="en">
    <body style="padding: 15px 50px; font-family: sans-serif;">
        <table width="100%">
            <tr>
                <td width="50%" style="text-align: left">
                    <img src="<?=asset('images/'.$data['logo'])?>" height="80px" />
                </td>
                <td width="50%" style="text-align: right">
                    <p><b><?=Lang::get('facturas.factura');?> <?=(isset($reserva['direccion']) && $reserva['direccion'] == null)?Lang::get('facturas.simplificada'):''?> nº</b> <span style="color: #017ebc"><?=$numeroF?></span></p>
                    <p><b><?=Lang::get('facturas.fecha');?>:</b> <span style="color: #017ebc"><?=\Carbon\Carbon::parse($reserva['fechaPago'])->format("d/m/Y");?></span></p>
                </td>
            </tr>
        </table>

        <table width="100%" style="margin-top: 25px">
            <tr>
                <td width="40%" style="text-align: left; background-color: #f5f5f5; padding: 15px; border: 1px solid #ddd;">
                    <?=Lang::get('facturas.de');?>: <span style="color: #017ebc"><?=$data['compania']?></span>
                </td>
                <td></td>
                <td width="40%" style="text-align: left; background-color: #f5f5f5; padding: 15px; border: 1px solid #ddd;">
                    <?=Lang::get('facturas.para');?>: <span style="color: #017ebc"><?=$reserva['nombreF']?></span>
                </td>
            </tr>
            <tr>
                <td width="40%" style="text-align: left; padding: 15px; border: 1px solid #ddd;">
                    <p><b><?=Lang::get('facturas.cifPropio');?>:</b> <?=$data['cif']?></p>
                    <p><b><?=Lang::get('facturas.direccion');?>:</b> <?=$data['direccion']?></p>
                    <p><?=$data['cp']?>, <?=$data['ciudad']?></p>
                </td>
                <td></td>
                <td width="40%" style="text-align: left; padding: 15px; border: 1px solid #ddd;" valign="top">
                    <?php
                    if(!empty($reserva['DNI'])){
                    ?>
                        <p><b><?=Lang::get('facturas.dni');?>:</b> <?=$reserva['DNI']?></p>
                    <?php
                    }elseif(!empty($reserva['CIF'])){
                    ?>
                        <p><b><?=Lang::get('facturas.cif');?>:</b> <?=$reserva['CIF']?></p>
                    <?php
                    }
                    ?>
                    @if(false)<p><b><?=Lang::get('facturas.telefono');?>:</b> <?=$reserva['telefono']?></p>@endif
                    <?=(isset($reserva['direccion']) && $reserva['direccion'] != null)?'<p><b>'.Lang::get("facturas.direccion").':</b> '.$reserva['direccion'].'</p>':''?>
                </td>
            </tr>
        </table>
        <table style="margin-top: 45px; width: 100%">
            <tr>
                <td style="border-bottom: 2px solid #ddd;"><b><?=Lang::get('facturas.descripcion');?></b></td>
                <td style="border-bottom: 2px solid #ddd;"><b><?=Lang::get('facturas.precio');?></b></td>
            </tr>
            <tr>
                <td><?=(isset($reserva['codigo']))?Lang::get("facturas.bonoTexto"):Lang::get("facturas.sesionTexto")?> id: <?=$reserva['id']?></td>
                <td><?=round($reserva['precio']/(1+($data['IVA']/100)),2)?>&euro;</td>
            </tr>
        </table>
        <table style="margin-top: 45px; width: 100%">
            <tr>
                <td width="50%"></td>
                <td width="30%" style="text-align: right">
                    <p><b><?=Lang::get("facturas.iva")?>:</b> <?=round($reserva['precio']-($reserva['precio']/(1+($data['IVA']/100))), 2)?>&euro;</p>
                    <p><b><?=Lang::get("facturas.subtotal")?>:</b> <?=round($reserva['precio']/(1+($data['IVA']/100)),2)?>&euro;</p>
                    <p><b><?=Lang::get("facturas.descuento")?>:</b> <?=$reserva['descuento']?> %</p>
                    <p><b><?=Lang::get("facturas.total")?>:</b> <?=round($reserva['precio']-($reserva['precio']*$reserva['descuento']/100),2)?>&euro;</p>
                </td>
                <td width="20%"></td>
            </tr>
        </table>

        <table style="margin-top: 12cm; width: 80%; margin-left: 10%; border-top: 2px solid #ddd;">
            <tr>
                <td width="50%">
                    <p><b><?=Lang::get("facturas.telefono")?>:</b> <?=$data['telefono']?></p>
                    <p><b><?=Lang::get("facturas.fax")?>:</b> <?=$data['fax']?></p>
                </td>
                <td width="50%" style="text-align: right">
                    <p><b><?=Lang::get("facturas.email")?>:</b> <?=$data['email']?></p>
                    <p><b><?=Lang::get("facturas.web")?>:</b> <?=$data['web']?></p>
                </td>
            </tr>
        </table>
    </body>
</html>