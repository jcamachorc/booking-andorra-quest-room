@extends('app')

@section('header')
@endsection

@section('content')
<div class="container">
	<div class="row">
        @if($errors->any())
            <div class="alert alert-danger">{{ $errors->first()}}</div>
        @endif

        @if(Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}}</div>
        @endif
	    <div class="panel panel-default">
	        {!! Form::open(array('url' => 'facturar/generar', 'method' => 'post', 'target' => '_new')) !!}
            <div class="panel-heading">
                <i class="glyphicon glyphicon-th-list"></i>
                Listado de facturas
                <a class="btn btn-xs btn-success addReserva"><i class="glyphicon glyphicon-plus-sign"></i> Nueva factura</a>
            </div>

            <div class="panel-body">
                <table id="facturas-table" class="table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Num factura</th>
                            <th>Fecha de facturacion</th>
                            <th>Items facturados</th>
                            <th>Total</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="panel-footer">
                <div class="input-group" style="margin-left: 15px; margin-top: 8px; float: left !important;">Generar facturas</div>

                <div class="input-group col-md-3" style="margin-left: 15px; float: left !important;">
                    <span class="input-group-addon" id="basic-addon1">Fecha Inicio</span>
                    <input type="text" class="form-control datepickers" name="txtFechaInicio" value="<?=\Carbon\Carbon::now()->firstOfMonth()->toDateString()?>"  placeholder="" aria-describedby="basic-addon1">
                </div>
                <div class="input-group col-md-3" style="margin-left: 15px; float: left !important;">
                    <span class="input-group-addon" id="basic-addon1">Fecha Final</span>
                    <input type="text" class="form-control datepickers" name="txtFechaFinal" value="<?=\Carbon\Carbon::now()->endOfMonth()->toDateString()?>"  placeholder="" aria-describedby="basic-addon1">
                </div>
                <input type="submit" class="btn btn-success pull-right" value="Generar" />
                <div class="clearfix"></div>
            </div>
            {!! Form::close() !!}
        </div>
	</div>
</div>
@endsection


@section('footer')
<script>
var ReservasTable, facturasR;
$(function() {
    ReservasTable = $('#facturas-table').DataTable({
        sPaginationType: "full_numbers",
        oLanguage:
        {
            "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
            "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
            "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
            "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
            "sInfo":           "<?=Lang::get('general.sInfo');?>",
            "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
            "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
            "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
            "sSearch":         "<?=Lang::get('general.sSearch');?>",
            "sUrl":            "<?=Lang::get('general.sUrl');?>",
            "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
            "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
            "oPaginate": {
                "sFirst":    "<?=Lang::get('general.sFirst');?>",
                "sLast":     "<?=Lang::get('general.sLast');?>",
                "sNext":     "<?=Lang::get('general.sNext');?>",
                "sPrevious": "<?=Lang::get('general.sPrevious');?>"
            },
            "oAria": {
                "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
            }
        },
        order: [[1,'desc']],
        autoWidth: false,
        processing: true,
        serverSide: true,
        ajax: '<?= url('/facturar/listar') ?>',
        columns: [
            {data: 'numF', name: 'numF', orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nombre', name: 'nombre'},
            {data: 'precio', name: 'precio'},
            {data: 'accion', name: 'Opciones', orderable: false, searchable: false, sWidth: "160px"}
        ]
    });
});

$(function() {
    $('.datepickers').datepicker({dateFormat: 'yy-mm-dd'});
});

$(".addReserva").on( "click", function() {
    newModal('Añadir factura',    //Titulo de la ventana
                '{{ url('/facturar/addFactura') }}',        //Url del contenido de la ventana -> La vista
                 true,                                              //Se debe mostrar el contenido que acabamos de cargar
                 true,                                              //Debe aparecer el boton de Aceptar/Añadir...
                 "<?=Lang::get('general.add');?>",                  //Texto del boton Aceptar/Añadir...
                 true,                                              //Debe aparecer el boton de Cancelar/Cerrar..
                 "<?=Lang::get('general.cerrar');?>",               //Texto del boton Cancelar/Cerrar..
                 '{{ url('/api/modal') }}',                         //Url del diseño de la ventana modal -> Siempre es el mismo
                 '{{ csrf_token() }}',                              //Token de seguridad -> Seimpre es el mismo
                 750,                                               //Ancho de la ventana modal (0 -> Default;)
                 'ReservasTable'                                    //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
             );
});
</script>
@endsection