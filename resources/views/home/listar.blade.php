@extends('app')

@section('header')

@endsection

@section('content')
<div class="container">
	<div class="row">
	    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
          <span class="sr-only">Success:</span> {{ Session::get('success') }}
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span> {{ Session::get('error') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-th-list"></i>
                Envio de mailing
            </div>

            <div class="panel-body">
                <table id="mailing-table" class="table table-striped table-hover dt-responsive">
                    <thead>
                        <tr>
                            <th>Grupo</th>
                            <th>Revisado</th>
                            <th>Envio</th>
                            <th>Fecha de creacion</th>
                            <th><?=Lang::get('configuracion_ajustes.opciones');?></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
	</div>
</div>
@endsection


@section('footer')
<script>
var mailingtable;
$(function() {
      mailingtable = $('#mailing-table').DataTable({
        sPaginationType: "full_numbers",
        oLanguage:
        {
            "sProcessing":     "<?=Lang::get('general.sProcessing');?>",
            "sLengthMenu":     "<?=Lang::get('general.sLengthMenu');?>",
            "sZeroRecords":    "<?=Lang::get('general.sZeroRecords');?>",
            "sEmptyTable":     "<?=Lang::get('general.sEmptyTable');?>",
            "sInfo":           "<?=Lang::get('general.sInfo');?>",
            "sInfoEmpty":      "<?=Lang::get('general.sInfoEmpty');?>",
            "sInfoFiltered":   "<?=Lang::get('general.sInfoFiltered');?>",
            "sInfoPostFix":    "<?=Lang::get('general.sInfoPostFix');?>",
            "sSearch":         "<?=Lang::get('general.sSearch');?>",
            "sUrl":            "<?=Lang::get('general.sUrl');?>",
            "sInfoThousands":  "<?=Lang::get('general.sInfoThousands');?>",
            "sLoadingRecords": "<?=Lang::get('general.sLoadingRecords');?>",
            "oPaginate": {
                "sFirst":    "<?=Lang::get('general.sFirst');?>",
                "sLast":     "<?=Lang::get('general.sLast');?>",
                "sNext":     "<?=Lang::get('general.sNext');?>",
                "sPrevious": "<?=Lang::get('general.sPrevious');?>"
            },
            "oAria": {
                "sSortAscending":  "<?=Lang::get('general.sSortAscending');?>",
                "sSortDescending": "<?=Lang::get('general.sSortDescending');?>"
            }
        },
        order: [[0,'desc']],
        autoWidth: false,
        processing: true,
        serverSide: true,
        ajax: '<?= url('api/home/getListMailing') ?>',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'revisado', name: 'revisado'},
            {data: 'envio', name: 'envio'},
            {data: 'created_at', name: 'created_at'},
            {data: 'accion', name: 'accion', orderable: false, searchable: false, sWidth: "160px"}
        ]
    });
});
</script>
@endsection