@extends('app')

@section('header')

@endsection

@section('content')
<div class="container">
	<div class="row">
        <?php
        if($modo == 1){
        ?>
        {!! Form::open(array('url' => '/journey/comentario/add/'.$id, 'method' => 'POST')) !!}
            <div class="form-group has-feedback col-md-12">
                <div class="input-group">
                    <span class="input-group-addon">Comentario/Incidencia</span>
                    {!! Form::text('comentario', null, array('class' => 'form-control', 'id' => 'comentario')) !!}
                    <div class="input-group-btn">
                        {!! Form::select('type', array('comentario' => 'Comentario', 'incidencia' => 'Incidencia'), null, array('class' => 'form-control alert-warning', 'style' => 'min-width: 180px')) !!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr />
        {!! Form::close() !!}
        {!! Form::open(array('url' => 'journey/update/'.$id, 'method' => 'POST')) !!}
            <div class="col-md-12" style="display: block;">
                <input name="revision" type="checkbox" <?=($revision)?'checked':''?>>
                <label>Revision</label>
            </div>

            <div class="col-md-12" style="display: block;">
                <input name="correos" type="checkbox" <?=($correo)?'checked':''?>>
                <label>Revisar time slot</label>
            </div>

            <div class="col-md-12" style="display: block;">
                <input name="redes" type="checkbox" <?=($redes)?'checked':''?>>
                <label>Redes sociales</label>
            </div>

            <button type="submit" class="btn btn-success">Guardar Feedback Jornada</button>
            <div class="clearfix"></div>
        {!! Form::close() !!}
            <hr />
        <?php
        }
        ?>
        <div class="col-md-3 col-md-offset-4">
        <?php
        if($modo == 0){
        ?>
        {!! Form::open(array('url' => 'journey/start', 'method' => 'POST')) !!}
            <button type="submit" class="btn btn-success pull-left">Iniciar Jornada</button>
        {!! Form::close() !!}
        <?php
        }elseif($modo == 2){
        ?>
        {!! Form::open(array('url' => '/journey/continuar/'.$id, 'method' => 'POST')) !!}
            <button type="submit" class="btn btn-warning pull-left">Continuar Jornada</button>
        {!! Form::close() !!}
        <?php
        }else{
        ?>
        {!! Form::open(array('url' => '/journey/finalizar/'.$id, 'method' => 'POST')) !!}
            <button type="submit" class="btn btn-danger pull-right">Finalzar Jornada</button>
        {!! Form::close() !!}
        <?php
        }
        ?>
        </div>
        <div class="clearfix"></div>
        <hr>
        <?php
        foreach ($comentarios as $comentario) {
        ?>
        <div class="col-md-12 comentario-journey">
            <div class="comentario-journey-head <?=(!$comentario['type'])? 'btn-danger' : 'btn-success'?>" >
                Escrito <?=time_elapsed_string(strtotime($comentario['created_at']))?>

                <span class="pull-right"><a class="btn-xs btn-warning" href="<?=url('journey/comentario/del/'.$comentario['id'])?>"><i class="glyphicon glyphicon-remove"></i></a></span>
            </div>
            <div class="comentario-journey-body"><?=$comentario['comentario']?></div>
        </div>
        <?php
        }
        ?>
	</div>
</div>
@endsection


@section('footer')
@endsection