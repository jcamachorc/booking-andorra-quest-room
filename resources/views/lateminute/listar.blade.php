@extends('app')

@section('header')

@endsection

@section('content')
<div class="container">
	<div class="row">
	    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
          <span class="sr-only">Success:</span> {{ Session::get('success') }}
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span> {{ Session::get('error') }}
        </div>
        @endif
	    {!! Form::open(array('url' => '/lateminute/enviar', 'method' => 'POST')) !!}
            <div class="form-group has-feedback col-md-12">
                <div class="input-group">
                    <span class="input-group-addon">Mensaje</span>
                    <textarea name="mensaje" class="form-control"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Enviar mensaje a todos</button>
        {!! Form::close() !!}
        <div class="clearfix"></div>
        <hr>
        @if(Auth::user()->idRol == 1)
        <table class="table table-bordered">
            <thead>
                <th>Correo</th>
                <th>Fecha</th>
                <th>Opciones</th>
            </thead>
            <tbody>
                <?php
                foreach ($datos as $item) {
                ?>
                <tr class="btn-default">
                    <td><?=$item['correo']?></td>
                    <td><?=$item['created_at']?></td>
                    <td><a class="btn btn-xs btn-danger" href="<?=url('lateminute/del/'.$item['id'].'/'.$item['correo'])?>">Borrar</a></td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        @endif
	</div>
</div>
@endsection


@section('footer')
@endsection