@extends('app')

@section('header')

@endsection

@section('content')
<div class="container">
	<div class="row">
	@if (Session::has('success'))
    <div class="alert alert-success" role="alert">
      <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
      <span class="sr-only">Success:</span> {{ Session::get('success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span> {{ Session::get('error') }}
    </div>
    @endif
    <!--<h3 style="font-weight: bold">Grupo nº <?=$idGrupo?></h3>-->
	{!! Form::open(array('url' => 'mailing/edit', 'method' => 'POST', 'files' => true)) !!}
	    <input type="hidden" class="form-control" name="id" value="<?=$mailing['id']?>">
	    <div class="col-md-12">
	        <?php
	        if (Input::old('correo')){
                foreach (Input::old('correo') as $key => $correo) {
                    $dt = $key;
                    ?>
                    <div class="form-group has-feedback col-md-4">
                        <div class="input-group">
                            <span class="input-group-addon">Nombre <?=$key+1?></span>
                            <input type="text" class="form-control" name="nombre[]" value="<?=Input::old('nombre')[$key]?>">
                        </div>
                    </div>
                    <div class="form-group has-feedback col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon">Correo <?=$key+1?></span>
                            <input type="email" class="form-control" name="correo[]" value="<?=$correo?>">
                        </div>
                    </div>
                    <?php
                }
            }else{
                for($i = 1; $i < 7; ++$i){
                ?>
                <div class="form-group has-feedback col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Nombre <?=$i?></span>
                        <input type="text" class="form-control" name="nombre[]" value="<?=(isset($dataCorreos[$i-1]['nombre']))?$dataCorreos[$i-1]['nombre']:''?>">
                    </div>
                </div>
                <div class="form-group has-feedback col-md-8">
                    <div class="input-group">
                        <span class="input-group-addon">Correo <?=$i?></span>
                        <input type="email" class="form-control" name="correo[]" value="<?=(isset($dataCorreos[$i-1]['mail']))?$dataCorreos[$i-1]['mail']:''?>">
                    </div>
                </div>
                <?php
                }
            }
            ?>
        </div>
        <div class="col-md-12">
            <h2>Subir imagen</h2>
            <div class="dropzone" id="uploadIcono" style="width: 100%;"></div>
            <?php
            $val = '';
            foreach ($dataImg as $img) {
                if($val == "") $val = $img['nombre'];
                else $val .= ','.$img['nombre'];
            }
            ?>
            <input type="hidden" style="width: 100%" name="imagenes" value="<?=$val?>" id="upImg">
        </div>
        <div class="clearfix"></div><br/>
        <div class="col-md-12">
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Donde</span>
                    <?php
                    if(Input::old('donde') != ""){
                        $valor = Input::old('donde');
                    }elseif(isset($mailing["donde"])){
                        $valor = $mailing["donde"];
                    }else{
                        $valor = "";
                    }
                    ?>
                    {!! Form::select('donde', $donde, $valor, array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Idioma</span>
                    <?php
                    if(Input::old('idioma') != ""){
                        $valor = Input::old('idioma');
                    }elseif(isset($mailing["idioma"])){
                        $valor = $mailing["idioma"];
                    }else{
                        $valor = "";
                    }
                    ?>
                    {!! Form::select('idioma', $idioma, $valor, array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Nivel</span>
                    <?php
                    if(Input::old('nivel') != ""){
                        $valor = Input::old('nivel');
                    }elseif(isset($mailing["nivel"])){
                        $valor = $mailing["nivel"];
                    }else{
                        $valor = "";
                    }
                    ?>
                    {!! Form::select('nivel', $nivel, $valor, array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Plantilla</span>
                    <?php
                    if(Input::old('KeyPlantilla') != ""){
                        $valor = Input::old('KeyPlantilla');
                    }elseif(isset($mailing["KeyPlantilla"])){
                        $valor = $mailing["KeyPlantilla"];
                    }else{
                        $valor = "";
                    }
                    ?>
                    {!! Form::select('persona', $persona, $valor, array('class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group has-feedback col-md-12">
                <span class="input-group-addon">Notas</span>
                <?php
                if(Input::old('comentario') != ""){
                    $valor = Input::old('comentario');
                }elseif(isset($mailing["comentario"])){
                    $valor = $mailing["comentario"];
                }else{
                    $valor = "";
                }
                ?>
                {!! Form::textarea('comentario', $valor, array('class' => 'form-control', 'style' => 'white-space: pre-wrap !important;')) !!}
            </div>
        </div>
        <div class="clearfix"></div>
        <button type="submit" class="btn btn-success">Guardar</button>
    {!! Form::close() !!}
	</div>
</div>
@endsection


@section('footer')
<script>
/*Sistema de upload de imagenes drag'n'drop*/
var imgs = [];
var imgsNames = [];

var tk = '{{ csrf_token() }}';
var dt = '<?= url('/mailing/upload?_token=') ?>';
var myDropzone = new Dropzone("div#uploadIcono",{
    url: dt + tk,
    maxFilesize: 5,
    init: function() {
        var myDropzone = this;
        myDropzone.on("addedfile", function(file) {
           file.previewElement.addEventListener("click", function(){
               var name = file.name;
               $.ajax({
                   type: 'POST',
                   url: '<?= url('/mailing/delete?_token=') ?>'+ tk,
                   data: "nombre=" + name,
                   dataType: 'html'
               }).success(function(nombre){
                    delete imgs[imgsNames.indexOf(name)];
                    delete imgsNames[imgsNames.indexOf(name)];
                    $('#upImg').val(imgs.join());
                    myDropzone.removeFile(file);
               });
           });
        });

        @foreach ($dataImg as $img)
            var mockFile = {
                name: "<?=$img['nombre']?>",
                size: "<?=filesize(public_path('uploads/imagenes/'.$img['nombre']))?>",
                type: 'image/jpg',
                status: Dropzone.ADDED,
                url: "<?=asset('uploads/imagenes/'.$img['nombre'])?>"
            };
            imgs.push('<?=$img['nombre']?>');
            imgsNames.push('<?=$img['nombre']?>');

            myDropzone.emit("addedfile", mockFile);
            myDropzone.emit("thumbnail", mockFile, "<?=asset('uploads/imagenes/'.$img['nombre'])?>");
            myDropzone.createThumbnailFromUrl(mockFile, "<?=url('uploads/imagenes/'.$img['nombre'])?>");
            myDropzone.emit("complete", mockFile);

            myDropzone.files.push(mockFile);
        @endforeach

    }
});

myDropzone.on("success", function(file, dt) {
    imgs.push(dt);
    imgsNames.push(file.name);
    if($('#upImg').val() != '')
        $('#upImg').val($('#upImg').val() + ',' + dt);
    else
        $('#upImg').val(dt);
});

myDropzone.on("sending", function(file, xhr, formData) {});
/*FIN Sistema de upload de imagenes drag'n'drop*/
</script>
@endsection