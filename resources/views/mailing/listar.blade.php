@extends('app')

@section('header')

@endsection

@section('content')
<div class="container">
	<div class="row">
	@if (Session::has('success'))
    <div class="alert alert-success" role="alert">
      <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
      <span class="sr-only">Success:</span> {{ Session::get('success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger" role="alert">
      <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
      <span class="sr-only">Error:</span> {{ Session::get('error') }}
    </div>
    @endif
    <!--<h3 style="font-weight: bold">Grupo nº <?=$idGrupo?></h3>-->
	{!! Form::open(array('url' => 'mailing/save', 'method' => 'POST', 'files' => true, 'id' => 'formSn')) !!}
	    <div class="col-md-9">
	        <?php
	        if (Input::old('correo')){
                foreach (Input::old('correo') as $key => $correo) {
                    $dt = $key;
                    ?>
                    <div class="form-group has-feedback col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">Nombre <?=$key+1?></span>
                            <input type="text" class="form-control" name="nombre[]" autocomplete="off" value="<?=Input::old('nombre')[$key]?>">
                        </div>
                    </div>
                    <div class="form-group has-feedback col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">Correo <?=$key+1?></span>
                            <input type="email" class="form-control" name="correo[]" autocomplete="off" value="<?=$correo?>">
                        </div>
                    </div>
                    <?php
                }
            }else{
                for($i = 1; $i < 7; ++$i){
                ?>
                <div class="form-group has-feedback col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Nombre <?=$i?></span>
                        <input type="text" class="form-control" name="nombre[]" autocomplete="off" value="">
                    </div>
                </div>
                <div class="form-group has-feedback col-md-8">
                    <div class="input-group">
                        <span class="input-group-addon">Correo <?=$i?></span>
                        <input type="email" class="form-control" name="correo[]" autocomplete="off" value="">
                    </div>
                </div>
                <?php
                }
            }
            ?>
        </div>
        <div class="col-md-3">
            <h2>Subir imagen</h2>
            <div class="dropzone" id="uploadIcono" style="width: 100%;"></div>
            <input type="hidden" name="imagenes" value="" id="upImg">
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Donde</span>
                    {!! Form::select('donde', $donde, Input::old('donde'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Idioma</span>
                    {!! Form::select('idioma', $idioma, Input::old('idioma'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Nivel</span>
                    {!! Form::select('nivel', $nivel, Input::old('nivel'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="form-group has-feedback col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">Plantilla</span>
                    {!! Form::select('persona', $persona, Input::old('persona')?Input::old('persona'):$MyId, array('class' => 'form-control')) !!}
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group has-feedback col-md-12">
                <span class="input-group-addon">Notas</span>
                {!! Form::textarea('comentario', Input::old('comentario'), array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-md-12">
            <p>
                {!! Form::checkbox('checkbox', null, false) !!}
                <?= getConfig('lopd');?>
            </p>
        </div>
        <div class="clearfix"></div>
        <div style="text-align: center; margin-bottom: 160px">
            <button type="submit" class="btn btn-success"  onclick="return confirm('Ya has introducido todos los datos?')" >Guardar</button>
        </div>
    {!! Form::close() !!}
	</div>
</div>
@endsection


@section('footer')
<script>
/*Sistema de upload de imagenes drag'n'drop*/
var tk = '{{ csrf_token() }}';
var dt = '<?= url('/mailing/upload?_token=') ?>';
var myDropzone = new Dropzone("div#uploadIcono",{url: dt + tk, maxFilesize: 5});

myDropzone.on("addedfile", function(file) {
    /*file.previewElement.addEventListener("click", function(){
        var name = file.name;
        $.ajax({
            type: 'POST',
            url: '<?= url('/mailing/delete?_token=') ?>'+ tk,
            data: "nombre=" + name,
            dataType: 'html'
        }).success(function(){
            myDropzone.removeFile(file);
        });
        //Explode $('#upImg').val() AND remove item from list
    });*/
});
myDropzone.on("success", function(file, dt) {
    if($('#upImg').val() != '')
        $('#upImg').val($('#upImg').val() + ',' + dt);
    else
        $('#upImg').val(dt);
});

myDropzone.on("sending", function(file, xhr, formData) {});
/*FIN Sistema de upload de imagenes drag'n'drop*/
</script>
@endsection