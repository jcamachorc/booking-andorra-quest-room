@extends('app')

@section('content')
<div class="container">
	<div class="row">
	    <div class="alert alert-danger" style="display: none;" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span id="inner-error">Test</span>
        </div>
		<div class="col-*-12" id="dataPut">

		</div>
		<div class="modal-footer">
            <?php if(isset($botonAceptar) && $botonAceptar == 'true'){ ?>
                <button type="button" class="btn btn-success" id="accept"><?=$textoAceptar?></button>
            <?php }?>
            <?php if(isset($botonCancelar) && $botonCancelar != ''){ ?>
                <button type="button" class="btn btn-danger" id="close"><?=$botonCancelar?></button>
            <?php }?>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
var tmp = loadTemporal();
$.ajax({
    method: "GET",
    url: '<?=$_POST['to']?>',
    data: {
        _token: token,
        actualID: 0
    },
    statusCode: {
        404: function(xhr) {
            loadErrorLoading(tmp, "error.404.png");
        },
        403: function(xhr) {
            loadErrorLoading(tmp, "error.403.png");
        },
        500: function(xhr) {
            loadErrorLoading(tmp, "error.500.png");
        }
    }
})
.done(function (dataContent) {
    deleteTemporal(tmp);
    $("#dataPut").html(dataContent);
});

$("#accept").click(function() {
    var tmp = loadTemporal();
    var postData = new FormData($('form')[0]);
    $.ajax({
        url : $('form').attr("action"),
        type: $('form').attr("method"),
        data : postData,
        cache: false,
        contentType: false,
        processData: false,
        success:function(data)
        {
            gotoMain();
        },
        error: function(data)
        {
            deleteTemporal(tmp);
            $("#dataPut .alert-danger").show();
            $("#dataPut .alert-danger #inner-error").html(data.responseJSON.message);
            $("input[name='" + data.responseJSON.name + "']").parent().addClass('has-error');
        }
    });
});

$("#close").click(function() {
    gotoMain();
});

function gotoMain(){
    window.location = '<?=$redirect?>';
}
</script>
@endsection