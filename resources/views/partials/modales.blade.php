<?php if(!isset($id_modal)) exit; ?>
<div class="modal-backdrop fade in" id="backdrop-modal-<?=$id_modal?>"></div>
<div id="modal-<?=$id_modal?>" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php if(isset($titulo)){ ?>
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h4 class="modal-title"><?=$titulo?></h4>
                </div>
            <?php } ?>

            <?php if(isset($contenido)){ ?>
                <div class="modal-body" id="modal-body-<?=$id_modal?>" <?= ($viewContenido == 'true')? '':'style="display: none;"' ?>>
                    <div class="alert alert-danger" style="display: none;" role="alert">
                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                      <span id="inner-error">Test</span>
                    </div>


                    <?=$contenido?>
                </div>
            <?php } ?>

            <div class="modal-footer">
                <?php if(isset($botonAceptar) && $botonAceptar == 'true'){ ?>
                    <button type="button" class="btn btn-success" id="accept-modal-<?=$id_modal?>"><?=$textoAceptar?></button>
                <?php }?>
                <?php if(isset($botonCancelar) && $botonCancelar == 'true'){ ?>
                    <button type="button" class="btn btn-danger" id="close-modal-<?=$id_modal?>"><?=$textoCancelar?></button>
                <?php }?>
            </div>
        </div>
    </div>
    <script>
    function maxHeight(){
        var actualWindow = $(window).height() - 170;
        $('#modal-body-<?=$id_modal?>').css("max-height", actualWindow+"px");
    }

    maxHeight();

    $( window ).resize(function() {
        maxHeight();
    });


/*Fin ayuda de campos*/
    $("#accept-modal-<?=$id_modal?>").click(function() {
        $("#modal-<?=$id_modal?> .alert-danger").hide();
        $("#modal-<?=$id_modal?> .alert-danger #inner-error").html("");

        if(typeof $('#modal-<?=$id_modal?> form').val() == "undefined"){
            $("#modal-<?=$id_modal?>").modal('hide');
            $("#modal-<?=$id_modal?>").remove();
        }else{
            //#ajax-form is the id of your form.
            var postData = $('#modal-<?=$id_modal?> form').serializeArray();
            var formURL = $('#modal-<?=$id_modal?> form').attr("action");

            error = false;
            $('#modal-<?=$id_modal?> form input').each(function() {
                var hasError = false;
                if(!hasError && $(this).attr('type') == 'email' && $(this).val() != '' && !isValidEmailAddress($(this).val())){
                    $(this).parent().addClass('has-error');
                    $(this).parent().removeClass('has-success');
                    hasError = window.error = true;
                }else if(!hasError && $(this).attr('type') == 'email' && $(this).val() != '' && isValidEmailAddress($(this).val())){
                    $(this).parent().addClass('has-success ');
                    $(this).parent().removeClass('has-error');
                }

                if(!($(this).val() == '' && $(this).attr('required') != 'required') && $(this).attr('data-numeric')){
                    if(!hasError && !isNumber($(this).val())){
                        $(this).parent().addClass('has-error');
                        $(this).parent().removeClass('has-success');
                        hasError = window.error = true;
                    }else if(!hasError){
                         $(this).parent().addClass('has-success ');
                         $(this).parent().removeClass('has-error');
                    }
                }

                if($(this).val() == '' && $(this).attr('required') == 'required' && $(this).attr('type') == 'hidden'){
                    $("#modal-<?=$id_modal?> .alert-danger").show();
                    $("#modal-<?=$id_modal?> .alert-danger #inner-error").html("Error: " + $(this).attr('data-error-msg'));
                    hasError = window.error = true;
                }

                var maxLen = -1;
                if(!($(this).val() == '' && $(this).attr('required') != 'required') && isNumber($(this).attr('data-max-length'))){
                    maxLen = $(this).attr('data-max-length');
                }

                var minLen = -1;
                if(!($(this).val() == '' && $(this).attr('required') != 'required') && isNumber($(this).attr('data-min-length'))){
                    minLen = $(this).attr('data-min-length');
                }

                if(!($(this).val() == '' && $(this).attr('required') != 'required') && $(this).attr('data-numeric-inf')){
                    if(!hasError && (!isNumber($(this).val()) &&  $(this).val() != '#')){
                        $(this).parent().addClass('has-error');
                        $(this).parent().removeClass('has-success');
                        hasError = window.error = true;
                    }else if(!hasError){
                         $(this).parent().addClass('has-success ');
                         $(this).parent().removeClass('has-error');
                    }
                }
                if(!($(this).val() == '' && $(this).attr('required') != 'required') && $(this).attr('data-cif')){
                    if(!hasError && !validateCIF($(this).val())){
                        $(this).parent().addClass('has-error');
                        $(this).parent().removeClass('has-success');
                        hasError = window.error = true;
                    }else if(!hasError){
                         $(this).parent().addClass('has-success ');
                         $(this).parent().removeClass('has-error');
                    }
                }

                if(!($(this).val() == '' && $(this).attr('required') != 'required') && $(this).attr('data-din')){
                    if(!hasError && !validateDNI($(this).val())){
                        $(this).parent().addClass('has-error');
                        $(this).parent().removeClass('has-success');
                        hasError = window.error = true;
                    }else if(!hasError){
                         $(this).parent().addClass('has-success ');
                         $(this).parent().removeClass('has-error');
                    }
                }

                if(!hasError && $(this).attr('required') == 'required' && $(this).val() == ''){
                    $(this).parent().addClass('has-error');
                    $(this).parent().removeClass('has-success');
                    hasError = window.error = true;
                }else if(!hasError && $(this).attr('required') == 'required' && $(this).val() != ''){
                    $(this).parent().addClass('has-success ');
                    $(this).parent().removeClass('has-error');
                }

                if(!hasError && !validLenght($(this).val(), maxLen, minLen)){
                    $(this).parent().addClass('has-error');
                    $(this).parent().removeClass('has-success');
                    hasError = window.error = true;
                }else if(!hasError && validLenght($(this).val(), maxLen, minLen) && $(this).attr('required') == 'required' && $(this).val() != ''){
                     $(this).parent().addClass('has-success ');
                     $(this).parent().removeClass('has-error');
                }
            });

            if(error) return false;

            $('#modal-<?=$id_modal?> .modal-content').append("<img id='modal-<?=$id_modal?>-loading' src='<?=URL::asset('/images/loading.gif')?>' alt='loading' style='position: absolute; top: 0; left: 0; z-index: 999999; width: 100%;' />");

            var postData = new FormData($('#modal-<?=$id_modal?> form')[0]);
            $.ajax({
                url : formURL,
                type: $('#modal-<?=$id_modal?> form').attr("method"),
                data : postData,
                cache: false,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    $('#modal-<?=$id_modal?> .modal-content').attr('style', 'width: 200px !important; margin: auto;');
                    $('#modal-<?=$id_modal?> .modal-content').html("<img src='<?=URL::asset('/images/success.png')?>' alt='success' style='width: 200px; margin-left: calc(50% - 100px);' />");

                    var refresh = true;
                    if (typeof superObject[<?=$id_modal?>].special === "undefined") {
                        refresh = false;
                    }

                    if(superObject[<?=$id_modal?>].fromModal != -1){
                        var this_id = superObject[<?=$id_modal?>].fromModal;
                        newModal(superObject[this_id].Titulo,            //Titulo de la ventana
                             superObject[this_id].URLcontenido,          //Url del contenido de la ventana -> La vista
                             superObject[this_id].showContenido,         //Se debe mostrar el contenido que acabamos de cargar
                             superObject[this_id].Aceptar,               //Debe aparecer el boton de Aceptar/Añadir...
                             superObject[this_id].TextoAceptar,          //Texto del boton Aceptar/Añadir...
                             superObject[this_id].Cancelar,              //Debe aparecer el boton de Cancelar/Cerrar..
                             superObject[this_id].TextoCancelar,         //Texto del boton Cancelar/Cerrar..
                             superObject[this_id].URLmodal,              //Url del diseño de la ventana modal -> Siempre es el mismo
                             superObject[this_id].token,                 //Token de seguridad -> Seimpre es el mismo
                             superObject[this_id].extraWidth,            //Ancho de la ventana modal (0 -> Default;)
                             superObject[this_id].reloadTable,           //Nombre de la tabla a refrescar (si no hay tabla dejarlo a false)
                             superObject[this_id].fromModal              //Referral modal
                         );

                        $("#modal-" + this_id).modal('hide');
                        $("#modal-" + this_id).remove();
                        $("#backdrop-modal-"+this_id).remove();
                        $('.container-fluid').removeClass('blur');
                    }

                    setTimeout(function(){
                        $("#modal-<?=$id_modal?>").modal('hide');
                        $("#modal-<?=$id_modal?>").remove();
                        $("#backdrop-modal-<?=$id_modal?>").remove();
                        $('.helper-wizard').remove();
                        $('.container-fluid').removeClass('blur');

                    }, 1000);

                    <?php
                        if(empty($tableReload)){
                    ?>
                        window.location.reload();
                    <?php
                        }
                    ?>

                    <?php
                        if($tableReload != 'false'){
                    ?>
                        var reload = eval(<?=$tableReload?>);
                        reload.ajax.reload(null, false);
                    <?php
                        }
                    ?>

                    if(superObject[<?=$id_modal?>].fromModal != -1){
                        setTimeout(function(){
                            $('body').addClass('modal-open');
                        }, 1500);
                    }

                    if(refresh){
                        window.location.reload();
                    }
                },
                error: function(data)
                {
                    $('#modal-<?=$id_modal?>-loading').remove();
                    $("#modal-<?=$id_modal?> .alert-danger").show();
                    $("#modal-<?=$id_modal?> .alert-danger #inner-error").html(data.responseJSON.message);
                    $("input[name='" + data.responseJSON.name + "']").parent().addClass('has-error');

                    $("#modal-body-<?=$id_modal?>").css('display','block');

                    $('.container-fluid').removeClass('blur');
                }
            });
        }
    });

    $("#close-modal-<?=$id_modal?>").click(function() {
        $("#modal-<?=$id_modal?>").modal('hide');
        $("#modal-<?=$id_modal?>").remove();
        $("#backdrop-modal-<?=$id_modal?>").remove();
        $('.helper-wizard').remove();

        if(superObject[<?=$id_modal?>].fromModal != -1){
            $('body').addClass('modal-open');
        }

         $('.container-fluid').removeClass('blur');

    });
    </script>
</div>