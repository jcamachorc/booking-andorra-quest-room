<div class="brand-col pull-left" style="background-color: #336699; height: 65px;">
    <a class="navbar-brand" href="<?=url('/')?>" style="color: #fff !important; margin-top: 7px">
        <img src="<?=asset('images/logo.png')?>" class="logo" style="height: 50px;" />
        Quest
    </a>
    <div class="clearfix"></div>
</div>

<div class="profile <?=($menuType == 1)? 'minimal-sidebar' : ''?>" > <!--style="background-image: url({{asset('images/profile-menu.jpg')}})"-->
    {{-- Llama a la funcion getImage del modelo User y devuelve el valor en el parametro background-image --}}
    <div class="img-user-side menu-hidden-minimum <?=($menuType == 1)? 'minimal-menu' : ''?>" style="background-image: url({{ \App\User::getImage() }});"></div>
    <div class="user-name-side menu-hidden-minimum <?=($menuType == 1)? 'minimal-menu' : ''?>">{{ Auth::user()->name }}</div>
    <div class="clearfix"></div>
</div>

<ul class="nav nav-sidebar">
<?php
foreach($menu as $item){
    if($item['SubMenu'] != NULL){;
    ?>
        <li class="dropdown">
            <a class="dropdown-toggle-side">
                <i class="fa fa-<?=$item['icon']?>"></i>
                <span class="menu-hidden-minimum <?=($menuType == 1)? 'minimal-menu' : ''?>"><?= $item['nombre'] ?></span>
                <img src="{{asset('images/plus-empty.png')}}" style="height: 25px" class="caret-plus pull-right menu-hidden-minimum <?=($menuType == 1)? 'minimal-menu' : ''?>"/>
            </a>
            <ul class="dropdown-items <?=(isset($item['selected']))? 'active-dropdown' : ''?>" <?=($item['selected'])? 'style="display: block"' : ''?>>
                <?php
                foreach($item['SubMenu'] as $submenu){
                ?>
                    <li class="<?=(isset($submenu['active']))? 'active menu-hidden-minimum' : 'menu-hidden-minimum'?> <?=($menuType == 1)? ' minimal-menu' : ''?>"><a href="<?= url($submenu['ruta']); ?>"><?= $submenu['nombre'] ?></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
    <?php
    }else{
        $selected = ($item['selected'])? 'activeSingle' : '';
        ?>
        <li class="<?=$selected?>">
            <a href="<?=url($item['ruta'])?>">
                <i class="fa fa-<?=$item['icon']?>"></i>
                <span class="menu-hidden-minimum <?=(($menuType == 1)? 'minimal-menu' : ''); ?>"><?=$item['nombre']?></span>
            </a>
        </li>
        <?php
    }
}
?>
</ul>
