<div class="topDiv-nav">
<ol class="breadcrumb" style="float:left;">
<?php
if(is_numeric(end($actualMenu))){
    array_pop($actualMenu);
}
foreach ($actualMenu as $key => $item){
    if($key == 0)
        echo '<li class="tab-actual-title size-actual-'.count($actualMenu).'">'.$item.'</li>';
    elseif($key == (count($actualMenu) - 1))
        echo  '<li class="active tab-actual-title size-actual-'.count($actualMenu).'">'.$item.'</li>';
    else{
        $url = '';
        for($i = 0; $i <= $key; ++$i){
            if($url == '')
                $url = $actualMenu[$i];
            else
                $url .= "/".$actualMenu[$i];
        }

        echo '<li><a href="'.url($url).'" class="tab-actual-title size-actual-'.count($actualMenu).' classExtraTab">'.$item.'</a></li>';
    }
}
?>
</ol>

<!-- Panel de la derecha-->
<div class="header-col pull-right" id="central-col-right">
    <ul class="nav navbar-nav navbar-right">
        <!-- Configuracion -->
        <li class="dropdown pull-right hidden-xs">
            <a href="#" class="dropdown-toggle full-glyp-lang" data-toggle="dropdown" id="ajustes-salir"><i class="glyphicon glyphicon-cog"></i></a>
            <ul class="dropdown-menu menu-lang">
                <li><a href="{{url('/auth/logout')}}">Salir</a> </li>
            </ul>
        </li>
        <!-- Hide menu (sidebar)-->
        <div class="menu-toggle pull-right visible-xs pepe" style="margin-top: -2px !important;">
            <a href="#">
                <i class="glyphicon glyphicon-list" style="color: #337ab7;"></i>
            </a>
        </div>
     </ul>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>