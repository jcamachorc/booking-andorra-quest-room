<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid-header">

        <!-- Panel de la derecha-->
        <div class="header-col pull-right" id="central-col-right">
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown pull-right hidden-xs">
                    <a href="#" class="hidden-menu-lat">
                        <i class="fa fa-outdent" id="icon-menu-hidden-lat" style="font-size: 25px; color: #fff"></i>
                    </a>
                </li>

                <!-- Configuracion -->
                <li class="dropdown pull-right hidden-xs">
                    <a href="#" class="dropdown-toggle full-glyp-lang" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                    <ul class="dropdown-menu menu-lang">
                        <li><a href="{{url('ajustes')}}">Ajustes</a> </li>
                        <li><a href="{{url('/auth/logout')}}">Salir</a> </li>
                    </ul>
                </li>

                <!-- Buscador -->
                <li class="dropdown pull-right hidden-xs">
                    <a href="#" class="dropdown-toggle full-glyp-lang" id="search-system" data-toggle="dropdown"><i class="glyphicon glyphicon-search"></i></a>
                </li>

                <!-- Hide menu (sidebar)-->
                <div class="menu-toggle pull-right visible-xs">
                    <a href="#">
                        <i class="glyphicon glyphicon-list"></i>
                    </a>
                </div>
             </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</nav>